package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FotosEvidenciaRelacion;

/**
 * Created by camovilesb on 24/3/2017.
 */

public class FotosBDHelper extends DBHelper {


    public FotosBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public long insertFoto(int id_sucursal, String archivo, String nombre_archivo, double latitud, double longitud, String usuario) {

        long ID;
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_FOTOS_EVIDENCIAID_SUCURSAL, id_sucursal);
        values.put(COLUMNA_FOTOS_EVIDENCIA_ARCHIVO, archivo);
        values.put(COLUMNA_FOTOS_EVIDENCIA_FILE_NAME, nombre_archivo);
        values.put(COLUMNA_FOTOS_EVIDENCIA_LATITUD, latitud);
        values.put(COLUMNA_FOTOS_EVIDENCIA_LONGITUD, longitud);
        values.put(COLUMNA_FOTOS_EVIDENCIA_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_FOTOS_EVIDENCIA_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_FOTOS_EVIDENCIA_USUARIO, usuario);
        values.put(COLUMNA_FOTOS_EVIDENCIA_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, 2);
        ID = database.insert(TABLA_FOTOS_EVIDENCIA, null, values);
        database.close();
        return ID;
    }

    public void insertFotoRelacion(int id_foto, String tabla, int id_tabla) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_FOTOS_EVIDENCIA_RELACION_ID_FOTO, id_foto);
        values.put(COLUMNA_FOTOS_EVIDENCIA_RELACION_TABLA, tabla);
        values.put(COLUMNA_FOTOS_EVIDENCIA_RELACION_ID, id_tabla);
        values.put(COLUMNA_ESTATUS_DOS, 2);
        database.insert(TABLA_FOTOS_EVIDENCIA_RELACION, null, values);
        database.close();
    }

    public ArrayList<FotosEvidenciaRelacion> getFotos(String tabla, int id) {
        ArrayList<FotosEvidenciaRelacion> fotos = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT FOTOS_EVIDENCIA.ID_FOTO, FOTOS_EVIDENCIA.ARCHIVO, FOTOS_EVIDENCIA.FILE_NAME " +
                "FROM FOTOS_EVIDENCIA INNER JOIN Fotos_Evidencia_Relacion ON FOTOS_EVIDENCIA.ID_FOTO = Fotos_Evidencia_Relacion.ID_FOTO " +
                "WHERE Fotos_Evidencia_Relacion.TABLA = '" + tabla + "' AND Fotos_Evidencia_Relacion.ID = " + id + " AND FOTOS_EVIDENCIA.ESTATUS = 1", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_FOTO = Integer.parseInt(cursor.getString(0));
                String ARCHIVO = cursor.getString(1);
                String FILE_NAME = cursor.getString(2);
                FotosEvidenciaRelacion fotosevidencai = new FotosEvidenciaRelacion(ID_FOTO, ARCHIVO, FILE_NAME);
                fotos.add(fotosevidencai);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return fotos;
    }

}
