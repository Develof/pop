package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 17/03/2016.
 */
public class AdapterMaterialPop {
    private int id;
    int id_material;
    private String nombre;
    private int cantidad;
    private int cantidad_antrior;
    private int existencia;

    public AdapterMaterialPop(){};

    public AdapterMaterialPop(int id, int id_material, String nombre, int cantidad, int cantidad_antrior, int existencia) {
        this.id = id;
        this.id_material = id_material;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.cantidad_antrior = cantidad_antrior;
        this.existencia = existencia;
    }

    public AdapterMaterialPop(String nombre, int cantidad, int cantidad_antrior, int existencia) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.cantidad_antrior = cantidad_antrior;
        this.existencia = existencia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_material() {
        return id_material;
    }

    public void setId_material(int id_material) {
        this.id_material = id_material;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad_antrior() {
        return cantidad_antrior;
    }

    public void setCantidad_antrior(int cantidad_antrior) {
        this.cantidad_antrior = cantidad_antrior;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }
}
