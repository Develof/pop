package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CatInventario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProductoInventario;

public class CatInventarioBDHelper extends DBHelper {


    public CatInventarioBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public List<CatInventario> getCatInv() {

        List<CatInventario> categorias = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM CAT_INVENTARIO ORDER BY " + COLUMNA_CATEGORIA_INVENTARIO_ID , null);

        if (cursor.moveToNext()) {
            do {
                CatInventario catInventario = new CatInventario();
                catInventario.setIdCategoriaInv(Integer.parseInt(cursor.getString(0)));
                catInventario.setCatInve(cursor.getString(1));
                catInventario.setDescipcion(cursor.getString(2));
                catInventario.setEstatus(Integer.parseInt(cursor.getString(6)));

                categorias.add(catInventario);
            } while (cursor.moveToNext());
        }
        return categorias;
    }


    public List<ProductoInventario> getProductoInventario(int idProducto, int idSucursal) {

        List<ProductoInventario> inventarios = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLA_PRODUCTO_INVENTARIO + " WHERE " + COLUMNA_PRODUCTO_INVENTARIO_ID_PRODUCTO + " = " + idProducto + " AND " + COLUMNA_PRODUCTO_INVENTARIO_ID_SUCURSAL + " = " + idSucursal +  " ORDER BY " + COLUMNA_PRODUCTO_CATEGORIA_ID, null);

        if (cursor.moveToNext()) {
            do {
                ProductoInventario prodInv = new ProductoInventario();
                prodInv.setIdProductoInventario(Integer.parseInt(cursor.getString(0)));
                prodInv.setIdCatInv(Integer.parseInt(cursor.getString(1)));
                prodInv.setIdSucursal(Integer.parseInt(cursor.getString(2)));
                prodInv.setIdProducto(Integer.parseInt(cursor.getString(3)));
                prodInv.setCantidad(Integer.parseInt(cursor.getString(4)));
                prodInv.setEstatus(Integer.parseInt(cursor.getString(9)));

                inventarios.add(prodInv);
            } while (cursor.moveToNext());
        }
        return inventarios;
    }


    public void insertaInventario(int idCatInventario, int idSucursal, int idProducto, int cantidad, String usuario) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM PRODUCTO_INVENTARIO WHERE ID_CATEGORIA_INVENTARIO = " + idCatInventario + "" +
                " AND ID_SUCURSAL = " + idSucursal + " AND ID_PRODUCTO = " + idProducto + ";", null);

        if (cursor.moveToNext()) {
            SQLiteDatabase database = this.getWritableDatabase();

            String strSQL = "UPDATE " + TABLA_PRODUCTO_INVENTARIO + " SET " + COLUMNA_PRODUCTO_INVENTARIO_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_PRODUCTO_INVENTARIO_CANTIDAD + " = '" + cantidad + "', " + COLUMNA_PRODUCTO_INVENTARIO_FECHA_CREACION + " = '" + getDateTime() + "'" + "' WHERE " + COLUMNA_PRODUCTO_INVENTARIO_ID_PRODUCTO + " = " + idProducto + " AND " + COLUMNA_PRODUCTO_INVENTARIO_ID_SUCURSAL + " = " + idSucursal + " AND " + COLUMNA_PRODUCTO_CATEGORIA_ID + " = " + idCatInventario + "";
            database.execSQL(strSQL);
            database.close();
        } else {

            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMNA_PRODUCTO_CATEGORIA_ID, idCatInventario);
            values.put(COLUMNA_PRODUCTO_INVENTARIO_ID_SUCURSAL, idSucursal);
            values.put(COLUMNA_PRODUCTO_INVENTARIO_ID_PRODUCTO, idProducto);
            values.put(COLUMNA_PRODUCTO_INVENTARIO_CANTIDAD, cantidad);
            values.put(COLUMNA_PRODUCTO_INVENTARIO_FECHA_CREACION, getDateTime());
            values.put(COLUMNA_PRODUCTO_INVENTARIO_ULTIMA_MODIFICACION, getDateTime());
            values.put(COLUMNA_PRODUCTO_INVENTARIO_USUARIO, usuario);
            values.put(COLUMNA_PRODUCTO_INVENTARIO_ESTATUS, 1);
            values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
            database.insert(TABLA_PRODUCTO_INVENTARIO, null, values);
            database.close();
        }

    }

}
