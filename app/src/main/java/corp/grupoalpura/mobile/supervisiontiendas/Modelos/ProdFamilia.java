package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by consultor on 3/2/16.
 */
public class ProdFamilia {

    private int id;
    private String familia;
    private int estatus;

    public ProdFamilia(){}

    public ProdFamilia(int id, String familia, int estatus) {
        this.id = id;
        this.familia = familia;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }


    @Override
    public String toString() {
        return familia;
    }
}
