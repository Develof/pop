package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by consultor on 3/2/16.
 */
public class ProductosCatalogo {
    private int id;
    private int id_sucursal;
    private int id_producto;
    private String ultima_modificacion;
    private String fecha_creacion;
    private String usuario;
    private int estatus;

    public ProductosCatalogo() {
    }

    public ProductosCatalogo(int id, int id_sucursal, int id_producto, String ultima_modificacion, String usuario, String fecha_creacion, int estatus) {
        this.id = id;
        this.id_sucursal = id_sucursal;
        this.id_producto = id_producto;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.fecha_creacion = fecha_creacion;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
