package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 10/06/16.
 */
public class AdapterPuesto {
    private int id_puest;
    private String puesto;

    public AdapterPuesto(int id_puest, String puesto) {
        this.id_puest = id_puest;
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return puesto;
    }
    public int getId_puest() {
        return id_puest;
    }


    public void setId_puest(int id_puest) {
        this.id_puest = id_puest;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }
}
