package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by prestamo on 31/05/2016.
 */
public class Dates {

    public static Date stringToDate(String strDate) {
        Date date = null;
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            date = format.parse(strDate);
        } catch (ParseException e) {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                date = format.parse(strDate);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
        return date;
    }

    public static String createDateToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String reportDate = df.format(date);
        return reportDate;
    }

    public static String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
        String reportDate = df.format(date);
        return reportDate;
    }

    public static String dateToStringAlerta(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String reportDate = df.format(date);
        return reportDate;
    }


}
