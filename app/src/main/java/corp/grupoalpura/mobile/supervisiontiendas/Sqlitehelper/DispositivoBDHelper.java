package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.DispositivoBDModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Develof on 28/10/16.
 */

public class DispositivoBDHelper extends DBHelper {

    public DispositivoBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    /**
     * Inserta una dispositivo en la BD.
     *
     * @param noSerie Numero de Serie del dispositivo.
     * @param imei    IMEI del Sim.
     * @param sim     Numero de Sim.
     * @param modelo  Modelo del dispositivo.
     * @param android Verison de android.
     * @return true si se insertó correctamente.
     */
    public long insertarDispositivo(String noSerie, String imei, String sim, String modelo, String android, String version, int idUsuario, int idUdn, int estatusDos, String username) {

        SQLiteDatabase database = this.getWritableDatabase();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();

        ContentValues registro = new ContentValues();
        registro.put(DispositivoBDModel.ID_Usuario, idUsuario);
        registro.put(DispositivoBDModel.ID_UDN, idUdn);
        registro.put(DispositivoBDModel.FECHA, dateFormat.format(date));
        registro.put(DispositivoBDModel.NO_SERIE, noSerie);
        registro.put(DispositivoBDModel.IMEI, imei);
        registro.put(DispositivoBDModel.SIM, sim);
        registro.put(DispositivoBDModel.MODELO, modelo);
        registro.put(DispositivoBDModel.ANDROID, android);
        registro.put(DispositivoBDModel.VERSION, version);
        registro.put(DispositivoBDModel.ESTATUS_DOS, estatusDos);
        registro.put(DispositivoBDModel.FECHA_CREACION, getDateTime());
        registro.put(DispositivoBDModel.ULTIMA_MODIFICACION, getDateTime());
        registro.put(DispositivoBDModel.USUARIO, username);
        long registroInsertado = database.insert(TABLA_DISPOSITIVO, null, registro);
        database.close();

        return registroInsertado;
    }

    public long getIdDispositivo() {
        long idDispositivo = 0;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + COLUMNA_DISPOSIVITO_ID + " FROM " + TABLA_DISPOSITIVO, null);

        if (cursor.moveToFirst()) {
            idDispositivo = Integer.parseInt(cursor.getString(0));
        }

        cursor.close();
        db.close();
        return idDispositivo;
    }

    public DispositivoBDModel getDispositivo() {
        DispositivoBDModel dispositivo = new DispositivoBDModel();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT FECHA, NO_SERIE, VERSION_BD FROM " + TABLA_DISPOSITIVO, null);

        if (cursor.moveToFirst()) {
            dispositivo.setFecha(cursor.getString(0));
            dispositivo.setNoSerie(cursor.getString(1));
            dispositivo.setVersionBd(cursor.getString(2));
        }

        cursor.close();
        db.close();

        return dispositivo;
    }

    public void updateDispositivo(String version) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_DISPOSITIVO + " SET " + COLUMNA_DISPOSIVITO_VERSION_BD + " = '" + version + "'";
        database.execSQL(strSQL);
        database.close();
    }


}
