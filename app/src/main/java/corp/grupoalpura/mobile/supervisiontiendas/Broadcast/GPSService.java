package corp.grupoalpura.mobile.supervisiontiendas.Broadcast;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.LocalizacionUsuario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.UsuarioBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import java.util.Calendar;
import java.util.Date;


/**
  Created by Develof on 16/06/16.
 **/
public class GPSService extends Service implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleApiClient;
    protected LocationRequest mLocationRequest;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 300000; // 5 min
    double latitud = 0, longitud = 0;
    Usuario usuario;
    UsuarioBDHelper usuarioBDHelper;
    PendingResult<Status> locationUpdates;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        usuarioBDHelper = new UsuarioBDHelper(this);
        usuario = usuarioBDHelper.getFistUsuario();
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        googleApiClient.connect();
        return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        } else {

            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {

                latitud = location.getLatitude();
                longitud = location.getLongitude();

                if (usuario != null) {
                    usuarioBDHelper.insetUserLocation(usuario.getId(), latitud, longitud, usuario.getUsername());
                }

                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


                locationUpdates = LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        latitud = location.getLatitude();
                        longitud = location.getLongitude();

                        if (usuario != null) {
                            LocalizacionUsuario localizacionUsuario = usuarioBDHelper.getLastUserLocation();
                            if(localizacionUsuario != null){
                                Date ultimaFecha = Dates.stringToDate(localizacionUsuario.getFecha_creacion());

                                Calendar previous = Calendar.getInstance();
                                previous.setTime(ultimaFecha);
                                Calendar now = Calendar.getInstance();
                                long diff = now.getTimeInMillis() - previous.getTimeInMillis();
                                if (diff >= UPDATE_INTERVAL_IN_MILLISECONDS) {
                                    usuarioBDHelper.insetUserLocation(usuario.getId(), latitud, longitud, usuario.getUsername());
                                }
                            }
                        }
                    }
                });

            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
