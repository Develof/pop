package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by consultor on 2/26/16.
 */
public class Visita {
    private int id_visita;
    private int id_sucursal;
    private int id_usuario;
    private String hora_check_inicio;
    private String hora_check_fin;
    private double latitud_checkin;
    private double longitud_checkin;
    private double latitud_checkout;
    private double longitud_checkout;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;

    public Visita() {}

    public Visita(int id_visita, int id_sucursal, int id_usuario, String hora_check_inicio, String hora_check_fin, double latitud_checkin, double longitud_checkin, double latitud_checkout, double longitud_checkout, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.id_visita = id_visita;
        this.id_sucursal = id_sucursal;
        this.id_usuario = id_usuario;
        this.hora_check_inicio = hora_check_inicio;
        this.hora_check_fin = hora_check_fin;
        this.latitud_checkin = latitud_checkin;
        this.longitud_checkin = longitud_checkin;
        this.latitud_checkout = latitud_checkout;
        this.longitud_checkout = longitud_checkout;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public int getId_visita() {
        return id_visita;
    }

    public void setId_visita(int id_visita) {
        this.id_visita = id_visita;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getHora_check_inicio() {
        return hora_check_inicio;
    }

    public void setHora_check_inicio(String hora_check_inicio) {
        this.hora_check_inicio = hora_check_inicio;
    }

    public String getHora_check_fin() {
        return hora_check_fin;
    }

    public void setHora_check_fin(String hora_check_fin) {
        this.hora_check_fin = hora_check_fin;
    }

    public double getLatitud_checkin() {
        return latitud_checkin;
    }

    public void setLatitud_checkin(double latitud_checkin) {
        this.latitud_checkin = latitud_checkin;
    }

    public double getLongitud_checkin() {
        return longitud_checkin;
    }

    public void setLongitud_checkin(double longitud_checkin) {
        this.longitud_checkin = longitud_checkin;
    }

    public double getLatitud_checkout() {
        return latitud_checkout;
    }

    public void setLatitud_checkout(double latitud_checkout) {
        this.latitud_checkout = latitud_checkout;
    }

    public double getLongitud_checkout() {
        return longitud_checkout;
    }

    public void setLongitud_checkout(double longitud_checkout) {
        this.longitud_checkout = longitud_checkout;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
