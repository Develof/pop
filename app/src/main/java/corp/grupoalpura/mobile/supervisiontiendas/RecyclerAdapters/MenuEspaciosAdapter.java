package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.app.Fragment;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.MenuEspaciosFragment;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Espacios;
import corp.grupoalpura.mobile.supervisiontiendas.R;

public class MenuEspaciosAdapter extends RecyclerView.Adapter<MenuEspaciosAdapter.MenuEspaciosViewHolder> {

    private final LayoutInflater mInflater;
    List<Espacios> espacios;
    Context context;
    MenuEspaciosFragment fragment;

    public MenuEspaciosAdapter(Context context, List<Espacios> espacios, MenuEspaciosFragment menuEspaciosFragment) {
        mInflater = LayoutInflater.from(context);
        this.espacios = espacios;
        this.context = context;
        this.fragment = menuEspaciosFragment;
    }

    @NonNull
    @Override
    public MenuEspaciosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = mInflater.inflate(R.layout.activity_menu_espacios, parent, false);

        int height = parent.getMeasuredHeight() / 6;
        itemView.setMinimumHeight(height);

        return new MenuEspaciosViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuEspaciosViewHolder holder, int position) {
        Espacios espacio = espacios.get(position);
        holder.tvEspacioTitulo.setText(espacio.getEspacio());

        holder.rlEspacio.setOnClickListener(view -> {
            fragment.startFragment(espacio.getIdEspacio(), espacio.getEspacio());
        });
    }

    @Override
    public int getItemCount() {
        return espacios.size();
    }


    public class MenuEspaciosViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlEspacio;
        TextView tvEspacioTitulo;

        public MenuEspaciosViewHolder(View itemView) {
            super(itemView);
            rlEspacio = (RelativeLayout) itemView.findViewById(R.id.rl_menu_espacio);
            tvEspacioTitulo = (TextView) itemView.findViewById(R.id.tv_espacio_titulo);
        }

    }

}
