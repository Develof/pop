package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 29/04/16.
 */
public class AdapterActividad {
    int id_visita;
    int id_actividad;
    int id_producto_actividad;
    String titulo_visita;
    String desc_visita;
    int obligacion_visita;
    int terminado;

    public AdapterActividad(int id_visita, String titulo_visita, String desc_visita, int obligacion_visita, int terminado) {
        this.id_visita = id_visita;
        this.titulo_visita = titulo_visita;
        this.desc_visita = desc_visita;
        this.obligacion_visita = obligacion_visita;
        this.terminado = terminado;
    }


    public AdapterActividad(int id_visita, int id_actividad, int id_producto_actividad, String titulo_visita, String desc_visita, int obligacion_visita, int terminado) {
        this.id_visita = id_visita;
        this.id_actividad = id_actividad;
        this.id_producto_actividad = id_producto_actividad;
        this.titulo_visita = titulo_visita;
        this.desc_visita = desc_visita;
        this.obligacion_visita = obligacion_visita;
        this.terminado = terminado;
    }

    public int getId_visita() {
        return id_visita;
    }

    public void setId_visita(int id_visita) {
        this.id_visita = id_visita;
    }

    public int getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(int id_actividad) {
        this.id_actividad = id_actividad;
    }

    public int getId_producto_actividad() {
        return id_producto_actividad;
    }

    public void setId_producto_actividad(int id_producto_actividad) {
        this.id_producto_actividad = id_producto_actividad;
    }

    public String getTitulo_visita() {
        return titulo_visita;
    }

    public void setTitulo_visita(String titulo_visita) {
        this.titulo_visita = titulo_visita;
    }

    public String getDesc_visita() {
        return desc_visita;
    }

    public void setDesc_visita(String desc_visita) {
        this.desc_visita = desc_visita;
    }

    public int getObligacion_visita() {
        return obligacion_visita;
    }

    public void setObligacion_visita(int obligacion_visita) {
        this.obligacion_visita = obligacion_visita;
    }

    public int getTerminado() {
        return terminado;
    }

    public void setTerminado(int terminado) {
        this.terminado = terminado;
    }
}
