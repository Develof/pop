package corp.grupoalpura.mobile.supervisiontiendas;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.*;
import corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters.EspaciosAdapter;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.FamiliasDBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.SucursalEspacioBDHelper;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.models.EZPhotoPickConfig;
import siclo.com.ezphotopicker.api.models.PhotoSource;
import siclo.com.ezphotopicker.models.PhotoIntentException;

public class EspaciosFragment extends Fragment implements LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    RecyclerView recyclerView;
    EspaciosAdapter espaciosAdapter;

    private ClienteSucursal sucursal;
    private ArrayList<SucursalEspacio> sucursalEspacios = new ArrayList<>();
    SucursalEspacioBDHelper sucursalEspacioBDHelper;
    private List<ProdFamilia> familias;

    DBHelper dbHelper;
    FamiliasDBHelper fdbHelper;
    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String ID_ESPACIO = "ID_ESPACIO";

    ArrayList<String> image_uris = new ArrayList<>();
    Usuario usuario;
    GoogleApiClient googleApiClient;
    double latitud = 0, longitud = 0;


    protected LocationRequest mLocationRequest;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 5;
    ArrayList<Departamentos> ubicaciones = new ArrayList<>();
    int nUbicacion = 0, idEspacio = 0, tipoEspacio = 0, categoria = 0;
    ArrayList<String> arregloNombreImagen;
    ArrayList<Uri> urlImagenes;
    ArrayList<Integer> arregloFotosTomadas = new ArrayList<>();


    public EspaciosFragment() {
        // Required empty public constructor
    }

    public EspaciosFragment newInstance(ClienteSucursal sucursal, Usuario usuario, Integer idEspacio) {
        EspaciosFragment fragment = new EspaciosFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(ID_ESPACIO, idEspacio);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ((Contenedor) getActivity()).activity = -1;
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            idEspacio = (Integer) getArguments().getSerializable(ID_ESPACIO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_espacios, container, false);

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null) {
            googleApiClient.connect();
        } else {
            LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 5, 0, this);
            } else {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 5, 0, this);
            }
        }

        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        fdbHelper = new FamiliasDBHelper(getActivity());
        recyclerView = view.findViewById(R.id.recycler_view_espacios);

        sucursalEspacioBDHelper = new SucursalEspacioBDHelper(getActivity());
        sucursalEspacios = sucursalEspacioBDHelper.getSucursalEspacio(sucursal.getId(), idEspacio);

        espaciosAdapter = new EspaciosAdapter(this, getActivity(), sucursalEspacios, sucursal, usuario, getFragmentManager(), idEspacio);
        recyclerView.setAdapter(espaciosAdapter);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_actividades_competencias, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.actividad_competencia_agregar) {

            ubicaciones = dbHelper.getDepartamentos();
            tipoEspacio = dbHelper.getTipoEspacio(idEspacio);
            familias = fdbHelper.getFamiliasEspacio();
            final int[] tipo_adquisicion = {0};
            urlImagenes = new ArrayList<>();
            arregloNombreImagen = new ArrayList<>();

            //ADAPTER PARA LOS SPINNER DE DEPARTAMENTOS Y Espacios
            ArrayAdapter<Departamentos> dataAdapterUbicacion = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, ubicaciones);
            ArrayAdapter<ProdFamilia> dataAdapterfamilias = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, familias);
            dataAdapterUbicacion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapterfamilias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            final Dialog dialogUpadateEspacio = new Dialog(getActivity());
            dialogUpadateEspacio.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogUpadateEspacio.setContentView(R.layout.dialog_update_espacio);
            dialogUpadateEspacio.setCancelable(false);

            Spinner spUbicacion = dialogUpadateEspacio.findViewById(R.id.spinnerUbicacion);

            TextView btnEditar = dialogUpadateEspacio.findViewById(R.id.btn_aceptar_espacio);
            TextView btnCancelar = dialogUpadateEspacio.findViewById(R.id.btn_cancelar);

            TextView txtTituloDepartamento = dialogUpadateEspacio.findViewById(R.id.textView_cargaEspacio_ubicacion);
            RelativeLayout rlNiveles = dialogUpadateEspacio.findViewById(R.id.rl_niveles);
            final EditText edNiveles = dialogUpadateEspacio.findViewById(R.id.editText_cargaEpsacios_niveles);
            RelativeLayout rlTarimas = dialogUpadateEspacio.findViewById(R.id.rl_tarimas);
            final EditText edTarimas = dialogUpadateEspacio.findViewById(R.id.editText_cargaEpsacios_tarimas);
            RelativeLayout rlTramo = dialogUpadateEspacio.findViewById(R.id.rl_tramos);
            final EditText edTramo = dialogUpadateEspacio.findViewById(R.id.editText_cargaEpsacios_tramos);
            RelativeLayout rlPuertas = dialogUpadateEspacio.findViewById(R.id.rl_puertas);
            final EditText edPuertas = dialogUpadateEspacio.findViewById(R.id.editText_cargaEpsacios_puertas);

            RelativeLayout rlAdquisicion = dialogUpadateEspacio.findViewById(R.id.contenedor_cargaEspacios_adquisicion);
            RadioButton rbRentado = dialogUpadateEspacio.findViewById(R.id.rb_cargaEspacios_rentado);
            RadioButton rbGanado = dialogUpadateEspacio.findViewById(R.id.rb_cargaEspacios_ganado);
            RadioButton rbBase = dialogUpadateEspacio.findViewById(R.id.rb_cargaEspacios_base);
            ImageView imgFoto = dialogUpadateEspacio.findViewById(R.id.imageView_evidencia);

            setTipoEspacio(rlNiveles, rlTarimas, rlTramo, rlPuertas);

            rbBase.setChecked(true);

            spUbicacion.setAdapter(dataAdapterUbicacion);


            if (tipoEspacio == 1) {
                tipo_adquisicion[0] = 2;
                rlAdquisicion.setVisibility(View.GONE);

                txtTituloDepartamento.setText("Categoría");
                spUbicacion.setAdapter(dataAdapterfamilias);
            }

            spUbicacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (tipoEspacio != 1) {
                        categoria = 0;
                        nUbicacion = ubicaciones.get(position).getId_departamento();
                    } else {
                        nUbicacion = 0;
                        categoria = familias.get(position).getId();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            rbRentado.setOnClickListener(view -> tipo_adquisicion[0] = 0);

            rbGanado.setOnClickListener(view -> tipo_adquisicion[0] = 1);

            rbBase.setOnClickListener(view -> tipo_adquisicion[0] = 2);

            imgFoto.setOnClickListener(v -> {

                EZPhotoPickConfig config = new EZPhotoPickConfig();
                config.photoSource = PhotoSource.CAMERA;
                config.pathRoot = "IMG_ESPACIO_";
                config.needToAddToGallery = true;
                config.exportingSize = 1000;
                try {
                    EZPhotoPick.startPhotoPickActivity(this, config);
                } catch (PhotoIntentException e) {
                    e.printStackTrace();
                }
            });


            btnEditar.setOnClickListener(view -> {

                if (edNiveles.getText().toString().equals("")) edNiveles.setText("0");
                if (edTarimas.getText().toString().equals("")) edTarimas.setText("0");
                if (edTramo.getText().toString().equals("")) edTramo.setText("0");
                if (edPuertas.getText().toString().equals("")) edPuertas.setText("0");


                if (image_uris.size() > 0) {
                    long id_espacio = dbHelper.insertSucursalEspacios(sucursal.getId(), idEspacio, nUbicacion, categoria, tipo_adquisicion[0], Double.parseDouble(edNiveles.getText().toString()), Integer.parseInt(edPuertas.getText().toString()), Double.parseDouble(edTramo.getText().toString()), Double.parseDouble(edTarimas.getText().toString()), "ruta", usuario.getUsername());
                    if (id_espacio != 0)

                        for (int i = 0; i < image_uris.size(); i++) {
                            String dirNombre = image_uris.get(i).toString().substring(image_uris.get(i).toString().lastIndexOf("/") + 1, image_uris.get(i).toString().length());
                            long id_foto = dbHelper.insertFoto(sucursal.getId(), image_uris.get(i).toString(), dirNombre, latitud, longitud, usuario.getUsername());
                            if (id_foto != 0) {
                                int id_foto_evidencia = (int) id_foto;
                                arregloFotosTomadas.add(id_foto_evidencia);
                            }
                        }

                    int espacio_evidencia = (int) id_espacio;
                    for (int i = 0; i < arregloFotosTomadas.size(); i++) {
                        dbHelper.insertFotoRelacion(arregloFotosTomadas.get(i), DBHelper.TABLA_SUCURSAL_ESPACIOS, espacio_evidencia);
                    }


                    sucursalEspacios = sucursalEspacioBDHelper.getSucursalEspacio(sucursal.getId(), idEspacio);
                    recyclerView.removeAllViews();

                    espaciosAdapter = new EspaciosAdapter(this, getActivity(), sucursalEspacios, sucursal, usuario, getFragmentManager(), idEspacio);
                    recyclerView.setAdapter(espaciosAdapter);
                    espaciosAdapter.notifyDataSetChanged();
                    image_uris.clear();
                    arregloFotosTomadas.clear();
                    arregloNombreImagen.clear();
                    Toast toast = Toast.makeText(getActivity(), "Se ha creado el espacio", Toast.LENGTH_LONG);
                    TextView tv = toast.getView().findViewById(android.R.id.message);
                    tv.setTextColor(Color.WHITE);
                    toast.show();
                    dialogUpadateEspacio.dismiss();

                } else {
                    Toast toast = Toast.makeText(getActivity(), "Debes tomar al menos una fotografía", Toast.LENGTH_LONG);
                    TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                    tv.setTextColor(Color.WHITE);
                    toast.show();
                }

            });

            btnCancelar.setOnClickListener(view -> dialogUpadateEspacio.dismiss());


            dialogUpadateEspacio.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialogUpadateEspacio.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        return super.onOptionsItemSelected(item);
    }


    private void setTipoEspacio(RelativeLayout niveles, RelativeLayout tarimas, RelativeLayout tramos, RelativeLayout puertas) {

        String llenado = dbHelper.getTipoLlenado(idEspacio);
        for (int i = 0; i < llenado.length(); i++) {
            char llenadoPosicion = llenado.charAt(i);

            switch (llenadoPosicion) {

                case 'P':
                    puertas.setVisibility(View.VISIBLE);
                    break;

                case 'N':
                    niveles.setVisibility(View.VISIBLE);
                    break;

                case 'T':
                    tramos.setVisibility(View.VISIBLE);
                    break;

                case 'A':
                    tarimas.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EZPhotoPick.PHOTO_PICK_CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                image_uris = data.getStringArrayListExtra(EZPhotoPick.PICKED_PHOTO_NAMES_KEY);
            } else {
                Toast toast = Toast.makeText(getActivity(), "Ocurrio un error al cargar la imagen vuelve a tomar la foto", Toast.LENGTH_SHORT);
                TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                tv.setTextColor(Color.WHITE);
                toast.show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);

    }


    //REMPLAZA EL FRAGMENT
    public void replaceFragment(SucursalEspacio espacio, int idEspacio) {

        ((Contenedor) getActivity()).activity = 4;
        EspacioFrenteFragment espacioFrenteFragment = new EspacioFrenteFragment().newInstance(sucursal, usuario, espacio, idEspacio);
        ((Contenedor) getActivity()).tvToolbar.setText("Producto en espacio");

        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, espacioFrenteFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (getActivity() != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {

                latitud = location.getLatitude();
                longitud = location.getLongitude();

            } else {
                mLocationRequest = new LocationRequest();

                mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

                mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                PendingResult<Status> locationUpdates = LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        latitud = location.getLatitude();
                        longitud = location.getLongitude();

                    }
                });
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        latitud = location.getLatitude();
        longitud = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

}
