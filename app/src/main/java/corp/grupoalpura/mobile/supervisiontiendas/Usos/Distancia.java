package corp.grupoalpura.mobile.supervisiontiendas.Usos;

/**
 * Created by camovilesb on 5/4/2017.
 */

public class Distancia {

    /* OBTIENE LA DISTANCIA EN METROS */
    public Double distanciaCoord(double lat1, double lng1, double lat2, double lng2) {

        Double R = 6371e3; // metres
        Double lt1 = Math.toRadians(lat1);
        Double lt2 = Math.toRadians(lat2);
        Double Dlt = Math.toRadians(lat2-lat1) ;
        Double Dlg = Math.toRadians(lng2-lng1);
        Double a = (Math.sin(Dlt/2) * Math.sin(Dlt/2)) +(
                Math.cos(lt1) * Math.cos(lt2)) *(
                Math.sin(Dlg/2) * Math.sin(Dlg/2));
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distancia = R * c;

        return distancia;
    }

}
