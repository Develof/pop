package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by uriel_000 on 03/03/2016.
 */
public class Cliente {
    private int id;
    private String alias;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;

    public Cliente(){

    }

    public Cliente(int id, String alias, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.id = id;
        this.alias = alias;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
