package corp.grupoalpura.mobile.supervisiontiendas.Database.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Usuario;

@Dao
public interface UsuarioDao {

    String USUARIO = "USUARIO";

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertaUsuario(Usuario users);

    @Query("SELECT * FROM " + USUARIO + " " +
           "WHERE USERNAME = :username " +
           "AND PASSWORD = :pwd")
    Usuario getUsuario(String username, String pwd);


    @Query("SELECT * FROM " + USUARIO)
    Usuario getLastUsuario();

}
