package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;

/**
 * Created by Develof on 16/03/17.
 */

public class SucursalEspacio implements Serializable {

    private int idSucursalEspacio;
    private int idDepartemento;
    private int idEspacio;
    private int idCategoria;
    private String departamento;
    private String categoria;
    private String espacio;
    private int tipo_adquisicion;
    private double nivel;
    private int puertas;
    private double tramos;
    private double tarimas;
    private int estatus_dos;

    public SucursalEspacio(){};

    public SucursalEspacio(int idSucursalEspacio, String departamento, String espacio) {
        this.idSucursalEspacio = idSucursalEspacio;
        this.departamento = departamento;
        this.espacio = espacio;
    }

    public int getIdSucursalEspacio() {
        return idSucursalEspacio;
    }

    public void setIdSucursalEspacio(int idSucursalEspacio) {
        this.idSucursalEspacio = idSucursalEspacio;
    }

    public int getIdDepartemento() {
        return idDepartemento;
    }

    public void setIdDepartemento(int idDepartemento) {
        this.idDepartemento = idDepartemento;
    }

    public int getIdEspacio() {
        return idEspacio;
    }

    public void setIdEspacio(int idEspacio) {
        this.idEspacio = idEspacio;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getEspacio() {
        return espacio;
    }

    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }

    public int getTipo_adquisicion() {
        return tipo_adquisicion;
    }

    public void setTipo_adquisicion(int tipo_adquisicion) {
        this.tipo_adquisicion = tipo_adquisicion;
    }

    public double getNivel() {
        return nivel;
    }

    public void setNivel(double nivel) {
        this.nivel = nivel;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public double getTramos() {
        return tramos;
    }

    public void setTramos(double tramos) {
        this.tramos = tramos;
    }

    public double getTarimas() {
        return tarimas;
    }

    public void setTarimas(double tarimas) {
        this.tarimas = tarimas;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
