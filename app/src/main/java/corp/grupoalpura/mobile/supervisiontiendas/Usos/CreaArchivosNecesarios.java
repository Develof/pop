package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
  Created by Develof on 16/01/17.
 **/

public class CreaArchivosNecesarios {

    public void copyDataBase(Context context) throws IOException {

        InputStream myInput = context.getAssets().open("manual.pdf");
        File outFilePath = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)));
        outFilePath.mkdirs();
        File outFileName = new File(outFilePath, "manual.pdf");
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

}
