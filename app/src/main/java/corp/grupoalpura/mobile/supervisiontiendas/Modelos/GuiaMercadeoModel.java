package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 06/06/2016.
 */
public class GuiaMercadeoModel {

    private int idGuiaMercadeo;
    private int idCadena;
    private String promocion;
    private String producto;
    private String vigencia;
    private String precioOferta;
    private String region;
    private String fechaInicio;
    private String fechaVencimiento;
    private int estauts;


    public GuiaMercadeoModel() {
    }

    public GuiaMercadeoModel(int idGuiaMercadeo, int idCadena, String promocion, String producto, String vigencia, String precioOferta, String region, int estatus) {
        this.idGuiaMercadeo = idGuiaMercadeo;
        this.idCadena = idCadena;
        this.promocion = promocion;
        this.producto = producto;
        this.vigencia = vigencia;
        this.precioOferta = precioOferta;
        this.region = region;
        this.estauts = estatus;
    }

    public int getIdGuiaMercadeo() {
        return idGuiaMercadeo;
    }

    public void setIdGuiaMercadeo(int idGuiaMercadeo) {
        this.idGuiaMercadeo = idGuiaMercadeo;
    }

    public int getIdCadena() {
        return idCadena;
    }

    public void setIdCadena(int idCadena) {
        this.idCadena = idCadena;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public String getPrecioOferta() {
        return precioOferta;
    }

    public void setPrecioOferta(String precioOferta) {
        this.precioOferta = precioOferta;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public int getEstauts() {
        return estauts;
    }

    public void setEstauts(int estauts) {
        this.estauts = estauts;
    }
}
