package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 11/03/2016.
 */
public class AdapterPromotores {
    private String udn_alias;
    private String supervisor;
    private int id_promotor;
    private String promotor;
    private String nombre;
    private String ap_paterno;
    private String ap_materno;
    private String tienda;

    AdapterPromotores(){

    };

    public AdapterPromotores(String udn_alias, String supervisor, int id_promotor, String promotor, String nombre, String ap_paterno, String ap_materno, String tienda) {
        this.udn_alias = udn_alias;
        this.supervisor = supervisor;
        this.id_promotor = id_promotor;
        this.promotor = promotor;
        this.nombre = nombre;
        this.ap_paterno = ap_paterno;
        this.ap_materno = ap_materno;
        this.tienda = tienda;
    }



    public String getUdn_alias() {
        return udn_alias;
    }

    public void setUdn_alias(String udn_alias) {
        this.udn_alias = udn_alias;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public int getId_promotor() {
        return id_promotor;
    }

    public void setId_promotor(int id_promotor) {
        this.id_promotor = id_promotor;
    }

    public String getPromotor() {
        return promotor;
    }

    public void setPromotor(String promotor) {
        this.promotor = promotor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAp_paterno() {
        return ap_paterno;
    }

    public void setAp_paterno(String ap_paterno) {
        this.ap_paterno = ap_paterno;
    }

    public String getAp_materno() {
        return ap_materno;
    }

    public void setAp_materno(String ap_materno) {
        this.ap_materno = ap_materno;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }
}
