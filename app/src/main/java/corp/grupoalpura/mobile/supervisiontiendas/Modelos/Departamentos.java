package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 12/07/16.
 */
public class Departamentos  {
    private int id_departamento;
    private String nombre;
    private int estatus;

    public Departamentos() {}

    public Departamentos(int id_departamento, String nombre, int estatus) {
        this.id_departamento = id_departamento;
        this.nombre = nombre;
        this.estatus = estatus;
    }

    public Departamentos(int id_departamento, String nombre) {
        this.id_departamento = id_departamento;
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public int getId_departamento() {
        return id_departamento;
    }

    public void setId_departamento(int id_departamento) {
        this.id_departamento = id_departamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
