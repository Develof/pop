package corp.grupoalpura.mobile.supervisiontiendas.view_pager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.support.v4.app.Fragment;

import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.LoadImage;


/**
 * Created by consultor on 22/03/2016.
 */
public class ImageSlidePager extends Fragment {
    private static final String INDEX = "indexImage";
    private String image;

    public static ImageSlidePager newInstance(String idImage) {
        //Agregar index al nuevo fragment
        ImageSlidePager fragment = new ImageSlidePager();
        Bundle args = new Bundle();
        args.putString(INDEX, idImage);
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    public ImageSlidePager() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Agrega nuevo valor al index
        this.image = getArguments().getString(INDEX);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_slide_pager, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageview_slidepage_img);

        LoadImage loadImage = new LoadImage();
        loadImage.load(getActivity(), image, imageView);

        return view;
    }
}

