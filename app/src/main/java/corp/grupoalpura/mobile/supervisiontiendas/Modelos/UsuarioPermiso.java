package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by uriel_000 on 03/03/2016.
 */
public class UsuarioPermiso {
    private int id_usuario;
    private int id_permiso;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;

    UsuarioPermiso(){

    }

    public UsuarioPermiso(int id_usuario, int id_permiso, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.id_usuario = id_usuario;
        this.id_permiso = id_permiso;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_permiso() {
        return id_permiso;
    }

    public void setId_permiso(int id_permiso) {
        this.id_permiso = id_permiso;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
