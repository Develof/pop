package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 22/08/16.
 */

public class CategoriaIncidencia {
    private int id_categoria;
    private String categoria;

    public CategoriaIncidencia(int id_categoria, String categoria) {
        this.id_categoria = id_categoria;
        this.categoria = categoria;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return categoria;
    }
}
