package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by consultor on 2/29/16.
 */
public class Producto {

    private int id;
    private int id_presentacion;
    private int id_familia;
    private int id_compania;
    private String clave;
    private String unidad;
    private String Nombre;
    private String Descripcion;
    private String upc;
    private double precio_unitario;
    private int aplica_iva;
    private String ultima_modificacion;
    private String fecha_creacion;
    private String usuario;
    private int estatus;

    public Producto() { }

    public Producto(int id, int id_presentacion, int id_familia, String clave, String unidad, String nombre, String descripcion, double precio_unitario, int aplica_iva, String ultima_modificacion, String usuario, String fecha_creacion, int estatus) {
        this.id = id;
        this.id_presentacion = id_presentacion;
        this.id_familia = id_familia;
        this.clave = clave;
        this.unidad = unidad;
        Nombre = nombre;
        Descripcion = descripcion;
        this.precio_unitario = precio_unitario;
        this.aplica_iva = aplica_iva;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.fecha_creacion = fecha_creacion;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_presentacion() {
        return id_presentacion;
    }

    public void setId_presentacion(int id_presentacion) {
        this.id_presentacion = id_presentacion;
    }

    public int getId_familia() {
        return id_familia;
    }

    public void setId_familia(int id_familia) {
        this.id_familia = id_familia;
    }

    public int getId_compania() {
        return id_compania;
    }

    public void setId_compania(int id_compania) {
        this.id_compania = id_compania;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public double getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(double precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public int getAplica_iva() {
        return aplica_iva;
    }

    public void setAplica_iva(int aplica_iva) {
        this.aplica_iva = aplica_iva;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }
}
