package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import corp.grupoalpura.mobile.supervisiontiendas.R;

/**
 * Created by Develof on 26/03/17.
 */

public class EspaciosViewHolder extends RecyclerView.ViewHolder {

    CardView cvEspacio;
    ImageView imgEspacio;
    TextView txtHeaderEspacio, txtSubHeaderEspacio, txtTipoAdquisicion, txtProducto;
    Toolbar toolbar;


    public EspaciosViewHolder(View itemView) {
        super(itemView);
        cvEspacio = (CardView) itemView.findViewById(R.id.card_view_espacios);
        imgEspacio = (ImageView) itemView.findViewById(R.id.img_espacio);
        txtHeaderEspacio = (TextView) itemView.findViewById(R.id.text_header_espacio);
        txtSubHeaderEspacio = (TextView) itemView.findViewById(R.id.text_sub_header_espacio);
        txtProducto = itemView.findViewById(R.id.text_sub_header_producto);
        txtTipoAdquisicion = (TextView) itemView.findViewById(R.id.text_tipo_adquisicion);
        toolbar = (Toolbar) itemView.findViewById(R.id.menu_toolbar);
    }

}
