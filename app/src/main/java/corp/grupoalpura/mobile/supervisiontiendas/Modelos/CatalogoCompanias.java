package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 26/08/16.
 */

public class CatalogoCompanias {
    private int id_compania;
    String compania;

    public CatalogoCompanias(int id_compania, String compania) {
        this.id_compania = id_compania;
        this.compania = compania;
    }

    public int getId_compania() {
        return id_compania;
    }

    public void setId_compania(int id_compania) {
        this.id_compania = id_compania;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    @Override
    public String toString() {
        return compania;
    }

}
