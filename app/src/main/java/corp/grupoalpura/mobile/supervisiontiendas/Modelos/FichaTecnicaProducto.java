package corp.grupoalpura.mobile.supervisiontiendas.Modelos;


import java.io.Serializable;

/**
 * Created by Consultor on 03/03/16.
 */
public class FichaTecnicaProducto implements Serializable {
    private int id;
    private String UPC;
    private String clave;
    private String descripcion;
    private int id_producto_existencia;
    private int exhibido;
    private int cantidad;
    private String presentacion;
    private String familia;
    private int idPresentacion;
    private String nombre;
    private int id_subcategoria;
    private int top;
    private int id_compania;

    //PARA EL PEDIDO SIN ORDEN
    private String agregar;
    private String cambios;
    private String degustacion;
    private int inventario;
    private int dias_inventario;
    private double promedio_venta;

    //PARA LOS PRODUCTOS SELECCIONADOS CON PRECIO
    private int isPrieceActive;

    @Override
    public String toString() {
        return descripcion;
    }

    public FichaTecnicaProducto(){}

    public FichaTecnicaProducto(int id, String clave, String descripcion, int id_producto_existencia, int exhibido, int cantidad, String presentacion) {
        this.id = id;
        this.clave = clave;
        this.descripcion = descripcion;
        this.id_producto_existencia = id_producto_existencia;
        this.exhibido = exhibido;
        this.cantidad = cantidad;
        this.presentacion = presentacion;
    }

    public FichaTecnicaProducto(int id, String clave, String descripcion, String presentacion) {
        this.id = id;
        this.clave = clave;
        this.descripcion = descripcion;
        this.presentacion = presentacion;
    }

    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, String presentacion) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.presentacion = presentacion;
    }


    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, int idPresentacion) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.idPresentacion = idPresentacion;
    }



    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, int idPresentacion, String presentacion) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.idPresentacion = idPresentacion;
        this.presentacion = presentacion;
    }


    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, int idPresentacion, String presentacion, String nombre) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.idPresentacion = idPresentacion;
        this.presentacion = presentacion;
        this.nombre = nombre;
    }


    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, int idPresentacion, String presentacion, String nombre, int idSubcategoria, int top, int idCompania) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.idPresentacion = idPresentacion;
        this.presentacion = presentacion;
        this.nombre = nombre;
        this.id_subcategoria = idSubcategoria;
        this.top = top;
        this.id_compania = idCompania;
    }


    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, String presentacion, String agregar, String cambios) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.presentacion = presentacion;
        this.agregar = agregar;
        this.cambios = cambios;
    }

    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, String presentacion, String agregar, String cambios, int inventario, int dias_inventario, double promedio_venta) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.presentacion = presentacion;
        this.agregar = agregar;
        this.cambios = cambios;
        this.inventario = inventario;
        this.dias_inventario = dias_inventario;
        this.promedio_venta = promedio_venta;
    }

    public FichaTecnicaProducto(String agregar, String cambios) {
        this.agregar = agregar;
        this.cambios = cambios;
    }


    public FichaTecnicaProducto(int id, String UPC, String clave, String descripcion, String presentacion, int isPrieceActive) {
        this.id = id;
        this.UPC = UPC;
        this.clave = clave;
        this.descripcion = descripcion;
        this.presentacion = presentacion;
        this.isPrieceActive = isPrieceActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUPC() {
        return UPC;
    }

    public void setUPC(String UPC) {
        this.UPC = UPC;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId_producto_existencia() {
        return id_producto_existencia;
    }

    public void setId_producto_existencia(int id_producto_existencia) {
        this.id_producto_existencia = id_producto_existencia;
    }

    public int getExhibido() {
        return exhibido;
    }

    public void setExhibido(int exhibido) {
        this.exhibido = exhibido;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getAgregar() {
        return agregar;
    }

    public void setAgregar(String agregar) {
        this.agregar = agregar;
    }

    public String getCambios() {
        return cambios;
    }

    public void setCambios(String cambios) {
        this.cambios = cambios;
    }

    public int getInventario() {
        return inventario;
    }

    public void setInventario(int inventario) {
        this.inventario = inventario;
    }

    public int getDias_inventario() {
        return dias_inventario;
    }

    public void setDias_inventario(int dias_inventario) {
        this.dias_inventario = dias_inventario;
    }

    public double getPromedio_venta() {
        return promedio_venta;
    }

    public void setPromedio_venta(double promedio_venta) {
        this.promedio_venta = promedio_venta;
    }

    public int getIsPrieceActive() {
        return isPrieceActive;
    }

    public void setIsPrieceActive(int isPrieceActive) {
        this.isPrieceActive = isPrieceActive;
    }

    public String getDegustacion() {
        return degustacion;
    }

    public void setDegustacion(String degustacion) {
        this.degustacion = degustacion;
    }

    public int getIdPresentacion() {
        return idPresentacion;
    }

    public void setIdPresentacion(int idPresentacion) {
        this.idPresentacion = idPresentacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId_subcategoria() {
        return id_subcategoria;
    }

    public void setId_subcategoria(int id_subcategoria) {
        this.id_subcategoria = id_subcategoria;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getId_compania() {
        return id_compania;
    }

    public void setId_compania(int id_compania) {
        this.id_compania = id_compania;
    }
}
