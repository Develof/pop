package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.InventarioBO;

public class ViewFactInventory {

    private static final String O_VIEW_FACT_INVENTORY_TOTAL = "O_ViewFactInventoryTotal";

    @SerializedName(O_VIEW_FACT_INVENTORY_TOTAL)
    private List<InventarioBO> inventario;

    public List<InventarioBO> getInventario() {
        return inventario;
    }

    public void setInventario(List<InventarioBO> inventario) {
        this.inventario = inventario;
    }
}
