package corp.grupoalpura.mobile.supervisiontiendas.DataBases;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
  Created by oflores on 03/08/16.
 */
public class DBBackup {

    private String salida;

    public void createBackupDB(String packageName, String databaseName, ClienteSucursal sucursal) {
        File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator);
        File data = Environment.getDataDirectory();
        try {
            if (sd.canWrite()) {
                String currentDBPath = "//data//" + packageName + "//databases//" + databaseName + "";
                String backupDBPath = "" + sucursal.getAlias() + databaseName + "";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String backupDb(String packageName, String databaseName) {
        File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator);
        File data = Environment.getDataDirectory();
        //CREAMOS RESPALDO DE LA BD
        try {
            if (sd.canWrite()) {
                String currentDBPath = "//data//" + packageName + "//databases//" + databaseName + "";
                String backupDBPath = "" + databaseName + "";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }

                //METEMOS LA BD EN UNA CARPETA COMPRIMIDA
                try {
                    FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory() + "/ServicioTiendas.zip");
                    ZipOutputStream zos = new ZipOutputStream(fos);
                    //File f = new File(fileDB);
                    byte[] buffer = new byte[1024];
                    FileInputStream fis = new FileInputStream(backupDB.getPath());
                    zos.putNextEntry(new ZipEntry(backupDB.getName()));
                    int length;
                    while ((length = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, length);
                    }
                    zos.closeEntry();
                    fis.close();

                    zos.close();
                } catch (IOException ignored) {
                    Log.v("error", ignored.getMessage());
                }


                //LA CARPETA ZIP LA PASAMOS A BASE64
                InputStream inputStream;
                inputStream = new FileInputStream(Environment.getExternalStorageDirectory() + "/ServicioTiendas.zip");
                byte[] buffer = new byte[10192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output64.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                output64.close();
                salida = output.toString();

            }

        } catch (Exception ignored) {
            Log.v("error", ignored.getMessage());
        }

        return salida;
    }


    public String getImageBase64(String filePath) {

        try {
            File f = new File(filePath);
            Bitmap bm = BitmapFactory.decodeFile(f.toString());
            bm = Bitmap.createScaledBitmap(bm, bm.getWidth() / 4, bm.getHeight() / 4, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 30, baos);

            bm.recycle();
            bm = null;

            byte[] b = baos.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        } catch (Exception e) {
            Crashlytics.logException(e);
            return "";
        }

    }


}
