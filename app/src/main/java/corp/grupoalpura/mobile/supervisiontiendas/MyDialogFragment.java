package corp.grupoalpura.mobile.supervisiontiendas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.LocalizacionGps.Localizacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FotosEvidenciaRelacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.view_pager.FragmentAdapter;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.models.EZPhotoPickConfig;
import siclo.com.ezphotopicker.api.models.PhotoSource;
import siclo.com.ezphotopicker.models.PhotoIntentException;


import com.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by oflores on 23/08/16.
 */

public class MyDialogFragment extends DialogFragment {

    int id_component, id_foto;
    DBHelper dbHelper;
    String table_name, titulo, dirNombre, FIRST_NAME, file;
    Uri outputFileUri;
    ClienteSucursal sucursal;
    double latitud = 0, longitud = 0;
    Usuario usuario;
    Localizacion localizacion;

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String USUARIO = "usuario";

    public static MyDialogFragment newInstance(int id_component, String table, String titulo, String photo_name, ClienteSucursal sucursal, Usuario usuario) {
        MyDialogFragment fragment = new MyDialogFragment();
        Bundle args = new Bundle();
        args.putInt("id_foto", id_component);
        args.putString("table", table);
        args.putString("title", titulo);
        args.putString("name", photo_name);
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(USUARIO, usuario);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null)
            outputFileUri = savedInstanceState.getParcelable("outputFileUri");
        super.onCreate(savedInstanceState);
        localizacion = new Localizacion(getActivity());
        this.id_component = getArguments().getInt("id_foto");
        this.table_name = getArguments().getString("table");
        this.titulo = getArguments().getString("title");
        this.FIRST_NAME = getArguments().getString("name");
        this.sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
        this.usuario = (Usuario) getArguments().getSerializable(USUARIO);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View dialogFoto = getActivity().getLayoutInflater().inflate(R.layout.fragment_fotos, container, false);

        getDialog().setTitle(titulo);
        final ArrayList<FotosEvidenciaRelacion> fotos;
        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        fotos = dbHelper.getFotos(table_name, id_component);
        Button btnBack = (Button) dialogFoto.findViewById(R.id.btnBack);

        btnBack.setText("SALIR");
        CirclePageIndicator indicator = dialogFoto.findViewById(R.id.indicator);
        final ViewPager pager = dialogFoto.findViewById(R.id.pager);
        Toolbar toolbar = dialogFoto.findViewById(R.id.menu_toolbar);

        if (fotos.size() > 0) {
            pager.setAdapter(new FragmentAdapter(getChildFragmentManager(), fotos));
            indicator.setViewPager(pager);
        } else {
            Toast toast = Toast.makeText(getActivity(), "No se ha encontrado fotos", Toast.LENGTH_SHORT);
            TextView tv = toast.getView().findViewById(android.R.id.message);
            tv.setTextColor(Color.WHITE);
            toast.show();
        }

        btnBack.setOnClickListener(view -> dismiss());


        toolbar.inflateMenu(R.menu.menu_fotos);
        toolbar.setOnMenuItemClickListener(item -> {
            final int id = item.getItemId();

            AudioManager mgr = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            int streamType = AudioManager.STREAM_SYSTEM;
            mgr.setStreamSolo(streamType, true);
            mgr.setStreamMute(streamType, true);

            switch (id) {

                case R.id.alerta_tomar_foto:

                    EZPhotoPickConfig config = new EZPhotoPickConfig();
                    config.photoSource = PhotoSource.CAMERA;
                    config.pathRoot = table_name;
                    config.needToAddToGallery = true;
                    config.exportingSize = 1000;
                    try {
                        EZPhotoPick.startPhotoPickActivity(this, config);
                    } catch (PhotoIntentException e) {
                        e.printStackTrace();
                    }

                    break;

                case R.id.alerta_eliminar:

                    String myPath = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_foto)) + "/ " + table_name + "/" + fotos.get(pager.getCurrentItem()).getFile_name();
                    File f = new File(myPath);
                    f.delete();

                    dbHelper.deleteFoto(fotos.get(pager.getCurrentItem()).getId_foto());

                    dismiss();
                    DialogFragment newFragment = MyDialogFragment.newInstance(id_component, table_name, titulo, FIRST_NAME, sucursal, usuario);
                    newFragment.show(getFragmentManager(), "");
                    newFragment.setCancelable(false);
                    break;
            }
            return true;

        });

        return dialogFoto;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constantes.FOTOS.TAKE_PHOTO_CODE_FRAGMENT && resultCode == Activity.RESULT_OK) {
            if (outputFileUri != null && outputFileUri.toString() != "") {

                String archivo = dbHelper.getArchivo(id_foto);
                if (archivo != null) {
                    String myPath = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_foto)) + "/ " + table_name + "/" + archivo;
                    File f = new File(myPath);
                    f.delete();
                }


                dbHelper.updateimage(id_foto, outputFileUri.toString(), dirNombre, 0.0, 0.0);

                dismiss();
                DialogFragment newFragment = MyDialogFragment.newInstance(id_component, table_name, titulo, FIRST_NAME, sucursal, usuario);
                newFragment.show(getFragmentManager(), "");
                newFragment.setCancelable(false);

                Toast toast = Toast.makeText(getActivity(), "Has cambiado la imagen", Toast.LENGTH_SHORT);
                TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                tv.setTextColor(Color.WHITE);
                toast.show();


            }
        } else if (resultCode == getActivity().RESULT_CANCELED) {
            outputFileUri = null;
            Toast toast = Toast.makeText(getActivity(), "Has cancelado la foto", Toast.LENGTH_SHORT);
            TextView tv = toast.getView().findViewById(android.R.id.message);
            tv.setTextColor(Color.WHITE);
            toast.show();
        } else if (requestCode == EZPhotoPick.PHOTO_PICK_CAMERA_REQUEST_CODE) {

            ArrayList<Integer> arregloFotosTomadas = new ArrayList<>();
            List<String> image_uris = data.getStringArrayListExtra(EZPhotoPick.PICKED_PHOTO_NAMES_KEY);

            for (int i = 0; i < image_uris.size(); i++) {
                String dirNombre = image_uris.get(i).toString().substring(image_uris.get(i).toString().lastIndexOf("/") + 1, image_uris.get(i).toString().length());
                long id_foto = dbHelper.insertFoto(sucursal.getId(), image_uris.get(i).toString(), dirNombre, latitud, longitud, usuario.getUsername());
                if (id_foto != 0) {
                    int id_foto_evidencia = (int) id_foto;
                    arregloFotosTomadas.add(id_foto_evidencia);
                }
            }

            for (int i = 0; i < arregloFotosTomadas.size(); i++) {
                dbHelper.insertFotoRelacion(arregloFotosTomadas.get(i), table_name, id_component);
            }


            dismiss();
            DialogFragment newFragment = MyDialogFragment.newInstance(id_component, table_name, titulo, FIRST_NAME, sucursal, usuario);
            newFragment.show(getFragmentManager(), "");
            newFragment.setCancelable(false);

        } else {
            outputFileUri = null;
            Toast toast = Toast.makeText(getActivity(), "Ocurrio un error al cargar la imagen vuelve a tomar la foto", Toast.LENGTH_SHORT);
            TextView tv = toast.getView().findViewById(android.R.id.message);
            tv.setTextColor(Color.WHITE);
            toast.show();
        }
    }

}
