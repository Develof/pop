package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by prestamo on 11/05/2016.
 */
public class FotosEvidenciaRelacion implements Serializable, Parcelable {
    private int id_foto;
    private String archivo;
    private String file_name;

    public FotosEvidenciaRelacion(){

    }

    public FotosEvidenciaRelacion(int id_foto, String archivo, String file_name) {
        this.id_foto = id_foto;
        this.archivo = archivo;
        this.file_name = file_name;
    }

    protected FotosEvidenciaRelacion(Parcel in) {
        id_foto = in.readInt();
        archivo = in.readString();
        file_name = in.readString();
    }

    public static final Creator<FotosEvidenciaRelacion> CREATOR = new Creator<FotosEvidenciaRelacion>() {
        @Override
        public FotosEvidenciaRelacion createFromParcel(Parcel in) {
            return new FotosEvidenciaRelacion(in);
        }

        @Override
        public FotosEvidenciaRelacion[] newArray(int size) {
            return new FotosEvidenciaRelacion[size];
        }
    };

    public int getId_foto() {
        return id_foto;
    }

    public void setId_foto(int id_foto) {
        this.id_foto = id_foto;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id_foto);
        parcel.writeString(archivo);
        parcel.writeString(file_name);
    }
}
