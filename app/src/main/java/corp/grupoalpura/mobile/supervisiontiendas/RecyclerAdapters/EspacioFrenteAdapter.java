package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterEspacioFrente;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.SucursalEspacio;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.EspacioFrenteBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.LoadImage;

public class EspacioFrenteAdapter extends RecyclerView.Adapter<EspacioFrenteViewHolder> {

    private final LayoutInflater mInflater;
    private Context context;
    private List<AdapterEspacioFrente> frentes;
    private int idEspacio;
    private SucursalEspacio sucursalEspacio;
    private EspacioFrenteBDHelper dbHelper;
    private int tipoEspacio = 0;

    public EspacioFrenteAdapter(Context context, SucursalEspacio espacio, List<AdapterEspacioFrente> frentes, int idEspacio, ClienteSucursal sucursal, Usuario usuario) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.sucursalEspacio = espacio;
        this.frentes = frentes;
        this.idEspacio = idEspacio;
        dbHelper = new EspacioFrenteBDHelper(context);
    }

    @NonNull
    @Override
    public EspacioFrenteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = mInflater.inflate(R.layout.adapter_espacio_frentes, parent, false);
        return new EspacioFrenteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EspacioFrenteViewHolder holder, int position) {

        AdapterEspacioFrente frente = frentes.get(position);

        LoadImage loadImage = new LoadImage();
        String imageResource = Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_img_productos)) + File.separator + frente.getClave().toLowerCase() + ".png";
        loadImage.load(context, imageResource, holder.imgProducto);

        holder.tvProducto.setText(frente.getDescipcion());
        holder.tvCompania.setText(frente.getNombre_compania());
        holder.tvFrente.setText(String.valueOf(frente.getFrentes()));

        holder.cvEspacio.setOnClickListener(view -> {

            String tipoFrente = dbHelper.getTipoFrente(idEspacio);
            tipoEspacio = dbHelper.getTipoEspacio(idEspacio);

            final Dialog dialogFrente = new Dialog(context);
            dialogFrente.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogFrente.setContentView(R.layout.dialog_update_espacio_frente);
            dialogFrente.setCancelable(false);


            EditText edFrentes = dialogFrente.findViewById(R.id.ed_frentes);
            TextView btnCancelar = dialogFrente.findViewById(R.id.btn_cancelar);
            TextView btnAceptat = dialogFrente.findViewById(R.id.btn_aceptar);
            TextView btnEliminar = dialogFrente.findViewById(R.id.btn_eliminar);

            edFrentes.setHint(tipoFrente);

            btnAceptat.setOnClickListener(view2 -> {
                if (tipoEspacio == 2) {
                    if (!edFrentes.getText().toString().isEmpty()) {
                        double noFrentes = dbHelper.getNumeroFrentes(sucursalEspacio.getIdSucursalEspacio(), frente.getId_espacio_frente());
                        noFrentes = noFrentes + Double.parseDouble(edFrentes.getText().toString());
                        if (validaProducto(noFrentes, sucursalEspacio)) {
                            dbHelper.updateFrentes(frente, Double.parseDouble(edFrentes.getText().toString()));
                            frentes = dbHelper.getFrentes(sucursalEspacio.getIdSucursalEspacio());
                            notifyDataSetChanged();
                            dialogFrente.dismiss();
                        } else {
                            Toast.makeText(context, "No cuentas con espacio para almacenar el producto", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    dbHelper.updateFrentes(frente, Double.parseDouble(edFrentes.getText().toString()));
                    frentes = dbHelper.getFrentes(sucursalEspacio.getIdSucursalEspacio());
                    notifyDataSetChanged();
                    dialogFrente.dismiss();
                }
            });


            btnEliminar.setOnClickListener(view3 -> {
                dbHelper.disableFrentes(frente);
                frentes = dbHelper.getFrentes(sucursalEspacio.getIdSucursalEspacio());
                notifyDataSetChanged();
                dialogFrente.dismiss();
            });

            btnCancelar.setOnClickListener(view1 -> {
                dialogFrente.dismiss();
            });

            dialogFrente.show();
            WindowManager.LayoutParams lp1 = new WindowManager.LayoutParams();
            Window window1 = dialogFrente.getWindow();
            lp1.copyFrom(window1.getAttributes());

            lp1.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp1.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window1.setAttributes(lp1);
        });

    }

    @Override
    public int getItemCount() {
        return frentes.size();
    }


    private boolean validaProducto(double cant, SucursalEspacio espacio) {

        boolean cantidadValida = false;
        String llenado = dbHelper.getTipoLlenado(idEspacio);
        double comparador = 1;

        for (int i = 0; i < llenado.length(); i++) {
            char llenadoPosicion = llenado.charAt(i);

            switch (llenadoPosicion) {
                case 'A':
                    comparador = espacio.getTarimas();
                    break;
            }
        }

        if (comparador >= cant) {
            cantidadValida = true;
        }

        return cantidadValida;
    }

}
