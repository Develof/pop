package corp.grupoalpura.mobile.supervisiontiendas;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.LocalizacionGps.Localizacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterActividadesCompetencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CatalogoActividades;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CatalogoCompanias;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.models.EZPhotoPickConfig;
import siclo.com.ezphotopicker.api.models.PhotoSource;
import siclo.com.ezphotopicker.models.PhotoIntentException;

public class ActividadesCompetencia extends Fragment {

    DBHelper dbHelper;
    private ClienteSucursal sucursal;
    private Usuario usuario;
    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    public static String TITULO = "Titulo";
    CharSequence mTitutlo;
    ArrayList<AdapterActividadesCompetencia> arregloActividades = new ArrayList<>();
    CustomAdapter adapter;
    Date updateFechaInicio = null, updateFechaFin = null;
    ArrayList<CatalogoCompanias> arregloCompania;
    ArrayList<CatalogoActividades> arregloActividad;
    ArrayAdapter<CatalogoCompanias> dataAdapterCompania;
    ArrayAdapter<CatalogoActividades> dataAdapterActividades;
    private int posicionCompania;
    private int posicionActividad;
    ArrayList<Integer> arregloFotosTomadas = new ArrayList<>();
    ArrayList<String> image_uris = new ArrayList<>();
    Double latitud = 0.0, longitud = 0.0;
    Localizacion localizacion;
    String nombreCompania = "";
    boolean isCompaniaOtros = false;


    public ActividadesCompetencia newInstance(ClienteSucursal sucursal, Usuario usuario, CharSequence titulo) {

        ActividadesCompetencia fragment = new ActividadesCompetencia();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putCharSequence(TITULO, titulo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mTitutlo = getArguments().getCharSequence(TITULO);
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
        }

        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        arregloCompania = dbHelper.getCompania();
        arregloActividad = dbHelper.getActividadCategoria();
        localizacion = new Localizacion(getActivity());
        if (localizacion.connect()) {
            localizacion.getLocalizacion();
            latitud = localizacion.getLatitud();
            longitud = localizacion.getLongitud();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_actividad_competencia, container, false);

        ListView listaActividades = (ListView) view.findViewById(R.id.listView_actividades_competencia);
        dataAdapterCompania = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arregloCompania);
        dataAdapterActividades = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arregloActividad);
        arregloActividades = dbHelper.getActividadescompetencia(sucursal.getId());
        adapter = new CustomAdapter(getActivity());
        listaActividades.setAdapter(adapter);

        return view;
    }

    private class CustomAdapter extends BaseAdapter {
        Context context;

        public CustomAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return arregloActividades.size();
        }

        @Override
        public Object getItem(int position) {
            return arregloActividades.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.adapter_desc, parent, false);

            final TextView txtCategoria = (TextView) row.findViewById(R.id.textView_categoria);
            final TextView txtFecha = (TextView) row.findViewById(R.id.textView_fecha);
            TextView txtCompania = (TextView) row.findViewById(R.id.textView_compania);
            final TextView txtDescripcion = (TextView) row.findViewById(R.id.textView_descripcion);
            Toolbar toolbar = (Toolbar) row.findViewById(R.id.menu_toolbar);

            txtCompania.setVisibility(View.VISIBLE);
            txtFecha.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

            Date fecha_creacion = Dates.stringToDate(arregloActividades.get(position).getFecha());
            final String fecha = Dates.dateToString(fecha_creacion);

            txtCategoria.setText(arregloActividades.get(position).getActividad_categoria());
            txtFecha.setText(fecha);

            txtCompania.setText((arregloActividades.get(position).getCompania().equals("OTROS") ? arregloActividades.get(position).getDescripcion_compania() : arregloActividades.get(position).getCompania()));
            txtDescripcion.setText(arregloActividades.get(position).getDescripcion());


            toolbar.inflateMenu(R.menu.menu_info_actividades_competencia);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    final int id = item.getItemId();
                    switch (id) {
                        case R.id.actividad_ver_foto:

                            DialogFragment newFragment = MyDialogFragment.newInstance(arregloActividades.get(position).getId_actividad_competencia(), DBHelper.TABLA_ACTIVIDAD_COMPETENCIA, arregloActividades.get(position).getDescripcion(), "IMG_ACTIVIDAD_", sucursal, usuario);
                            newFragment.show(getFragmentManager(), "");
                            newFragment.setCancelable(false);
                            break;

                        case R.id.actividad_eliminar:

                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Actividad en tienda")
                                    .setMessage("¿Estás seguro que deseas eliminar la actividad?")
                                    .setCancelable(false)
                                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    })
                                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            if (arregloActividades.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || arregloActividades.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                                dbHelper.eliminarActividadCompetencia(arregloActividades.get(position).getId_actividad_competencia(), sucursal.getId(), usuario.getUsername(), Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
                                            } else {
                                                dbHelper.eliminarActividadCompetencia(arregloActividades.get(position).getId_actividad_competencia(), sucursal.getId(), usuario.getUsername(), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
                                            }

                                            arregloActividades = dbHelper.getActividadescompetencia(sucursal.getId());
                                            adapter.notifyDataSetChanged();
                                            Toast.makeText(getActivity(), "Actividad Eliminada", Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .show();

                            break;

                        case R.id.actividad_editar:

                            final Dialog dialogEditaIncidencia = new Dialog(getActivity());
                            dialogEditaIncidencia.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialogEditaIncidencia.setContentView(R.layout.fragment_actividades_competencia);

                            ImageView imgFoto = (ImageView) dialogEditaIncidencia.findViewById(R.id.imageView_foto);
                            Spinner spCompanias = (Spinner) dialogEditaIncidencia.findViewById(R.id.spinner_actividades_competencia_compania);
                            Spinner spActividades = (Spinner) dialogEditaIncidencia.findViewById(R.id.spinner_actividades_competencia_actividad);
                            TextInputLayout textInputLayout = (TextInputLayout) dialogEditaIncidencia.findViewById(R.id.inputLayout_actividades_competencia_descripcio);
                            final EditText txtDescripcion = (EditText) dialogEditaIncidencia.findViewById(R.id.editText_actividades_competencia_descripcio);
                            ImageView imgFechaInicio = (ImageView) dialogEditaIncidencia.findViewById(R.id.imageView_fechaInicio);
                            ImageView imgFechaFin = (ImageView) dialogEditaIncidencia.findViewById(R.id.imageView_fechaFin);
                            final TextView txtFechaInicio = (TextView) dialogEditaIncidencia.findViewById(R.id.textView_actividades_competencia_fechaInicio);
                            final TextView txtFechaFin = (TextView) dialogEditaIncidencia.findViewById(R.id.textView_actividades_competencia_fechaFin);
                            Button btnCancelar = (Button) dialogEditaIncidencia.findViewById(R.id.btnCancelar);
                            Button btnGuardar = (Button) dialogEditaIncidencia.findViewById(R.id.btnGuardar);
                            final RelativeLayout rlParticipantes = (RelativeLayout) dialogEditaIncidencia.findViewById(R.id.contenedor_actividades_participantes);
                            final EditText edParticipantes = (EditText) dialogEditaIncidencia.findViewById(R.id.editText_actividades_competencia_participantes);

                            textInputLayout.setVisibility(View.VISIBLE);
                            txtDescripcion.setText(arregloActividades.get(position).getDescripcion());
                            btnGuardar.setText(getActivity().getResources().getString(R.string.txt_guardar));

                            int index = 0;
                            int indexarreglo;
                            for (int i = 0; i < arregloCompania.size(); i++) {
                                indexarreglo = arregloCompania.get(i).getCompania().indexOf(arregloActividades.get(position).getCompania());
                                if (indexarreglo != -1) {
                                    index = i;
                                }
                            }

                            spCompanias.setAdapter(dataAdapterCompania);
                            spCompanias.setSelection(index);
                            spCompanias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    posicionCompania = arregloCompania.get(position).getId_compania();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });

                            int indexActi = 0;
                            int indexarregloActi;
                            for (int i = 0; i < arregloActividad.size(); i++) {
                                indexarregloActi = arregloActividad.get(i).getActividad().indexOf(arregloActividades.get(position).getActividad_categoria());
                                if (indexarregloActi != -1) {
                                    indexActi = i;
                                }
                            }

                            spActividades.setAdapter(dataAdapterActividades);
                            spActividades.setSelection(indexActi);
                            spActividades.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position_, long id) {
                                    posicionActividad = arregloActividad.get(position_).getId_actividad();

                                    if (posicionActividad == 4) {
                                        rlParticipantes.setVisibility(View.VISIBLE);
                                        edParticipantes.setText(String.valueOf(arregloActividades.get(position).getParticipantes()));
                                    } else {
                                        rlParticipantes.setVisibility(View.GONE);
                                        edParticipantes.setText("0");
                                    }

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });


                            imgFechaInicio.setOnClickListener(v -> {

                                final Calendar date;
                                final Calendar currentDate = Calendar.getInstance();
                                date = Calendar.getInstance();

                                new DatePickerDialog(getActivity(), (view, year, monthOfYear, dayOfMonth) -> {
                                    date.set(year, monthOfYear, dayOfMonth);
                                    updateFechaInicio = date.getTime();
                                    String fechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(updateFechaInicio);
                                    txtFechaInicio.setText(fechaInicio);
                                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();

                            });

                            imgFechaFin.setOnClickListener(v -> {

                                final Calendar date;
                                final Calendar currentDate = Calendar.getInstance();
                                date = Calendar.getInstance();

                                new DatePickerDialog(getActivity(), (view, year, monthOfYear, dayOfMonth) -> {
                                    date.set(year, monthOfYear, dayOfMonth);
                                    updateFechaFin = date.getTime();
                                    String fechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(updateFechaFin);
                                    txtFechaFin.setText(fechaInicio);
                                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();

                            });

                            btnCancelar.setOnClickListener(v -> dialogEditaIncidencia.dismiss());

                            btnGuardar.setOnClickListener(v -> {
                                if (txtDescripcion.getText().toString().equals("")) {
                                    Toast toast = Toast.makeText(getActivity(), "Llena todos los campos", Toast.LENGTH_SHORT);
                                    TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                                    tv.setTextColor(Color.WHITE);
                                    toast.show();
                                } else {

                                    String fechaInicio, fechaFin;
                                    if (updateFechaInicio == null || updateFechaFin == null) {
                                        fechaInicio = "";
                                        fechaFin = "";
                                    } else {
                                        fechaInicio = Dates.createDateToString(updateFechaInicio);
                                        fechaFin = Dates.createDateToString(updateFechaFin);
                                    }

                                    if (arregloActividades.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || arregloActividades.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                        dbHelper.updateActividadCompetencia(arregloActividades.get(position).getId_actividad_competencia(), posicionActividad, posicionCompania, sucursal.getId(), "", txtDescripcion.getText().toString(), edParticipantes.getText().toString(), fechaInicio, fechaFin, Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL, usuario.getUsername());
                                    } else {
                                        dbHelper.updateActividadCompetencia(arregloActividades.get(position).getId_actividad_competencia(), posicionActividad, posicionCompania, sucursal.getId(), "", txtDescripcion.getText().toString(), edParticipantes.getText().toString(), fechaInicio, fechaFin, Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL, usuario.getUsername());
                                    }

                                    Toast toast = Toast.makeText(getActivity(), "Se ha modificado la actividad", Toast.LENGTH_SHORT);
                                    TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                                    tv.setTextColor(Color.WHITE);
                                    toast.show();

                                    arregloActividades = dbHelper.getActividadescompetencia(sucursal.getId());
                                    adapter.notifyDataSetChanged();
                                    dialogEditaIncidencia.dismiss();

                                }
                            });

                            imgFoto.setVisibility(View.GONE);

                            dialogEditaIncidencia.show();

                            WindowManager.LayoutParams lps = new WindowManager.LayoutParams();
                            Window windows = dialogEditaIncidencia.getWindow();
                            lps.copyFrom(windows.getAttributes());
                            lps.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lps.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            windows.setAttributes(lps);

                            break;
                    }
                    return true;
                }

            });


            return row;
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_actividades_competencias, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.actividad_competencia_agregar) {

            final Dialog dialogCargaActividad = new Dialog(getActivity());
            dialogCargaActividad.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogCargaActividad.setContentView(R.layout.fragment_actividades_competencia);
            dialogCargaActividad.setCancelable(false);

            final Spinner spinnerCompania = (Spinner) dialogCargaActividad.findViewById(R.id.spinner_actividades_competencia_compania);
            Spinner spinnerActividades = (Spinner) dialogCargaActividad.findViewById(R.id.spinner_actividades_competencia_actividad);
            final EditText edDescripcion = (EditText) dialogCargaActividad.findViewById(R.id.editText_actividades_competencia_descripcio);
            ImageView imgFechaInicio = (ImageView) dialogCargaActividad.findViewById(R.id.imageView_fechaInicio);
            ImageView imgFechaFin = (ImageView) dialogCargaActividad.findViewById(R.id.imageView_fechaFin);
            final ImageView imgFoto = (ImageView) dialogCargaActividad.findViewById(R.id.imageView_foto);
            final TextView txtFechaInicio = (TextView) dialogCargaActividad.findViewById(R.id.textView_actividades_competencia_fechaInicio);
            final TextView txtFechaFin = (TextView) dialogCargaActividad.findViewById(R.id.textView_actividades_competencia_fechaFin);
            Button btnGuardar = (Button) dialogCargaActividad.findViewById(R.id.btnGuardar);
            Button btnCancelar = (Button) dialogCargaActividad.findViewById(R.id.btnCancelar);
            final TextView textCompania = (TextView) dialogCargaActividad.findViewById(R.id.textView_title_compania_otros);
            final TextView edCompaniaOtros = (EditText) dialogCargaActividad.findViewById(R.id.edittext_descripcion_marca);
            final RelativeLayout rlParticipantes = (RelativeLayout) dialogCargaActividad.findViewById(R.id.contenedor_actividades_participantes);
            final EditText edParticipantes = (EditText) dialogCargaActividad.findViewById(R.id.editText_actividades_competencia_participantes);

            dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);

            arregloCompania = dbHelper.getCompania();
            arregloActividad = dbHelper.getActividadCategoria();
            ArrayAdapter<CatalogoCompanias> dataAdapterCompania = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arregloCompania);
            ArrayAdapter<CatalogoActividades> dataAdapterActividad = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arregloActividad);

            imgFechaInicio.setOnClickListener(v -> {

                final Calendar date;
                final Calendar currentDate = Calendar.getInstance();
                date = Calendar.getInstance();

                new DatePickerDialog(getActivity(), (view, year, monthOfYear, dayOfMonth) -> {
                    date.set(year, monthOfYear, dayOfMonth);
                    updateFechaInicio = date.getTime();
                    String fechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(updateFechaInicio);
                    txtFechaInicio.setText(fechaInicio);
                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();

            });

            imgFechaFin.setOnClickListener(v -> {

                final Calendar date;
                final Calendar currentDate = Calendar.getInstance();
                date = Calendar.getInstance();

                new DatePickerDialog(getActivity(), (view, year, monthOfYear, dayOfMonth) -> {
                    date.set(year, monthOfYear, dayOfMonth);
                    updateFechaFin = date.getTime();
                    String fechaInicio = new SimpleDateFormat("yyyy-MM-dd").format(updateFechaFin);
                    txtFechaFin.setText(fechaInicio);
                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();

            });


            spinnerCompania.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    posicionCompania = arregloCompania.get(position).getId_compania();


                    TextView textView = (TextView) spinnerCompania.getSelectedView();
                    nombreCompania = textView.getText().toString();

                    if (nombreCompania.toString().equals("OTROS")) {
                        textCompania.setVisibility(View.VISIBLE);
                        edCompaniaOtros.setVisibility(View.VISIBLE);
                        isCompaniaOtros = true;
                    } else {
                        textCompania.setVisibility(View.GONE);
                        edCompaniaOtros.setVisibility(View.GONE);
                        isCompaniaOtros = false;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            spinnerActividades.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    posicionActividad = arregloActividad.get(position).getId_actividad();

                    if (posicionActividad == 4) {
                        rlParticipantes.setVisibility(View.VISIBLE);
                        edParticipantes.setText("");
                    } else {
                        rlParticipantes.setVisibility(View.GONE);
                        edParticipantes.setText("0");
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            spinnerCompania.setAdapter(dataAdapterCompania);
            spinnerActividades.setAdapter(dataAdapterActividad);

            imgFoto.setOnClickListener(v -> {

                EZPhotoPickConfig config = new EZPhotoPickConfig();
                config.photoSource = PhotoSource.CAMERA;
                config.pathRoot = "IMG_ACTIVIDAD_";
                config.needToAddToGallery = true;
                config.exportingSize = 1000;
                try {
                    EZPhotoPick.startPhotoPickActivity(ActividadesCompetencia.this, config);
                } catch (PhotoIntentException e) {
                    e.printStackTrace();
                }


            });

            btnCancelar.setOnClickListener(v -> dialogCargaActividad.dismiss());


            btnGuardar.setOnClickListener(v -> {

                String fechaInicio, fechaFin;
                if (updateFechaInicio == null || updateFechaFin == null) {
                    fechaInicio = "";
                    fechaFin = "";
                } else {
                    fechaInicio = Dates.createDateToString(updateFechaInicio);
                    fechaFin = Dates.createDateToString(updateFechaFin);
                }

                long id_actividad_competencia = dbHelper.insertActividadCompetencia(sucursal.getId(), posicionCompania, posicionActividad, "", edDescripcion.getText().toString(), fechaInicio, fechaFin, usuario.getUsername(), edCompaniaOtros.getText().toString(), Integer.parseInt(edParticipantes.getText().toString()));
                if (id_actividad_competencia != 0) {
                    int id_actividad = (int) id_actividad_competencia;

                    for (int i = 0; i < image_uris.size(); i++) {
                        String dirNombre = image_uris.get(i).toString().substring(image_uris.get(i).toString().lastIndexOf("/") + 1, image_uris.get(i).toString().length());
                        long id_foto = dbHelper.insertFoto(sucursal.getId(), image_uris.get(i).toString(), dirNombre, latitud, longitud, usuario.getUsername());
                        if (id_foto != 0) {
                            int id_foto_evidencia = (int) id_foto;
                            arregloFotosTomadas.add(id_foto_evidencia);
                        }
                    }

                    for (int i = 0; i < arregloFotosTomadas.size(); i++) {
                        dbHelper.insertFotoRelacion(arregloFotosTomadas.get(i), DBHelper.TABLA_ACTIVIDAD_COMPETENCIA, id_actividad);
                    }

                    Toast toast = Toast.makeText(getActivity(), "Se ha guardado la actividad", Toast.LENGTH_SHORT);
                    TextView tv = toast.getView().findViewById(android.R.id.message);
                    tv.setTextColor(Color.WHITE);
                    toast.show();

                    arregloActividades = dbHelper.getActividadescompetencia(sucursal.getId());
                    adapter.notifyDataSetChanged();

                    dialogCargaActividad.dismiss();
                }
            });

            dialogCargaActividad.show();


            WindowManager.LayoutParams lps = new WindowManager.LayoutParams();
            Window windows = dialogCargaActividad.getWindow();
            lps.copyFrom(windows.getAttributes());
            lps.width = WindowManager.LayoutParams.MATCH_PARENT;
            lps.height = WindowManager.LayoutParams.WRAP_CONTENT;
            windows.setAttributes(lps);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EZPhotoPick.PHOTO_PICK_CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
               image_uris = data.getStringArrayListExtra(EZPhotoPick.PICKED_PHOTO_NAMES_KEY);
            } else {
                Toast toast = Toast.makeText(getActivity(), "Ocurrio un error al cargar la imagen vuelve a tomar la foto", Toast.LENGTH_SHORT);
                TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                tv.setTextColor(Color.WHITE);
                toast.show();
            }
        }
    }

}
