package corp.grupoalpura.mobile.supervisiontiendas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;

public class SubMenuEspacioFragment extends Fragment {

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";

    Usuario usuario;
    ClienteSucursal sucursal;

    @BindView(R.id.text_espacio_base)
    TextView tvEspacioBase;
    @BindView(R.id.text_exhibicion_adicional)
    TextView tvExhibicionAdicional;

    @BindView(R.id.rl_espacios_base)
    RelativeLayout rlEspacioBase;
    @BindView(R.id.rl_exhibicion_adicional)
    RelativeLayout rlExhibicionAdicional;

    public static SubMenuEspacioFragment newInstance(ClienteSucursal sucursal, Usuario usuario) {
        SubMenuEspacioFragment fragment = new SubMenuEspacioFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Contenedor) getActivity()).activity = 3;
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submenu_espacios, container, false);
        ButterKnife.bind(this, view);

        rlEspacioBase.setOnClickListener(view1 -> {
            ((Contenedor) getActivity()).activity = 2;
            ((Contenedor) getActivity()).tvToolbar.setText(getResources().getString(R.string.menu_espacios));
            MenuEspaciosFragment espacios = new MenuEspaciosFragment().newInstance(sucursal, usuario, 1);
            replaceFragment(espacios);
        });


        rlExhibicionAdicional.setOnClickListener(view2 -> {
            ((Contenedor) getActivity()).activity = 2;
            ((Contenedor) getActivity()).tvToolbar.setText(getResources().getString(R.string.menu_espacios));
            MenuEspaciosFragment espacios = new MenuEspaciosFragment().newInstance(sucursal, usuario, 2);
            replaceFragment(espacios);
        });

        return view;
    }


    /**
     * Metodo para reemplazar el fragmento de la actividad
     *
     * @param fragment nuevo fragment a presentar
     */
    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

}
