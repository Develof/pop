package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 19/07/16.
 */
public class EspacioFrente {
    private int id_espacio_frente;
    private String nombre_compania;
    private double frente;
    private String agrupacion;
    private int id_sucursal_espacio;
    private int id_compania;
    private int id_agrupacion;
    private int id_presentacion;
    private int estatus_dos;

    public EspacioFrente(){}

    public EspacioFrente(int id_espacio_frente, String nombre_compania, int frente, String agrupacion, int estatus_dos) {
        this.id_espacio_frente = id_espacio_frente;
        this.nombre_compania = nombre_compania;
        this.frente = frente;
        this.agrupacion = agrupacion;
        this.estatus_dos = estatus_dos;
    }


    public EspacioFrente(int id_compania, int id_agrupacion, int id_presentacion, double frente) {
        this.id_compania = id_compania;
        this.id_agrupacion = id_agrupacion;
        this.id_presentacion = id_presentacion;
        this.frente = frente;
    }

    public int getId_espacio_frente() {
        return id_espacio_frente;
    }

    public void setId_espacio_frente(int id_espacio_frente) {
        this.id_espacio_frente = id_espacio_frente;
    }

    public String getNombre_compania() {
        return nombre_compania;
    }

    public void setNombre_compania(String nombre_compania) {
        this.nombre_compania = nombre_compania;
    }

    public double getFrente() {
        return frente;
    }

    public void setFrente(double frente) {
        this.frente = frente;
    }

    public String getAgrupacion() {
        return agrupacion;
    }

    public void setAgrupacion(String agrupacion) {
        this.agrupacion = agrupacion;
    }

    public int getId_sucursal_espacio() {
        return id_sucursal_espacio;
    }

    public void setId_sucursal_espacio(int id_sucursal_espacio) {
        this.id_sucursal_espacio = id_sucursal_espacio;
    }

    public int getId_compania() {
        return id_compania;
    }

    public void setId_compania(int id_compania) {
        this.id_compania = id_compania;
    }

    public int getId_agrupacion() {
        return id_agrupacion;
    }

    public void setId_agrupacion(int id_agrupacion) {
        this.id_agrupacion = id_agrupacion;
    }

    public int getId_presentacion() {
        return id_presentacion;
    }

    public void setId_presentacion(int id_presentacion) {
        this.id_presentacion = id_presentacion;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }
}
