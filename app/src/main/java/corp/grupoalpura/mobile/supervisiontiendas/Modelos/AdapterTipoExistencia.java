package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 27/07/16.
 */
public class AdapterTipoExistencia {
    int id_tipo_existencia;
    String nombre_existencia;

    AdapterTipoExistencia(){

    }

    public AdapterTipoExistencia(int id_tipo_existencia, String nombre_existencia) {
        this.id_tipo_existencia = id_tipo_existencia;
        this.nombre_existencia = nombre_existencia;
    }

    @Override
    public String toString() {
        return nombre_existencia;
    }

    public int getId_tipo_existencia() {
        return id_tipo_existencia;
    }

    public void setId_tipo_existencia(int id_tipo_existencia) {
        this.id_tipo_existencia = id_tipo_existencia;
    }

    public String getNombre_existencia() {
        return nombre_existencia;
    }

    public void setNombre_existencia(String nombre_existencia) {
        this.nombre_existencia = nombre_existencia;
    }
}
