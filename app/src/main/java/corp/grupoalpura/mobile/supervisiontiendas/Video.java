package corp.grupoalpura.mobile.supervisiontiendas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.jakelee.vidsta.VidstaPlayer;

public class Video extends AppCompatActivity {

    @BindView(R.id.player)
    VidstaPlayer video;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);

        video.setVideoSource("http://appmoviles.alpura.com:8080/Datos/video/video_memo.mp4");
        video.setAutoLoop(false);
        video.setAutoPlay(true);

    }
}
