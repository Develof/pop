package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.telephony.TelephonyManager;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.DispositivoBDModel;

import java.lang.reflect.Method;

/**
 * Created by Develof on 28/10/16.
 */

public class InformacionDispositivo {

    String noSerieDevice;
    NetworkInfo networkInfo;

    public DispositivoBDModel getInfromacion(Context context) {

        DispositivoBDModel dispositivoBDModel = new DispositivoBDModel();
        TelephonyManager telemamanger = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        String version = "";

        networkInfo = cm.getActiveNetworkInfo();

        noSerieDevice = Build.SERIAL;
        if (!noSerieDevice.equals("0000")) {
            try {
                Class<?> cq = Class.forName("android.os.SystemProperties");
                Method get = cq.getMethod("get", String.class, String.class);
                noSerieDevice = (String) get.invoke(cq, "ril.serialnumber", "unknown");
            } catch (Exception ignored) {
            }
        }

        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        dispositivoBDModel.setNoSerie(noSerieDevice);
        dispositivoBDModel.setImei(telemamanger.getDeviceId());
        dispositivoBDModel.setSim(telemamanger.getSimSerialNumber());
        dispositivoBDModel.setModelo(android.os.Build.MODEL);
        dispositivoBDModel.setAndroid(android.os.Build.VERSION.RELEASE);
        dispositivoBDModel.setVersion(version);

        return dispositivoBDModel;

    }

    /**
     * Obtiene el nivel de la bateria actual
     * @return nivel de bateria
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public  Integer getBatteryLevel(Context context){
        BatteryManager bm = (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        return batLevel;
    }



}
