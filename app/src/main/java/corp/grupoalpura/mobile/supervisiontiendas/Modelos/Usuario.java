package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;

/**
 * Created by consultor on 2/24/16.
 */

public class Usuario implements Serializable {
    private int id;
    private int udn;
    private int id_jefe;
    private int tipo_usuario;
    private String nombre;
    private String email;
    private String username;
    private String password;
    private String rol;
    private int cambio_password;
    private int cambio_localizacion;
    private String hash_reset;
    private int intentos;
    private String fecha_error;
    private int nivel;
    private String fecha_password;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;

    public Usuario() {
    }


    public Usuario(int id, int udn, int id_jefe, int tipo_usuario, String nombre, String email, String username, String password,
                   String rol, int cambio_password, String hash_reset, int cambio_localizacion, int intentos, String fecha_error,
                   int nivel, String fecha_password, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.id = id;
        this.udn = udn;
        this.id_jefe = id_jefe;
        this.tipo_usuario = tipo_usuario;
        this.nombre = nombre;
        this.email = email;
        this.username = username;
        this.password = password;
        this.rol = rol;
        this.cambio_password = cambio_password;
        this.hash_reset = hash_reset;
        this.cambio_localizacion = cambio_localizacion;
        this.intentos = intentos;
        this.fecha_error = fecha_error;
        this.nivel = nivel;
        this.fecha_password = fecha_password;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUdn() {
        return udn;
    }

    public void setUdn(int udn) {
        this.udn = udn;
    }

    public int getId_jefe() {
        return id_jefe;
    }

    public void setId_jefe(int id_jefe) {
        this.id_jefe = id_jefe;
    }

    public int getTipo_usuario() {
        return tipo_usuario;
    }

    public void setTipo_usuario(int tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public int getCambio_password() {
        return cambio_password;
    }

    public void setCambio_password(int cambio_password) {
        this.cambio_password = cambio_password;
    }

    public String getHash_reset() {
        return hash_reset;
    }

    public void setHash_reset(String hash_reset) {
        this.hash_reset = hash_reset;
    }

    public int getIntentos() {
        return intentos;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }

    public String getFecha_error() {
        return fecha_error;
    }

    public void setFecha_error(String fecha_error) {
        this.fecha_error = fecha_error;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getFecha_password() {
        return fecha_password;
    }

    public void setFecha_password(String fecha_password) {
        this.fecha_password = fecha_password;
    }

    public int getCambio_localizacion() {
        return cambio_localizacion;
    }

    public void setCambio_localizacion(int cambio_localizacion) {
        this.cambio_localizacion = cambio_localizacion;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }


}
