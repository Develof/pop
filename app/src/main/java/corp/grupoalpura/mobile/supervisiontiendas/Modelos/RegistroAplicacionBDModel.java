package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;

/**
 * Created by Develof on 30/10/16.
 */

public class RegistroAplicacionBDModel implements Serializable {

    public RegistroAplicacionBDModel() {
    }

    public RegistroAplicacionBDModel(int no_sucursal, int is_check_in, int is_sinc_inicial) {
        this.no_sucursal = no_sucursal;
        this.is_check_in = is_check_in;
        this.is_sinc_inicial = is_sinc_inicial;
    }

    private int no_sucursal;
    private int is_check_in;
    private int is_sinc_inicial;

    public int getNo_sucursal() {
        return no_sucursal;
    }

    public void setNo_sucursal(int no_sucursal) {
        this.no_sucursal = no_sucursal;
    }

    public int getIs_check_in() {
        return is_check_in;
    }

    public void setIs_check_in(int is_check_in) {
        this.is_check_in = is_check_in;
    }

    public int getIs_sinc_inicial() {
        return is_sinc_inicial;
    }

    public void setIs_sinc_inicial(int is_sinc_inicial) {
        this.is_sinc_inicial = is_sinc_inicial;
    }
}
