package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;

/**
 * Created by prestamo on 23/03/2016.
 */
public class MensajesSMS implements Serializable {
    private String id_mensaje;
    private String adress;
    private String mensaje;
    private String fecha;
    private int estado_leido;

    public MensajesSMS() {}

    public MensajesSMS(String id_mensaje, String adress, String mensaje, String fecha, int estado_leido) {
        this.id_mensaje = id_mensaje;
        this.adress = adress;
        this.mensaje = mensaje;
        this.fecha = fecha;
        this.estado_leido = estado_leido;
    }

    public String getId_mensaje() {
        return id_mensaje;
    }

    public void setId_mensaje(String id_mensaje) {
        this.id_mensaje = id_mensaje;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getEstado_leido() {
        return estado_leido;
    }

    public void setEstado_leido(int estado_leido) {
        this.estado_leido = estado_leido;
    }
}
