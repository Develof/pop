package corp.grupoalpura.mobile.supervisiontiendas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Espacios;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters.MenuEspaciosAdapter;

public class MenuEspaciosFragment extends Fragment {

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String ID_TIPO_ESPACIO = "ID_TIPO_ESPACIO";

    //Variable globales
    Usuario usuario;
    ClienteSucursal sucursal;
    DBHelper dbHelper;
    List<Espacios> espacios = new ArrayList<>();
    private int idTipoEspacio;
    RecyclerView.LayoutManager recyclerViewLayoutManager;

    @BindView(R.id.rv_espacios)
    RecyclerView rvEspacios;

    /**
     * @return A new instance of fragment FamiliasFragment.
     */
    public static MenuEspaciosFragment newInstance(ClienteSucursal sucursal, Usuario usuario, Integer idTipoEspacio) {
        MenuEspaciosFragment fragment = new MenuEspaciosFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(ID_TIPO_ESPACIO, idTipoEspacio);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Contenedor) getActivity()).activity = 2;
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            idTipoEspacio = (int) getArguments().getSerializable(ID_TIPO_ESPACIO);
        }

        ((Contenedor) getActivity()).getVentanaEspacio(idTipoEspacio);
        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        espacios = dbHelper.getEspaciosTipo(idTipoEspacio);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_menu_espacios_base, container, false);
        ButterKnife.bind(this, view);

        recyclerViewLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvEspacios.setLayoutManager(recyclerViewLayoutManager);

        MenuEspaciosAdapter recyclerView_Adapter = new MenuEspaciosAdapter(getActivity(), espacios, this);
        rvEspacios.setAdapter(recyclerView_Adapter);

        return view;

    }

    /**
     * Inicia fragment de producto pasando como parametro id de la Familia
     *
     * @param idEspacio Es el id de la Espacios seleccionada
     */
    public void startFragment(int idEspacio, String espacio) {
        ((Contenedor) getActivity()).activity = -1;
        ((Contenedor) getActivity()).tvToolbar.setText(espacio);
        EspaciosFragment espacios = new EspaciosFragment().newInstance(sucursal, usuario, idEspacio);
        replaceFragment(espacios);
    }

    /**
     * Metodo para reemplazar el fragmento de la actividad
     *
     * @param fragment nuevo fragment a presentar
     */
    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }


}
