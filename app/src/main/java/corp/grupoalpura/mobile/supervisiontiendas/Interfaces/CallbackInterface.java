package corp.grupoalpura.mobile.supervisiontiendas.Interfaces;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;

public interface CallbackInterface {
    void setProduct(FichaTecnicaProducto producto);
}
