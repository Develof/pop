package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 28/02/16.
 */
public class MenuFichaFecnica {

    //private String acciones[] = new String[]{"Datos Generales", "Alertas o Recordatorios", "Personal", "Presupuesto", "Guía de Mercadeo", "Manual de Usuario"};
    //private String caractAcciones[] = new String[]{"Dirección, Teléfonos, Horarios, Directorio, Días de Visita, etc", "Recordatorios de proximas visitas, alertas de caducidad, revisiones, etc", "Datos generales del personal en la tienda", "Datos generales sobre el presupuesto", "Información sobre la guia de mercadeo", "Revisar el manual de uso de la aplicación"}; //, "Contenido de Material POP",

    private String acciones[] = new String[]{"Datos Generales", "Alertas o Recordatorios", "Personal", "Manual de Usuario"};
    private String caractAcciones[] = new String[]{"Dirección, Teléfonos, Horarios, Directorio, Días de Visita, etc", "Recordatorios de proximas visitas, alertas de caducidad, revisiones, etc", "Datos generales del personal en la tienda", "Revisar el manual de uso de la aplicación"}; //, "Contenido de Material POP",

    public MenuFichaFecnica() {
    }

    public String[] getAcciones() {
        return acciones;
    }

    public String[] getCaractAcciones() {
        return caractAcciones;
    }

}
