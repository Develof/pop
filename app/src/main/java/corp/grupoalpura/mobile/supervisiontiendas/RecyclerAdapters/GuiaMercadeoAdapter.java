package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.GuiaMercadeoModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.CatGuiaMercadeoBDHelper;

public class GuiaMercadeoAdapter extends RecyclerView.Adapter<GuiaMercadeoViewHolder> {

    private Context ctx;
    private Usuario usuario;
    private List<GuiaMercadeoModel> listGuia;
    private LayoutInflater mInflater;

    private static final int GUIA_ACTIVA = 1;

    public GuiaMercadeoAdapter(Context context, List<GuiaMercadeoModel> listGuia, Usuario usuario) {
        mInflater = LayoutInflater.from(context);
        this.ctx = context;
        this.listGuia = listGuia;
        this.usuario = usuario;
    }

    @Override
    public GuiaMercadeoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = mInflater.inflate(R.layout.adapter_guia_mercadeo, parent, false);

        return new GuiaMercadeoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GuiaMercadeoViewHolder holder, int position) {
        final GuiaMercadeoModel guia = listGuia.get(position);

        holder.tvProducto.setText(guia.getProducto());
        holder.tvPromocion.setText(guia.getPromocion());
        holder.tvVigencia.setText(guia.getVigencia());
        holder.tvRegion.setText(guia.getRegion());

        if (guia.getEstauts() == GUIA_ACTIVA) {
            holder.cbGuiaMercadeo.setChecked(true);
        } else {
            holder.cbGuiaMercadeo.setChecked(false);
        }


        holder.cvGuiaMercadeo.setOnClickListener(v -> {
            final CatGuiaMercadeoBDHelper catguiaBDHelper = new CatGuiaMercadeoBDHelper(ctx);
            if (holder.cbGuiaMercadeo.isChecked()) {
                final Dialog dialogCancelacion = new Dialog(ctx);
                dialogCancelacion.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogCancelacion.setContentView(R.layout.dialog_motivo_actualizacion);

                final EditText edMotivo = dialogCancelacion.findViewById(R.id.editText_motivo_eliminacion);
                TextView tvCancelar = dialogCancelacion.findViewById(R.id.btn_cancelar);
                TextView tvAceptar = dialogCancelacion.findViewById(R.id.btn_actualizar);


                tvAceptar.setOnClickListener(v1 -> {
                    if (!edMotivo.getText().toString().isEmpty()) {
                        catguiaBDHelper.insertaFaltante(guia.getIdGuiaMercadeo(), guia.getIdCadena(), edMotivo.getText().toString(), 0, usuario.getUsername());
                        dialogCancelacion.dismiss();
                        holder.cbGuiaMercadeo.setChecked(false);
                    }
                });

                tvCancelar.setOnClickListener(v12 -> dialogCancelacion.dismiss());


                dialogCancelacion.show();

            } else {
                holder.cbGuiaMercadeo.setChecked(true);
                catguiaBDHelper.eliminaEstatusGuia(guia.getIdGuiaMercadeo());
            }
        });

    }

    @Override
    public int getItemCount() {
        return listGuia.size();
    }
}
