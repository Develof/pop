package corp.grupoalpura.mobile.supervisiontiendas;

import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProdFamilia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.FamiliasDBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.LoadImage;

public class FamiliasFragment extends Fragment {

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String VENTANA = "VENTANA";

    //Variable globales
    Usuario usuario;
    ClienteSucursal sucursal;

    private List<ProdFamilia> familias;
    private int ventana;
    String imageName = "";
    ImageView imagen;
    FamiliasDBHelper dbHelper;

    @BindView(R.id.familias_text_crema_ref)
    TextView tvCremaRef;
    @BindView(R.id.familias_text_evaporada)
    TextView tvEvaporada;
    @BindView(R.id.familias_text_past_sabor)
    TextView tvPastSaborizada;
    @BindView(R.id.familias_text_leche_past)
    TextView tvLechePast;
    @BindView(R.id.familias_text_leche_uht_sabor)
    TextView tvLecheUhtSabor;
    @BindView(R.id.familias_text_leche_blanca_uht)
    TextView tvLecheBlancaUht;
    @BindView(R.id.familias_text_yoghurt_batido)
    TextView tvYoghurtBatido;
    @BindView(R.id.familias_text_yoghurt_bebible)
    TextView tvYoghurtBebible;

    @BindView(R.id.familia_rl_crema_ref)
    RelativeLayout rlCremaRef;
    @BindView(R.id.familia_rl_evaporada)
    RelativeLayout rlEvaporada;
    @BindView(R.id.menu_rl_menu_past_sabor)
    RelativeLayout rlPastSabor;
    @BindView(R.id.familia_rl_leche_past)
    RelativeLayout rlLechePast;
    @BindView(R.id.familia_rl_leche_uht_sabor)
    RelativeLayout rlLecheUhtSabor;
    @BindView(R.id.familia_rl_leche_blanca_uht)
    RelativeLayout rlLecheBlancaUht;
    @BindView(R.id.familia_rl_yoghurt_batido)
    RelativeLayout rlYoghurtBatido;
    @BindView(R.id.familia_rl_yoghurt_bebible)
    RelativeLayout rlYoghurtBebible;

    @BindView(R.id.familias_iv_crema_refrigerada)
    ImageView ivCremaRef;
    @BindView(R.id.familias_iv_evaporada)
    ImageView ivEvaporada;
    @BindView(R.id.familias_iv_past_sabor)
    ImageView ivPastSabor;
    @BindView(R.id.familias_iv__leche_past)
    ImageView ivLechePast;
    @BindView(R.id.familias_iv_leche_uht_sabor)
    ImageView ivUhtSabor;
    @BindView(R.id.familias_iv_leche_blanca_uht)
    ImageView ivLecheBlanca;
    @BindView(R.id.familias_iv_yoghurt_batido)
    ImageView ivYoghurtBatido;
    @BindView(R.id.familias_iv_yoghurt_bebible)
    ImageView ivYoghurtBebible;

    /**
     * @return A new instance of fragment FamiliasFragment.
     */
    public static FamiliasFragment newInstance(ClienteSucursal sucursal, Usuario usuario, Integer ventana) {
        FamiliasFragment fragment = new FamiliasFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(VENTANA, ventana);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new FamiliasDBHelper(getActivity());
        familias = dbHelper.getFamilias();

        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            ventana = (Integer) getArguments().getSerializable(VENTANA);
        }

        if (ventana == 1) {
            ((Contenedor) getActivity()).activity = 3;
        } else if (ventana == 0) {
            ((Contenedor) getActivity()).activity = 3;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_familias, container, false);
        ButterKnife.bind(this, view);

        setFamilia();
        for (int i = 0; i < familias.size(); i++) {
            selectImage(i);
        }

        rlCremaRef.setOnClickListener(v -> startFragment(familias.get(0).getId()));

        rlEvaporada.setOnClickListener(v -> startFragment(familias.get(1).getId()));

        rlPastSabor.setOnClickListener(v -> startFragment(familias.get(2).getId()));

        rlLechePast.setOnClickListener(v -> startFragment(familias.get(3).getId()));

        rlLecheUhtSabor.setOnClickListener(v -> startFragment(familias.get(4).getId()));

        rlLecheBlancaUht.setOnClickListener(v -> startFragment(familias.get(5).getId()));

        rlYoghurtBatido.setOnClickListener(v -> startFragment(familias.get(6).getId()));

        rlYoghurtBebible.setOnClickListener(v -> startFragment(familias.get(7).getId()));

        return view;

    }

    private void setFamilia() {
        if (familias != null) {
            tvCremaRef.setText(familias.get(0).getFamilia());
            tvEvaporada.setText(familias.get(1).getFamilia());
            tvPastSaborizada.setText(familias.get(2).getFamilia());
            tvLechePast.setText(familias.get(3).getFamilia());
            tvLecheUhtSabor.setText(familias.get(4).getFamilia());
            tvLecheBlancaUht.setText(familias.get(5).getFamilia());
            tvYoghurtBatido.setText(familias.get(6).getFamilia());
            tvYoghurtBebible.setText(familias.get(7).getFamilia());
        }
    }

    private void selectImage(int posicion) {
        switch (posicion) {

            case 0:
                imageName = "cr16";
                imagen = ivCremaRef;
                break;
            case 1:
                imageName = "ler1";
                imagen = ivEvaporada;
                break;
            case 2:
                imageName = "lc01";
                imagen = ivPastSabor;
                break;
            case 3:
                imageName = "le96";
                imagen = ivLechePast;
                break;
            case 4:
                imageName = "ls70";
                imagen = ivUhtSabor;
                break;
            case 5:
                imageName = "ls50";
                imagen = ivLecheBlanca;
                break;
            case 6:
                imageName = "yo10";
                imagen = ivYoghurtBatido;
                break;
            case 7:
                imageName = "yf47";
                imagen = ivYoghurtBebible;
                break;
        }

        String imageResource = Environment.getExternalStoragePublicDirectory(getActivity().getResources().getString(R.string.app_img_productos)) + File.separator + imageName + ".png";
        setImageFamilia(imageResource, imagen);
    }

    /**
     * Colocamos la imagen de la familia dependiendo del tipo de familia
     *
     * @param imageFamilia Ruta donde se encuentra la imagen
     * @param view         ImageView donde se cargara la imagen
     */
    private void setImageFamilia(String imageFamilia, ImageView view) {
        LoadImage loadImage = new LoadImage();
        loadImage.load(getActivity(), imageFamilia, view);
    }

    /**
     * Inicia fragment de producto pasando como parametro id de la Familia
     *
     * @param idFamilia Es el id de la familia seleccionada
     */
    private void startFragment(int idFamilia) {
        if (ventana == 1) {
            ((Contenedor) getActivity()).activity = 5;
            ProductosCatalogadosFragment producto_cat = new ProductosCatalogadosFragment().newInstance(sucursal, usuario, 1, idFamilia);
            replaceFragment(producto_cat);
            ((Contenedor) getActivity()).tvToolbar.setText(getResources().getString(R.string.menu_productos));
        } else {
            ((Contenedor) getActivity()).activity = -1;
            ((Contenedor) getActivity()).tvToolbar.setText(getResources().getString(R.string.menu_espacios));
            MenuEspaciosFragment espacios = new MenuEspaciosFragment().newInstance(sucursal, usuario, idFamilia);
            replaceFragment(espacios);
        }
    }

    //REMPLAZA EL FRAGMENT
    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

}
