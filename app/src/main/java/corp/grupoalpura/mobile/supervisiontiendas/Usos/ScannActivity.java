package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.Result;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DetalleProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.ProductoBDHelper;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String VENTANA = "ventana";

    private static final int VENTANA_PRODUCTO = 1;
    private static final int VENTANA_ESPACIO = 2;

    private ZXingScannerView mScannerView;
    private ClienteSucursal cs;
    Usuario usuario;
    ProductoBDHelper dbHelper;
    int ventana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        usuario = (Usuario) getIntent().getSerializableExtra(ID_USUARIO);
        cs = (ClienteSucursal) getIntent().getSerializableExtra(ID_SUCURSAL);
        ventana = getIntent().getIntExtra(VENTANA, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mScannerView != null) mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {

        dbHelper = new ProductoBDHelper(ScannActivity.this);
        FichaTecnicaProducto producto = dbHelper.getProductoUPC(result.getText(), cs.getId());
        if (producto.getId() != 0) {

            if (ventana == VENTANA_PRODUCTO) {
                Intent detalle_producto = new Intent(ScannActivity.this, DetalleProducto.class);
                detalle_producto.putExtra(ID_SUCURSAL, cs);
                detalle_producto.putExtra(ID_USUARIO, usuario);
                detalle_producto.putExtra("producto", producto);
                detalle_producto.putExtra("ventana", Constantes.ACTIVIDAD_HOME.PRODUCTO_CATALOGADO);
                startActivity(detalle_producto);
                this.finish();
            } else if (ventana == VENTANA_ESPACIO) {
                Intent output = new Intent();
                output.putExtra("PRODUCTO", producto);
                setResult(1, output);
                this.finish();
            }
        } else {
            if (ventana == VENTANA_ESPACIO) {
                FichaTecnicaProducto prodCom = new FichaTecnicaProducto();
                prodCom.setUPC(result.getText());
                prodCom.setId(-1);
                prodCom.setId_compania(-1);
                prodCom.setIdPresentacion(5);
                prodCom.setClave(result.getText());
                prodCom.setUPC(result.getText());

                Intent output = new Intent();
                output.putExtra("PRODUCTO", prodCom);
                setResult(1, output);
                this.finish();

            }
            Toast.makeText(this, "Falló, centra la cámara al codigo de barras", Toast.LENGTH_LONG).show();
            onBackPressed();
        }
    }

}
