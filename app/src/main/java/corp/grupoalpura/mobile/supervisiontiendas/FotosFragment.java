package corp.grupoalpura.mobile.supervisiontiendas;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FotosEvidenciaRelacion;
import corp.grupoalpura.mobile.supervisiontiendas.view_pager.FragmentAdapter;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FotosFragment extends Fragment implements LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    ClienteSucursal sucursal;
    int id_table;
    DBHelper dbHelper;
    int id_foto;
    String dir, dirNombre, table_name, first_name, table_main;
    Uri outputFileUri;
    GoogleApiClient googleApiClient;
    double latitud = 0, longitud = 0;
    protected LocationRequest mLocationRequest;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 5;

    private static final String TABLE_MAIN = "table_main";
    private static final String ID_TABLE = "id_table";
    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String TABLE_NAME = "table_name";
    private static final String FIRST_NAME = "first_name";

    public FotosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FotosFragment.
     */
    public static FotosFragment newInstance(int id, ClienteSucursal sucursal, String table_name, String first_name, String table_main) {
        FotosFragment fragment = new FotosFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putInt(ID_TABLE, id);
        args.putString(TABLE_NAME, table_name);
        args.putString(FIRST_NAME, first_name);
        args.putString(TABLE_MAIN, table_main);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) outputFileUri = savedInstanceState.getParcelable("outputFileUri");
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            id_table = getArguments().getInt(ID_TABLE);
            table_name = getArguments().getString(TABLE_NAME);
            first_name = getArguments().getString(FIRST_NAME);
            table_main = getArguments().getString(TABLE_MAIN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fotos, container, false);

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null) {
            googleApiClient.connect();
        } else {
            LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 5, 0, this);
            } else {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 5, 0, this);
            }
        }

        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        final ArrayList<FotosEvidenciaRelacion> fotos;
        fotos = dbHelper.getFotos(table_main, id_table);

        CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        final ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.menu_toolbar);

        if (fotos.size() > 0) {
            pager.setAdapter(new FragmentAdapter(getChildFragmentManager(), fotos));
            indicator.setViewPager(pager);
        } else {
            Toast toast = Toast.makeText(getActivity(), "No se ha encontrado fotos", Toast.LENGTH_SHORT);
            TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
            tv.setTextColor(Color.WHITE);
            toast.show();
            toolbar.setVisibility(View.GONE);
        }


        toolbar.inflateMenu(R.menu.menu_fotos);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final int id = item.getItemId();
                switch (id) {
                    case R.id.alerta_editar:
                        id_foto = fotos.get(pager.getCurrentItem()).getId_foto();
                        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
                        dir = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_foto)) + "/ " + table_name + "/";
                        dirNombre = FIRST_NAME + timeStamp + ".jpg";
                        File newdir = new File(dir);
                        newdir.mkdirs();
                        String file = dir + dirNombre;
                        File newfile = new File(file);
                        try {
                            newfile.createNewFile();
                        } catch (IOException e) {
                        }

                        outputFileUri = Uri.fromFile(newfile);
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(takePictureIntent, Constantes.FOTOS.TAKE_PHOTO_CODE_FRAGMENT);
                        }
                        break;
                    case R.id.alerta_eliminar:
                        String myPath = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_foto)) + "/ " + table_name + "/" + fotos.get(pager.getCurrentItem()).getFile_name();
                        File f = new File(myPath);
                        f.delete();

                        dbHelper.deleteFoto(fotos.get(pager.getCurrentItem()).getId_foto());

                        getFragmentManager().beginTransaction().replace(R.id.container, new FotosFragment().newInstance(id_table, sucursal, table_name, first_name, table_main)).commit();


                        break;
                }
                return true;
            }

        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constantes.FOTOS.TAKE_PHOTO_CODE_FRAGMENT && resultCode == Activity.RESULT_OK) {
            if (outputFileUri != null && outputFileUri.toString() != "") {

                String archivo = dbHelper.getArchivo(id_foto);
                if (archivo != null) {
                    String myPath = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_foto)) + "/ " + table_name + "/" + archivo;
                    File f = new File(myPath);
                    f.delete();
                }

                dbHelper.updateimage(id_foto, outputFileUri.toString(), dirNombre, latitud, longitud);
                Toast toast = Toast.makeText(getActivity(), "Has cambiado la imagen", Toast.LENGTH_SHORT);
                TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                tv.setTextColor(Color.WHITE);
                toast.show();

                getFragmentManager().beginTransaction().replace(R.id.container, new FotosFragment().newInstance(id_table, sucursal, table_name, first_name, table_main)).commit();

            }
        } else if (resultCode == getActivity().RESULT_CANCELED) {
            outputFileUri = null;
            Toast toast = Toast.makeText(getActivity(), "Has cancelado la foto", Toast.LENGTH_SHORT);
            TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
            tv.setTextColor(Color.WHITE);
            toast.show();
        } else {
            outputFileUri = null;
            Toast toast = Toast.makeText(getActivity(), "Ocurrio un error al cargar la imagen vuelve a tomar la foto", Toast.LENGTH_SHORT);
            TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
            tv.setTextColor(Color.WHITE);
            toast.show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(getActivity() != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {

                latitud = location.getLatitude();
                longitud = location.getLongitude();

            } else {
                mLocationRequest = new LocationRequest();

                mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

                mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                PendingResult<Status> locationUpdates = LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        latitud = location.getLatitude();
                        longitud = location.getLongitude();

                    }
                });
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        latitud = location.getLatitude();
        longitud = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }


}
