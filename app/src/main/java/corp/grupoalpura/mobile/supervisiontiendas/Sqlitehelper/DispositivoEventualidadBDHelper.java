package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;

/**
 * Created by camovilesb on 10/4/2017.
 */

public class DispositivoEventualidadBDHelper extends DBHelper {

    public DispositivoEventualidadBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public Boolean insertarDispositivoEventualidad(int idDispositivo, int idUsuario, String eventualidad, String bateria, int estatusDos, String username) {
        Boolean dispositivodRegistrado = false;

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues registro = new ContentValues();
        registro.put(COLUMNA_DISPOSIVITO_ID_DISPOSITIVO, idDispositivo);
        registro.put(COLUMNA_DISPOSIVITO_EVENTUALIDAD_ID_USUARIO, idUsuario);
        registro.put(COLUMNA_DISPOSIVITO_MODULO_EVENTUALIDAD, eventualidad);
        registro.put(COLUMNA_DISPOSIVITO_BATERIA, bateria);
        registro.put(COLUMNA_DISPOSIVITO_EVENTUALIDAD_FECHA_CREACION, getDateTime());
        registro.put(COLUMNA_DISPOSIVITO_EVENTUALIDAD_ULTIMA_MODIFICACION, getDateTime());
        registro.put(COLUMNA_DISPOSIVITO_EVENTUALIDAD_USUARIO, username);
        registro.put(COLUMNA_DISPOSIVITO_EVENTUALIDAD_ESTATUS, 1);
        registro.put(COLUMNA_ESTATUS_DOS, estatusDos);
        long registroInsertado = database.insert(TABLA_DISPOSITIVO_EVENTUALIDAD, null, registro);
        database.close();

        if (registroInsertado <= 0)
            dispositivodRegistrado = false;
        else
            dispositivodRegistrado = true;

        return dispositivodRegistrado;
    }


}
