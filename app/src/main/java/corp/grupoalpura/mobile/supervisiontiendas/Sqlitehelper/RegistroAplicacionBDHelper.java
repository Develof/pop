package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.RegistroAplicacionBDModel;

/**
 * Created by Develof on 30/10/16.
 */

public class RegistroAplicacionBDHelper extends DBHelper {

    public RegistroAplicacionBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }


    /**
     * Inserta un registro de aplicacion a la BD
     *
     * @param no_sucursal   El numero actual de la sucursal en la que estamos
     * @param isCheckIn     si hay un check in activo
     * @param isSincInicial si ya se hiso la sinc Inicial
     * @return isRegistred si se registro el campo correctamente
     */
    public boolean insertaRegistroAplicacion(int no_sucursal, int isCheckIn, int isSincInicial) {
        boolean isRegistred;

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put(COLUMNA_REGISTRO_APLICACION_NO_SUCURSAL, no_sucursal);
        registro.put(COLUMNA_REGISTRO_APLICACION_CHECK_IN_ACTIVO, isCheckIn);
        registro.put(COLUMNA_REGISTRO_APLICACION_SINC_INICIAL_ACTIVO, isSincInicial);
        long registroInsertado = database.insert(TABLA_REGISTRO_APLICACION, null, registro);
        database.close();

        if (registroInsertado <= 0)
            isRegistred = false;
        else
            isRegistred = true;

        return isRegistred;
    }

    /**
     *
     */
    public RegistroAplicacionBDModel getRegistroAplicacion(){
        RegistroAplicacionBDModel registroAplicacion = new RegistroAplicacionBDModel();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT NO_SUCURSAL, CHECK_IN_ACTIVO, SINC_INICIAL_ACTIVO FROM " + TABLA_REGISTRO_APLICACION, null);

        if (cursor.moveToFirst()) {
            registroAplicacion.setNo_sucursal(Integer.parseInt(cursor.getString(0)));
            registroAplicacion.setIs_check_in(Integer.parseInt(cursor.getString(1)));
            registroAplicacion.setIs_sinc_inicial(Integer.parseInt(cursor.getString(2)));
        }

        cursor.close();
        db.close();
        return registroAplicacion;
    }

    public void updateNoSucursal(int noSucursal){
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_REGISTRO_APLICACION + " SET " + COLUMNA_REGISTRO_APLICACION_NO_SUCURSAL + " = '" + noSucursal + "'";
        database.execSQL(strSQL);
        database.close();
    }

    public void updateCheckIn(int isActive){
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_REGISTRO_APLICACION + " SET " + COLUMNA_REGISTRO_APLICACION_CHECK_IN_ACTIVO + " = '" + isActive + "'";
        database.execSQL(strSQL);
        database.close();
    }

    public void updateSincInicial(int isActive){
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_REGISTRO_APLICACION + " SET " + COLUMNA_REGISTRO_APLICACION_SINC_INICIAL_ACTIVO + " = '" + isActive + "'";
        database.execSQL(strSQL);
        database.close();
    }

}
