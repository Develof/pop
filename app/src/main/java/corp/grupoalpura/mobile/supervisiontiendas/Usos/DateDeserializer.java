package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

public class DateDeserializer implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        String date = element.getAsString();
        date = date.replace("\"", "");

        Dates dates = new Dates();

        Date fecha = dates.stringToDate(date);
        return fecha;

        }
    }
