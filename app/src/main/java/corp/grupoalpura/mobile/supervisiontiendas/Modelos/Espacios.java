package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 09/11/16.
 */

public class Espacios {

    private int idEspacio;
    private String espacio;


    public Espacios() {
    }

    public Espacios(int idEspacio, String espacio) {
        this.idEspacio = idEspacio;
        this.espacio = espacio;
    }

    @Override
    public String toString() {
        return espacio;
    }

    public int getIdEspacio() {
        return idEspacio;
    }

    public void setIdEspacio(int idEspacio) {
        this.idEspacio = idEspacio;
    }

    public String getEspacio() {
        return espacio;
    }

    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }
}
