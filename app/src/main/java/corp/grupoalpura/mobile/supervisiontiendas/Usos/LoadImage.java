package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import corp.grupoalpura.mobile.supervisiontiendas.R;

public class LoadImage {

    private RequestOptions options = new RequestOptions().error(R.mipmap.ic_app);

    public void load(Context ctx, int imageResource, ImageView imageView){
        Glide.with(ctx).load(imageResource).apply(options).into(imageView);
    }


    public void load(Context ctx, String imageResource, ImageView imageView){
        Glide.with(ctx).load(imageResource).apply(options).into(imageView);
    }

}