package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Telefonos;

import java.util.ArrayList;

/**
 * Created by prestamo on 11/03/2016.
 */
public class AdapterPromotorInformacion {
    private String sucursal_alias;
    private String nombre_promotor;
    private String apPaterno_promotor;
    private String apMaterno_promotor;
    private String email;
    private String fecha_ingreso;
    private ArrayList<Telefonos> telefono;
    private String supervisor;

    public AdapterPromotorInformacion(){};

    public AdapterPromotorInformacion(String sucursal_alias, String promotor, String apPaterno, String apMaterno, String email, String fecha_ingreso, String supervisor) {
        this.sucursal_alias = sucursal_alias;
        this.nombre_promotor = promotor;
        this.apPaterno_promotor = apPaterno;
        this.apMaterno_promotor = apMaterno;
        this.email = email;
        this.fecha_ingreso = fecha_ingreso;
        this.supervisor = supervisor;
    }

    public AdapterPromotorInformacion(ArrayList<Telefonos> telefonos){
        this.telefono = telefonos;
    }

    public String getSucursal_alias() {
        return sucursal_alias;
    }

    public void setSucursal_alias(String sucursal_alias) {
        this.sucursal_alias = sucursal_alias;
    }

    public String getNombre_promotor() {
        return nombre_promotor;
    }

    public void setNombre_promotor(String nombre_promotor) {
        this.nombre_promotor = nombre_promotor;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getApPaterno_promotor() {
        return apPaterno_promotor;
    }

    public void setApPaterno_promotor(String apPaterno_promotor) {
        this.apPaterno_promotor = apPaterno_promotor;
    }

    public String getApMaterno_promotor() {
        return apMaterno_promotor;
    }

    public void setApMaterno_promotor(String apMaterno_promotor) {
        this.apMaterno_promotor = apMaterno_promotor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Telefonos> getTelefono() {
        return telefono;
    }

    public void setTelefono(ArrayList<Telefonos> telefono) {
        this.telefono = telefono;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }
}
