package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.R;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class GuiaMercadeoViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textview_region)
    TextView tvRegion;
    @BindView(R.id.textview_producto)
    TextView tvProducto;
    @BindView(R.id.textview_promocion)
    TextView tvPromocion;
    @BindView(R.id.textview_fecha_vigencia)
    TextView tvVigencia;
    @BindView(R.id.cb_guia_mercadeo)
    CheckBox cbGuiaMercadeo;
    @BindView(R.id.card_view_guia_mercadeo)
    CardView cvGuiaMercadeo;


    public GuiaMercadeoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
