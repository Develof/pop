package corp.grupoalpura.mobile.supervisiontiendas.Interactor;


import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.EndPoints;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ImageRequest;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ViewFactInventory;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface WSInteractor {

    @GET(EndPoints.METODO_INVENTARIO_TIENDA)
    Call<ViewFactInventory> getInventoryRest(@Query("p_cadena") String cadena, @Query("p_fecha") String fecha,  @Query("p_store_number") String store_number);


    @POST(EndPoints.METODO_NOMBRE_PRODUCTOS)
    Call<List<String>> getImagesName();


    @POST(EndPoints.METODO_BASE64_PRODUCTOS)
    Call<String> getBase64Image(@Body ImageRequest imageRequest);

}
