package corp.grupoalpura.mobile.supervisiontiendas.Broadcast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;


import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Dashboard;
import corp.grupoalpura.mobile.supervisiontiendas.MensajesPush;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.MensajesSMS;
import corp.grupoalpura.mobile.supervisiontiendas.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by prestamo on 23/03/2016.
 */
public class SMSReceiver extends BroadcastReceiver {
    NotificationManager notificationManager;
    Notification notification;
    ArrayList<String> mensajes = new ArrayList<>();
    ArrayList<String> numeros = new ArrayList<>();
    ArrayList<Integer> status = new ArrayList<>();
    ArrayList<Long> dates = new ArrayList<>();
    ArrayList<MensajesSMS> mensajesSMSes = new ArrayList<>();
    int read, estado_leido;

    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    numeros.add(phoneNumber);
                    mensajes.add(currentMessage.getDisplayMessageBody());
                    status.add(0);
                    dates.add(currentMessage.getTimestampMillis());
                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);
        }

        for (int i = 0; i < numeros.size(); i++) {
            ContentValues values = new ContentValues();
            values.put("address", numeros.get(i).toString());
            values.put("body", mensajes.get(i).toString());
            values.put("read", 0);
            values.put("date_sent", dates.get(i));
            context.getContentResolver().insert(Uri.parse("content://sms/inbox"), values);
        }

        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if (cursor.moveToFirst())
            do {

                read = Integer.parseInt(cursor.getString(cursor.getColumnIndex("read")));
                if (read == Constantes.MENSAJES.NO_LEIDO) {
                    estado_leido = 0;
                } else {
                    estado_leido = 1;
                }

                MensajesSMS sms = new MensajesSMS(cursor.getString(cursor.getColumnIndex("_id")), cursor.getString(cursor.getColumnIndex("address")), cursor.getString(cursor.getColumnIndex("body")), convertEpochToDate(Long.parseLong(cursor.getString(cursor.getColumnIndex("date_sent")))), estado_leido);
                mensajesSMSes.add(sms);

            } while (cursor.moveToNext());

        for (int n = 0; n < numeros.size(); n++) {
            if (mensajesSMSes.size() > 0) {
                notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                Intent notificationIntent = new Intent(context, MensajesPush.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pending = PendingIntent.getActivity(context, 0, notificationIntent, 0);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                notification = builder.setContentIntent(pending)
                        .setSmallIcon(R.mipmap.ic_app).setTicker("").setWhen(System.currentTimeMillis())
                        .setAutoCancel(false)
                        .setContentTitle("Supervisión Tiendas")
                        .setContentText(mensajes.get(n).toString())
                        .setVibrate(new long[]{5000, 5000, 5000, 5000, 5000})
                        .build();

                notification.defaults |= Notification.DEFAULT_SOUND;
                notificationManager.notify(0, notification);

            }
        }

    }


    private String convertEpochToDate(long epoch) {
        Date date = new Date(epoch);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(date);
        return dateString;
    }
}
