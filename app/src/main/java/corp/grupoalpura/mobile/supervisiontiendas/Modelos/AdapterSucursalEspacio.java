package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Develof on 21/03/17.
 */

public class AdapterSucursalEspacio implements Serializable {

    private int idSucursalEspacio;
    private ArrayList<Compania> listaCompania;
    private ArrayList<AdapterEspacios> listaEspacios;
    private String sucursal;
    private String departamento;
    private String espacio;
    private int tipo_adquisicon;
    private int nivel;

    public AdapterSucursalEspacio(){}

    public int getIdSucursalEspacio() {
        return idSucursalEspacio;
    }

    public void setIdSucursalEspacio(int idSucursalEspacio) {
        this.idSucursalEspacio = idSucursalEspacio;
    }

    public ArrayList<Compania> getListaCompania() {
        return listaCompania;
    }

    public void setListaCompania(ArrayList<Compania> listaCompania) {
        this.listaCompania = listaCompania;
    }

    public ArrayList<AdapterEspacios> getListaEspacios() {
        return listaEspacios;
    }

    public void setListaEspacios(ArrayList<AdapterEspacios> listaEspacios) {
        this.listaEspacios = listaEspacios;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getEspacio() {
        return espacio;
    }

    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }

    public int getTipo_adquisicon() {
        return tipo_adquisicon;
    }

    public void setTipo_adquisicon(int tipo_adquisicon) {
        this.tipo_adquisicon = tipo_adquisicon;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

}
