package corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Compania;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProductoPrecio;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.ProductoPreciosBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.LoadImage;


import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProductoPrecioCompetencia extends Fragment {

    DBHelper dbHelper;
    ProductoPreciosBDHelper ppHelper;
    ClienteSucursal sucursal;
    Usuario usuario;
    FichaTecnicaProducto producto;
    static CustomAdapter adapterCompania;
    double precio_real;
    int posicionCompania = 0;
    ArrayList<Compania> companias = new ArrayList<>();
    String nombreCompania = "";
    boolean isCompaniaOtros = false;

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String PRODUCTO = "producto";
    DecimalFormat form = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
    List<ProductoPrecio> precioProductos = new ArrayList<>();
    CustomAdapterProducto adapterProductos;
    ArrayList<FichaTecnicaProducto> productoList;


    @BindView(R.id.listView_precios_competencia)
    ListView listaCompanias;
    @BindView(R.id.fab)
    FloatingActionButton fab;


    public static ProductoPrecioCompetencia newInstance(ClienteSucursal sucursal, Usuario usuario, FichaTecnicaProducto producto) {
        ProductoPrecioCompetencia fragment = new ProductoPrecioCompetencia();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(PRODUCTO, producto);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        ppHelper = new ProductoPreciosBDHelper(getContext());
        form.applyPattern(Constantes.FORMATO_PRECIO);

        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            producto = (FichaTecnicaProducto) getArguments().getSerializable(PRODUCTO);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_producto_precio_competencia, container, false);
        ButterKnife.bind(this, view);

        precio_real = dbHelper.getPrecioUnitarioReal(producto.getClave());
        precioProductos = dbHelper.getAllPrecios(sucursal.getId(), producto.getId());
        companias = dbHelper.getCompanias();

        adapterCompania = new CustomAdapter(getActivity());
        listaCompanias.setAdapter(adapterCompania);


        fab.setOnClickListener(view1 -> {

            final Dialog dialogUpadatePrecio = new Dialog(getActivity());
            dialogUpadatePrecio.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogUpadatePrecio.setContentView(R.layout.dialog_producto_precio);

            ArrayAdapter<Compania> dataAdapterCompanias = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, companias);
            final Spinner spCompania = (Spinner) dialogUpadatePrecio.findViewById(R.id.spinner_compania_precio);
            spCompania.setAdapter(dataAdapterCompanias);

            dataAdapterCompanias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            final EditText edPrecio = (EditText) dialogUpadatePrecio.findViewById(R.id.edittext_precio_producto);
            final EditText edPrecioPromocion = (EditText) dialogUpadatePrecio.findViewById(R.id.edittext_precio_promocion);
            final CheckBox cbAplicaPrecio = (CheckBox) dialogUpadatePrecio.findViewById(R.id.checkBox_aplica_precio);
            Button btnGuardaPrecio = (Button) dialogUpadatePrecio.findViewById(R.id.btn_guardar_precio);
            Button btnCerrarPrecio = (Button) dialogUpadatePrecio.findViewById(R.id.btn_cerrar_precios);
            final TextView textHabilitaPrecio = (TextView) dialogUpadatePrecio.findViewById(R.id.textView_habilitar_precio);
            final TextView textCompniaOtros = (TextView) dialogUpadatePrecio.findViewById(R.id.textView_title_compania_otros);
            final EditText edCompaniaOtros = (EditText) dialogUpadatePrecio.findViewById(R.id.edittext_descripcion_marca);
            final SwitchCompat scHabilitaPrecio = (SwitchCompat) dialogUpadatePrecio.findViewById(R.id.switch_habilita_precios);
            final ListView lvProductos = (ListView) dialogUpadatePrecio.findViewById(R.id.listView_precio_producto);

            int idFamilia = dbHelper.getFamiliaPorProducto(producto.getId());
            int idPresentacion = dbHelper.getPresentacionPorProducto(producto.getId());
            productoList = dbHelper.getAllProductoPorPrecio(sucursal.getId(), idFamilia, idPresentacion);
            adapterProductos = new CustomAdapterProducto(getActivity());

            String compania = dbHelper.getCompania(producto.getId_compania());
            int index = 0;
            int indexarreglo;
            for (int i = 0; i < companias.size(); i++) {
                indexarreglo = companias.get(i).getNombre().indexOf(compania);
                if (indexarreglo != -1) {
                    index = i;
                }
            }

            if (producto.getId_compania() != 8) {
                spCompania.setSelection(index);
            }

            textCompniaOtros.setVisibility(View.GONE);
            edCompaniaOtros.setVisibility(View.GONE);

            lvProductos.setAdapter(adapterProductos);
            lvProductos.setVisibility(View.GONE);
            scHabilitaPrecio.setChecked(true);
            textHabilitaPrecio.setVisibility(View.GONE);
            scHabilitaPrecio.setVisibility(View.GONE);

            if (edPrecio.getText().toString().equals("")) {
                edPrecioPromocion.setEnabled(false);
            } else {
                edPrecioPromocion.setEnabled(true);
            }

            scHabilitaPrecio.setOnClickListener(v -> {
                if (scHabilitaPrecio.isChecked()) {
                    for (int i = 0; i < productoList.size(); i++) {
                        productoList.get(i).setIsPrieceActive(Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_ACTIVO);
                    }
                    adapterProductos.notifyDataSetChanged();
                } else {
                    for (int i = 0; i < productoList.size(); i++) {
                        productoList.get(i).setIsPrieceActive(Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_INACTIVO);
                    }
                    adapterProductos.notifyDataSetChanged();
                }
            });

            scHabilitaPrecio.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (scHabilitaPrecio.isChecked()) {
                    for (int i = 0; i < productoList.size(); i++) {
                        productoList.get(i).setIsPrieceActive(Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_ACTIVO);
                    }
                    adapterProductos.notifyDataSetChanged();
                } else {
                    for (int i = 0; i < productoList.size(); i++) {
                        productoList.get(i).setIsPrieceActive(Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_INACTIVO);
                    }
                    adapterProductos.notifyDataSetChanged();
                }
            });


            edPrecio.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (edPrecio.getText().toString().equals("")) {
                        edPrecioPromocion.setEnabled(false);
                        edPrecioPromocion.setText("");
                    } else {
                        edPrecioPromocion.setEnabled(true);
                    }
                }
            });

            cbAplicaPrecio.setOnClickListener(v -> {
                if (cbAplicaPrecio.isChecked()) {
                    lvProductos.setVisibility(View.VISIBLE);
                    textHabilitaPrecio.setVisibility(View.VISIBLE);
                    scHabilitaPrecio.setVisibility(View.VISIBLE);
                } else {
                    lvProductos.setVisibility(View.GONE);
                    textHabilitaPrecio.setVisibility(View.GONE);
                    scHabilitaPrecio.setVisibility(View.GONE);
                }
            });

            spCompania.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view1, int position, long id) {
                    posicionCompania = companias.get(position).getId();

                    TextView textView = (TextView) spCompania.getSelectedView();
                    nombreCompania = textView.getText().toString();

                    if (nombreCompania.toString().equals("OTROS")) {
                        textCompniaOtros.setVisibility(View.VISIBLE);
                        edCompaniaOtros.setVisibility(View.VISIBLE);
                        isCompaniaOtros = true;
                    } else {
                        textCompniaOtros.setVisibility(View.GONE);
                        edCompaniaOtros.setVisibility(View.GONE);
                        isCompaniaOtros = false;
                    }


                    adapterProductos.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            lvProductos.setOnItemClickListener((parent, view11, position, id) -> {
                CheckBox cbProductoPrecio = (CheckBox) view11.findViewById(R.id.cb_guia_mercadeo);

                if (cbProductoPrecio != null) {
                    if (cbProductoPrecio.isChecked()) {
                        cbProductoPrecio.setChecked(false);
                        productoList.get(position).setIsPrieceActive(Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_INACTIVO);
                    } else {
                        cbProductoPrecio.setChecked(true);
                        productoList.get(position).setIsPrieceActive(Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_ACTIVO);
                    }
                }
            });


            btnCerrarPrecio.setOnClickListener(v -> {
                precioProductos = dbHelper.getAllPrecios(sucursal.getId(), producto.getId());
                adapterCompania.notifyDataSetChanged();
                dialogUpadatePrecio.dismiss();
            });

            btnGuardaPrecio.setOnClickListener(v -> {
                if (!cbAplicaPrecio.isChecked()) {

                    if (isCompaniaOtros && edCompaniaOtros.getText().toString().equals("")) {
                        Toast toast = Toast.makeText(getActivity(), "Especifica la compañia", Toast.LENGTH_LONG);
                        TextView tv = toast.getView().findViewById(android.R.id.message);
                        tv.setTextColor(Color.WHITE);
                        toast.show();
                        return;
                    }

                    if (edPrecio.getText().toString().equals("")) {
                        Toast toast = Toast.makeText(getActivity(), "El precio no puede estar vacio", Toast.LENGTH_LONG);
                        TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                        tv.setTextColor(Color.WHITE);
                        toast.show();

                    } else {
                        /** IRA UN SELECT AL PRODUCTO COMPAÑIA PARA REVISAR SI EL PRODUCTO CON LA COMPAÑIA ASIGNADA NO TIENE NINGUN VALOR ASIGNADO YA EN LA BD **/
                        ProductoPrecio idPrecioProducto = null;
                        idPrecioProducto = dbHelper.getPrecioUnitario(posicionCompania, producto.getId(), sucursal.getId());

                        if (isCompaniaOtros) {
                            idPrecioProducto = dbHelper.getPrecioUnitario(posicionCompania, producto.getClave(), sucursal.getId(), edCompaniaOtros.getText().toString());
                        }

                        if (idPrecioProducto.getId() == 0) {
                            if (edPrecioPromocion.getText().toString().equals(""))
                                edPrecioPromocion.setText("0");
                            String precioform = edPrecio.getText().toString().replaceAll("$", "");
                            dbHelper.insertPrecioUnitario(posicionCompania, Double.parseDouble(precioform), Double.parseDouble(edPrecioPromocion.getText().toString()), sucursal.getId(), producto.getId(), usuario.getUsername(), edCompaniaOtros.getText().toString());

                            Toast.makeText(getActivity(), "Precio Guardado ", Toast.LENGTH_SHORT).show();

                            edPrecio.setText("");
                            edPrecioPromocion.setText("");

                            dialogUpadatePrecio.dismiss();

                            /** SI YA EXISTE EL VALOR LO ACTUALIZAMOS **/
                        } else {
                            if (edPrecioPromocion.getText().toString().equals(""))
                                edPrecioPromocion.setText("0");
                            String precioform = edPrecio.getText().toString().replaceAll("$", "");
                            dbHelper.updatePrecioUnitario(idPrecioProducto.getId(), Double.parseDouble(precioform), Double.parseDouble(edPrecioPromocion.getText().toString()), posicionCompania, sucursal.getId(), edCompaniaOtros.getText().toString(), usuario.getUsername(), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);

                            Toast.makeText(getActivity(), "Precio Actualizado ", Toast.LENGTH_SHORT).show();

                            edPrecio.setText("");
                            edPrecioPromocion.setText("");

                        }
                    }
                } else {

                    if (isCompaniaOtros && edCompaniaOtros.getText().toString().equals("")) {
                        Toast toast = Toast.makeText(getActivity(), "Especifica la compañia", Toast.LENGTH_LONG);
                        TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                        tv.setTextColor(Color.WHITE);
                        toast.show();
                        return;
                    }

                    if (edPrecio.getText().toString().equals("")) {
                        Toast toast = Toast.makeText(getActivity(), "El precio no puede estar vacio", Toast.LENGTH_LONG);
                        TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                        tv.setTextColor(Color.WHITE);
                        toast.show();
                        return;
                    }

                    for (int i = 0; i < productoList.size(); i++) {

                        if (productoList.get(i).getIsPrieceActive() == Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_ACTIVO) {
                            ProductoPrecio idPrecioProducto = null;
                            idPrecioProducto = dbHelper.getPrecioUnitario(posicionCompania, productoList.get(i).getId(), sucursal.getId());

                            if (isCompaniaOtros) {
                                idPrecioProducto = dbHelper.getPrecioUnitario(posicionCompania, productoList.get(i).getClave(), sucursal.getId(), edCompaniaOtros.getText().toString());
                            }

                            if (idPrecioProducto.getId() == 0) {
                                if (edPrecioPromocion.getText().toString().equals(""))
                                    edPrecioPromocion.setText("0");
                                String precioform = edPrecio.getText().toString().replaceAll("$", "");
                                dbHelper.insertPrecioUnitario(posicionCompania, Double.parseDouble(precioform), Double.parseDouble(edPrecioPromocion.getText().toString()), sucursal.getId(), productoList.get(i).getId(), usuario.getUsername(), edCompaniaOtros.getText().toString());

                            } else {
                                if (edPrecioPromocion.getText().toString().equals(""))
                                    edPrecioPromocion.setText("0");
                                String precioform = edPrecio.getText().toString().replaceAll("$", "");
                                dbHelper.updatePrecioUnitario(idPrecioProducto.getId(), Double.parseDouble(precioform), Double.parseDouble(edPrecioPromocion.getText().toString()), posicionCompania, sucursal.getId(), edCompaniaOtros.getText().toString(), usuario.getUsername(), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);

                            }
                        }
                    }


                    Toast.makeText(getActivity(), "Precios Actualizados ", Toast.LENGTH_SHORT).show();
                    edPrecio.setText("");
                    edPrecioPromocion.setText("");

                }
            });

            dialogUpadatePrecio.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialogUpadatePrecio.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        });

        return view;
    }


    private class CustomAdapter extends BaseAdapter {

        Context context;

        public CustomAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return precioProductos.size();
        }

        @Override
        public Object getItem(int position) {
            return precioProductos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.adapter_producto_precio, parent, false);
            }

            final ProductoPrecio productoPrecio = precioProductos.get(position);

            CardView cvPrecio = convertView.findViewById(R.id.card_view_producto_catalogado);
            TextView txtCompania = convertView.findViewById(R.id.textView_nombre_compania);
            TextView txtPrecioUnitario = convertView.findViewById(R.id.textView_precio_compania);
            TextView txtUltimaActualizacion = convertView.findViewById(R.id.textView_ultima_actualizacion);
            Button btnActualizaPrecio = convertView.findViewById(R.id.btn_aplica_precio_tienda);

            if (productoPrecio.getCompania().toString().equals("OTROS")) {
                txtCompania.setText(productoPrecio.getDescripcionCompania());
            } else {
                txtCompania.setText(productoPrecio.getCompania());
            }


            txtPrecioUnitario.setText(" $ " + form.format(productoPrecio.getPrecio_unitario()));

            txtUltimaActualizacion.setText(productoPrecio.getUltima_modificacion());

            cvPrecio.setOnLongClickListener(view -> {

                new AlertDialog.Builder(getActivity())
                        .setTitle("Precio de " + precioProductos.get(position).getCompania())
                        .setMessage("¿Estás seguro que deseas eliminar el precio del " + precioProductos.get(position).getUltima_modificacion() + " ?")
                        .setCancelable(false)
                        .setNegativeButton("NO", (dialog, id) -> {
                        })
                        .setPositiveButton("SI", (dialog, which) -> {

                            if (precioProductos.get(position).getEstatus() == Constantes.ESTATUS.CREADO_WEB || precioProductos.get(position).getEstatus() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                dbHelper.deleteProductoPrecio(precioProductos.get(position).getId(), sucursal.getId(), Constantes.ESTATUS.CREADO_WEB);
                            } else if (precioProductos.get(position).getEstatus() == Constantes.ESTATUS.CREADO_MOVIL || precioProductos.get(position).getEstatus() == Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL) {
                                dbHelper.deleteProductoPrecio(precioProductos.get(position).getId(), sucursal.getId(), Constantes.ESTATUS.CREADO_MOVIL);
                            }

                            precioProductos = dbHelper.getAllPrecios(sucursal.getId(), producto.getId());
                            adapterCompania.notifyDataSetChanged();
                            dialog.dismiss();
                            Toast.makeText(getActivity(), "Precio Eliminado", Toast.LENGTH_LONG).show();
                        })
                        .show();

                return false;
            });

            btnActualizaPrecio.setOnClickListener(v -> {

                dbHelper.updatePrecioUnitario(productoPrecio.getId(), productoPrecio.getPrecio_unitario(), productoPrecio.getPrecio_promocion(), productoPrecio.getId_compania(), sucursal.getId(), "", usuario.getUsername(), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
                precioProductos = dbHelper.getAllPrecios(sucursal.getId(), producto.getId());
                adapterCompania.notifyDataSetChanged();
                Snackbar.make(v, "Precio Actualizado", Snackbar.LENGTH_LONG).setAction("Action", null).show();

            });


            return convertView;
        }
    }


    private class CustomAdapterProducto extends BaseAdapter {

        Context context;

        public CustomAdapterProducto(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return productoList.size();
        }

        @Override
        public Object getItem(int position) {
            return productoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.adapter_lista_producto_precio, parent, false);

            ProductoPrecio productoPrecio = dbHelper.getPrecioUnitario(posicionCompania, productoList.get(position).getId(), sucursal.getId());

            ImageView imgProducto = (ImageView) row.findViewById(R.id.img_descripcion);
            TextView textClave = (TextView) row.findViewById(R.id.textView_value_productos_catalogo_clave);
            TextView textNombre = (TextView) row.findViewById(R.id.textView_value_productos_catalogo_nombre);
            TextView textPresentacion = (TextView) row.findViewById(R.id.textView_value_productos_catalogo_presentacion);
            TextView textPrecioTienda = (TextView) row.findViewById(R.id.textView_value_productos_catalogo_precio_tienda);
            CheckBox cbPrecio = (CheckBox) row.findViewById(R.id.cb_guia_mercadeo);

            LoadImage loadImage = new LoadImage();

            String imageResource = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_img_productos)) + File.separator + productoList.get(position).getClave().toLowerCase() + ".png";

            loadImage.load(context, imageResource, imgProducto);
            textClave.setText(productoList.get(position).getClave());
            textNombre.setText(productoList.get(position).getDescripcion());
            textPresentacion.setText(productoList.get(position).getPresentacion());
            textPrecioTienda.setText("$ " + form.format(productoPrecio.getPrecio_unitario()));


            if (productoList.get(position).getIsPrieceActive() == Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_ACTIVO) {
                cbPrecio.setChecked(true);
            } else {
                cbPrecio.setChecked(false);
            }

            return row;
        }
    }


}
