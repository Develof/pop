package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;

/**
 * Created by uriel_000 on 03/03/2016.
 */
public class ClienteSucursal implements Serializable{
    private int id;
    private int id_cadena;
    private int id_udn;
    private String clave;
    private String alias;
    private String calle;
    private String no_ext;
    private String no_int;
    private String colonia;
    private String cp;
    private String municipio;
    private String estado;
    private double latitud;
    private double longitud;
    private int localizacion_activa;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;

    //CLIENTE Y CADENA
    private String cadena;
    private String cliente;

    public ClienteSucursal(){

    }

    public ClienteSucursal(int id, int id_cadena, int id_udn, String alias, String calle, String no_ext, String no_int, String colonia, String cp, String municipio, String estado, double latitud, double longitud, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.id = id;
        this.id_cadena = id_cadena;
        this.id_udn = id_udn;
        this.alias = alias;
        this.calle = calle;
        this.no_ext = no_ext;
        this.no_int = no_int;
        this.colonia = colonia;
        this.cp = cp;
        this.municipio = municipio;
        this.estado = estado;
        this.latitud = latitud;
        this.longitud = longitud;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }


    public ClienteSucursal(int id, int id_cadena, int id_udn, String clave, String alias, String calle, String no_ext, String no_int, String colonia, String cp, String municipio, String estado, double latitud, double longitud, int localizacion_activa, String fecha_creacion, String ultima_modificacion, String usuario, int estatus, String cadena, String cliente) {
        this.id = id;
        this.id_cadena = id_cadena;
        this.id_udn = id_udn;
        this.clave = clave;
        this.alias = alias;
        this.calle = calle;
        this.no_ext = no_ext;
        this.no_int = no_int;
        this.colonia = colonia;
        this.cp = cp;
        this.municipio = municipio;
        this.estado = estado;
        this.latitud = latitud;
        this.longitud = longitud;
        this.localizacion_activa = localizacion_activa;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
        this.cadena = cadena;
        this.cliente = cliente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_cadena() {
        return id_cadena;
    }

    public void setId_cadena(int id_cadena) {
        this.id_cadena = id_cadena;
    }

    public int getId_udn() {
        return id_udn;
    }

    public void setId_udn(int id_udn) {
        this.id_udn = id_udn;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNo_ext() {
        return no_ext;
    }

    public void setNo_ext(String no_ext) {
        this.no_ext = no_ext;
    }

    public String getNo_int() {
        return no_int;
    }

    public void setNo_int(String no_int) {
        this.no_int = no_int;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public int getLocalizacion_activa() {
        return localizacion_activa;
    }

    public void setLocalizacion_activa(int localizacion_activa) {
        this.localizacion_activa = localizacion_activa;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
}
