package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 14/03/2016.
 */
public class AdapterAlertas {
    private String nombre;
    private int id;
    private String descripcion;
    private String fecha;
    private String leida;
    private String alerta_sucursal;
    private int estatus_dos;

    AdapterAlertas(){}

    public AdapterAlertas(String nombre, int id, String descripcion, String fecha, String leida, int estatus_dos) {
        this.nombre = nombre;
        this.id = id;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.leida = leida;
        this.estatus_dos = estatus_dos;
    }

    public AdapterAlertas(int id, String descripcion, String fecha, String leida, String alerta_sucursal) {
        this.id = id;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.leida = leida;
        this.alerta_sucursal = alerta_sucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLeida() {
        return leida;
    }

    public void setLeida(String leida) {
        this.leida = leida;
    }

    public String getAlerta_sucursal() {
        return alerta_sucursal;
    }

    public void setAlerta_sucursal(String alerta_sucursal) {
        this.alerta_sucursal = alerta_sucursal;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }
}
