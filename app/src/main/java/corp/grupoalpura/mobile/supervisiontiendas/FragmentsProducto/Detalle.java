package corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Application.PopApplication;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Database.DBPop;
import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.InventarioBO;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.GuiaMercadeoModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.GuiaMercadeoBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.ProductoPreciosBDHelper;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by oflores on 11/07/16.
 **/
public class Detalle extends Fragment {

    ProductoPreciosBDHelper dbHelper;
    GuiaMercadeoBDHelper gdbHelper;

    ClienteSucursal sucursal;
    Usuario usuario;
    FichaTecnicaProducto producto;
    Double precioTienda = 0.0, promocionTienda = 0.0;
    GuiaMercadeoModel promocionGuia;
    InventarioBO inventarioBO;

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String PRODUCTO = "Producto";

    @BindView(R.id.textView_title_sku_producto)
    TextView tvSku;
    @BindView(R.id.textView_title_clave_producto)
    TextView tvClave;
    @BindView(R.id.textView_title_precio_tienda)
    TextView tvPrecioTienda;
    @BindView(R.id.textView_title_promocion_tienda)
    TextView tvPromocionTienda;
    @BindView(R.id.textView_title_promocion_guia)
    TextView tvPromocionGuia;
    @BindView(R.id.textView_title_vigencia_gm)
    TextView tvVigencia;
    @BindView(R.id.textView_title_inv_bo)
    TextView tvInvBo;

    static DecimalFormat form = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);

    public static Detalle newInstance(ClienteSucursal sucursal, Usuario usuario, FichaTecnicaProducto producto) {
        Detalle fragment = new Detalle();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(PRODUCTO, producto);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper = new ProductoPreciosBDHelper(getActivity());
        gdbHelper = new GuiaMercadeoBDHelper(getActivity());
        form.applyPattern(Constantes.FORMATO_PRECIO_SIGNO);

        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            producto = (FichaTecnicaProducto) getArguments().getSerializable(PRODUCTO);
        }

        precioTienda = dbHelper.getPrecioTienda(producto.getId(), sucursal.getId(), 4);
        promocionTienda = dbHelper.getPromocionTienda(producto.getId(), sucursal.getId(), 4);
        promocionGuia = gdbHelper.getPromocionGuia(producto.getNombre(), sucursal.getId_cadena());

        DBPop database = ((PopApplication) getActivity().getApplication()).database;
        inventarioBO = database.inventarioBODao().getInventoryProd(sucursal.getClave(), producto.getClave());

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detalle_datos_producto, container, false);
        ButterKnife.bind(this, view);

        String clv = getResources().getString(R.string.detalle_clave) + "\n" + producto.getClave();
        String upc = getResources().getString(R.string.detalle_sku) + "\n" + producto.getUPC();
        String ptienda = getResources().getString(R.string.detalle_precio_tienda) + "\n" + form.format(precioTienda);
        String promtienda = getResources().getString(R.string.detalle_promocion_tienda) + "\n" + form.format(promocionTienda);

        if (promocionGuia != null) {
            if (promocionGuia.getPrecioOferta() != null) {
                if (promocionGuia.getPrecioOferta().equals("-")) {

                    String promGuia = getResources().getString(R.string.detalle_promocion_gm) + "\n" + promocionGuia.getPromocion().replaceAll("\\(Nacional\\)", "");
                    String vigenciaGuia = getResources().getString(R.string.detalle_vigencia_gm) + "\n" + promocionGuia.getVigencia();
                    tvPromocionGuia.setText(promGuia);
                    tvVigencia.setText(vigenciaGuia);
                } else {
                    String promGuia = getResources().getString(R.string.detalle_promocion_gm) + "\n" + form.format(Double.parseDouble(promocionGuia.getPrecioOferta()));
                    String vigenciaGuia = getResources().getString(R.string.detalle_vigencia_gm) + "\n" + promocionGuia.getVigencia();
                    tvPromocionGuia.setText(promGuia);
                    tvVigencia.setText(vigenciaGuia);
                }
            }
        }

        String invBo = null;

        if (inventarioBO != null) {
            invBo = getResources().getString(R.string.detalle_inventario_bo) + "\n" + String.valueOf(inventarioBO.getHand()) + " pz.";
        } else {
            invBo = getResources().getString(R.string.detalle_inventario_bo);
        }

        tvSku.setText(upc);
        tvClave.setText(clv);
        tvPrecioTienda.setText(ptienda);
        tvPromocionTienda.setText(promtienda);

        tvInvBo.setText(invBo);


        return view;
    }
}
