package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prestamo on 10/03/2016.
 */
public class AdapterEspacios implements Serializable {
    private int id_sucursal_espacio;
    private String nombre_espacio;
    private String nombre_dpto;
    private String sucursal;
    private String agrupacion;
    private String presentacion;
    private int tipo_adquisicion;
    private int nivel;
    private int id_departamento;
    private int id_espacio;
    private int estatus_dos;
    private int idSucursal;
    private int idAgrupacion;
    private int idPresentacion;
    private List<Double> frente;
    private List<Long> idEspacioFrente;

    @Override
    public String toString() {
        return nombre_dpto + " -> " + nombre_espacio;
    }

    public AdapterEspacios(){}

    public AdapterEspacios(int id_sucursal_espacio) {
        this.id_sucursal_espacio = id_sucursal_espacio;
    }

    public AdapterEspacios(int id_sucursal_espacio, String nombre_espacio, String nombre_dpto, int id_departamento, int id_espacio) {
        this.id_sucursal_espacio = id_sucursal_espacio;
        this.nombre_espacio = nombre_espacio;
        this.nombre_dpto = nombre_dpto;
        this.id_departamento = id_departamento;
        this.id_espacio = id_espacio;
    }

    public AdapterEspacios(int id_sucursal_espacio, String nombre_espacio, int tipo_adquisicion, int nivel, int id_departamento, int id_espacio, int estatus_dos) {
        this.id_sucursal_espacio = id_sucursal_espacio;
        this.nombre_espacio = nombre_espacio;
        this.tipo_adquisicion = tipo_adquisicion;
        this.nivel = nivel;
        this.id_departamento = id_departamento;
        this.id_espacio = id_espacio;
        this.estatus_dos = estatus_dos;
    }


    public AdapterEspacios(int id_sucursal_espacio, String nombre_espacio, String nombre_dpto, int tipo_adquisicion, int nivel, int id_departamento, int id_espacio) {
        this.id_sucursal_espacio = id_sucursal_espacio;
        this.nombre_espacio = nombre_espacio;
        this.nombre_dpto = nombre_dpto;
        this.tipo_adquisicion = tipo_adquisicion;
        this.nivel = nivel;
        this.id_departamento = id_departamento;
        this.id_espacio = id_espacio;
    }

    public int getId_sucursal_espacio() {
        return id_sucursal_espacio;
    }

    public void setId_sucursal_espacio(int id_sucursal_espacio) {
        this.id_sucursal_espacio = id_sucursal_espacio;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombre_espacio() {
        return nombre_espacio;
    }

    public void setNombre_espacio(String nombre_espacio) {
        this.nombre_espacio = nombre_espacio;
    }

    public int getTipo_adquisicion() {
        return tipo_adquisicion;
    }

    public void setTipo_adquisicion(int tipo_adquisicion) {
        this.tipo_adquisicion = tipo_adquisicion;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getId_departamento() {
        return id_departamento;
    }

    public void setId_departamento(int id_departamento) {
        this.id_departamento = id_departamento;
    }

    public int getId_espacio() {
        return id_espacio;
    }

    public void setId_espacio(int id_espacio) {
        this.id_espacio = id_espacio;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }

    public String getNombre_dpto() {
        return nombre_dpto;
    }

    public void setNombre_dpto(String nombre_dpto) {
        this.nombre_dpto = nombre_dpto;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public int getIdAgrupacion() {
        return idAgrupacion;
    }

    public void setIdAgrupacion(int idAgrupacion) {
        this.idAgrupacion = idAgrupacion;
    }

    public int getIdPresentacion() {
        return idPresentacion;
    }

    public void setIdPresentacion(int idPresentacion) {
        this.idPresentacion = idPresentacion;
    }

    public String getAgrupacion() {
        return agrupacion;
    }

    public void setAgrupacion(String agrupacion) {
        this.agrupacion = agrupacion;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public List<Double> getFrente() {
        return frente;
    }

    public void setFrente(List<Double> frente) {
        this.frente = frente;
    }

    public List<Long> getIdEspacioFrente() {
        return idEspacioFrente;
    }

    public void setIdEspacioFrente(List<Long> idEspacioFrente) {
        this.idEspacioFrente = idEspacioFrente;
    }
}
