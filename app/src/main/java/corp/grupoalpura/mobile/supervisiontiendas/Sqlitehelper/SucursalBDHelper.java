package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;

/**
 * Created by camovilesb on 7/4/2017.
 */

public class SucursalBDHelper extends DBHelper {

    public SucursalBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }


    public int getSucursalActual() {
        int sucursalActual = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM Visita", null);

        if (cursor.moveToFirst()) {
            sucursalActual = Integer.parseInt(cursor.getString(0));
        }

        cursor.close();
        db.close();
        return sucursalActual;
    }

    public ClienteSucursal getDataSucursal(int dia, String columna, int no_sucursal) {
        ClienteSucursal sucursal = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Cliente_Sucursal.ID_SUCURSAL, Cliente_Sucursal.ID_CADENA, Cliente_Sucursal.ID_UDN, " +
                "Cliente_Sucursal.CLAVE, Cliente_Sucursal.ALIAS, Cliente_Sucursal.CALLE, Cliente_Sucursal.NO_EXT, Cliente_Sucursal.NO_INT, " +
                "Cliente_Sucursal.COLONIA, Cliente_Sucursal.CP, Cliente_Sucursal.MUNICIPIO, Cliente_Sucursal.ESTADO, Cliente_Sucursal.LATITUD, " +
                "Cliente_Sucursal.LONGITUD, Cliente_Sucursal.LOCALIZACION_ACTIVA, Cliente_Sucursal.FECHA_CREACION, Cliente_Sucursal.ULTIMA_MODIFICACION, " +
                "Cliente_Sucursal.USUARIO, Cliente_Sucursal.ESTATUS, CADENA.ALIAS AS CADENA, CLIENTE.ALIAS AS CLIENTE " +
                "FROM Cliente_Sucursal " +
                "INNER JOIN Sucursal_Supervisor ON Cliente_Sucursal.ID_SUCURSAL = Sucursal_Supervisor.ID_SUCURSAL " +
                "INNER JOIN Cadena ON Cliente_Sucursal.ID_CADENA = Cadena.ID_CADENA " +
                "INNER JOIN CLIENTE ON Cadena.ID_CLIENTE = Cliente.ID_CLIENTE " +
                "INNER JOIN  Dias_Sucursal_Supervisor ON Sucursal_Supervisor.ID_SUCURSAL_SUPERVISOR = Dias_Sucursal_Supervisor.ID_SUCURSAL_SUPERVISOR " +
                "WHERE Dias_Sucursal_Supervisor." + columna + " = '" + no_sucursal + "' " +
                "AND Dias_Sucursal_Supervisor.id_dia = '" + dia + "' " +
                "AND Dias_Sucursal_Supervisor.ESTATUS = 1", null);

        if (cursor.moveToFirst()) {
            int ID_SUCRUSAL = Integer.parseInt(cursor.getString(0));
            int ID_CLIENTE = Integer.parseInt(cursor.getString(1));
            int ID_UDN = Integer.parseInt(cursor.getString(2));
            String CLAVE = cursor.getString(3);
            String ALIAS = cursor.getString(4);
            String CALLE = cursor.getString(5);
            String NO_EXT = cursor.getString(6);
            String NO_INT = cursor.getString(7);
            String COLONIA = cursor.getString(8);
            String CP = cursor.getString(9);
            String MUNICIPIO = cursor.getString(10);
            String ESTADO = cursor.getString(11);
            double LATITUD = Double.parseDouble(cursor.getString(12));
            double LONGITUD = Double.parseDouble(cursor.getString(13));
            int LOCALIZACION_ACTIVA = Integer.parseInt(cursor.getString(14));
            String FECHA_CREACION = cursor.getString(15);
            String FECHA_MODIFICACION = cursor.getString(16);
            String USUARIO = cursor.getString(17);
            int ESTATUS = Integer.parseInt(cursor.getString(18));
            String CADENA = cursor.getString(19);
            String CLIENTE = cursor.getString(20);

            sucursal = new ClienteSucursal(ID_SUCRUSAL, ID_CLIENTE, ID_UDN, CLAVE, ALIAS, CALLE, NO_EXT, NO_INT, COLONIA, CP, MUNICIPIO, ESTADO, LATITUD, LONGITUD, LOCALIZACION_ACTIVA, FECHA_CREACION, FECHA_MODIFICACION, USUARIO, ESTATUS, CADENA, CLIENTE);
        }

        cursor.close();
        return sucursal;
    }

    public ClienteSucursal getSucursal(int id_sucursal) {
        ClienteSucursal sucursal = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Cliente_Sucursal.ID_SUCURSAL, Cliente_Sucursal.ID_CADENA, Cliente_Sucursal.ID_UDN, Cliente_Sucursal.CLAVE, " +
                "Cliente_Sucursal.ALIAS, Cliente_Sucursal.CALLE, Cliente_Sucursal.NO_EXT, Cliente_Sucursal.NO_INT, Cliente_Sucursal.COLONIA, " +
                "Cliente_Sucursal.CP, Cliente_Sucursal.MUNICIPIO, Cliente_Sucursal.ESTADO, Cliente_Sucursal.LATITUD, Cliente_Sucursal.LONGITUD, " +
                "Cliente_Sucursal.LOCALIZACION_ACTIVA, Cliente_Sucursal.FECHA_CREACION, Cliente_Sucursal.ULTIMA_MODIFICACION, " +
                "Cliente_Sucursal.USUARIO, Cliente_Sucursal.ESTATUS, CADENA.ALIAS AS CADENA, CLIENTE.ALIAS AS CLIENTE " +
                "FROM Cliente_Sucursal INNER JOIN CADENA On Cliente_Sucursal.ID_CADENA = Cadena.ID_CADENA " +
                "INNER JOIN CLIENTE On Cadena.ID_CLIENTE = Cliente.ID_CLIENTE WHERE Cliente_Sucursal.ID_SUCURSAL = '" + id_sucursal + "'", null);

        if (cursor.moveToFirst()) {
            int ID_SUCRUSAL = Integer.parseInt(cursor.getString(0));
            int ID_CLIENTE = Integer.parseInt(cursor.getString(1));
            int ID_UDN = Integer.parseInt(cursor.getString(2));
            String CLAVE = cursor.getString(3);
            String ALIAS = cursor.getString(4);
            String CALLE = cursor.getString(5);
            String NO_EXT = cursor.getString(6);
            String NO_INT = cursor.getString(7);
            String COLONIA = cursor.getString(8);
            String CP = cursor.getString(9);
            String MUNICIPIO = cursor.getString(10);
            String ESTADO = cursor.getString(11);
            double LATITUD = Double.parseDouble(cursor.getString(12));
            double LONGITUD = Double.parseDouble(cursor.getString(13));
            int LOCALIZACION_ACTIVA = Integer.parseInt(cursor.getString(14));
            String FECHA_CREACION = cursor.getString(15);
            String FECHA_MODIFICACION = cursor.getString(16);
            String USUARIO = cursor.getString(17);
            int ESTATUS = Integer.parseInt(cursor.getString(18));
            String CADENA = cursor.getString(19);
            String CLIENTE = cursor.getString(20);

            sucursal = new ClienteSucursal(ID_SUCRUSAL, ID_CLIENTE, ID_UDN, CLAVE, ALIAS, CALLE, NO_EXT, NO_INT, COLONIA, CP, MUNICIPIO, ESTADO, LATITUD, LONGITUD, LOCALIZACION_ACTIVA, FECHA_CREACION, FECHA_MODIFICACION, USUARIO, ESTATUS, CADENA, CLIENTE);
        }

        cursor.close();
        return sucursal;
    }



    public void updateLocalizacionSucursal(int id_sucursal, double LATITUD, double LONGITUD) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_SUCURSAL + " SET " + COLUMNA_SUCURSAL_LOCALIZACION_ACTIVA + " = '" + Constantes.SUCURSAL_LOCALIZACION_ACTIVA.ACTIVA + "', " + COLUMNA_SUCURSAL_LATITUD + " = '" + LATITUD + "', " + COLUMNA_SUCURSAL_LONGITUD + " = '" + LONGITUD + "', " + COLUMNA_SUCURSAL_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_ESTATUS_DOS + " = '" + Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL + "' WHERE " + COLUMNA_SUCRUSAL_ID + " = '" + id_sucursal + "'";
        database.execSQL(strSQL);
        database.close();
    }


    public void disableLocalizacionSucursal(int id_sucursal, double LATITUD, double LONGITUD) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_SUCURSAL + " SET " + COLUMNA_SUCURSAL_LOCALIZACION_ACTIVA + " = '" + Constantes.SUCURSAL_LOCALIZACION_ACTIVA.INACTIVA + "', " + COLUMNA_SUCURSAL_LATITUD + " = '" + LATITUD + "', " + COLUMNA_SUCURSAL_LONGITUD + " = '" + LONGITUD + "', " + COLUMNA_SUCURSAL_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_ESTATUS_DOS + " = '" + Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL + "' WHERE " + COLUMNA_SUCRUSAL_ID + " = '" + id_sucursal + "'";
        database.execSQL(strSQL);
        database.close();
    }

}
