package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.EspacioFrente;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.SucursalEspacio;

/**
 * Created by Develof on 16/03/17.
 */

public class SucursalEspacioBDHelper extends DBHelper {

    public SucursalEspacioBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public ArrayList<SucursalEspacio> getSucursalEspacio(int idSucursal, int idEspacio) {

        ArrayList<SucursalEspacio> sucursalEspacioArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " +
                "SE.ID_SUCURSAL_ESPACIO ID_ESPACIO_SUCURSAL, " +
                "DEP.NOMBRE DEPARTAMENTO, " +
                "ESP.NOMBRE,  " +
                "SE.TIPO_ADQUISICION, " +
                "SE.NIVEL, " +
                "IFNULL(SE.PUERTAS, 0), " +
                "SE.TRAMOS, " +
                "SE.TARIMAS, " +
                "IFNULL(DEP.ID_DEPARTAMENTO, 0), " +
                "ESP.ID_ESPACIO, " +
                "SE.ESTATUS2, " +
                "IFNULL(PROF.ID_FAMILIA, 0)," +
                "PROF.FAMILIA " +
                "FROM SUCURSAL_ESPACIO SE  " +
                "LEFT JOIN DEPARTAMENTOS DEP ON DEP.ID_DEPARTAMENTO = SE.ID_DEPARTAMENTO  " +
                "LEFT JOIN PRODUCTO_FAMILIA PROF ON PROF.ID_FAMILIA = SE.ID_CATEGORIA  " +
                "INNER JOIN ESPACIOS ESP ON ESP.ID_ESPACIO = SE.ID_ESPACIO  " +
                "WHERE SE.ID_SUCURSAL = " + idSucursal + " " +
                "AND ESP.ID_ESPACIO = " + idEspacio + " " +
                "AND SE.ESTATUS = 1  " +
                "GROUP BY SE.ID_SUCURSAL_ESPACIO, DEP.NOMBRE, ESP.NOMBRE;", null);

        if (cursor.moveToFirst()) {
            do {
                SucursalEspacio sucursalEspacio = new SucursalEspacio();
                sucursalEspacio.setIdSucursalEspacio(Integer.parseInt(cursor.getString(0)));
                sucursalEspacio.setDepartamento(cursor.getString(1));
                sucursalEspacio.setEspacio(cursor.getString(2));
                sucursalEspacio.setTipo_adquisicion(Integer.parseInt(cursor.getString(3)));
                sucursalEspacio.setNivel(Double.parseDouble(cursor.getString(4)));
                sucursalEspacio.setPuertas(cursor.getString(5).equals("") ? 0 : Integer.parseInt(cursor.getString(5)));
                sucursalEspacio.setTramos(cursor.getString(6).equals("") ? 0 : Double.parseDouble(cursor.getString(6)));
                sucursalEspacio.setTarimas(cursor.getString(7).equals("") ? 0 : Double.parseDouble(cursor.getString(7)));
                sucursalEspacio.setIdDepartemento(cursor.getString(8).equals("") ? 0 : Integer.parseInt(cursor.getString(8)));
                sucursalEspacio.setIdEspacio(Integer.parseInt(cursor.getString(9)));
                sucursalEspacio.setEstatus_dos(Integer.parseInt(cursor.getString(10)));
                sucursalEspacio.setIdCategoria(cursor.getString(11).equals("") ? 0 : Integer.parseInt(cursor.getString(11)));
                sucursalEspacio.setCategoria(cursor.getString(12));
                sucursalEspacioArrayList.add(sucursalEspacio);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return sucursalEspacioArrayList;
    }


    public void insertFrentes(EspacioFrente arregloFrentes, int id_sucursal_espacio, String usuario) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues valuesFrentes = new ContentValues();

        EspacioFrente espacioFrente;
        espacioFrente = getEspacioFrente(id_sucursal_espacio, arregloFrentes.getId_compania(), arregloFrentes.getId_agrupacion(), arregloFrentes.getId_presentacion());

        if (espacioFrente == null) {

            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_ID_SUCURSAL_ESPACIO, id_sucursal_espacio);
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_ID_COMPANIA, arregloFrentes.getId_compania());
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_ID_AGRUPACION, arregloFrentes.getId_agrupacion());
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_ID_PRESENTACION, arregloFrentes.getId_presentacion());
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_FRENTES, arregloFrentes.getFrente());
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_FECHA_CREACION, getDateTime());
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_ULTIMA_MODIFICACION, getDateTime());
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_USUARIO, usuario);
            valuesFrentes.put(COLUMNA_ESPACIO_FRENTE_ESTATUS, 1);
            valuesFrentes.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
            long a = database.insert(TABLA_ESPACIO_FRENTE, null, valuesFrentes);


        } else {
            double frente = arregloFrentes.getFrente();
            if (espacioFrente.getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || espacioFrente.getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                updateFrentes(frente, espacioFrente.getId_espacio_frente(), 1, Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
            } else {
                updateFrentes(frente, espacioFrente.getId_espacio_frente(), 1, Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
            }
        }

        database.close();
    }


    private EspacioFrente getEspacioFrente(int idSucursalEspacio, int idCompania, int idAgrupacion, int idPresentacion) {

        EspacioFrente espacioFrente = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_ESPACIO_FRENTE, SUCURSAL_ESPACIO.ESTATUS2, ESPACIO_FRENTE.FRENTES " +
                "FROM ESPACIO_FRENTE " +
                "INNER JOIN SUCURSAL_ESPACIO ON ESPACIO_FRENTE.ID_SUCURSAL_ESPACIO = " +
                "SUCURSAL_ESPACIO.ID_SUCURSAL_ESPACIO " +
                "WHERE ESPACIO_FRENTE.ID_SUCURSAL_ESPACIO = '" + idSucursalEspacio + "' " +
                "AND ID_COMPANIA = '" + idCompania + "' AND ID_AGRUPACION = '" + idAgrupacion + "' AND ID_PRESENTACION = '" + idPresentacion + "' " +
                "ORDER BY ESPACIO_FRENTE.ULTIMA_MODIFICACION", null);

        if (cursor.moveToFirst()) {

            espacioFrente = new EspacioFrente();
            espacioFrente.setId_espacio_frente(Integer.parseInt(cursor.getString(0)));
            espacioFrente.setEstatus_dos(Integer.parseInt(cursor.getString(1)));
            espacioFrente.setFrente(Double.parseDouble(cursor.getString(2)));

        }

        return espacioFrente;

    }


    public double getFrentes(int idSucursalEspacio) {

        double totalFrente = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ESPACIO_FRENTE.FRENTES " +
                "FROM ESPACIO_FRENTE " +
                "INNER JOIN SUCURSAL_ESPACIO ON ESPACIO_FRENTE.ID_SUCURSAL_ESPACIO = " +
                "SUCURSAL_ESPACIO.ID_SUCURSAL_ESPACIO " +
                "WHERE ESPACIO_FRENTE.ID_SUCURSAL_ESPACIO = '" + idSucursalEspacio + "' AND ESPACIO_FRENTE.ESTATUS = 1 " +
                "ORDER BY ESPACIO_FRENTE.ULTIMA_MODIFICACION", null);

        if (cursor.moveToFirst()) {
            do {
                totalFrente += Double.parseDouble(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        return totalFrente;

    }


    public void updateEspacio(int id_sucursal_espacio, int tipo_espacio, int departamento, int idFamilia, double nivel, double tarima, double tramo, double puertas, int[] tipo_adquisicion, String usuario, int estatus_dos) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_SUCURSAL_ESPACIOS + " SET " + COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " +
                COLUMNA_SUCURSAL_ESPACIOS_NIVEL + " = '" + nivel + "', " + COLUMNA_SUCURSAL_ESPACIOS_TARIMAS + " = '" + tarima + "', " + COLUMNA_SUCURSAL_ESPACIOS_TRAMOS + " = '" + tramo + "', " + COLUMNA_SUCURSAL_ESPACIOS_PUERTAS + " = '" + puertas + "', " + COLUMNA_SUCURSAL_ESPACIOS_ID_ESPACIO + " = '" + tipo_espacio + "', " +
                COLUMNA_SUCURSAL_ESPACIOS_ID_DEPARTAMENTOS + " = " + departamento + ", " + COLUMNA_SUCURSAL_ESPACIOS_ID_CATEGORIA + " = " + idFamilia + ", " + COLUMNA_SUCURSAL_ESPACIOS_TIPO_ADQUISICION + " = " + tipo_adquisicion[0] + ", " +
                COLUMNA_ESTATUS_DOS + " = " + estatus_dos + " WHERE " + COLUMNA_SUCURSAL_ESPACIOS_ID + " = " + id_sucursal_espacio + "";
        database.execSQL(strSQL);
        database.close();
    }


    public void updateFrentes(double frentes, long idFrente, int estatus, int estatus_dos) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_ESPACIO_FRENTE + " SET " + COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "', " + COLUMNA_ESPACIO_FRENTE_FRENTES + " = '" + frentes + "', " + COLUMNA_ESPACIO_FRENTE_ESTATUS + " = '" + estatus + "' WHERE " + COLUMNA_ESPACIO_FRENTE_ID + " = " + idFrente;
        database.execSQL(strSQL);
    }

}
