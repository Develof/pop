package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by uriel_000 on 03/03/2016.
 */
public class UDN {
    private int id;
    private String alias;
    private double latitud;
    private double longitud;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;

    public UDN(){

    }

    public UDN(String alias, double latitud, double longitud, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.alias = alias;
        this.latitud = latitud;
        this.longitud = longitud;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public UDN(int id, String alias, double latitud, double longitud, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.id = id;
        this.alias = alias;
        this.latitud = latitud;
        this.longitud = longitud;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
