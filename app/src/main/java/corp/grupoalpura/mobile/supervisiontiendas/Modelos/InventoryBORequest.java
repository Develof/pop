package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import com.google.gson.annotations.SerializedName;

public class InventoryBORequest {

    public static final String P_CADENA = "p_cadena";
    public static final String P_FECHA = "p_fecha";
    public static final String P_STORE_NUMBER = "p_store_number";


    @SerializedName(P_CADENA)
    private String cadena;

    @SerializedName(P_FECHA)
    private String fecha;

    @SerializedName(P_STORE_NUMBER)
    private String clave_sucursal;

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getClave_sucursal() {
        return clave_sucursal;
    }

    public void setClave_sucursal(String clave_sucursal) {
        this.clave_sucursal = clave_sucursal;
    }
}
