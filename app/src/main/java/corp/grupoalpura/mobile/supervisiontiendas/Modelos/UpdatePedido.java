package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 06/09/16.
 */

public class UpdatePedido {
    private String clave;
    private String orden;

    public UpdatePedido(){}

    public UpdatePedido(String clave, String orden) {
        this.clave = clave;
        this.orden = orden;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }
}
