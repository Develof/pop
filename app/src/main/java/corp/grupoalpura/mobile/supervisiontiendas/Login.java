package corp.grupoalpura.mobile.supervisiontiendas;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Application.PopApplication;
import corp.grupoalpura.mobile.supervisiontiendas.Broadcast.GPSService;
import corp.grupoalpura.mobile.supervisiontiendas.Broadcast.JobScheduleBDBackup;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.EndPoints;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBBackup;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Database.DBPop;
import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Udn;
import corp.grupoalpura.mobile.supervisiontiendas.Interactor.WSInteractor;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.DispositivoBDModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.RegistroAplicacionBDModel;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DispositivoBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DispositivoEventualidadBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.RegistroAplicacionBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.SucursalBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.ConfigWsRetrofit;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.DateDeserializer;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Encryption;

import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Usuario;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import corp.grupoalpura.mobile.supervisiontiendas.Usos.Imagenes;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.InformacionDispositivo;
import io.fabric.sdk.android.Fabric;
import me.tatarka.support.job.JobInfo;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.json.JSONException;
import org.json.JSONObject;


public class Login extends AppCompatActivity {

    AppCompatActivity LOGIN = this;

    @BindView(R.id.etUserName)
    EditText editTextUsuario;
    @BindView(R.id.edPwd)
    EditText editTextPwd;
    @BindView(R.id.btnEntrar)
    Button buttonEntrar;
    @BindView(R.id.textView_version_code_value)
    TextView textVersionCode;
    @BindView(R.id.login_ivLogo)
    ImageView iv;

    Usuario usuario;
    Udn udn;

    int sucursalAtcual, numClick = 0;
    ProgressDialog progressDialog;
    String problema, token_firebase, error_ws = "Error WS";
    public static int CHECK_IN_INACTIVO = 0;
    public static int SINC_INICIAL_HECHA = 1;
    public static int SINC_INICIAL_SIN_HACER = 0;
    private static final int JOB_ID = 100;

    DBBackup respaldo;
    DBPop database;
    me.tatarka.support.job.JobScheduler jobScheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        jobScheduler = me.tatarka.support.job.JobScheduler.getInstance(this);


        database = ((PopApplication) this.getApplication()).database;
        askForContactPermission();

        editTextUsuario.setTextColor(ContextCompat.getColor(Login.this, R.color.colorWhite));
        editTextUsuario.setHintTextColor(ContextCompat.getColor(Login.this, R.color.colorWhiteHint));

        editTextUsuario.setText("JPASCACIO");
        editTextPwd.setText("Alpura123");

        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            textVersionCode.setText(getResources().getString(R.string.app_version) + ": " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        /* SINO EXISTE LA CARPETA DE DESCARGAS LA CREAMOS */
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator);
        if (!dir.exists()) {
            try {
                if (dir.mkdir()) {
                    System.out.println("Directory created");
                } else {
                    System.out.println("Directory is not created");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* SINO EXISTE LA CARPETA DE SUPERVISIONTIENDAS LA CREAMOS */
        File dirPromotoria = new File(Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_foto)) + File.separator);
        if (!dirPromotoria.exists()) {
            try {
                if (dirPromotoria.mkdir()) {
                    System.out.println("Directory created");
                } else {
                    System.out.println("Directory is not created");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* SINO EXISTE LA CARPETA DE PRODUCTOS LA CREAMOS */
        File dirFotoProd = new File(Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_img_productos)) + File.separator);
        if (!dirFotoProd.exists()) {
            try {
                if (dirFotoProd.mkdir()) {
                    System.out.println("Directory created");
                } else {
                    System.out.println("Directory is not created");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        iv.setOnClickListener(v -> {
            if (numClick == 0) {
                conteo(7000);
            }
            if (numClick <= 4) {
                numClick++;
            } else {
                numClick = 0;
                envioBase();
            }
        });

        buttonEntrar.setOnClickListener(view -> {
            token_firebase = FirebaseInstanceId.getInstance().getToken();
            if (!editTextUsuario.getText().toString().equals("") && !editTextPwd.getText().toString().equals("")) {
                String password = Encryption.md5(editTextPwd.getText().toString());
                usuario = database.usuarioDao().getUsuario(editTextUsuario.getText().toString(), password);
                progressDialog = new ProgressDialog(Login.this);
                progressDialog.setMessage("Iniciando Sesión");
                progressDialog.setCancelable(false);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                if (usuario == null) {
                    SincInicioSesion a = new SincInicioSesion();
                    a.execute(editTextUsuario.getText().toString(), password, token_firebase);

                } else {
                    usuario = database.usuarioDao().getUsuario(editTextUsuario.getText().toString(), password);
                    if (usuario != null && usuario.getIdUsuario() == 0) {
                        Toast toast = Toast.makeText(LOGIN, "Usuario o Contraseña Incorrectos", Toast.LENGTH_LONG);
                        TextView v = toast.getView().findViewById(android.R.id.message);
                        v.setTextColor(Color.WHITE);
                        toast.show();
                    } else {
                        RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(Login.this);
                        RegistroAplicacionBDModel registroAplicacion = registroAplicacionBDHelper.getRegistroAplicacion();
                        if (registroAplicacion.getIs_sinc_inicial() == SINC_INICIAL_HECHA) {
                            startActivity(usuario, LOGIN);
                        } else {
                            SincInicioSesion a = new SincInicioSesion();
                            a.execute(editTextUsuario.getText().toString(), password, token_firebase);
                        }
                    }
                }
            } else {
                Toast toast = Toast.makeText(LOGIN, getResources().getString(R.string.errorFaltanDatos), Toast.LENGTH_LONG);
                TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                v.setTextColor(Color.WHITE);
                toast.show();
            }
        });
    }

    private void conteo(long duracion) {
        new CountDownTimer(duracion, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                numClick = 0;
            }
        }.start();
    }

    private void envioBase() {
        new AlertDialog.Builder(LOGIN)
                .setTitle("Enviar base de datos")
                .setMessage("¿Estás seguro que quieres enviar los datos?")
                .setCancelable(false)
                .setNegativeButton("NO", (dialog, id) -> {
                })
                .setPositiveButton("SI", (dialog, which) -> runOnUiThread(() -> {
                    runOnUiThread(() -> {
                        progressDialog = new ProgressDialog(LOGIN);
                        progressDialog.setMessage("Enviando Datos...");
                        progressDialog.setCancelable(false);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();
                    });

                    usuario = database.usuarioDao().getLastUsuario();

                    if (usuario != null) {
                        respaldo = new DBBackup();
                        String respaldoBD = respaldo.backupDb(getApplicationContext().getPackageName(), DBHelper.DATABASE_NAME);
                        SincDB sincDb = new SincDB();
                        sincDb.execute(usuario.getUsername(), usuario.getPwd(), respaldoBD, "");
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(Login.this, "No hay datos que enviar", Toast.LENGTH_SHORT).show();
                    }
                }))
                .show();
    }

    private void guardaDatos(int sucursal) {

        // GUARDAMOS UN REGISTRO EN LA BD CON LA INFROMACION DE LA SUCURSAL ACTUAL, SI ESTAMOS EN ALGUN CHECK IN Y SI YA HEMOS HECHO SINC INICIAL
        RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(Login.this);

        /**
         * @params CHECK_IN_INACTIVO en 0 significa que aun no se ha realizado ningun check in en el dìa
         * @params SINC_INICIAL_SIN_HACER en 0 significa que aun no se ha hecho la sinc inicial
         */
        registroAplicacionBDHelper.insertaRegistroAplicacion(sucursal, CHECK_IN_INACTIVO, SINC_INICIAL_SIN_HACER);
    }

    private void logUser(String noSerie, String usuario) {
        Crashlytics.setUserIdentifier(noSerie);
        Crashlytics.setUserName(usuario);
    }


    private class SincInicioSesion extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... args) {

            String responseString, strResult = null, outputString = "";
            URL url;
            JSONObject obj = null;
            URLConnection connection = null;
            OutputStream out;
            InputStreamReader isr;
            SAXBuilder sxBuild = new SAXBuilder();
            Document doc;
            Element rootNode;
            rootNode = null;
            String salida = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:SiteControllerwsdl\">" +
                    "   <soapenv:Header/>" +
                    "   <soapenv:Body>" +
                    "      <urn:getDatos soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                    "         <usuario xsi:type=\"xsd:string\">" + args[0] + "</usuario>" +
                    "         <password xsi:type=\"xsd:string\">" + args[1] + "</password>" +
                    "         <tipo xsi:type=\"xsd:string\">1</tipo>" +
                    "         <token xsi:type=\"xsd:string\">" + args[2] + "</token>" +
                    "      </urn:getDatos>" +
                    "   </soapenv:Body>" +
                    "</soapenv:Envelope>";

            try {
                url = new URL(EndPoints.SupervisionURL);
                connection = url.openConnection();
                connection.setConnectTimeout(60000);
                connection.setReadTimeout(60000);
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buffer = salida.getBytes();
            if (httpConn != null) {
                try {
                    bout.write(buffer);
                    byte[] b = bout.toByteArray();
                    httpConn.setRequestProperty("Contenedor-Length", String.valueOf(b.length));
                    httpConn.setRequestProperty("Contenedor-Type", "text/xml; charset=utf-8");
                    httpConn.setRequestProperty("SOAPAction", EndPoints.SoapAction);
                    httpConn.setRequestMethod("POST");
                    httpConn.setConnectTimeout(60000);
                    httpConn.setReadTimeout(60000);
                    httpConn.setDoOutput(true);
                    httpConn.setDoInput(true);
                    out = httpConn.getOutputStream();
                    out.write(b);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Read the response.
            try {
                isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);
                while ((responseString = in.readLine()) != null) {
                    outputString = outputString + responseString;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (outputString != null) {
                    doc = sxBuild.build(new StringReader(outputString));
                    rootNode = doc.getRootElement();
                }
            } catch (JDOMException | IOException ex) {
                ex.printStackTrace();
            }

            if (rootNode != null) {
                List lista = rootNode.getChildren();
                for (int i = 0; i < lista.size(); i++) {
                    rootNode = (Element) lista.get(i);
                    lista = rootNode.getChildren();
                    for (int j = 0; j < lista.size(); j++) {
                        rootNode = (Element) lista.get(i);
                        lista = rootNode.getChildren();
                        rootNode = (Element) lista.get(0);
                        strResult = rootNode.getText();
                    }
                }

                try {
                    obj = new JSONObject(strResult);
                } catch (Throwable t) {
                    problema = strResult;
                    t.printStackTrace();
                }
            }

            return obj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (jsonObject != null) {
                try {

                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
                    usuario = gson.fromJson(jsonObject.getJSONObject("USUARIO").toString(), Usuario.class);
                    udn = gson.fromJson(jsonObject.getJSONObject("UDN").toString(), Udn.class);


                    if (usuario.getIdUsuario() != 0) {
                        database.usuarioDao().insertaUsuario(usuario);
                        database.udnDao().insertaUdn(udn);
                    }

                    if (usuario != null || usuario.getIdUsuario() != 0) {

                        FirebaseMessaging.getInstance().subscribeToTopic("/topics/alpura" + udn.getAlias());

                        InformacionDispositivo informacionDispositivo = new InformacionDispositivo();
                        DispositivoBDModel dispositivoBDModel1 = informacionDispositivo.getInfromacion(LOGIN);
                        logUser(dispositivoBDModel1.getNoSerie(), usuario.getUsername());

                        //OBTENERMOS LA SUCURSAL ACTUAL QUE EN ESTE CASO NO HAY NINGUNA Y LE SUMAMOS 1 PARA QUE TOMEMOS A PARTIR DE LA PRIMER SUCURSAL
                        SucursalBDHelper sucursalBDHelper = new SucursalBDHelper(LOGIN);
                        sucursalAtcual = sucursalBDHelper.getSucursalActual() + 1;
                        guardaDatos(sucursalAtcual);

                        // Creamos un hilo para obtener los datos de sincronizacion del usuario
                        progressDialog.setMessage("Descargando Datos...");
                        SincInicial inicial = new SincInicial();
                        inicial.execute();


                    } else {
                        problema = "Ocurrio un problema al hacer la sincronización";
                        progressDialog.dismiss();
                        Toast toast = Toast.makeText(Login.this, problema, Toast.LENGTH_SHORT);
                        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                        v.setTextColor(Color.WHITE);
                        toast.show();
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            } else {
                if (problema == null)
                    problema = "Ocurrio un problema al hacer la sincronización";
                progressDialog.dismiss();
                Toast toast = Toast.makeText(Login.this, problema, Toast.LENGTH_SHORT);
                TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                v.setTextColor(Color.WHITE);
                toast.show();
            }
            super.onPostExecute(jsonObject);
        }
    }

    /**
     * HILO PARA MANDAR LA BD SQLITE AL WS
     **/
    private class SincDB extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... args) {

            String responseString, strResult = null, outputString = "";
            URL url;
            URLConnection connection = null;
            OutputStream out;
            InputStreamReader isr;
            SAXBuilder sxBuild = new SAXBuilder();
            Document doc;
            Element rootNode;
            rootNode = null;
            String salida = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:SiteControllerwsdl\">" +
                    "   <soapenv:Header/>" +
                    "   <soapenv:Body>" +
                    "      <urn:setBdBase64 soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                    "         <usuario xsi:type=\"xsd:string\">" + args[0] + "</usuario>" +
                    "         <password xsi:type=\"xsd:string\">" + args[1] + "</password>" +
                    "         <dataBase64 xsi:type=\"xsd:string\">" + args[2] + "</dataBase64>" +
                    "         <fecha_base xsi:type=\"xsd:string\">" + args[3] + "</fecha_base>" +
                    "      </urn:setBdBase64>" +
                    "   </soapenv:Body>" +
                    "</soapenv:Envelope>";

            try {
                url = new URL(EndPoints.SupervisionURL);
                connection = url.openConnection();
                connection.setConnectTimeout(20000);
                connection.setReadTimeout(20000);
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buffer = salida.getBytes();
            if (httpConn != null) {
                try {
                    bout.write(buffer);
                    byte[] b = bout.toByteArray();
                    httpConn.setRequestProperty("Contenedor-Length", String.valueOf(b.length));
                    httpConn.setRequestProperty("Contenedor-Type", "text/xml; charset=utf-8");
                    httpConn.setRequestProperty("SOAPAction", EndPoints.SoapAction);
                    httpConn.setRequestMethod("POST");
                    httpConn.setConnectTimeout(20000);
                    httpConn.setReadTimeout(20000);
                    httpConn.setDoOutput(true);
                    httpConn.setDoInput(true);
                    out = httpConn.getOutputStream();
                    out.write(b);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Read the response.
            try {
                isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);
                while ((responseString = in.readLine()) != null) {
                    outputString = outputString + responseString;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                doc = sxBuild.build(new StringReader(outputString));
                rootNode = doc.getRootElement();

            } catch (JDOMException | IOException ex) {
                ex.printStackTrace();
            }

            if (rootNode != null) {
                List lista = rootNode.getChildren();
                for (int i = 0; i < lista.size(); i++) {
                    rootNode = (Element) lista.get(i);
                    lista = rootNode.getChildren();
                    for (int j = 0; j < lista.size(); j++) {
                        rootNode = (Element) lista.get(i);
                        lista = rootNode.getChildren();
                        rootNode = (Element) lista.get(0);
                        strResult = rootNode.getText();
                    }
                }
            }

            return strResult;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (response != null) {
                progressDialog.dismiss();
                Toast.makeText(LOGIN, "Base de datos enviada", Toast.LENGTH_SHORT).show();
            } else {
                progressDialog.dismiss();
                Toast.makeText(LOGIN, "Ocurrio un error al enviar la base de datos", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int recivePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
            int ubicacionPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int espacioPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int telefonoPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            int contactosPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int smsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
            int camaraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

            List<String> listPermissionsNeeded = new ArrayList<>();

            if (recivePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
            }

            if (ubicacionPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (espacioPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (telefonoPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }

            if (contactosPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (smsPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_SMS);
            }

            if (camaraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
                return;
            }

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100:
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator);
                        /* SINO EXISTE LA TABLA DE DESCARGAS LA CREAMOS */
                        if (!dir.exists()) {
                            try {
                                if (dir.mkdir()) {
                                    System.out.println("Directory created");
                                } else {
                                    System.out.println("Directory is not created");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        File dirPromotoria = new File(Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_foto)) + File.separator);
                        /* SINO EXISTE LA TABLA DE DESCARGAS LA CREAMOS */
                        if (!dirPromotoria.exists()) {
                            try {
                                if (dirPromotoria.mkdir()) {
                                    System.out.println("Directory created");
                                } else {
                                    System.out.println("Directory is not created");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        File dirFotoProd = new File(Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_img_productos)) + File.separator);
                        /* SINO EXISTE LA TABLA DE DESCARGAS LA CREAMOS */
                        if (!dirFotoProd.exists()) {
                            try {
                                if (dirFotoProd.mkdir()) {
                                    System.out.println("Directory created");
                                } else {
                                    System.out.println("Directory is not created");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        // Permission Denied
                        Toast.makeText(LOGIN, "Para poder usar la aplicación debes dar los permisos, podras agregarlos manualmente", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                        //Intent ingresar = new Intent(LOGIN, Saldo.class);
                        //startActivity(ingresar);
                        //finish();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /**
     * HILO PARA OBTENER LOS DATOS DE LA SINC INICIAL
     **/
    private class SincInicial extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... args) {

            String responseString, strResult = null, outputString = "";
            URL url;
            JSONObject obj = null;
            URLConnection connection = null;
            OutputStream out;
            InputStreamReader isr;
            SAXBuilder sxBuild = new SAXBuilder();
            Document doc;
            Element rootNode;
            rootNode = null;
            String salida = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:SiteControllerwsdl\">" +
                    "   <soapenv:Header/>" +
                    "   <soapenv:Body>" +
                    "      <urn:getDatos soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                    "         <usuario xsi:type=\"xsd:string\">" + usuario.getUsername() + "</usuario>" +
                    "         <password xsi:type=\"xsd:string\">" + usuario.getPwd() + "</password>" +
                    "         <tipo xsi:type=\"xsd:string\">2</tipo>" +
                    "      </urn:getDatos>" +
                    "   </soapenv:Body>" +
                    "</soapenv:Envelope>";

            try {
                url = new URL(EndPoints.SupervisionURL);
                connection = url.openConnection();
                connection.setConnectTimeout(10000000);
                connection.setReadTimeout(10000000);
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(10000000);
            httpConn.setReadTimeout(10000000);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buffer = salida.getBytes();
            if (httpConn != null) {
                try {
                    bout.write(buffer);
                    byte[] b = bout.toByteArray();
                    httpConn.setRequestProperty("Contenedor-Length", String.valueOf(b.length));
                    httpConn.setRequestProperty("Contenedor-Type", "text/xml; charset=utf-8");
                    httpConn.setRequestProperty("SOAPAction", EndPoints.SoapAction);
                    httpConn.setRequestMethod("POST");
                    httpConn.setDoOutput(false);
                    httpConn.setDoInput(true);
                    out = httpConn.getOutputStream();
                    out.write(b);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Read the response.
            try {
                isr = new InputStreamReader(connection.getInputStream());

                BufferedReader in = new BufferedReader(isr);
                while ((responseString = in.readLine()) != null) {
                    outputString = outputString + responseString;
                }
            } catch (IOException e) {
                e.printStackTrace();
                problema = e.getMessage();
            }


            try {
                if (outputString != null) {
                    doc = sxBuild.build(new StringReader(outputString));
                    rootNode = doc.getRootElement();
                }
            } catch (JDOMException ex) {
                ex.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (rootNode != null) {
                List lista = rootNode.getChildren();
                for (int i = 0; i < lista.size(); i++) {
                    rootNode = (Element) lista.get(i);
                    lista = rootNode.getChildren();
                    for (int j = 0; j < lista.size(); j++) {
                        rootNode = (Element) lista.get(i);
                        lista = rootNode.getChildren();
                        rootNode = (Element) lista.get(0);
                        strResult = rootNode.getText();
                    }
                }

                try {
                    obj = new JSONObject(strResult);
                } catch (Throwable t) {
                    problema = strResult;
                    Log.e("PROBLEMA", problema);
                    t.printStackTrace();
                }
            }

            return obj;
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            if (jsonObject != null) {

                /* INSERTAMOS UN CAMPO EN LA TABLA DISPOSITIVO CON LOS DATOS DE LA TABLETA Y EL USUARIO QUE LA UTILIZA */
                InformacionDispositivo info = new InformacionDispositivo();
                DispositivoBDModel dispositivoBDModel;
                DispositivoBDHelper dispositivoBDHelper = new DispositivoBDHelper(Login.this);
                dispositivoBDModel = info.getInfromacion(Login.this);

                long idDispositivo = dispositivoBDHelper.insertarDispositivo(dispositivoBDModel.getNoSerie(), dispositivoBDModel.getImei(), dispositivoBDModel.getSim(), dispositivoBDModel.getModelo(), dispositivoBDModel.getAndroid(), dispositivoBDModel.getVersion(), usuario.getIdUsuario(), udn.getIdUdn(), Constantes.ESTATUS.CREADO_MOVIL, usuario.getUsername());

                DispositivoEventualidadBDHelper dispositivoEventualidadBDHelper = new DispositivoEventualidadBDHelper(Login.this);
                InformacionDispositivo informacionDispositivo = new InformacionDispositivo();
                dispositivoEventualidadBDHelper.insertarDispositivoEventualidad((int) idDispositivo, usuario.getIdUsuario(), Constantes.TIPO_EVENTUALIDAD.TIPO_SINCRONIZACION_INICIAL, String.valueOf(informacionDispositivo.getBatteryLevel(Login.this)), Constantes.ESTATUS.CREADO_MOVIL, usuario.getUsername());

                progressDialog.setMessage("Cargando Datos...");
                InsertDB insertDB = new InsertDB();
                insertDB.execute(jsonObject);

            } else {
                if (problema.isEmpty()) problema = "La conexión a internet tardo mucho tiempo.";
                Toast.makeText(Login.this, problema, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }
    }

    /**
     * HILO PARA INSERTAR LOS DATOS DE LA SINC INICIAL EN LA BD SQLITE
     **/
    private class InsertDB extends AsyncTask<JSONObject, Void, Boolean> {

        //GPSService gpsService = new GPSService();
        //Intent iService = new Intent(Login.this, GPSService.class);

        @Override
        protected Boolean doInBackground(JSONObject... args) {

            DBHelper dbHelper = new DBHelper(Login.this, null, Constantes.VERSION_BASE_DE_DATOS);
            try {
                dbHelper.insertDinamicTableInicial(args[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            /*
            try {
                if (isMyServiceRunning(gpsService.getClass())) {
                    gpsService.stopSelf();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } */

            return true;
        }

        @Override
        protected void onPostExecute(Boolean response) {

            /*
            if (!isMyServiceRunning(gpsService.getClass())) {
                gpsService.startService(iService);
            } */

            RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(Login.this);
            registroAplicacionBDHelper.updateSincInicial(SINC_INICIAL_HECHA);

            // Obtenemos las imagenes de los productos desde el ws
            getImagesName();

            progressDialog.dismiss();

        }
    }


    /**
     * METODO PARA OBTENER LA LISTA DE IMAGENES DISPONIBLES EN EL WS Y COMPARARLA CON LAS IMAGENES QUE TENEMOS EN LA CARPETA DE PRODUCTOS
     */
    private void getImagesName() {

        ConfigWsRetrofit peticionesWs = new ConfigWsRetrofit();
        Retrofit retrofit = peticionesWs.getRetrofitParamsRest();
        WSInteractor wsInteractor = retrofit.create(WSInteractor.class);

        Call<List<String>> call = wsInteractor.getImagesName();
        call.enqueue(new retrofit2.Callback<List<String>>() {

            @Override
            public void onResponse(Call<List<String>> call, final Response<List<String>> response) {
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            Imagenes imagenes = new Imagenes();
                            imagenes.compareFiles(Login.this, response.body(), usuario);
                        } else {
                            startActivity(usuario, Login.this);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                Log.v(error_ws, t.getMessage());
                startActivity(usuario, Login.this);
            }
        });

    }


    /**
     * METODO PARA INICIAR LA PANTALLA DE DASHBOARD
     *
     * @param usuario USUARIO QUE INICIO SESION EN LA APLICACION
     * @param ctx     CONTEXTO DE LA ACTIVIDAD
     */
    public void startActivity(Usuario usuario, Context ctx) {

        //sendBD();
        Intent i = new Intent(ctx, GPSService.class);
        ctx.startService(i);

        Intent dashboard = new Intent(ctx, Dashboard.class);
        dashboard.putExtra("Usuario", createUsuario(usuario));
        ctx.startActivity(dashboard);
        ((Activity) ctx).finish();

    }


    /**
     * METODO PARA OBTENER EL MANDAR USUARIO A LAS OTRAS ACTIVITIES
     *
     * @param usuario
     * @return
     */
    private corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario createUsuario(Usuario usuario) {
        corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario user = new corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario();
        user.setId(usuario.getIdUsuario());
        user.setUdn(usuario.getIdUdn());
        user.setId_jefe(usuario.getIdUsuarioJefe());
        user.setNombre(usuario.getNombre());
        user.setEmail(usuario.getEmail());
        user.setUsername(usuario.getUsername());
        user.setRol(usuario.getRol());
        user.setCambio_password(usuario.getCambioPwd());
        user.setHash_reset(usuario.getHashReset());
        user.setIntentos(usuario.getIntentos());
        user.setFecha_error((usuario.getFechaError() == null ? "" : Dates.createDateToString(usuario.getFechaError())));
        user.setFecha_password(Dates.createDateToString(usuario.getFechaPwd()));
        user.setNivel(usuario.getNivel());
        user.setPassword(usuario.getPwd());
        user.setFecha_creacion(Dates.createDateToString(usuario.getFechaCreacion()));
        user.setUltima_modificacion(Dates.createDateToString(usuario.getUltimaModificacion()));
        user.setUsuario(usuario.getUsuario());
        user.setEstatus(usuario.getEstatus());
        return user;
    }


    /**
     * Método para enviar la base de datos cada hora
     */
    private void sendBD() {

        JobInfo job = new JobInfo.Builder(JOB_ID, new ComponentName(LOGIN, JobScheduleBDBackup.class))
                .setPeriodic(36000000) //1 hora
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setRequiresCharging(true)
                .setPersisted(true)
                .build();

        jobScheduler.schedule(job);
    }

}
