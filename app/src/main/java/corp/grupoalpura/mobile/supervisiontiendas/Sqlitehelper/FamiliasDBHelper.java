package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProdFamilia;

public class FamiliasDBHelper extends DBHelper {


    public FamiliasDBHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public ArrayList<ProdFamilia> getFamilias() {

        ArrayList<ProdFamilia> familias = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_FAMILIA, FAMILIA, ESTATUS  FROM PRODUCTO_FAMILIA " +
                "WHERE ESTATUS = 1 " +
                "AND ID_FAMILIA != 12 " +
                "AND ID_FAMILIA != 22 " +
                "AND ID_FAMILIA != 23 " +
                "AND ID_FAMILIA != 11 " +
                "AND ID_FAMILIA != 25 " +
                "AND ID_FAMILIA != 21 " +
                "AND ID_FAMILIA != 31 " +
                "AND ID_FAMILIA != 32 " +
                "AND ID_FAMILIA != 33 " +
                "AND ID_FAMILIA != 34 " +
                "ORDER BY FAMILIA", null);

        if (cursor.moveToFirst()) {
            do {
                ProdFamilia familia = new ProdFamilia();
                familia.setId(cursor.getInt(0));
                familia.setFamilia(cursor.getString(1));
                familia.setEstatus(cursor.getInt(2));

                familias.add(familia);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return familias;
    }

    public ArrayList<ProdFamilia> getFamiliasEspacio() {

        ArrayList<ProdFamilia> familias = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_FAMILIA, FAMILIA, ESTATUS  FROM PRODUCTO_FAMILIA " +
                "WHERE ESTATUS = 1 AND ORDENAMIENTO  = 1 " +
                "ORDER BY FAMILIA", null);

        if (cursor.moveToFirst()) {
            do {
                ProdFamilia familia = new ProdFamilia();
                familia.setId(cursor.getInt(0));
                familia.setFamilia(cursor.getString(1));
                familia.setEstatus(cursor.getInt(2));

                familias.add(familia);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return familias;
    }


}
