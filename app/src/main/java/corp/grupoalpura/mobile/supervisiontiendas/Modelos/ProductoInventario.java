package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

public class ProductoInventario {

    private int idProductoInventario;
    private int idCatInv;
    private  int idSucursal;
    private int idProducto;
    private int cantidad;
    private int estatus;

    public int getIdProductoInventario() {
        return idProductoInventario;
    }

    public void setIdProductoInventario(int idProductoInventario) {
        this.idProductoInventario = idProductoInventario;
    }

    public int getIdCatInv() {
        return idCatInv;
    }

    public void setIdCatInv(int idCatInv) {
        this.idCatInv = idCatInv;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
