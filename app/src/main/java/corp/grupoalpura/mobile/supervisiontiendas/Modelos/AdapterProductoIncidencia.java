package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 17/07/16.
 */
public class AdapterProductoIncidencia {
    private int id_producto_incidencia;
    private int id_categoria_incidencia;
    private String categoria;
    private int tipo_incidencia;
    private String descripcion;
    private String fecha;
    private int estatus_dos;

    private int id_producto;
    private String clave;
    private String producto;

    AdapterProductoIncidencia(){}

    public AdapterProductoIncidencia(int id_producto_incidencia, int id_categoria_incidencia, String categoria, int tipo_incidencia, String descripcion, String fecha, int estatus_dos) {
        this.id_producto_incidencia = id_producto_incidencia;
        this.id_categoria_incidencia = id_categoria_incidencia;
        this.categoria = categoria;
        this.tipo_incidencia = tipo_incidencia;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.estatus_dos = estatus_dos;
    }


    public AdapterProductoIncidencia(int id_producto_incidencia, int id_categoria_incidencia, int id_producto, String clave, String producto, String categoria, int tipo_incidencia, String descripcion, String fecha, int estatus_dos) {
        this.id_producto_incidencia = id_producto_incidencia;
        this.id_categoria_incidencia = id_categoria_incidencia;
        this.id_producto = id_producto;
        this.clave = clave;
        this.producto = producto;
        this.categoria = categoria;
        this.tipo_incidencia = tipo_incidencia;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.estatus_dos = estatus_dos;
    }

    public int getId_producto_incidencia() {
        return id_producto_incidencia;
    }

    public void setId_producto_incidencia(int id_producto_incidencia) {
        this.id_producto_incidencia = id_producto_incidencia;
    }

    public int getId_categoria_incidencia() {
        return id_categoria_incidencia;
    }

    public void setId_categoria_incidencia(int id_categoria_incidencia) {
        this.id_categoria_incidencia = id_categoria_incidencia;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getTipo_incidencia() {
        return tipo_incidencia;
    }

    public void setTipo_incidencia(int tipo_incidencia) {
        this.tipo_incidencia = tipo_incidencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }
}
