package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 12/04/2016.
 */
public class AdapterDatosGenerales {
    private int id_personal;
    private int id_puesto;
    private String cliente;
    private int id_sucursal;
    private String sucursal;
    private String domicilio;
    private String municipio;
    private int id_sucursal_supervisor;
    private String hora_inicio;
    private String hora_fin;
    private String dia;
    private String nombre_personal;
    private String ap_paterno_personal;
    private String ap_materno_personal;
    private String puesto;
    private String email;
    private String clave;
    private String fecha_contratacion;
    private int  estatus_dos;


    public AdapterDatosGenerales(String cliente, int id_sucursal, String sucursal, String domicilio, String municipio) {
        this.cliente = cliente;
        this.id_sucursal = id_sucursal;
        this.sucursal = sucursal;
        this.domicilio = domicilio;
        this.municipio = municipio;
    }

    public AdapterDatosGenerales(int id_sucursal_supervisor, String hora_inicio, String hora_fin, String dia) {
        this.id_sucursal_supervisor = id_sucursal_supervisor;
        this.hora_inicio = hora_inicio;
        this.hora_fin = hora_fin;
        this.dia = dia;
    }

    public AdapterDatosGenerales(int id_personal, int id_puesto, String puesto, String clave, String nombre_personal, String ap_paterno_personal, String ap_materno_personal, String email, String fecha_contratacion, int estatus_dos) {
        this.id_personal = id_personal;
        this.id_puesto = id_puesto;
        this.puesto = puesto;
        this.clave = clave;
        this.nombre_personal = nombre_personal;
        this.ap_paterno_personal = ap_paterno_personal;
        this.ap_materno_personal = ap_materno_personal;
        this.email = email;
        this.fecha_contratacion = fecha_contratacion;
        this.estatus_dos = estatus_dos;
    }

    public int getId_sucursal_supervisor() {
        return id_sucursal_supervisor;
    }

    public int getId_personal() {
        return id_personal;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getHora_inicio() {
        return hora_inicio;
    }

    public String getHora_fin() {
        return hora_fin;
    }

    public String getDia() {
        return dia;
    }

    public String getNombre_personal() {
        return nombre_personal;
    }

    public String getAp_paterno_personal() {
        return ap_paterno_personal;
    }

    public String getAp_materno_personal() {
        return ap_materno_personal;
    }

    public String getPuesto() {
        return puesto;
    }

    public String getEmail() {
        return email;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getFecha_contratacion() {
        return fecha_contratacion;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }
}
