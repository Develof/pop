package corp.grupoalpura.mobile.supervisiontiendas;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Application.PopApplication;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Database.DBPop;
import corp.grupoalpura.mobile.supervisiontiendas.Interactor.WSInteractor;
import corp.grupoalpura.mobile.supervisiontiendas.LocalizacionGps.Localizacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.RegistroAplicacionBDModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ViewFactInventory;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Visita;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DispositivoBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DispositivoEventualidadBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.RegistroAplicacionBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.SucursalBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.ConfigWsRetrofit;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Distancia;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.InformacionDispositivo;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Singleton;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.williamww.silkysignature.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class VisitaCheckInFragment extends Fragment implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback {

    public GoogleMap googleMap;
    MapView mapView;
    Usuario usuario = null;
    Visita visita = null;
    RelativeLayout relativeLayoutMapa;
    DBHelper dbHelper;
    DispositivoBDHelper dispositivoBDHelper;
    RegistroAplicacionBDHelper registroAplicacionBDHelper;
    SucursalBDHelper sucursalBDHelper;
    double latitud = 0, longitud = 0;
    int tipoVisita, sucursalTotal = 0;
    ClienteSucursal sucursal = null;
    boolean isRutaAleatoriaActiva;
    Marker marker;
    View viewMarker;
    Localizacion localizacion;

    public static String USUARIO = "Usuario";
    public static String TIPO_VISITA = "TipoVisitaKey";

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";

    RegistroAplicacionBDModel registroAplicacion;
    public static int CHECK_IN_ACTIVO = 1;
    private static final int MAX_UBICACION = 150;

    private com.williamww.silkysignature.views.SignaturePad signaturePad;
    private ImageView imgClear;


    @BindView(R.id.checkIn_btn_check)
    Button btnCheckIn;

    public VisitaCheckInFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment VisitaCheckInFragment.
     */
    public VisitaCheckInFragment newInstance(Usuario user, int tipoVisita) {
        VisitaCheckInFragment fragment = new VisitaCheckInFragment();
        Bundle args = new Bundle();
        args.putSerializable(USUARIO, user);
        args.putInt(TIPO_VISITA, tipoVisita);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usuario = (Usuario) getArguments().getSerializable(USUARIO);
            tipoVisita = getArguments().getInt(TIPO_VISITA);
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_visita_check_in, container, false);
        ButterKnife.bind(this, view);


        RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
        registroAplicacion = registroAplicacionBDHelper.getRegistroAplicacion();
        if (registroAplicacion.getNo_sucursal() <= sucursalTotal) {

            Calendar calendar = Calendar.getInstance();
            int dia = calendar.get(Calendar.DAY_OF_WEEK) - 1;
            if (dia == 0) dia = 7;

            sucursalTotal = dbHelper.getSucursalesTotal(dia);
            isRutaAleatoriaActiva = dbHelper.getRutaAlternaActiva(dia);

            SucursalBDHelper sucursalBDHelper = new SucursalBDHelper(getActivity());
            if (isRutaAleatoriaActiva) {
                sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD", registroAplicacion.getNo_sucursal());
            } else {
                sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD_SUGERIDA", registroAplicacion.getNo_sucursal());
            }
        }


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        width = width - 120;

        viewMarker = inflater.inflate(R.layout.adapter_marker_vista, container, false);
        viewMarker.setLayoutParams(new RelativeLayout.LayoutParams(width, RelativeLayout.LayoutParams.WRAP_CONTENT));


        relativeLayoutMapa = (RelativeLayout) view.findViewById(R.id.contenedorMapa);
        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        dispositivoBDHelper = new DispositivoBDHelper(getActivity());
        registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
        sucursalBDHelper = new SucursalBDHelper(getActivity());
        mapView = (MapView) view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        btnCheckIn.setOnClickListener(v -> {
            if (sucursal != null) {
                if (localizacion.connect()) {
                    localizacion.getLocalizacion();
                    latitud = localizacion.getLatitud();
                    longitud = localizacion.getLongitud();
                }
                //OBTENEMOS LA ULTIMA VISITA
                visita = dbHelper.getVisita(usuario.getId());


                DispositivoEventualidadBDHelper dispositivoEventualidadBDHelper = new DispositivoEventualidadBDHelper(getActivity());
                InformacionDispositivo informacionDispositivo = new InformacionDispositivo();

                long idDispositivo = dispositivoBDHelper.getIdDispositivo();
                dispositivoEventualidadBDHelper.insertarDispositivoEventualidad((int) idDispositivo, usuario.getId(), sucursal.getAlias(), String.valueOf(informacionDispositivo.getBatteryLevel(getActivity())), Constantes.ESTATUS.CREADO_MOVIL, usuario.getUsername());

                if (visita == null) {

                    if (sucursal.getLocalizacion_activa() == Constantes.SUCURSAL_LOCALIZACION_ACTIVA.INACTIVA) {

                        final Dialog dialogNuevaLocalizacion = new Dialog(getActivity());
                        dialogNuevaLocalizacion.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogNuevaLocalizacion.setContentView(R.layout.dialog_sucursal_localizacion);
                        dialogNuevaLocalizacion.setCancelable(false);

                        TextView textAceptar = (TextView) dialogNuevaLocalizacion.findViewById(R.id.btn_aceptar_msg);
                        TextView textCancelar = (TextView) dialogNuevaLocalizacion.findViewById(R.id.btn_aceptar_msg_cancelar);
                        TextView txtTituloLocalizacion = (TextView) dialogNuevaLocalizacion.findViewById(R.id.textview_title_mensaje_alerta);

                        txtTituloLocalizacion.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        textAceptar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        textCancelar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

                        textCancelar.setOnClickListener(v1 -> dialogNuevaLocalizacion.dismiss());

                        textAceptar.setOnClickListener(v12 -> {

                            if (latitud != 0 && longitud != 0) {
                                sucursalBDHelper.updateLocalizacionSucursal(sucursal.getId(), latitud, longitud);
                            } else {
                                Toast.makeText(getActivity(), "No se pudieron obtener las coordenadas se intentara obtener la proxima ves", Toast.LENGTH_LONG).show();
                            }

                            final Dialog dialogFirma = new Dialog(getActivity());
                            dialogFirma.setContentView(R.layout.dialog_firma);
                            dialogFirma.show();

                            imgClear = (ImageView) dialogFirma.findViewById(R.id.imageView_firma_clear);
                            TextView txtSignature = (TextView) dialogFirma.findViewById(R.id.btn_aceptar_firma);
                            signaturePad = (SignaturePad) dialogFirma.findViewById(R.id.signature);

                            imgClear.setOnClickListener(v1212 -> signaturePad.clear());


                            txtSignature.setOnClickListener(v121 -> {

                                Bitmap signature = signaturePad.getSignatureBitmap();
                                startVisit(signature);
                            });
                        });

                        dialogNuevaLocalizacion.show();

                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = dialogNuevaLocalizacion.getWindow();
                        lp.copyFrom(window.getAttributes());

                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    } else {

                        Distancia distancia = new Distancia();
                        Double mtsDistancia = distancia.distanciaCoord(latitud, longitud, sucursal.getLatitud(), sucursal.getLongitud());

                        if (mtsDistancia <= MAX_UBICACION) {

                            final Dialog dialogFirma = new Dialog(getActivity());
                            dialogFirma.setContentView(R.layout.dialog_firma);
                            dialogFirma.show();

                            imgClear = (ImageView) dialogFirma.findViewById(R.id.imageView_firma_clear);
                            TextView txtSignature = (TextView) dialogFirma.findViewById(R.id.btn_aceptar_firma);
                            signaturePad = (SignaturePad) dialogFirma.findViewById(R.id.signature);

                            imgClear.setOnClickListener(v12 -> signaturePad.clear());


                            txtSignature.setOnClickListener(v12 -> {

                                Bitmap signature = signaturePad.getSignatureBitmap();
                                startVisit(signature);
                            });

                        } else {

                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Check In")
                                    .setMessage("Tu ubicación actual es: " + roundTwoDecimal(mtsDistancia) + " mts de la tienda. ¿Deseas continuar?")
                                    .setCancelable(false)
                                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    })
                                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            final Dialog dialogFirma = new Dialog(getActivity());
                                            dialogFirma.setContentView(R.layout.dialog_firma);
                                            dialogFirma.show();

                                            imgClear = (ImageView) dialogFirma.findViewById(R.id.imageView_firma_clear);
                                            TextView txtSignature = (TextView) dialogFirma.findViewById(R.id.btn_aceptar_firma);
                                            signaturePad = (SignaturePad) dialogFirma.findViewById(R.id.signature);

                                            imgClear.setOnClickListener(v12 -> signaturePad.clear());


                                            txtSignature.setOnClickListener(v12 -> {

                                                Bitmap signature = signaturePad.getSignatureBitmap();
                                                startVisit(signature);
                                            });
                                        }
                                    })
                                    .show();
                        }
                    }

                } else {
                    // AQUI IRA EL CODIGO PARA SABER SI LA VISITA ESTA EN CHEKIN O EN CHECKOUT
                    //SI NO HAY CHECKOUT QUE REGISTRAR PODRA HACER UN NUEVO CHECKIN
                    Distancia distancia = new Distancia();

                    if (sucursal.getLocalizacion_activa() == Constantes.SUCURSAL_LOCALIZACION_ACTIVA.INACTIVA) {
                        final Dialog dialogNuevaLocalizacion = new Dialog(getActivity());
                        dialogNuevaLocalizacion.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogNuevaLocalizacion.setContentView(R.layout.dialog_sucursal_localizacion);

                        TextView textAceptar = (TextView) dialogNuevaLocalizacion.findViewById(R.id.btn_aceptar_msg);
                        TextView textCancelar = (TextView) dialogNuevaLocalizacion.findViewById(R.id.btn_aceptar_msg_cancelar);
                        TextView txtTituloLocalizacion = (TextView) dialogNuevaLocalizacion.findViewById(R.id.textview_title_mensaje_alerta);

                        txtTituloLocalizacion.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        textAceptar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        textCancelar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

                        textCancelar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogNuevaLocalizacion.dismiss();
                            }
                        });

                        textAceptar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (latitud != 0 && longitud != 0) {
                                    sucursalBDHelper.updateLocalizacionSucursal(sucursal.getId(), latitud, longitud);
                                } else {
                                    Toast.makeText(getActivity(), "No se pudieron obtener las coordenadas se intentara obtener la proxima ves", Toast.LENGTH_LONG).show();
                                }

                                final Dialog dialogFirma = new Dialog(getActivity());
                                dialogFirma.setContentView(R.layout.dialog_firma);
                                dialogFirma.show();

                                imgClear = (ImageView) dialogFirma.findViewById(R.id.imageView_firma_clear);
                                TextView txtSignature = (TextView) dialogFirma.findViewById(R.id.btn_aceptar_firma);
                                signaturePad = (SignaturePad) dialogFirma.findViewById(R.id.signature);

                                imgClear.setOnClickListener(v12 -> signaturePad.clear());


                                txtSignature.setOnClickListener(v12 -> {

                                    Bitmap signature = signaturePad.getSignatureBitmap();
                                    startVisit(signature);
                                });
                            }
                        });

                        dialogNuevaLocalizacion.show();

                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = dialogNuevaLocalizacion.getWindow();
                        lp.copyFrom(window.getAttributes());

                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    } else {
                        if (!visita.getHora_check_fin().equals("")) {

                            final Dialog dialogFirma = new Dialog(getActivity());
                            dialogFirma.setContentView(R.layout.dialog_firma);
                            dialogFirma.show();

                            imgClear = (ImageView) dialogFirma.findViewById(R.id.imageView_firma_clear);
                            TextView txtSignature = (TextView) dialogFirma.findViewById(R.id.btn_aceptar_firma);
                            signaturePad = (SignaturePad) dialogFirma.findViewById(R.id.signature);

                            imgClear.setOnClickListener(v12 -> signaturePad.clear());


                            txtSignature.setOnClickListener(v12 -> {

                                Bitmap signature = signaturePad.getSignatureBitmap();
                                startVisit(signature);
                            });

                        } else {
                            Snackbar.make(relativeLayoutMapa, "YA HAS HECHO CHECK IN EN ESTA SUCURSAL", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (permissions.length == 1 && permissions[0].equals("android.permission.ACCESS_FINE_LOCATION") && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(true);
                    UiSettings settings = googleMap.getUiSettings();

                    if (settings != null)
                        googleMap.getUiSettings().setMapToolbarEnabled(false);

                    if (marker != null) {
                        marker.remove();
                    }

                    RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
                    registroAplicacion = registroAplicacionBDHelper.getRegistroAplicacion();
                    if (registroAplicacion.getNo_sucursal() <= sucursalTotal) {
                        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(sucursal.getLatitud(), sucursal.getLongitud()));
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                        googleMap.moveCamera(center);
                        googleMap.animateCamera(zoom);

                        marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(sucursal.getLatitud(), sucursal.getLongitud())).icon(BitmapDescriptorFactory.fromResource(R.drawable.tienda)));
                        marker.showInfoWindow();
                    } else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("SupervisionTiendas")
                                .setMessage("Has completado toda tu ruta")
                                .show();
                    }


                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            if (marker != null) {
                                marker.showInfoWindow();
                            }
                        }
                    });
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {

        //googleMap = mapView.getMap();
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setOnMarkerClickListener(this);

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {

                // Getting view from the layout file info_window_layout
                Calendar c = Calendar.getInstance();
                TextView nombre_sucursal = (TextView) viewMarker.findViewById(R.id.textView_title_nombre_princial_sucursal);
                TextView direccion = (TextView) viewMarker.findViewById(R.id.textView_info_direccion);
                TextView hora_visita = (TextView) viewMarker.findViewById(R.id.textView_info_hora);

                hora_visita.setText(String.format("%02d", c.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", c.get(Calendar.MINUTE)));
                nombre_sucursal.setText(sucursal.getAlias());
                direccion.setText(sucursal.getCalle() + " " + sucursal.getNo_ext() + " " + sucursal.getColonia());

                return viewMarker;

            }
        });

        // SI LA VERSION ES 6.0 O MAYOR PERDIREMOS EL PERMISO PARA ACTIVAR GPS LA PRIMERA VES QUE ENTRA A LA PANTALLA, SINO ACTIVAMOS EL GSP SIN PEDIR PERMISO
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            googleMap.setMyLocationEnabled(true);
            UiSettings settings = googleMap.getUiSettings();

            if (settings != null)
                googleMap.getUiSettings().setMapToolbarEnabled(false);

            if (marker != null) {
                marker.remove();
            }

            RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
            registroAplicacion = registroAplicacionBDHelper.getRegistroAplicacion();
            if (registroAplicacion.getNo_sucursal() <= sucursalTotal) {

                Calendar calendar = Calendar.getInstance();
                int dia = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                if (dia == 0) dia = 7;

                sucursalTotal = dbHelper.getSucursalesTotal(dia);
                isRutaAleatoriaActiva = dbHelper.getRutaAlternaActiva(dia);

                SucursalBDHelper sucursalBDHelper = new SucursalBDHelper(getActivity());
                if (isRutaAleatoriaActiva) {
                    sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD", registroAplicacion.getNo_sucursal());
                } else {
                    sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD_SUGERIDA", registroAplicacion.getNo_sucursal());
                }

                if (sucursal != null) {

                    marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(sucursal.getLatitud(), sucursal.getLongitud())).icon(BitmapDescriptorFactory.fromResource(R.drawable.tienda)));
                    marker.showInfoWindow();

                    CameraUpdate center = CameraUpdateFactory.newLatLngZoom(new LatLng(sucursal.getLatitud(), sucursal.getLongitud()), 14);
                    googleMap.animateCamera(center, 500, new GoogleMap.CancelableCallback() {
                        @Override
                        public void onFinish() {
                            marker.showInfoWindow();
                        }

                        @Override
                        public void onCancel() {
                            marker.showInfoWindow();
                        }
                    });

                } else {

                    new AlertDialog.Builder(getActivity())
                            .setTitle("SUPERVISIÓN TIENDAS")
                            .setMessage("LA RUTA ACTUAL NO TIENE UN ORDENAMIENTO SUGERIDO. PIDE AL SUPERVISOR QUE GENERE EL ORDENAMIENTO O GENERA UNA RUTA ALTERNA EN EL MENU DE RUTA")
                            .setCancelable(false)
                            .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .show();
                }

            } else {
                new AlertDialog.Builder(getActivity())
                        .setTitle("SupervisionTiendas")
                        .setMessage("Has completado toda tu ruta")
                        .show();
            }


            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    if (marker != null) {
                        marker.showInfoWindow();
                    }
                }
            });
        }
    }


    private void startVisit(Bitmap bitmap) {

        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        String dir = Environment.getExternalStoragePublicDirectory("SupervisionTiendas") + "/";
        String dirNombre = "IMG_FIRMA_" + timeStamp + ".jpeg";
        final File photo = new File(dir, dirNombre);

        try {
            photo.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imgClear.setVisibility(View.INVISIBLE);

        try {
            FileOutputStream os = new FileOutputStream(photo);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 0, os);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        dbHelper.insertFoto(sucursal.getId(), photo.getAbsolutePath(), dirNombre, latitud, longitud, usuario.getUsername());
        getNotification();
        dbHelper.checkIn(sucursal.getId(), usuario.getId(), latitud, longitud, usuario.getUsername());
        RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
        registroAplicacionBDHelper.updateCheckIn(CHECK_IN_ACTIVO);

        getInventarios();
    }


    private void getInventarios() {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.ws_inv));
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        ConfigWsRetrofit peticionesWs = new ConfigWsRetrofit();
        Retrofit retrofit = peticionesWs.getRetrofitParamsRest();
        WSInteractor wsInteractor = retrofit.create(WSInteractor.class);

        //Dates dates = new Dates();
        //String fechaHoy = dates.dateToStringAlerta(new Date());

        Call<ViewFactInventory> call = wsInteractor.getInventoryRest(sucursal.getCliente(), null, sucursal.getClave());
        call.enqueue(new retrofit2.Callback<ViewFactInventory>() {

            @Override
            public void onResponse(Call<ViewFactInventory> call, final Response<ViewFactInventory> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    DBPop database = ((PopApplication) getActivity().getApplication()).database;
                    database.inventarioBODao().insertaInventario(response.body().getInventario());
                }
                Singleton.getInstance().setCs(sucursal);
                getActivity().finish();
            }

            @Override
            public void onFailure(Call<ViewFactInventory> call, Throwable t) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_ws_inventarios), Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
                Singleton.getInstance().setCs(sucursal);
                getActivity().finish();

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();


        Calendar calendar = Calendar.getInstance();
        int dia = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (dia == 0) dia = 7;


        RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
        registroAplicacion = registroAplicacionBDHelper.getRegistroAplicacion();
        sucursalTotal = dbHelper.getSucursalesTotal(dia);
        isRutaAleatoriaActiva = dbHelper.getRutaAlternaActiva(dia);

        SucursalBDHelper sucursalBDHelper = new SucursalBDHelper(getActivity());
        if (isRutaAleatoriaActiva) {
            sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD", registroAplicacion.getNo_sucursal());
        } else {
            sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD_SUGERIDA", registroAplicacion.getNo_sucursal());
        }

        localizacion = new Localizacion(getActivity());
        mapView.getMapAsync(this);

        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }


    private void getNotification() {

        Date dateHoy = new Date();
        String fechahoy = Dates.dateToStringAlerta(dateHoy);
        int listAlertas;

        Calendar day = Calendar.getInstance();
        int dia = day.get(Calendar.DAY_OF_WEEK) - 1;
        int dia_mes = day.get(Calendar.DAY_OF_MONTH);

        if (dia_mes == 1 || dia_mes == 2 || dia_mes == 3 || dia_mes == 4 || dia_mes == 5) {
            sendPushNotification("Recuerda capturar información");
        }

        if (dia == 1) {
            sendPushNotification("Recuerda revisar guia de mercadeo hoy Lunes");
        } else if (dia == 5) {
            sendPushNotification("Recuerda revisar lista de precios hoy Viernes");
        }

        listAlertas = dbHelper.getAlertasSucursal(sucursal.getId(), fechahoy);
        if (listAlertas > 0) {

            String recordatorio;
            if (listAlertas == 1) {
                recordatorio = "recordatorio";
            } else {
                recordatorio = "recordatorios";
            }

            sendPushNotification("Tienes " + listAlertas + " " + recordatorio);

        }
    }


    double roundTwoDecimal(double value) {
        DecimalFormat twoForm = (DecimalFormat) NumberFormat.getNumberInstance();
        twoForm.applyPattern("#.##");
        return Double.valueOf(twoForm.format(value));
    }


    private void sendPushNotification(String mensaje) {
        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(getActivity(), Alertas.class);
        notificationIntent.putExtra(ID_SUCURSAL, sucursal);
        notificationIntent.putExtra(ID_USUARIO, usuario);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pending = PendingIntent.getActivity(getActivity(), 0, notificationIntent, 0);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity());
        Notification notification = builder.setContentIntent(pending)
                .setSmallIcon(R.mipmap.ic_app).setTicker("").setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(getActivity().getResources().getString(R.string.app_name))
                .setContentText(mensaje)
                .setVibrate(new long[]{8000, 8000, 8000, 8000, 8000})
                .build();

        notification.defaults |= Notification.DEFAULT_SOUND;
        notificationManager.notify(0, notification);
    }


}
