package corp.grupoalpura.mobile.supervisiontiendas;

import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import corp.grupoalpura.mobile.supervisiontiendas.Application.PopApplication;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Database.DBPop;
import corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto.Detalle;
import corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto.IncidenciasProducto;
import corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto.ProductoInventario;
import corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto.ProductoPrecioCompetencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.LoadImage;
import corp.grupoalpura.mobile.supervisiontiendas.view_pager.SlidingTabLayout;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class DetalleProducto extends AppCompatActivity {

    Toolbar toolbar;
    static ViewPager viewPager;
    SlidingTabLayout slidingTabLayout;
    ClienteSucursal sucursal;
    ImageView imgProducto;
    TextView txtProducto;
    TextView txtPrecioBase;
    double precio_real;

    static DBHelper dbHelper;
    Usuario usuario;
    private static FichaTecnicaProducto producto;

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String PRODUCTO = "producto";
    MyPagerAdapter adapter;
    DecimalFormat form = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_producto);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        form.applyPattern(Constantes.FORMATO_PRECIO);

        dbHelper = new DBHelper(this, null, Constantes.VERSION_BASE_DE_DATOS);
        viewPager = (ViewPager) findViewById(R.id.viewpager_detalle_producto);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.slidingTab_detalle_producto);
        imgProducto = (ImageView) findViewById(R.id.ivPhoto);
        txtProducto = (TextView) findViewById(R.id.productoName);
        txtPrecioBase = (TextView) findViewById(R.id.productoPrecio);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sucursal = (ClienteSucursal) getIntent().getSerializableExtra(ID_SUCURSAL);
        usuario = (Usuario) getIntent().getSerializableExtra(ID_USUARIO);
        producto = (FichaTecnicaProducto) getIntent().getSerializableExtra(PRODUCTO);

        if (usuario.getUsername() == null) {
            DBPop database = ((PopApplication) this.getApplication()).database;
            usuario = createUsuario(database.usuarioDao().getLastUsuario());
        }

        adapter = new MyPagerAdapter(getSupportFragmentManager());
        slidingTabLayout.setDistributeEvenly(true);
        viewPager.setAdapter(adapter);
        slidingTabLayout.setViewPager(viewPager);
        slidingTabLayout.setDistributeEvenly(true);

        precio_real = dbHelper.getPrecioUnitarioReal(producto.getClave());

        LoadImage loadImage = new LoadImage();
        String imageResource = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_img_productos)) + File.separator + producto.getClave().toLowerCase() + ".png";
        loadImage.load(DetalleProducto.this, imageResource, imgProducto);

        txtProducto.setText(producto.getDescripcion());
        txtPrecioBase.setText("Precio Lleno: $ " + form.format(precio_real));

    }

    class MyPagerAdapter extends FragmentPagerAdapter {

        String[] tabs;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.tabs);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = null;

            switch (position) {
                case 0:
                    fragment = Detalle.newInstance(sucursal, usuario, producto);
                    break;

                case 1:
                    fragment = ProductoPrecioCompetencia.newInstance(sucursal, usuario, producto);
                    break;

                case 2:
                    fragment = ProductoInventario.newInstance(sucursal, usuario, producto);
                    break;

                /*
                case 3:
                    fragment = IncidenciasProducto.newInstance(sucursal, usuario, producto);
                    break;*/

            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }


        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }


    private Usuario createUsuario(corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Usuario usuario) {
        corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario user = new corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario();
        user.setId(usuario.getIdUsuario());
        user.setUdn(usuario.getIdUdn());
        user.setId_jefe(usuario.getIdUsuarioJefe());
        user.setNombre(usuario.getNombre());
        user.setEmail(usuario.getEmail());
        user.setUsername(usuario.getUsername());
        user.setRol(usuario.getRol());
        user.setCambio_password(usuario.getCambioPwd());
        user.setHash_reset(usuario.getHashReset());
        user.setIntentos(usuario.getIntentos());
        user.setFecha_error((usuario.getFechaError() == null ? "" : Dates.createDateToString(usuario.getFechaError())));
        user.setFecha_password(Dates.createDateToString(usuario.getFechaPwd()));
        user.setNivel(usuario.getNivel());
        user.setPassword(usuario.getPwd());
        user.setFecha_creacion(Dates.createDateToString(usuario.getFechaCreacion()));
        user.setUltima_modificacion(Dates.createDateToString(usuario.getUltimaModificacion()));
        user.setUsuario(usuario.getUsuario());
        user.setEstatus(usuario.getEstatus());
        return user;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
