package corp.grupoalpura.mobile.supervisiontiendas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterAlertas;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterTipoAlerta;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Alertas extends AppCompatActivity {

    private Toolbar toolbar;
    ListView listViewAlertas;
    DBHelper dbHelper;
    CustomAdapter adapter;
    CoordinatorLayout coordinatorAlerta;

    ClienteSucursal sucursal;
    Usuario usuario;
    Date updateFecha;
    ArrayList<AdapterAlertas> alertas;
    ArrayList<AdapterTipoAlerta> tipoAlertas = new ArrayList<>();
    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    String fecha = "";
    int nTipoAlerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listViewAlertas = (ListView) findViewById(R.id.listView_alertas);
        coordinatorAlerta = (CoordinatorLayout) findViewById(R.id.cordinator_alerta);

        sucursal = (ClienteSucursal) getIntent().getSerializableExtra("id_sucursal");
        usuario = (Usuario) getIntent().getSerializableExtra("Usuario");

        dbHelper = new DBHelper(this, null, Constantes.VERSION_BASE_DE_DATOS);
        alertas = dbHelper.getAlertas(sucursal.getId());

        listViewAlertas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                TextView txtDescripcion = (TextView) view.findViewById(R.id.textView_alerta_alerta_value);
                final TextView id_alerta = (TextView) view.findViewById(R.id.textView_alerta_id);
                final TextView fecha = (TextView) view.findViewById(R.id.textView_alerta_fecha_value);

                final Dialog dialogMensajeAlerta = new Dialog(Alertas.this);
                dialogMensajeAlerta.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogMensajeAlerta.setContentView(R.layout.dialog_mensaje_alerta);

                TextView txtTituloTipo = (TextView) dialogMensajeAlerta.findViewById(R.id.textview_title_mensaje_alerta);
                TextView txtMensajeAlerta = (TextView) dialogMensajeAlerta.findViewById(R.id.textView_seleccionaFecha_notificacion);
                TextView txtFecha = (TextView) dialogMensajeAlerta.findViewById(R.id.textview_title_mensaje_alerta_fecha);
                TextView txtAceptar = (TextView) dialogMensajeAlerta.findViewById(R.id.btn_aceptar_espacio);
                final CheckBox cbLeido = (CheckBox) dialogMensajeAlerta.findViewById(R.id.checkBox_leido);

                txtTituloTipo.setTextColor(ContextCompat.getColor(Alertas.this, R.color.colorPrimary));
                txtAceptar.setTextColor(ContextCompat.getColor(Alertas.this, R.color.colorPrimary));

                if (alertas.get(position).getLeida().equals("1")) {
                    cbLeido.setChecked(true);
                }

                txtTituloTipo.setText(alertas.get(position).getNombre());
                txtMensajeAlerta.setText(txtDescripcion.getText().toString());
                txtFecha.setText(fecha.getText().toString());

                txtAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertas = dbHelper.getAlertas(sucursal.getId());
                        adapter.notifyDataSetChanged();
                        dialogMensajeAlerta.dismiss();
                    }
                });

                cbLeido.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cbLeido.isChecked()) {

                            if (alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                dbHelper.updateAlertaLeido(Integer.parseInt(id_alerta.getText().toString()), String.valueOf(Constantes.MENSAJES.LEIDO), Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL, usuario.getUsername());
                            } else {
                                dbHelper.updateAlertaLeido(Integer.parseInt(id_alerta.getText().toString()), String.valueOf(Constantes.MENSAJES.LEIDO), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL, usuario.getUsername());
                            }

                        } else {

                            if (alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                dbHelper.updateAlertaLeido(Integer.parseInt(id_alerta.getText().toString()), String.valueOf(Constantes.MENSAJES.NO_LEIDO), Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL, usuario.getUsername());
                            } else {
                                dbHelper.updateAlertaLeido(Integer.parseInt(id_alerta.getText().toString()), String.valueOf(Constantes.MENSAJES.NO_LEIDO), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL, usuario.getUsername());
                            }
                        }
                    }
                });

                dialogMensajeAlerta.show();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialogMensajeAlerta.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });

        adapter = new CustomAdapter(this);
        listViewAlertas.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogCargaAlertas = new Dialog(Alertas.this);
                dialogCargaAlertas.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogCargaAlertas.setContentView(R.layout.activity_cargar_alertas);


                Button btnGuardarAlerta = (Button) dialogCargaAlertas.findViewById(R.id.button_cargarAlerta_guardar);
                Spinner spinnerTipoAlerta = (Spinner) dialogCargaAlertas.findViewById(R.id.spinnerTipoAlerta);
                ImageView imageViewDate = (ImageView) dialogCargaAlertas.findViewById(R.id.image_fecha);
                final TextView textViewFechaValue = (TextView) dialogCargaAlertas.findViewById(R.id.textView_carga_alerta_fecha_value);
                final EditText editTextAlerta = (EditText) dialogCargaAlertas.findViewById(R.id.editText_cargaAlert_descrpicion);

                btnGuardarAlerta.setTextColor(ContextCompat.getColor(Alertas.this, R.color.colorWhite));
                editTextAlerta.setImeOptions(EditorInfo.IME_ACTION_DONE);

                dbHelper = new DBHelper(Alertas.this, null, Constantes.VERSION_BASE_DE_DATOS);
                tipoAlertas = dbHelper.getTiposAlerta();
                ArrayAdapter<AdapterTipoAlerta> dataAdapterTipoAlerta = new ArrayAdapter<>(Alertas.this, android.R.layout.simple_spinner_item, tipoAlertas);
                dataAdapterTipoAlerta.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerTipoAlerta.setAdapter(dataAdapterTipoAlerta);

                imageViewDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Calendar c = Calendar.getInstance();
                        final int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);

                        final DatePickerDialog dpd = new DatePickerDialog(Alertas.this,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        Calendar date = Calendar.getInstance();
                                        date.set(year, monthOfYear, dayOfMonth);
                                        Date deliverDate = date.getTime();
                                        fecha = Dates.createDateToString(deliverDate);
                                        textViewFechaValue.setText(String.valueOf(String.format("%02d", dayOfMonth)) + " / " + String.format("%02d", monthOfYear + 1) + " / " + String.valueOf(year));
                                    }
                                }, mYear, mMonth, mDay);


                        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000 + (1000 * 60 * 60 * 24 * 1));
                        dpd.show();
                    }
                });


                spinnerTipoAlerta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        nTipoAlerta = tipoAlertas.get(position).getIdTipoAlerta();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                btnGuardarAlerta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (textViewFechaValue.getText().toString().equals("") || editTextAlerta.getText().toString().equals("")) {
                            Toast toast = Toast.makeText(Alertas.this, "Llena todos los campos", Toast.LENGTH_LONG);
                            TextView va = (TextView) toast.getView().findViewById(android.R.id.message);
                            va.setTextColor(Color.WHITE);
                            toast.show();
                        } else {
                            dbHelper.insertAlerta(sucursal.getId(), nTipoAlerta, editTextAlerta.getText().toString(), fecha, usuario.getUsername(), 1);
                            Toast toast = Toast.makeText(Alertas.this, "Se ha creado la alerta", Toast.LENGTH_LONG);
                            TextView va = (TextView) toast.getView().findViewById(android.R.id.message);
                            va.setTextColor(Color.WHITE);
                            toast.show();
                            Intent alertas = new Intent(Alertas.this, Alertas.class);
                            alertas.putExtra(ID_SUCURSAL, sucursal);
                            alertas.putExtra(ID_USUARIO, usuario);
                            startActivity(alertas);
                            finish();
                        }
                    }
                });

                dialogCargaAlertas.show();
                WindowManager.LayoutParams lps = new WindowManager.LayoutParams();
                Window windows = dialogCargaAlertas.getWindow();
                lps.copyFrom(windows.getAttributes());
                lps.width = WindowManager.LayoutParams.MATCH_PARENT;
                lps.height = WindowManager.LayoutParams.WRAP_CONTENT;
                windows.setAttributes(lps);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CustomAdapter extends BaseAdapter {
        Context context;

        public CustomAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return alertas.size();
        }

        @Override
        public Object getItem(int position) {
            return alertas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.adapter_ficha_tecnica_alerta, parent, false);

            final TextView fecha = (TextView) row.findViewById(R.id.textView_alerta_fecha_value);
            TextView descripcion = (TextView) row.findViewById(R.id.textView_alerta_alerta_value);
            TextView tipo_alerta = (TextView) row.findViewById(R.id.textView_alerta_tipo_value);
            final TextView id_alerta = (TextView) row.findViewById(R.id.textView_alerta_id);
            Toolbar toolbar = (Toolbar) row.findViewById(R.id.menu_toolbar);

            Date fechaAlerta = Dates.stringToDate(alertas.get(position).getFecha());
            Date dateHoy = new Date();

            Calendar calAlerta = Calendar.getInstance();
            Calendar calHoy = Calendar.getInstance();
            calAlerta.setTime(fechaAlerta);
            calHoy.setTime(dateHoy);

            boolean isAlertaHoy = calHoy.get(Calendar.YEAR) == calAlerta.get(Calendar.YEAR) &&
                    calHoy.get(Calendar.DAY_OF_MONTH) == calAlerta.get(Calendar.DAY_OF_MONTH) &&
                    calHoy.get(Calendar.MONTH) == calAlerta.get(Calendar.MONTH);

            if (isAlertaHoy) fecha.setTypeface(Typeface.DEFAULT_BOLD);

            if (alertas.get(position).getLeida().equals("0")) {
                descripcion.setTypeface(null, Typeface.BOLD);
                fecha.setTypeface(null, Typeface.BOLD);
                tipo_alerta.setTypeface(null, Typeface.BOLD);
                tipo_alerta.setTextColor(ContextCompat.getColor(Alertas.this, R.color.colorPrimary));
                fecha.setTextColor(ContextCompat.getColor(Alertas.this, R.color.colorPrimary));
            } else {
                descripcion.setTypeface(null, Typeface.NORMAL);
                fecha.setTypeface(null, Typeface.NORMAL);
                tipo_alerta.setTypeface(null, Typeface.NORMAL);
                descripcion.setTextColor(ContextCompat.getColor(Alertas.this, android.R.color.darker_gray));
                tipo_alerta.setTextColor(ContextCompat.getColor(Alertas.this, android.R.color.darker_gray));
                fecha.setTextColor(ContextCompat.getColor(Alertas.this, android.R.color.darker_gray));
            }

            fecha.setText(Dates.dateToString(fechaAlerta));
            descripcion.setText(alertas.get(position).getDescripcion());
            tipo_alerta.setText(alertas.get(position).getNombre());
            id_alerta.setText(String.valueOf(alertas.get(position).getId()));

            toolbar.inflateMenu(R.menu.menu_alertas);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    final int id = item.getItemId();
                    switch (id) {
                        case R.id.alerta_editar:
                            final Dialog dialogAlerta = new Dialog(Alertas.this);
                            dialogAlerta.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialogAlerta.setContentView(R.layout.dialog_update_alerta);

                            final EditText edDescripcion = (EditText) dialogAlerta.findViewById(R.id.editText_descripcion_notificacion);
                            Button btnFecha = (Button) dialogAlerta.findViewById(R.id.btn_seleccionaFecha_notificacion);
                            final TextView txtFechaSeleccionada = (TextView) dialogAlerta.findViewById(R.id.textView_seleccionaFecha_notificacion);
                            TextView btnCancelar = (TextView) dialogAlerta.findViewById(R.id.btn_cancelar);
                            TextView btnAceptar = (TextView) dialogAlerta.findViewById(R.id.btn_aceptar_espacio);

                            edDescripcion.setText(alertas.get(position).getDescripcion());

                            btnCancelar.setTextColor(ContextCompat.getColor(Alertas.this, R.color.colorPrimary));
                            btnAceptar.setTextColor(ContextCompat.getColor(Alertas.this, R.color.colorPrimary));

                            updateFecha = null;
                            btnFecha.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    final Calendar date;
                                    date = Calendar.getInstance();
                                    final String[] fecha = new String[1];
                                    final Calendar c = Calendar.getInstance();
                                    final int mYear = c.get(Calendar.YEAR);
                                    int mMonth = c.get(Calendar.MONTH);
                                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                                    DatePickerDialog dpd = new DatePickerDialog(Alertas.this,
                                            new DatePickerDialog.OnDateSetListener() {

                                                @Override
                                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                                    // Display Selected date in textbox
                                                    date.set(year, monthOfYear, dayOfMonth);
                                                    updateFecha = date.getTime();
                                                    fecha[0] = String.valueOf(String.format("%02d", dayOfMonth)) + "/" + String.valueOf(String.format("%02d", monthOfYear + 1)) + "/" + String.valueOf(year);
                                                    txtFechaSeleccionada.setText(fecha[0]);
                                                }
                                            }, mYear, mMonth, mDay);
                                    dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                                    dpd.show();
                                }
                            });

                            btnAceptar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String valorDescripcion = edDescripcion.getText().toString();
                                    if (valorDescripcion.equals("") || valorDescripcion.equals(null) || updateFecha == null) {
                                        Snackbar.make(coordinatorAlerta, "Llena todos los campos", Snackbar.LENGTH_LONG).show();
                                    } else {

                                        if (alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                            dbHelper.updateDescripcionAlerta(Integer.parseInt(id_alerta.getText().toString()), edDescripcion.getText().toString(), Dates.createDateToString(updateFecha), Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL, usuario.getUsername());
                                        } else {
                                            dbHelper.updateDescripcionAlerta(Integer.parseInt(id_alerta.getText().toString()), edDescripcion.getText().toString(), Dates.createDateToString(updateFecha), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL, usuario.getUsername());
                                        }
                                        alertas = dbHelper.getAlertas(sucursal.getId());
                                        notifyDataSetChanged();
                                        dialogAlerta.dismiss();
                                        Snackbar.make(coordinatorAlerta, "Alerta Actualizada", Snackbar.LENGTH_LONG).show();
                                    }
                                }
                            });

                            btnCancelar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogAlerta.dismiss();
                                }
                            });

                            dialogAlerta.show();

                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = dialogAlerta.getWindow();
                            lp.copyFrom(window.getAttributes());

                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                            break;
                        case R.id.alerta_eliminar:
                            new AlertDialog.Builder(Alertas.this)
                                    .setTitle("ALERTAS")
                                    .setMessage("¿Estás seguro que deseas eliminar esta alerta?")
                                    .setCancelable(false)
                                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    })
                                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            if (alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || alertas.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                                dbHelper.updateAlerta(Integer.parseInt(id_alerta.getText().toString()), Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL, usuario.getUsername());
                                            } else {
                                                dbHelper.updateAlerta(Integer.parseInt(id_alerta.getText().toString()), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL, usuario.getUsername());
                                            }
                                            alertas = dbHelper.getAlertas(sucursal.getId());
                                            notifyDataSetChanged();
                                            Snackbar.make(coordinatorAlerta, "Alerta Eliminada", Snackbar.LENGTH_LONG).show();
                                        }
                                    })
                                    .show();
                            break;

                    }
                    return true;
                }

            });


            return row;
        }
    }
}
