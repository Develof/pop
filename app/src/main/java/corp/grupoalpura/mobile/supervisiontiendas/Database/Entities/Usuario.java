package corp.grupoalpura.mobile.supervisiontiendas.Database.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import corp.grupoalpura.mobile.supervisiontiendas.Database.DateConverter;

@Entity
public class Usuario {

    private static final String ID_USUARIO = "ID_USUARIO";
    private static final String ID_UDN = "ID_UDN";
    private static final String ID_USUARIO_JEFE = "ID_USUARIO_JEFE";
    private static final String ID_CANAL = "ID_CANAL";
    private static final String NOMBRE = "NOMBRE";
    private static final String EMAIL = "EMAIL";
    private static final String USERNAME = "USERNAME";
    private static final String ROL = "ROL";
    private static final String CAMBIO_PASSWORD = "CAMBIO_PASSWORD";
    private static final String HASH_RESET = "HASH_RESET";
    private static final String INTENTOS = "INTENTOS";
    private static final String FECHA_ERROR = "FECHA_ERROR";
    private static final String FECHA_PWD = "FECHA_PWD";
    private static final String NIVEL = "NIVEL";
    private static final String PASSWORD = "PASSWORD";
    private static final String FECHA_CREACION = "FECHA_CREACION";
    private static final String ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    private static final String USUARIO = "USUARIO";
    private static final String ESTATUS = "ESTATUS";
    private static final String TOKEN = "TOKEN";
    private static final String CAMBIO_LOCALIZACION = "CAMBIO_LOCALIZACION";

    @PrimaryKey
    @ColumnInfo(name = ID_USUARIO)
    @SerializedName(ID_USUARIO)
    private int idUsuario;

    @ColumnInfo(name = ID_UDN)
    @SerializedName(ID_UDN)
    private int idUdn;

    @ColumnInfo(name = ID_USUARIO_JEFE)
    @SerializedName(ID_USUARIO_JEFE)
    private int idUsuarioJefe;

    @ColumnInfo(name = ID_CANAL)
    @SerializedName(ID_CANAL)
    private int idCanal;

    @ColumnInfo(name = NOMBRE)
    @SerializedName(NOMBRE)
    private String nombre;

    @ColumnInfo(name = EMAIL)
    @SerializedName(EMAIL)
    private String email;

    @ColumnInfo(name = USERNAME)
    @SerializedName(USERNAME)
    private String username;

    @ColumnInfo(name = ROL)
    @SerializedName(ROL)
    private String rol;

    @ColumnInfo(name = CAMBIO_PASSWORD)
    @SerializedName(CAMBIO_PASSWORD)
    private int cambioPwd;

    @ColumnInfo(name = HASH_RESET)
    @SerializedName(HASH_RESET)
    private String hashReset;

    @ColumnInfo(name = INTENTOS)
    @SerializedName(INTENTOS)
    private int intentos;

    @ColumnInfo(name = FECHA_ERROR)
    @TypeConverters(DateConverter.class)
    @SerializedName(FECHA_ERROR)
    private Date fechaError;

    @ColumnInfo(name = FECHA_PWD)
    @TypeConverters(DateConverter.class)
    @SerializedName(FECHA_PWD)
    private Date fechaPwd;

    @ColumnInfo(name = NIVEL)
    @SerializedName(NIVEL)
    private int nivel;

    @ColumnInfo(name = PASSWORD)
    @SerializedName(PASSWORD)
    private String pwd;

    @ColumnInfo(name = FECHA_CREACION)
    @TypeConverters(DateConverter.class)
    @SerializedName(FECHA_CREACION)
    private Date fechaCreacion;

    @ColumnInfo(name = ULTIMA_MODIFICACION)
    @TypeConverters(DateConverter.class)
    @SerializedName(ULTIMA_MODIFICACION)
    private Date ultimaModificacion;

    @ColumnInfo(name = USUARIO)
    @SerializedName(USUARIO)
    private String usuario;

    @ColumnInfo(name = ESTATUS)
    @SerializedName(ESTATUS)
    private int estatus;

    @ColumnInfo(name = TOKEN)
    @SerializedName(TOKEN)
    private String token;

    @ColumnInfo(name = CAMBIO_LOCALIZACION)
    @SerializedName(CAMBIO_LOCALIZACION)
    private int cambioLocalizacion;


    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdUdn() {
        return idUdn;
    }

    public void setIdUdn(int idUdn) {
        this.idUdn = idUdn;
    }

    public int getIdUsuarioJefe() {
        return idUsuarioJefe;
    }

    public void setIdUsuarioJefe(int idUsuarioJefe) {
        this.idUsuarioJefe = idUsuarioJefe;
    }

    public int getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(int idCanal) {
        this.idCanal = idCanal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public int getCambioPwd() {
        return cambioPwd;
    }

    public void setCambioPwd(int cambioPwd) {
        this.cambioPwd = cambioPwd;
    }

    public String getHashReset() {
        return hashReset;
    }

    public void setHashReset(String hashReset) {
        this.hashReset = hashReset;
    }

    public int getIntentos() {
        return intentos;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }

    public Date getFechaError() {
        return fechaError;
    }

    public void setFechaError(Date fechaError) {
        this.fechaError = fechaError;
    }

    public Date getFechaPwd() {
        return fechaPwd;
    }

    public void setFechaPwd(Date fechaPwd) {
        this.fechaPwd = fechaPwd;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getUltimaModificacion() {
        return ultimaModificacion;
    }

    public void setUltimaModificacion(Date ultimaModificacion) {
        this.ultimaModificacion = ultimaModificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCambioLocalizacion() {
        return cambioLocalizacion;
    }

    public void setCambioLocalizacion(int cambioLocalizacion) {
        this.cambioLocalizacion = cambioLocalizacion;
    }
}
