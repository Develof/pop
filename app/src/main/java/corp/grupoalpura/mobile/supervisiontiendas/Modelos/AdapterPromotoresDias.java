package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 11/03/2016.
 */
public class AdapterPromotoresDias {
    private String dia;
    private String hora_inicio;
    private String hora_fin;

    AdapterPromotoresDias(){};

    public AdapterPromotoresDias(String dia, String hora_inicio, String hora_fin) {
        this.dia = dia;
        this.hora_inicio = hora_inicio;
        this.hora_fin = hora_fin;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora_inicio() {
        return hora_inicio;
    }

    public void setHora_inicio(String hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public String getHora_fin() {
        return hora_fin;
    }

    public void setHora_fin(String hora_fin) {
        this.hora_fin = hora_fin;
    }
}
