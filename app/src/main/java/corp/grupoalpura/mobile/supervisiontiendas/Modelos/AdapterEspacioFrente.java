package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 18/07/16.
 */
public class AdapterEspacioFrente {
    private int id_espacio_frente;
    private double frentes;
    private String clave;
    private String nombre_compania;
    private String descipcion;
    private int estatus_dos;

    public  AdapterEspacioFrente(){}

    public AdapterEspacioFrente(int id_espacio_frente, int frentes, String clave, String nombre_compania, String descipcion, int estatus_dos) {
        this.id_espacio_frente = id_espacio_frente;
        this.frentes = frentes;
        this.clave = clave;
        this.nombre_compania = nombre_compania;
        this.descipcion = descipcion;
        this.estatus_dos = estatus_dos;
    }

    public int getId_espacio_frente() {
        return id_espacio_frente;
    }

    public void setId_espacio_frente(int id_espacio_frente) {
        this.id_espacio_frente = id_espacio_frente;
    }

    public double getFrentes() {
        return frentes;
    }

    public void setFrentes(double frentes) {
        this.frentes = frentes;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre_compania() {
        return nombre_compania;
    }

    public void setNombre_compania(String nombre_compania) {
        this.nombre_compania = nombre_compania;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }
}
