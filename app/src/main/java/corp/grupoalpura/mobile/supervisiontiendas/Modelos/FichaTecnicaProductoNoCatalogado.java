package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 06/03/16.
 */
public class FichaTecnicaProductoNoCatalogado {

    private int id;
    private String clave;
    private String descripcion;
    private String familia;
    private String presentacion;

    FichaTecnicaProductoNoCatalogado(){

    }

    public FichaTecnicaProductoNoCatalogado(int id, String clave, String descripcion, String familia, String presentacion) {
        this.id = id;
        this.clave = clave;
        this.descripcion = descripcion;
        this.familia = familia;
        this.presentacion = presentacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }
}
