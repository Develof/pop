package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import com.google.gson.annotations.SerializedName;

public class ImageRequest {

    @SerializedName("FILE_NAME")
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
