package corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.LocalizacionGps.Localizacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterProductoIncidencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CategoriaIncidencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.MyDialogFragment;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;
import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.models.EZPhotoPickConfig;
import siclo.com.ezphotopicker.api.models.PhotoSource;
import siclo.com.ezphotopicker.models.PhotoIntentException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by oflores on 11/07/16.
 **/
public class IncidenciasProducto extends Fragment {

    DBHelper dbHelper;
    ClienteSucursal sucursal;
    Usuario usuario;
    FichaTecnicaProducto producto;
    ArrayList<CategoriaIncidencia> arregloCatalogoIncidencia = new ArrayList<>();
    ArrayList<Integer> arregloFotosTomadas = new ArrayList<>();
    ArrayList<AdapterProductoIncidencia> arregloIncidencia = new ArrayList<>();
    double precio_real;
    int tipo_incidencia = 1;
    Double latitud = 0.0, longitud = 0.0;

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String PRODUCTO = "Producto";
    static DecimalFormat form = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
    int posicionCategoria = 0;
    CustomAdapter adapter;
    Localizacion localizacion;

    public static final int INCIDENCIAS_POSITIVAS = 1;
    public static final int INCIDENCIAS_NEGATIVAS = 2;
    public static final int INCIDENCIAS_PRODUCTO = 1;

    ArrayList<String> image_uris = new ArrayList<>();

    public IncidenciasProducto() {
    }

    public static IncidenciasProducto newInstance(ClienteSucursal sucursal, Usuario usuario, FichaTecnicaProducto producto) {
        IncidenciasProducto fragment = new IncidenciasProducto();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(PRODUCTO, producto);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        form.applyPattern(Constantes.FORMATO_PRECIO);
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            producto = (FichaTecnicaProducto) getArguments().getSerializable(PRODUCTO);
        }

        /* CONSULTAS A LAS BASES DE DATOS */
        precio_real = dbHelper.getPrecioUnitarioReal(producto != null ? producto.getClave() : "");
        arregloCatalogoIncidencia = dbHelper.getActividadCategoriaIncidencia(INCIDENCIAS_POSITIVAS, INCIDENCIAS_PRODUCTO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        localizacion = new Localizacion(getActivity());


        View view = inflater.inflate(R.layout.incidencias_producto, container, false);

        arregloIncidencia.clear();
        arregloIncidencia = dbHelper.getIncidencias(sucursal.getId(), producto.getId());


        ListView listIncidencia = (ListView) view.findViewById(R.id.listView_incidencia_producto);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);

        fab.setOnClickListener(v -> {
            final Dialog dialogEditaIncidencia = new Dialog(getActivity());
            dialogEditaIncidencia.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogEditaIncidencia.setContentView(R.layout.dialog_editar_incidencias);

            final ArrayAdapter<CategoriaIncidencia> dataAdapterCategoria = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arregloCatalogoIncidencia);
            arregloFotosTomadas.clear();

            final Spinner spCategoriaincidencia = (Spinner) dialogEditaIncidencia.findViewById(R.id.spinner_incidencias_producto);
            RadioButton radioIncidenciaPositiva = (RadioButton) dialogEditaIncidencia.findViewById(R.id.radioButton_incidencia_positiva);
            RadioButton radioIncidenciaNegativa = (RadioButton) dialogEditaIncidencia.findViewById(R.id.radioButton_incidencia_negativa);
            final EditText edDescripcion = (EditText) dialogEditaIncidencia.findViewById(R.id.editText_actividades_competencia_descripcio);
            final ImageView imgCamara = (ImageView) dialogEditaIncidencia.findViewById(R.id.imageView_foto);
            Button btnGuardar = (Button) dialogEditaIncidencia.findViewById(R.id.btn_guardar_incidencia);
            Button btnCancelar = (Button) dialogEditaIncidencia.findViewById(R.id.btn_atras_incidencia);


            btnGuardar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
            btnCancelar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));

            radioIncidenciaPositiva.setChecked(true);
            tipo_incidencia = Constantes.TIPO_INCIDENCIA.INCIDENCIA_NEGATIVA;

            spCategoriaincidencia.setAdapter(null);
            dataAdapterCategoria.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapterCategoria.clear();
            arregloCatalogoIncidencia = dbHelper.getActividadCategoriaIncidencia(INCIDENCIAS_NEGATIVAS, INCIDENCIAS_PRODUCTO);
            for (int i = 0; i < arregloCatalogoIncidencia.size(); i++) {
                dataAdapterCategoria.add(arregloCatalogoIncidencia.get(i));
            }

            dataAdapterCategoria.notifyDataSetChanged();
            spCategoriaincidencia.setAdapter(dataAdapterCategoria);


            spCategoriaincidencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view1, int position, long id) {
                    posicionCategoria = arregloCatalogoIncidencia.get(position).getId_categoria();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            imgCamara.setOnClickListener(v1 -> {

                EZPhotoPickConfig config = new EZPhotoPickConfig();
                config.photoSource = PhotoSource.CAMERA;
                config.pathRoot = "IMG_PINCIDENCIA_";
                config.needToAddToGallery = true;
                config.exportingSize = 1000;
                try {
                    EZPhotoPick.startPhotoPickActivity(this, config);
                } catch (PhotoIntentException e) {
                    e.printStackTrace();
                }
            });

            btnGuardar.setOnClickListener(view1 -> {


                if (edDescripcion.getText().toString().equals("") || edDescripcion.getText().toString().equals(null)) {
                    Toast toast = Toast.makeText(getActivity(), "Coloca una descripción a la incidencia", Toast.LENGTH_SHORT);
                    TextView v12 = (TextView) toast.getView().findViewById(android.R.id.message);
                    v12.setTextColor(Color.WHITE);
                    toast.show();
                } else {

                    long id_incidencia_producto = dbHelper.insertIncidenciProducto(posicionCategoria, sucursal.getId(), producto.getId(), edDescripcion.getText().toString(), tipo_incidencia, usuario.getUsername());
                    if (id_incidencia_producto != 0) {

                        int id_actividad = (int) id_incidencia_producto;
                        for (int i = 0; i < image_uris.size(); i++) {
                            String dirNombre = image_uris.get(i).toString().substring(image_uris.get(i).toString().lastIndexOf("/") + 1, image_uris.get(i).toString().length());
                            long id_foto = dbHelper.insertFoto(sucursal.getId(), image_uris.toString(), dirNombre, latitud, longitud, usuario.getUsername());
                            if (id_foto != 0) {
                                int id_foto_evidencia = (int) id_foto;
                                arregloFotosTomadas.add(id_foto_evidencia);
                            }
                        }

                        for (int i = 0; i < arregloFotosTomadas.size(); i++) {
                            dbHelper.insertFotoRelacion(arregloFotosTomadas.get(i), DBHelper.TABLA_PRODUCTO_INCIDENCIA, id_actividad);
                        }

                        arregloIncidencia = dbHelper.getIncidencias(sucursal.getId(), producto.getId());
                        adapter.notifyDataSetChanged();


                        dialogEditaIncidencia.dismiss();

                        Toast toast = Toast.makeText(getActivity(), "Se ha guardado la incidencia", Toast.LENGTH_SHORT);
                        TextView v12 = (TextView) toast.getView().findViewById(android.R.id.message);
                        v12.setTextColor(Color.WHITE);
                        toast.show();

                    }
                }
            });

            btnCancelar.setOnClickListener(view1 -> dialogEditaIncidencia.dismiss());

            dialogEditaIncidencia.setCancelable(false);
            dialogEditaIncidencia.show();

            WindowManager.LayoutParams lps = new WindowManager.LayoutParams();
            Window windows = dialogEditaIncidencia.getWindow();
            lps.copyFrom(windows != null ? windows.getAttributes() : null);
            lps.width = WindowManager.LayoutParams.MATCH_PARENT;
            lps.height = WindowManager.LayoutParams.WRAP_CONTENT;
            if (windows != null)

            {
                windows.setAttributes(lps);
            }
        });

        adapter = new CustomAdapter(getActivity());
        listIncidencia.setAdapter(adapter);

        listIncidencia.setOnItemClickListener((adapterView, view12, i, l) -> {

            final Dialog dialogActividadCompetencia = new Dialog(getActivity());
            dialogActividadCompetencia.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogActividadCompetencia.setContentView(R.layout.dialog_actividad_competencia);

            Date fecha_creacion = Dates.stringToDate(arregloIncidencia.get(i).getFecha());
            final String fecha = Dates.dateToString(fecha_creacion);

            TextView txtCategoria = (TextView) dialogActividadCompetencia.findViewById(R.id.textview_title_mensaje_actividad);
            TextView txtProducto = (TextView) dialogActividadCompetencia.findViewById(R.id.textView_producto_actividad);
            TextView textDescripcion = (TextView) dialogActividadCompetencia.findViewById(R.id.textView_descripcion_actividad);
            TextView textFecha = (TextView) dialogActividadCompetencia.findViewById(R.id.textview_title_mensaje_actividad_fecha);
            TextView btnAceptar = (TextView) dialogActividadCompetencia.findViewById(R.id.btn_aceptar_actividad);

            txtCategoria.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            btnAceptar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

            if (arregloIncidencia.get(i).getTipo_incidencia() == Constantes.TIPO_INCIDENCIA.INCIDENCIA_POSITIVA) {
                txtProducto.setText("INCIDENCIA POSITIVA");
            } else {
                txtProducto.setText("INCIDENCIA NEGATIVA");
            }

            txtCategoria.setText(arregloIncidencia.get(i).getCategoria());
            textDescripcion.setText(arregloIncidencia.get(i).getDescripcion());
            textFecha.setText(fecha);


            btnAceptar.setOnClickListener(v -> dialogActividadCompetencia.dismiss());

            dialogActividadCompetencia.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialogActividadCompetencia.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        });

        return view;
    }

    private class CustomAdapter extends BaseAdapter {
        Context context;


        public CustomAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return arregloIncidencia.size();
        }

        @Override
        public Object getItem(int position) {
            return arregloIncidencia.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.adapter_desc, parent, false);

            final TextView txtCategoria = (TextView) row.findViewById(R.id.textView_categoria);
            final TextView txtFecha = (TextView) row.findViewById(R.id.textView_fecha);
            final TextView txtDescripcion = (TextView) row.findViewById(R.id.textView_descripcion);
            Toolbar toolbar = (Toolbar) row.findViewById(R.id.menu_toolbar);
            txtFecha.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));

            Date fecha_creacion = Dates.stringToDate(arregloIncidencia.get(position).getFecha());
            final String fecha = Dates.dateToString(fecha_creacion);

            txtCategoria.setText(arregloIncidencia.get(position).getCategoria());
            txtDescripcion.setText(arregloIncidencia.get(position).getDescripcion());

            txtFecha.setText(fecha);
            if (arregloIncidencia.get(position).getTipo_incidencia() == Constantes.TIPO_INCIDENCIA.INCIDENCIA_POSITIVA) {
                txtFecha.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorVerde));
            }


            toolbar.inflateMenu(R.menu.menu_info_actividades_competencia);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    final int id = item.getItemId();
                    switch (id) {
                        case R.id.actividad_ver_foto:

                            DialogFragment newFragment = MyDialogFragment.newInstance(arregloIncidencia.get(position).getId_producto_incidencia(), DBHelper.TABLA_PRODUCTO_INCIDENCIA, producto.getClave() + producto.getDescripcion(), "IMG_PINCIDENCIA_", sucursal, usuario);
                            newFragment.show(getFragmentManager(), "");
                            newFragment.setCancelable(false);

                            break;
                        case R.id.actividad_editar:

                            final Dialog dialogEditaIncidencia = new Dialog(getActivity());
                            dialogEditaIncidencia.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialogEditaIncidencia.setContentView(R.layout.dialog_editar_incidencias);

                            ArrayAdapter<CategoriaIncidencia> dataAdapterCategoria = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arregloCatalogoIncidencia);

                            Spinner spCategoriaincidencia = (Spinner) dialogEditaIncidencia.findViewById(R.id.spinner_incidencias_producto);
                            RadioButton radioIncidenciaPositiva = (RadioButton) dialogEditaIncidencia.findViewById(R.id.radioButton_incidencia_positiva);
                            RadioButton radioIncidenciaNegativa = (RadioButton) dialogEditaIncidencia.findViewById(R.id.radioButton_incidencia_negativa);
                            final EditText edDescripcion = (EditText) dialogEditaIncidencia.findViewById(R.id.editText_actividades_competencia_descripcio);
                            Button btnGuardar = (Button) dialogEditaIncidencia.findViewById(R.id.btn_guardar_incidencia);
                            Button btnCancelar = (Button) dialogEditaIncidencia.findViewById(R.id.btn_atras_incidencia);
                            TextView txtTtitleImage = (TextView) dialogEditaIncidencia.findViewById(R.id.textView_actividades_competencia_foto);
                            ImageView imgFoto = (ImageView) dialogEditaIncidencia.findViewById(R.id.imageView_foto);
                            TextInputLayout inputLayput = (TextInputLayout) dialogEditaIncidencia.findViewById(R.id.inputLayout_actividades_competencia_descripcio);

                            txtTtitleImage.setVisibility(View.GONE);
                            imgFoto.setVisibility(View.GONE);
                            inputLayput.setVisibility(View.VISIBLE);

                            edDescripcion.setText(arregloIncidencia.get(position).getDescripcion());

                            tipo_incidencia = arregloIncidencia.get(position).getTipo_incidencia();
                            if (tipo_incidencia == Constantes.TIPO_INCIDENCIA.INCIDENCIA_POSITIVA) {
                                radioIncidenciaPositiva.setChecked(true);
                            } else {
                                radioIncidenciaNegativa.setChecked(true);
                            }

                            radioIncidenciaPositiva.setOnClickListener(view -> tipo_incidencia = Constantes.TIPO_INCIDENCIA.INCIDENCIA_POSITIVA);

                            radioIncidenciaNegativa.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    tipo_incidencia = Constantes.TIPO_INCIDENCIA.INCIDENCIA_NEGATIVA;
                                }
                            });

                            int index = 0;
                            int indexarreglo;
                            for (int i = 0; i < arregloCatalogoIncidencia.size(); i++) {
                                indexarreglo = arregloCatalogoIncidencia.get(i).getCategoria().indexOf(arregloIncidencia.get(position).getCategoria());
                                if (indexarreglo != -1) {
                                    index = i;
                                }
                            }

                            spCategoriaincidencia.setAdapter(dataAdapterCategoria);
                            spCategoriaincidencia.setSelection(index);
                            spCategoriaincidencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    posicionCategoria = arregloCatalogoIncidencia.get(position).getId_categoria();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });

                            btnGuardar.setOnClickListener(view -> {
                                if (arregloIncidencia.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || arregloIncidencia.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                    dbHelper.updateProductoIncidencia(arregloIncidencia.get(position).getId_producto_incidencia(), posicionCategoria, sucursal.getId(), producto.getId(), edDescripcion.getText().toString(), tipo_incidencia, Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
                                } else {
                                    dbHelper.updateProductoIncidencia(arregloIncidencia.get(position).getId_producto_incidencia(), posicionCategoria, sucursal.getId(), producto.getId(), edDescripcion.getText().toString(), tipo_incidencia, Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
                                }
                                arregloIncidencia.clear();
                                arregloIncidencia = dbHelper.getIncidencias(sucursal.getId(), producto.getId());
                                notifyDataSetChanged();
                                dialogEditaIncidencia.dismiss();
                            });

                            btnCancelar.setOnClickListener(view -> dialogEditaIncidencia.dismiss());

                            dialogEditaIncidencia.show();

                            WindowManager.LayoutParams lps = new WindowManager.LayoutParams();
                            Window windows = dialogEditaIncidencia.getWindow();
                            lps.copyFrom(windows.getAttributes());
                            lps.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lps.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            windows.setAttributes(lps);

                            break;

                        case R.id.actividad_eliminar:

                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Incidencia Producto")
                                    .setMessage("¿Estás seguro que deseas eliminar la incidencia?")
                                    .setCancelable(false)
                                    .setNegativeButton("NO", (dialog, id1) -> {
                                    })
                                    .setPositiveButton("SI", (dialog, which) -> {

                                        if (arregloIncidencia.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || arregloIncidencia.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                            dbHelper.eliminarProductoIncidencia(arregloIncidencia.get(position).getId_producto_incidencia(), sucursal.getId(), producto.getId(), Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
                                        } else {
                                            dbHelper.eliminarProductoIncidencia(arregloIncidencia.get(position).getId_producto_incidencia(), sucursal.getId(), producto.getId(), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
                                        }
                                        arregloIncidencia.clear();
                                        arregloIncidencia = dbHelper.getIncidencias(sucursal.getId(), producto.getId());
                                        notifyDataSetChanged();
                                    })
                                    .show();

                            break;

                    }
                    return true;
                }

            });


            return row;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EZPhotoPick.PHOTO_PICK_CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                image_uris = data.getStringArrayListExtra(EZPhotoPick.PICKED_PHOTO_NAMES_KEY);
            } else {
                Toast toast = Toast.makeText(getActivity(), "Ocurrio un error al cargar la imagen vuelve a tomar la foto", Toast.LENGTH_SHORT);
                TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                tv.setTextColor(Color.WHITE);
                toast.show();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


}
