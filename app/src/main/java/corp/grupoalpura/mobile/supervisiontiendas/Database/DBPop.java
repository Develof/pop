package corp.grupoalpura.mobile.supervisiontiendas.Database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Database.Dao.InventarioBODao;
import corp.grupoalpura.mobile.supervisiontiendas.Database.Dao.UdnDao;
import corp.grupoalpura.mobile.supervisiontiendas.Database.Dao.UsuarioDao;
import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.InventarioBO;
import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Udn;
import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Usuario;

@Database(entities = {Usuario.class, Udn.class, InventarioBO.class}, version = 1, exportSchema = false)
public abstract class DBPop extends RoomDatabase {

    public abstract UsuarioDao usuarioDao();
    public abstract UdnDao udnDao();
    public abstract InventarioBODao inventarioBODao();

}
