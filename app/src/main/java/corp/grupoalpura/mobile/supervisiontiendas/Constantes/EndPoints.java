package corp.grupoalpura.mobile.supervisiontiendas.Constantes;

/**
  Created by prestamo on 26/04/2016.
 */
public class EndPoints {
    public static String SupervisionURL = "http://187.141.40.37/webservice/yii/autoservicios/site/index/ws/1";
    public static String SoapAction = "getDatos";


    private static final String IP_WEBSERVICE = "http://148.223.155.8:17005/";  // TEST 148.223.155.11
    private static final String IP_AUTOSERVICIOS = "http://appmoviles.alpura.com/webservice/yii/autoservicios/";
    public static final String URL_WEBSERVICE = IP_WEBSERVICE + "AlpSBPromotoria/ConsultaInvPromotoria/";
    public static final String METODO_INVENTARIO_TIENDA =  URL_WEBSERVICE + "getInvPromotoria";

    public static final String URL_WEBSERVICE_AS = IP_AUTOSERVICIOS + "api/";
    public static final String METODO_NOMBRE_PRODUCTOS = URL_WEBSERVICE_AS + "getImageName";
    public static final String METODO_BASE64_PRODUCTOS = URL_WEBSERVICE_AS + "getBase64Images";

}
