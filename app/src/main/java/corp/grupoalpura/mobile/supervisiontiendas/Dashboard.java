package corp.grupoalpura.mobile.supervisiontiendas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Broadcast.GPSService;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.EndPoints;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBBackup;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.DispositivoBDModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.RegistroAplicacionBDModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Visita;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DispositivoBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DispositivoEventualidadBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.RegistroAplicacionBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.SucursalBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.InformacionDispositivo;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.ScannActivity;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Singleton;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Dashboard extends AppCompatActivity {

    AppCompatActivity DASHBOARD = this;
    Usuario usuario;
    Toolbar toolbar;
    Visita visita = null;
    DBHelper dbHelper;
    ClienteSucursal cs = new ClienteSucursal();
    int contadorImage, numeroFotos;
    ProgressDialog progressDialog;
    SincImag a;
    DBBackup respaldo;
    String path, filename, imgBase64;
    String responseDB = "";

    RegistroAplicacionBDModel registroAplicacion;

    @BindView(R.id.menu_rl_menu_asistencia)
    RelativeLayout rlAsistencia;
    @BindView(R.id.menu_rl_menu_guia_mercadeo)
    RelativeLayout rlGuiaMercadeo;
    @BindView(R.id.menu_rl_menu_espacios)
    RelativeLayout rlEspacios;
    @BindView(R.id.menu_rl_menu_prouctos)
    RelativeLayout rlProducto;
    @BindView(R.id.menu_rl_menu_competencia)
    RelativeLayout rlComptencia;
    @BindView(R.id.menu_iv_user)
    ImageView ivUser;
    @BindView(R.id.menu_tv_cs)
    TextView tvSucursal;
    @BindView(R.id.menu_tv_username)
    TextView tvUsername;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView navigationView;

    private static final int CHECK_IN_ACTIVO = 1;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);


        usuario = (Usuario) getIntent().getSerializableExtra("Usuario");

        dbHelper = new DBHelper(DASHBOARD, null, Constantes.VERSION_BASE_DE_DATOS);
        tvUsername.setText(usuario.getNombre());


        rlAsistencia.setOnClickListener(v -> startActivity(Constantes.ACTIVIDADES.ASISTENCIA));

        rlGuiaMercadeo.setOnClickListener(v -> startActivity(Constantes.ACTIVIDADES.GUIA_MERCADEO));

        rlEspacios.setOnClickListener(v -> startActivity(Constantes.ACTIVIDADES.ESPACIOS));

        rlProducto.setOnClickListener(v -> startActivity(Constantes.ACTIVIDADES.FAMILIAS));

        rlComptencia.setOnClickListener(view -> startActivity(Constantes.ACTIVIDADES.ACTIVIDAD_COMPETENCIA));

        navigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {

                case R.id.menu_video_memo:
                    Intent video = new Intent(Dashboard.this, Video.class);
                    startActivity(video);
                    break;

                case R.id.menu_message:
                    //Intent message = new Intent(Dashboard.this, MensajesPush.class);
                    //startActivity(message);
                    if (cs != null) {
                        Intent intent = new Intent(Dashboard.this, ScannActivity.class);
                        intent.putExtra("Usuario", usuario);
                        intent.putExtra("id_sucursal", cs);
                        intent.putExtra("ventana", 1);
                        startActivity(intent);
                    }
                    break;

                case R.id.menu_sincronizacion_final:
                    new AlertDialog.Builder(DASHBOARD)
                            .setTitle("Sincronización Final")
                            .setMessage("¿Estás seguro que quieres enviar los datos?")
                            .setCancelable(false)
                            .setNegativeButton("NO", (dialog, id) -> {
                            })
                            .setPositiveButton("SI", (dialog, which) -> runOnUiThread(() -> {
                                runOnUiThread(() -> {
                                    progressDialog = new ProgressDialog(Dashboard.this);
                                    progressDialog.setMessage("Sincronizando Datos...");
                                    progressDialog.setCancelable(false);
                                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    progressDialog.show();
                                });

                                respaldo = new DBBackup();
                                String respaldoBD = respaldo.backupDb(getApplicationContext().getPackageName(), DBHelper.DATABASE_NAME);

                                DispositivoBDHelper dispositivoBDHelper = new DispositivoBDHelper(DASHBOARD);
                                DispositivoBDModel dispositivo = dispositivoBDHelper.getDispositivo();
                                Date fechaDispositivo = Dates.stringToDate(dispositivo.getFecha());
                                if (DateUtils.isToday(fechaDispositivo.getTime())) {

                                    if (dispositivo.getVersionBd() == null) {
                                        SincDB sincDb = new SincDB();
                                        sincDb.execute(usuario.getUsername(), usuario.getPassword(), respaldoBD, dispositivo.getFecha());

                                    } else {

                                        numeroFotos = dbHelper.getNumeroFotos();
                                        String response = dispositivo.getVersionBd();

                                        if (numeroFotos > 0) {

                                            responseDB = response;
                                            progressDialog = new ProgressDialog(Dashboard.this);
                                            progressDialog.setMessage("Enviando Imagenes...");
                                            progressDialog.setCancelable(false);
                                            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                                            progressDialog.setMax(numeroFotos);
                                            progressDialog.show();

                                            respaldo = new DBBackup();
                                            a = new SincImag();
                                            a.execute(usuario.getUsername(), usuario.getPassword(), response);

                                        } else {

                                            ArrayList<String> tablas;
                                            tablas = dbHelper.getTables();

                                            //DETENEMOS EL SERVICO DE LOCALIZACION DE USUARIO
                                            Intent in = new Intent(Dashboard.this, GPSService.class);
                                            Dashboard.this.stopService(in);

                                            //BORRAMOS LOS DATOS DE LA DB
                                            for (int i = 0; i < tablas.size(); i++) {
                                                dbHelper.clearDB(tablas.get(i));
                                            }

                                            String path = Environment.getExternalStoragePublicDirectory(Dashboard.this.getString(R.string.app_foto)) + File.separator;

                                            File directory = new File(path);
                                            File[] files = directory.listFiles();

                                            for (File file : files) {
                                                if (file.getName().contains("IMG_FIRMA")) {
                                                    file.delete();
                                                }
                                            }

                                            //MANDAMOS A LA PANTALLA DE LOGIN Y CERRAMOS TODAS LAS ACTIVIDDADES
                                            Intent login = new Intent(DASHBOARD, Login.class);
                                            startActivity(login);
                                            finish();

                                            progressDialog.dismiss();
                                            contadorImage = 0;
                                            Toast.makeText(Dashboard.this, "FINALIZO", Toast.LENGTH_SHORT).show();

                                        }

                                    }
                                } else {
                                    progressDialog.dismiss();
                                    new AlertDialog.Builder(Dashboard.this)
                                            .setTitle(getResources().getString(R.string.app_name))
                                            .setMessage("Los datos de la tableta no corresponden al día actual")
                                            .setCancelable(false)
                                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog1, int which1) {
                                                    dialog1.dismiss();
                                                }
                                            })
                                            .show();
                                }
                            }))
                            .show();

            }
            return true;
        });

    }


    private void startActivity(int actividad) {
        Intent contenedor = new Intent(Dashboard.this, Contenedor.class);
        contenedor.putExtra("USUARIO", usuario);
        contenedor.putExtra("CLIENTE_SUCURSAL", cs);
        contenedor.putExtra("ACTIVIDAD", actividad);
        startActivity(contenedor);
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* OBTENEMOS LOS VALORES DE REGISTRO EN NUESTRA APLICACION */
        RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(DASHBOARD);
        registroAplicacion = registroAplicacionBDHelper.getRegistroAplicacion();


        if (registroAplicacion.getIs_check_in() == CHECK_IN_ACTIVO) {
            cs = Singleton.getInstance().getCs();
            if (cs == null) {

                SucursalBDHelper sucursalBDHelper = new SucursalBDHelper(Dashboard.this);

                Calendar calendar = Calendar.getInstance();
                int dia = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                if (dia == 0) dia = 7;

                boolean isRutaAleatoriaActiva = dbHelper.getRutaAlternaActiva(dia);
                if (isRutaAleatoriaActiva) {
                    cs = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD", registroAplicacion.getNo_sucursal());
                } else {
                    cs = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD_SUGERIDA", registroAplicacion.getNo_sucursal());
                }
            }


            if (cs != null) {
                tvSucursal.setText(cs.getAlias());
                tvSucursal.setVisibility(View.VISIBLE);
            }
        } else {
            tvSucursal.setText(getResources().getString(R.string.dashboard_tv_sin_sucursla));
            tvSucursal.setTextColor(getResources().getColor(R.color.colorAmarillo));
            tvSucursal.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * HILO PARA MANDAR LA BD SQLITE AL WS
     **/
    private class SincDB extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... args) {

            String responseString, strResult = null, outputString = "";
            URL url;
            URLConnection connection = null;
            OutputStream out;
            InputStreamReader isr;
            SAXBuilder sxBuild = new SAXBuilder();
            Document doc;
            Element rootNode;
            rootNode = null;
            String salida =
                    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:SiteControllerwsdl\">" +
                            "   <soapenv:Header/>" +
                            "   <soapenv:Body>" +
                            "      <urn:setBdBase64 soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                            "         <usuario xsi:type=\"xsd:string\">" + args[0] + "</usuario>" +
                            "         <password xsi:type=\"xsd:string\">" + args[1] + "</password>" +
                            "         <dataBase64 xsi:type=\"xsd:string\">" + args[2] + "</dataBase64>" +
                            "         <fecha_base xsi:type=\"xsd:string\">" + args[3] + "</fecha_base>" +
                            "      </urn:setBdBase64>" +
                            "   </soapenv:Body>" +
                            "</soapenv:Envelope>";

            try {
                url = new URL(EndPoints.SupervisionURL);
                connection = url.openConnection();
                connection.setConnectTimeout(20000);
                connection.setReadTimeout(20000);
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buffer = salida.getBytes();
            if (httpConn != null) {
                try {
                    bout.write(buffer);
                    byte[] b = bout.toByteArray();
                    httpConn.setRequestProperty("Contenedor-Length", String.valueOf(b.length));
                    httpConn.setRequestProperty("Contenedor-Type", "text/xml; charset=utf-8");
                    httpConn.setRequestProperty("SOAPAction", EndPoints.SoapAction);
                    httpConn.setRequestMethod("POST");
                    httpConn.setConnectTimeout(20000);
                    httpConn.setReadTimeout(20000);
                    httpConn.setDoOutput(true);
                    httpConn.setDoInput(true);
                    out = httpConn.getOutputStream();
                    out.write(b);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Read the response.
            try {
                isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);
                while ((responseString = in.readLine()) != null) {
                    outputString = outputString + responseString;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                doc = sxBuild.build(new StringReader(outputString));
                rootNode = doc.getRootElement();
            } catch (JDOMException | IOException ex) {
                ex.printStackTrace();
            }

            if (rootNode != null) {
                List lista = rootNode.getChildren();
                for (int i = 0; i < lista.size(); i++) {
                    rootNode = (Element) lista.get(i);
                    lista = rootNode.getChildren();
                    for (int j = 0; j < lista.size(); j++) {
                        rootNode = (Element) lista.get(i);
                        lista = rootNode.getChildren();
                        rootNode = (Element) lista.get(0);
                        strResult = rootNode.getText();
                    }
                }
            }

            return strResult;
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (response != null) {
                progressDialog.dismiss();
                numeroFotos = dbHelper.getNumeroFotos();

                DispositivoBDHelper dispositivoBDHelper = new DispositivoBDHelper(Dashboard.this);
                DispositivoEventualidadBDHelper dispositivoEventualidadBDHelper = new DispositivoEventualidadBDHelper(DASHBOARD);
                InformacionDispositivo informacionDispositivo = new InformacionDispositivo();
                dispositivoBDHelper.updateDispositivo(response);

                long idDispositivo = dispositivoBDHelper.getIdDispositivo();
                dispositivoEventualidadBDHelper.insertarDispositivoEventualidad((int) idDispositivo, usuario.getId(), Constantes.TIPO_EVENTUALIDAD.TIPO_SINCRONIZACION_FINAL,
                        String.valueOf(informacionDispositivo.getBatteryLevel(DASHBOARD)), Constantes.ESTATUS.CREADO_MOVIL, usuario.getUsername());

                if (numeroFotos > 0) {
                    responseDB = response;
                    progressDialog = new ProgressDialog(Dashboard.this);
                    progressDialog.setMessage("Enviando Imagenes...");
                    progressDialog.setCancelable(false);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progressDialog.setMax(numeroFotos);
                    progressDialog.show();

                    a = new SincImag();
                    a.execute(usuario.getUsername(), usuario.getPassword(), response);

                } else {

                    //SI YA NO HAY SUCURSALES POR VISITAR SE ELIMINARA LA BASE DE DATOS (SE LIMPIARA PARA EL PROXIMO DIA)
                    ArrayList<String> tablas;
                    tablas = dbHelper.getTables();

                    //DETENEMOS EL SERVICO DE LOCALIZACION DE USUARIO
                    Intent in = new Intent(Dashboard.this, GPSService.class);
                    Dashboard.this.stopService(in);

                    //BORRAMOS LOS DATOS DE LA DB
                    for (int i = 0; i < tablas.size(); i++) {
                        dbHelper.clearDB(tablas.get(i));
                    }


                    String path = Environment.getExternalStoragePublicDirectory(Dashboard.this.getString(R.string.app_foto)) + File.separator;

                    File directory = new File(path);
                    File[] files = directory.listFiles();
                    for (File file : files) {
                        if (file.getName().contains("IMG_FIRMA")) {
                            file.delete();
                        }
                    }

                    //MANDAMOS A LA PANTALLA DE LOGIN Y CERRAMOS TODAS LAS ACTIVIDDADES
                    Intent login = new Intent(DASHBOARD, Login.class);
                    startActivity(login);
                    finish();

                    progressDialog.dismiss();
                    contadorImage = 0;
                    Toast.makeText(Dashboard.this, "FINALIZO", Toast.LENGTH_SHORT).show();

                }
            } else {
                progressDialog.dismiss();
                Toast.makeText(Dashboard.this, "Ocurrio un error al sincronizar. Vuelve a intentar", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * HILO PARA MANDAR TODAS LAS IMAGENES TOMADAS
     **/
    private class SincImag extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            contadorImage++;
            filename = dbHelper.getArchivo(contadorImage);
            path = Environment.getExternalStoragePublicDirectory(Dashboard.this.getString(R.string.app_foto)) + File.separator;
            imgBase64 = respaldo.getImageBase64(path + filename);
        }

        @Override
        protected String doInBackground(String... args) {

            String responseString, strResult = null, outputString = "";
            URL url;
            URLConnection connection = null;
            OutputStream out;
            InputStreamReader isr;
            SAXBuilder sxBuild = new SAXBuilder();
            Document doc;
            Element rootNode;
            rootNode = null;
            String salida =
                    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:SiteControllerwsdl\">\n" +
                            "   <soapenv:Header/>\n" +
                            "   <soapenv:Body>\n" +
                            "      <urn:setImageBase64 soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
                            "         <usuario xsi:type=\"xsd:string\">" + args[0] + "</usuario>\n" +
                            "         <password xsi:type=\"xsd:string\">" + args[1] + "</password>\n" +
                            "         <dataBase64 xsi:type=\"xsd:string\">" + imgBase64 + "</dataBase64>\n" +
                            "         <id_peticion xsi:type=\"xsd:string\">" + args[2] + "</id_peticion>\n" +
                            "         <path xsi:type=\"xsd:string\">" + path + "</path>\n" +
                            "         <filename xsi:type=\"xsd:string\">" + filename + "</filename>\n" +
                            "      </urn:setImageBase64>\n" +
                            "   </soapenv:Body>\n" +
                            "</soapenv:Envelope>";

            try {
                url = new URL(EndPoints.SupervisionURL);
                connection = url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buffer = salida.getBytes();
            if (httpConn != null) {
                try {
                    bout.write(buffer);
                    byte[] b = bout.toByteArray();
                    httpConn.setRequestProperty("Contenedor-Length", String.valueOf(b.length));
                    httpConn.setRequestProperty("Contenedor-Type", "text/xml; charset=utf-8");
                    httpConn.setRequestProperty("SOAPAction", EndPoints.SoapAction);
                    httpConn.setRequestMethod("POST");
                    httpConn.setDoOutput(true);
                    httpConn.setDoInput(true);
                    out = httpConn.getOutputStream();
                    out.write(b);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Read the response.
            try {
                isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);
                while ((responseString = in.readLine()) != null) {
                    outputString = outputString + responseString;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                doc = sxBuild.build(new StringReader(outputString));
                rootNode = doc.getRootElement();
            } catch (JDOMException | IOException ex) {
                ex.printStackTrace();
            }

            if (rootNode != null) {
                List lista = rootNode.getChildren();
                for (int i = 0; i < lista.size(); i++) {
                    rootNode = (Element) lista.get(i);
                    lista = rootNode.getChildren();
                    for (int j = 0; j < lista.size(); j++) {
                        rootNode = (Element) lista.get(i);
                        lista = rootNode.getChildren();
                        rootNode = (Element) lista.get(0);
                        strResult = rootNode.getText();
                    }
                }
            }

            return strResult;
        }

        @Override
        protected void onPostExecute(String jsonObject) {
            super.onPostExecute(jsonObject);
            if (jsonObject != null) {
                numeroFotos = dbHelper.getNumeroFotos();
                progressDialog.setProgress(contadorImage);
                if (contadorImage < numeroFotos) {
                    isAsyntaskRun();
                } else {

                    ArrayList<String> tablas;
                    tablas = dbHelper.getTables();

                    Intent in = new Intent(Dashboard.this, GPSService.class);
                    Dashboard.this.stopService(in);

                    //BORRAMOS LOS DATOS DE LA DB
                    for (int i = 0; i < tablas.size(); i++) {
                        dbHelper.clearDB(tablas.get(i));
                    }

                    String path = Environment.getExternalStoragePublicDirectory(Dashboard.this.getString(R.string.app_foto)) + File.separator;

                    File directory = new File(path);
                    File[] files = directory.listFiles();

                    for (File file : files) {
                        if (file.getName().contains("IMG_FIRMA")) {
                            file.delete();
                        }
                    }
                    //MANDAMOS A LA PANTALLA DE LOGIN Y CERRAMOS TODAS LAS ACTIVIDDADES
                    Intent login = new Intent(DASHBOARD, Login.class);
                    startActivity(login);
                    finish();

                    contadorImage = 0;
                    Toast.makeText(Dashboard.this, "FINALIZO", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            } else {
                contadorImage = 0;
                a.cancel(true);
            }
        }
    }


    private void isAsyntaskRun() {
        if (a.getStatus() == AsyncTask.Status.FINISHED) {
            a.execute(usuario.getUsername(), usuario.getPassword(), responseDB);
        } else {
            a.cancel(true);
            a = null;
            a = new SincImag();
            a.execute(usuario.getUsername(), usuario.getPassword(), responseDB);
        }
    }
}