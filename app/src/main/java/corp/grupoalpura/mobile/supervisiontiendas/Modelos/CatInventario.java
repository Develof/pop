package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

public class CatInventario {

    private int idCategoriaInv;
    private String catInve;
    private String descipcion;
    private int estatus;

    public CatInventario(){}

    public int getIdCategoriaInv() {
        return idCategoriaInv;
    }

    public void setIdCategoriaInv(int idCategoriaInv) {
        this.idCategoriaInv = idCategoriaInv;
    }

    public String getCatInve() {
        return catInve;
    }

    public void setCatInve(String catInve) {
        this.catInve = catInve;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    @Override
    public String toString() {
        return catInve;
    }
}
