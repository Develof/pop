package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by consultor on 3/2/16.
 */
public class ProdPresentacion {

    private int id;
    private String presentacion;
    private int estatus;

    ProdPresentacion(){

    }

    public ProdPresentacion(int id, String presentacion, int estatus) {
        this.id = id;
        this.presentacion = presentacion;
        this.estatus = estatus;
    }


    @Override
    public String toString() {
        return presentacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
