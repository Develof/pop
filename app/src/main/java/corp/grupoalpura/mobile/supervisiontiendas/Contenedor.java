package corp.grupoalpura.mobile.supervisiontiendas;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Interfaces.CallbackInterface;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Espacios;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Visita;
import corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters.EspaciosAdapter;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.ScannActivity;

public class Contenedor extends AppCompatActivity {

    Usuario usuario;
    int activity, ventanaEspacio = 1, idEspacio = 0;
    private Espacios espacio;
    private ClienteSucursal cs;

    @BindView(R.id.menu_toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar)
    TextView tvToolbar;

    private static final int VISITA = 0;

    CallbackInterface callbackInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        usuario = (Usuario) getIntent().getSerializableExtra("USUARIO");
        cs = (ClienteSucursal) getIntent().getSerializableExtra("CLIENTE_SUCURSAL");
        activity = getIntent().getIntExtra("ACTIVIDAD", 0);

        if (activity != VISITA) checkVisita();
        loadFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scanner, menu);

        for (int i = 0; i < menu.size(); i++) {
            Drawable drawable = menu.getItem(i).getIcon();
            if (drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
            }
        }

        if (activity != 5)
            menu.getItem(0).setVisible(false);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (activity == 5) {
                    tvToolbar.setText("Familias");
                    FamiliasFragment familias = new FamiliasFragment().newInstance(cs, usuario, 1);
                    replaceFragment(familias);
                } else if (activity == -1) {
                    tvToolbar.setText("Espacios");
                    MenuEspaciosFragment familias = new MenuEspaciosFragment().newInstance(cs, usuario, ventanaEspacio);
                    replaceFragment(familias);
                } else if (activity == 2) {
                    tvToolbar.setText("Espacios");
                    SubMenuEspacioFragment espacios = new SubMenuEspacioFragment().newInstance(cs, usuario);
                    replaceFragment(espacios);
                } else if (activity == 2) {
                    tvToolbar.setText("Espacios");
                    SubMenuEspacioFragment espacios = new SubMenuEspacioFragment().newInstance(cs, usuario);
                    replaceFragment(espacios);
                } else if (activity == 4) {
                    tvToolbar.setText(espacio.getEspacio());
                    EspaciosFragment espacios = new EspaciosFragment().newInstance(cs, usuario, idEspacio);
                    replaceFragment(espacios);
                } else {
                    onBackPressed();
                }
                return true;
            case R.id.action_scanner:
                if (cs != null) {
                    Intent intent = new Intent(Contenedor.this, ScannActivity.class);
                    intent.putExtra("id_sucursal", cs);
                    intent.putExtra("Usuario", usuario);
                    intent.putExtra("ventana", 1);
                    startActivity(intent);
                }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadFragment() {
        switch (activity) {
            case Constantes.ACTIVIDADES.ASISTENCIA:

                DBHelper dbHelper = new DBHelper(Contenedor.this, null, Constantes.VERSION_BASE_DE_DATOS);
                Visita visita = dbHelper.getVisita(usuario.getId());

                /**
                 Si no existe una visita mandaremos al check in
                 Si existe una visita y la hora de salida esta vacia mandaremos a check out de lo contrario a check in
                 */
                if (visita != null) {
                    if (visita.getHora_check_fin().equals("")) {
                        tvToolbar.setText(getResources().getString(R.string.visita_ckeckOut));
                        final VisitaCheckOutFragment visitaCheckOutFragment = new VisitaCheckOutFragment().newInstance(usuario, Constantes.TIPO_VISITA.CHECK_OUT);
                        replaceFragment(visitaCheckOutFragment);
                    } else {
                        tvToolbar.setText(getResources().getString(R.string.visita_ckeckIn));
                        VisitaCheckInFragment visitaCheckInFragment = new VisitaCheckInFragment().newInstance(usuario, Constantes.TIPO_VISITA.CHECK_IN);
                        replaceFragment(visitaCheckInFragment);
                    }
                } else {
                    tvToolbar.setText(getResources().getString(R.string.visita_ckeckIn));
                    VisitaCheckInFragment visitaCheckInFragment = new VisitaCheckInFragment().newInstance(usuario, Constantes.TIPO_VISITA.CHECK_IN);
                    replaceFragment(visitaCheckInFragment);
                }
                break;
            case Constantes.ACTIVIDADES.GUIA_MERCADEO:
                tvToolbar.setText(getResources().getString(R.string.menu_guia_mercadeo));
                GuiaMercadeo guia_mercadeo = new GuiaMercadeo().newInstance(cs, usuario);
                replaceFragment(guia_mercadeo);
                break;
            case Constantes.ACTIVIDADES.ESPACIOS:
                activity = 3;
                tvToolbar.setText(getResources().getString(R.string.menu_espacios));
                SubMenuEspacioFragment espacios = new SubMenuEspacioFragment().newInstance(cs, usuario);
                replaceFragment(espacios);
                break;
            case Constantes.ACTIVIDADES.PRODUCTOS:
                tvToolbar.setText(getResources().getString(R.string.menu_productos));
                ProductosCatalogadosFragment producto_cat = new ProductosCatalogadosFragment().newInstance(cs, usuario, 0, 0);
                replaceFragment(producto_cat);
                break;
            case Constantes.ACTIVIDADES.FAMILIAS:
                tvToolbar.setText("Familias");
                FamiliasFragment familias = new FamiliasFragment().newInstance(cs, usuario, 1);
                replaceFragment(familias);
                break;
            case Constantes.ACTIVIDADES.ACTIVIDAD_COMPETENCIA:
                tvToolbar.setText(getResources().getString(R.string.menu_competencias));
                ActividadesCompetencia competencia = new ActividadesCompetencia().newInstance(cs, usuario, "");
                replaceFragment(competencia);
                break;
        }
    }

    private void checkVisita() {
        if (cs != null) {
            if (cs.getId() == 0) showAlert();
        } else {
            showAlert();
            ;
        }
    }

    private void showAlert() {
        new AlertDialog.Builder(Contenedor.this)
                .setTitle("ASISTENCIA")
                .setMessage("No existe ninguna visita registrada")
                .setCancelable(false)
                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Contenedor.this.finish();
                    }
                })
                .show();
    }

    public void getVentanaEspacio(int espacio) {
        ventanaEspacio = espacio;
    }


    public void getIdEspacio(int idEspacio, Espacios espacios) {
        this.idEspacio = idEspacio;
        this.espacio = espacios;
    }


    public void backInterface(CallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }


    //REMPLAZA EL FRAGMENT
    public void replaceFragment(android.support.v4.app.Fragment fragment) {

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != 0) {
            if (requestCode == 1) {
                FichaTecnicaProducto producto = (FichaTecnicaProducto) data.getSerializableExtra("PRODUCTO");
                callbackInterface.setProduct(producto);
            }
        }
    }

}
