package corp.grupoalpura.mobile.supervisiontiendas.Database.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import corp.grupoalpura.mobile.supervisiontiendas.Database.DateConverter;

@Entity
public class Udn {

    private static final String ID_UDN = "ID_UDN";
    private static final String CLAVE = "CLAVE";
    private static final String ALIAS = "ALIAS";
    private static final String LATITUD = "LATITUD";
    private static final String LONGITUD = "LONGITUD";
    private static final String DIRECCION = "DIRECCION";
    private static final String FECHA_CREACION = "FECHA_CREACION";
    private static final String ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    private static final String USUARIO = "USUARIO";
    private static final String ESTATUS = "ESTATUS";


    @PrimaryKey
    @ColumnInfo(name = ID_UDN)
    @SerializedName(ID_UDN)
    private int idUdn;

    @ColumnInfo(name = CLAVE)
    @SerializedName(CLAVE)
    private int clave;

    @ColumnInfo(name = ALIAS)
    @SerializedName(ALIAS)
    private String alias;

    @ColumnInfo(name = DIRECCION)
    @SerializedName(DIRECCION)
    private String direccion;

    @ColumnInfo(name = LATITUD)
    @SerializedName(LATITUD)
    private Double latitud;

    @ColumnInfo(name = LONGITUD)
    @SerializedName(LONGITUD)
    private Double longitud;

    @ColumnInfo(name = FECHA_CREACION)
    @TypeConverters(DateConverter.class)
    @SerializedName(FECHA_CREACION)
    private Date fechaCreacion;

    @ColumnInfo(name = ULTIMA_MODIFICACION)
    @TypeConverters(DateConverter.class)
    @SerializedName(ULTIMA_MODIFICACION)
    private Date ultimaModificacion;

    @ColumnInfo(name = USUARIO)
    @SerializedName(USUARIO)
    private String usuario;

    @ColumnInfo(name = ESTATUS)
    @SerializedName(ESTATUS)
    private int estatus;


    public int getIdUdn() {
        return idUdn;
    }

    public void setIdUdn(int idUdn) {
        this.idUdn = idUdn;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getUltimaModificacion() {
        return ultimaModificacion;
    }

    public void setUltimaModificacion(Date ultimaModificacion) {
        this.ultimaModificacion = ultimaModificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
