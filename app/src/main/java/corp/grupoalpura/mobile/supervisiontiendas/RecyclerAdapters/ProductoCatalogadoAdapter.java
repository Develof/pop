package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.DetalleProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterTipoExistencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Compania;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProductoPrecio;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.MyDialogFragment;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.LoadImage;


import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.widget.Toast;

/**
 * Created by Develof on 20/11/16.
 */

public class ProductoCatalogadoAdapter extends RecyclerView.Adapter<ProductoCatalogadoViewHolder> {

    DBHelper dbHelper;
    private final List<FichaTecnicaProducto> mProducts;
    ClienteSucursal sucursal;
    Usuario usuario;
    private final LayoutInflater mInflater;
    Context context;
    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final int TOP = 1;
    DecimalFormat form = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);

    double latitud = 0, longitud = 0;
    int imagenesTomadas = 0;
    ArrayList<String> arregloNombreImagen;
    ArrayList<Uri> urlImagenes;
    FragmentManager fg;
    private SharedPreferences.Editor loginEditor;
    public static String PRODUCTO_POSICION = "position";
    private SharedPreferences loginPrefs;


    public ProductoCatalogadoAdapter(Context context, List<FichaTecnicaProducto> mProducts, ClienteSucursal sucursal, Usuario usuario, FragmentManager fragmentManager) {
        mInflater = LayoutInflater.from(context);
        this.mProducts = new ArrayList<>(mProducts);
        this.sucursal = sucursal;
        this.usuario = usuario;
        this.fg = fragmentManager;
        this.context = context;
    }

    @Override
    public ProductoCatalogadoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //CREAMOS UN OBJETO SHAREDPREFERENCES PARA GUARDAR LA SESION
        loginPrefs = context.getSharedPreferences("producto_posicion", context.MODE_PRIVATE);
        loginEditor = loginPrefs.edit();
        form.applyPattern(Constantes.FORMATO_PRECIO);
        dbHelper = new DBHelper(context, null, Constantes.VERSION_BASE_DE_DATOS);
        final View itemView = mInflater.inflate(R.layout.adapter_ficha_tecnica_productos_catalogo, parent, false);
        return new ProductoCatalogadoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductoCatalogadoViewHolder holder, final int position) {

        final FichaTecnicaProducto producto = mProducts.get(position);

        final int id_producto_existencia = dbHelper.getIdProductoExhibido(producto.getId(), sucursal.getId());

        // Cargamos las imagenes de los productos dentro del objeto Imageview
        LoadImage loadImage = new LoadImage();

        String imageResource = Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_img_productos)) + File.separator + producto.getClave().toLowerCase() + ".png";
        loadImage.load(context, imageResource, holder.imgDescripcion);

        double precio_real = dbHelper.getPrecioUnitarioReal(producto.getClave());

        holder.imgRate.setVisibility(View.GONE);
        if (producto.getTop() == TOP) {
            holder.imgRate.setVisibility(View.VISIBLE);
            holder.imgRate.setColorFilter(ContextCompat.getColor(context, R.color.colorAmarillo), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        holder.txtClveProducto.setText(producto.getClave());
        holder.txtNombreProducto.setText(" - " + producto.getDescripcion());
        holder.txtPrecioBaseProducto.setText(" $ " + form.format(precio_real));
        holder.txtPresentcionProducto.setText(producto.getPresentacion());

        holder.txtPrecioBaseTitulo.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        holder.txtPrecioBaseProducto.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));


        if (id_producto_existencia == Constantes.EXHIBIDO.EXHIBIDO) {
            holder.checkBoxExhibido.setChecked(true);
        } else {
            holder.checkBoxExhibido.setChecked(false);
        }

        LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        ArrayList<Compania> arreglo_compania = dbHelper.getCompaniaProducto(producto.getId(), sucursal.getId());

        holder.contenidoPrecios.removeAllViews();
        for (int i = 0; i < arreglo_compania.size(); i++) {

            final ProductoPrecio productoPrecio = dbHelper.getPrecioUnitario(arreglo_compania.get(i).getId(), producto.getId(), sucursal.getId());
            if (productoPrecio.getId() != 0 && productoPrecio.getPrecio_unitario() != 0) {
                TextView txtCompania = new TextView(context);
                txtCompania.setLayoutParams(txtParams);
                txtCompania.setGravity(Gravity.CENTER);
                txtCompania.setTextSize(13);
                txtCompania.setText(arreglo_compania.get(i).getNombre() + "\n$ " + form.format(productoPrecio.getPrecio_unitario()));
                holder.contenidoPrecios.addView(txtCompania);
            }
        }


        holder.relativeLayoutProductoExhibido.setOnClickListener(v -> {
            if (id_producto_existencia == Constantes.EXHIBIDO.EXHIBIDO) {

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_producto_categoria_exhibicion);
                dialog.setCancelable(false);

                urlImagenes = new ArrayList<>();
                arregloNombreImagen = new ArrayList<>();
                imagenesTomadas = 0;


                Spinner spTipoExistencia = (Spinner) dialog.findViewById(R.id.spinner_tipo_existencia);
                final EditText edDescripcion = (EditText) dialog.findViewById(R.id.editText_descripcion_exhibicion);
                final ImageView imgEvidencia = (ImageView) dialog.findViewById(R.id.editTextAnexar);
                imgEvidencia.setVisibility(View.GONE);

                final int[] nTipo = new int[1];

                final ArrayList<AdapterTipoExistencia> arregloTipoExistencia;
                arregloTipoExistencia = dbHelper.getTipoExistencia();
                ArrayAdapter<AdapterTipoExistencia> dataAdapterExstencia = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, arregloTipoExistencia);

                spTipoExistencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position1, long id) {
                        nTipo[0] = arregloTipoExistencia.get(position1).getId_tipo_existencia();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                spTipoExistencia.setAdapter(dataAdapterExstencia);


                Button btnDialogCancelar = (Button) dialog.findViewById(R.id.btn_cancelar_exhibicion);
                btnDialogCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.checkBoxExhibido.setChecked(true);
                        for (String archivo : arregloNombreImagen) {
                            String myPath = Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_foto)) + "/" + archivo;
                            File f = new File(myPath);
                            f.delete();
                        }
                        dialog.dismiss();
                    }
                });

                Button btnGuardar = (Button) dialog.findViewById(R.id.btn_guardar_exhibicion);
                btnGuardar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!edDescripcion.getText().toString().equals("")) {
                            long id_sucursal_producto_existencia = dbHelper.insertProductoExistencia(sucursal.getId(), producto.getId(), nTipo[0], edDescripcion.getText().toString(), usuario.getUsername());
                            notifyDataSetChanged();
                            Toast toast = Toast.makeText(context, "Información Guardada", Toast.LENGTH_SHORT);
                            TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                            tv.setTextColor(Color.WHITE);
                            toast.show();
                            dialog.dismiss();

                            for (int i = 0; i < urlImagenes.size(); i++) {
                                long id_foto = dbHelper.insertFoto(sucursal.getId(), urlImagenes.get(i).toString(), arregloNombreImagen.get(i).toString(), latitud, longitud, usuario.getUsername());
                                if (id_foto != 0) {
                                    int id_foto_evidencia = (int) id_foto;
                                    int id_evidencia_existencia = (int) id_sucursal_producto_existencia;
                                    dbHelper.insertFotoRelacion(id_foto_evidencia, DBHelper.TABLA_SUCURSAL_PRODUCTO_EXISTENCIA, id_evidencia_existencia);

                                } else {
                                    Toast toast_error = Toast.makeText(context, "Ocurrio un error al guardar las imagenes", Toast.LENGTH_SHORT);
                                    TextView tv_error = (TextView) toast_error.getView().findViewById(android.R.id.message);
                                    tv_error.setTextColor(Color.WHITE);
                                    toast_error.show();
                                }
                            }
                        } else {
                            Toast toast = Toast.makeText(context, "Llena todos los campos", Toast.LENGTH_SHORT);
                            TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                            tv.setTextColor(Color.WHITE);
                            toast.show();
                        }
                    }
                });

                dialog.setOnKeyListener(new Dialog.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                            //Do something on back press
                            holder.checkBoxExhibido.setChecked(true);

                            for (String archivo : arregloNombreImagen) {
                                String myPath = Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_foto)) + "/" + archivo;
                                File f = new File(myPath);
                                f.delete();
                            }
                            dialog.dismiss();
                        }
                        return false;
                    }
                });

                dialog.show();

            } else {

                final AdapterTipoExistencia tipoExhibicion = dbHelper.getNombreProductoExistencia(id_producto_existencia);
                String comentarioExhibicion = dbHelper.getComentarioProductoExistencia(id_producto_existencia);


                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_producto_categoria_exhibicion_view);
                dialog.setCancelable(false);


                urlImagenes = new ArrayList<>();
                arregloNombreImagen = new ArrayList<>();
                imagenesTomadas = 0;

                TextView txtTipoExhibicion = (TextView) dialog.findViewById(R.id.textView_value_categoria_exhibicion);
                final EditText edDescripcion = (EditText) dialog.findViewById(R.id.editText_descripcion_exhibicion);
                final ImageView imgEvidencia = (ImageView) dialog.findViewById(R.id.editTextAnexar);

                txtTipoExhibicion.setText(tipoExhibicion.getNombre_existencia());
                edDescripcion.setText(comentarioExhibicion);


                imgEvidencia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogFragment newFragment = MyDialogFragment.newInstance(id_producto_existencia, DBHelper.TABLA_SUCURSAL_PRODUCTO_EXISTENCIA, producto.getClave() + producto.getDescripcion(), "IMG_PRODUCTO_EXISTENCIA_", sucursal, usuario);
                        newFragment.show(fg, "");
                        newFragment.setCancelable(false);

                    }
                });


                Button btnGuardarEx = (Button) dialog.findViewById(R.id.btn_guardar_exhibicion);
                btnGuardarEx.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!edDescripcion.getText().toString().equals("")) {
                            dbHelper.updateProductoExistenciaNo(id_producto_existencia, sucursal.getId(), usuario.getUsername(), edDescripcion.getText().toString(), tipoExhibicion.getId_tipo_existencia());
                            notifyDataSetChanged();
                            Toast toast = Toast.makeText(context, "Información Guardada", Toast.LENGTH_SHORT);
                            TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                            tv.setTextColor(Color.WHITE);
                            toast.show();
                            dialog.dismiss();

                            for (int i = 0; i < urlImagenes.size(); i++) {
                                long id_foto = dbHelper.insertFoto(sucursal.getId(), urlImagenes.get(i).toString(), arregloNombreImagen.get(i).toString(), latitud, longitud, usuario.getUsername());
                                if (id_foto != 0) {
                                    int id_foto_evidencia = (int) id_foto;
                                    int id_evidencia_existencia = id_producto_existencia;
                                    dbHelper.insertFotoRelacion(id_foto_evidencia, DBHelper.TABLA_SUCURSAL_PRODUCTO_EXISTENCIA, id_evidencia_existencia);

                                } else {
                                    Toast toast_error = Toast.makeText(context, "Ocurrio un error al guardar las imagenes", Toast.LENGTH_SHORT);
                                    TextView tv_error = (TextView) toast_error.getView().findViewById(android.R.id.message);
                                    tv_error.setTextColor(Color.WHITE);
                                    toast_error.show();
                                }
                            }
                        } else {
                            Toast toast = Toast.makeText(context, "Llena todos los campos", Toast.LENGTH_SHORT);
                            TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
                            tv.setTextColor(Color.WHITE);
                            toast.show();
                        }
                    }
                });

                Button btnDialogCancelar = (Button) dialog.findViewById(R.id.btn_cancelar_exhibicion);
                btnDialogCancelar.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                btnDialogCancelar.setOnClickListener(v1 -> {
                    holder.checkBoxExhibido.setChecked(true);
                    dbHelper.deleteProductoExistencia(context, id_producto_existencia, producto.getId(), sucursal.getId());
                    notifyDataSetChanged();
                    dialog.dismiss();
                });

                dialog.setOnKeyListener((dialog1, keyCode, event) -> {
                    for (String archivo : arregloNombreImagen) {
                        String myPath = Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_foto)) + "/" + archivo;
                        File f = new File(myPath);
                        f.delete();
                    }
                    dialog1.dismiss();
                    return false;
                });

                dialog.show();

            }
        });


        holder.cardViewProducto.setOnClickListener(v -> {
            loginEditor.putInt(PRODUCTO_POSICION, position);
            loginEditor.commit();
            Intent detalle_producto = new Intent(context, DetalleProducto.class);
            detalle_producto.putExtra(ID_SUCURSAL, sucursal);
            detalle_producto.putExtra(ID_USUARIO, usuario);
            detalle_producto.putExtra("producto", producto);
            detalle_producto.putExtra("ventana", Constantes.ACTIVIDAD_HOME.PRODUCTO_CATALOGADO);
            context.startActivity(detalle_producto);
        });
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }


    /**
     * Uso de la busqueda en poductos
     *
     * @param models
     */
    public void animateTo(List<FichaTecnicaProducto> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<FichaTecnicaProducto> newModels) {
        for (int i = mProducts.size() - 1; i >= 0; i--) {
            final FichaTecnicaProducto model = mProducts.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<FichaTecnicaProducto> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final FichaTecnicaProducto model = newModels.get(i);
            if (!mProducts.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<FichaTecnicaProducto> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final FichaTecnicaProducto model = newModels.get(toPosition);
            final int fromPosition = mProducts.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public FichaTecnicaProducto removeItem(int position) {
        final FichaTecnicaProducto model = mProducts.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, FichaTecnicaProducto model) {
        mProducts.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final FichaTecnicaProducto model = mProducts.remove(fromPosition);
        mProducts.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }


}
