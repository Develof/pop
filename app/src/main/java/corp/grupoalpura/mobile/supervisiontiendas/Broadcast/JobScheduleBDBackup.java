package corp.grupoalpura.mobile.supervisiontiendas.Broadcast;

import android.os.AsyncTask;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.EndPoints;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBBackup;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.DispositivoBDModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DispositivoBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.UsuarioBDHelper;
import me.tatarka.support.job.JobParameters;
import me.tatarka.support.job.JobService;

/**
  Created by Develof on 08/03/17.
 **/

public class JobScheduleBDBackup extends JobService {

    Usuario usuario;
    UsuarioBDHelper dbHelper;
    DBBackup respaldo;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {

        dbHelper = new UsuarioBDHelper(this);
        usuario = dbHelper.getFistUsuario();
        

        if (usuario != null) {
            BDBackup a = new BDBackup();
            a.execute(usuario.getUsername(), usuario.getPassword());
        }

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }


    private class BDBackup extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... args) {

            String responseString, strResult = null, outputString = "";
            URL url;
            URLConnection connection = null;
            OutputStream out;
            InputStreamReader isr;
            SAXBuilder sxBuild = new SAXBuilder();
            Document doc;
            Element rootNode;
            rootNode = null;
            String salida = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:SiteControllerwsdl\">" +
                    "   <soapenv:Header/>" +
                    "   <soapenv:Body>" +
                    "      <urn:askSetBdBase64BackUp soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                    "         <usuario xsi:type=\"xsd:string\">" + args[0] + "</usuario>" +
                    "         <password xsi:type=\"xsd:string\">" + args[1] + "</password>" +
                    "      </urn:askSetBdBase64BackUp>" +
                    "   </soapenv:Body>" +
                    "</soapenv:Envelope>";

            try {
                url = new URL(EndPoints.SupervisionURL);
                connection = url.openConnection();
                connection.setConnectTimeout(15000);
                connection.setReadTimeout(15000);
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buffer = salida.getBytes();
            if (httpConn != null) {
                try {
                    bout.write(buffer);
                    byte[] b = bout.toByteArray();
                    httpConn.setRequestProperty("Contenedor-Length", String.valueOf(b.length));
                    httpConn.setRequestProperty("Contenedor-Type", "text/xml; charset=utf-8");
                    httpConn.setRequestProperty("SOAPAction", EndPoints.SoapAction);
                    httpConn.setConnectTimeout(15000);
                    httpConn.setReadTimeout(15000);
                    httpConn.setRequestMethod("POST");
                    httpConn.setDoOutput(true);
                    httpConn.setDoInput(true);
                    out = httpConn.getOutputStream();
                    out.write(b);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Read the response.
            try {
                isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);
                while ((responseString = in.readLine()) != null) {
                    outputString = outputString + responseString;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (outputString != null) {
                    doc = sxBuild.build(new StringReader(outputString));
                    rootNode = doc.getRootElement();
                }
            } catch (JDOMException ex) {
                ex.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (rootNode != null) {
                List lista = rootNode.getChildren();
                for (int i = 0; i < lista.size(); i++) {
                    rootNode = (Element) lista.get(i);
                    lista = rootNode.getChildren();
                    for (int j = 0; j < lista.size(); j++) {
                        rootNode = (Element) lista.get(i);
                        lista = rootNode.getChildren();
                        rootNode = (Element) lista.get(0);
                        strResult = rootNode.getText();
                    }
                }
            }

            return strResult;
        }

        @Override
        protected void onPostExecute(String response) {
            if (response != null) {
                if (response.equals("true")) {

                    usuario = dbHelper.getFistUsuario();
                    respaldo = new DBBackup();
                    String respaldoBD = respaldo.backupDb(getApplicationContext().getPackageName(), DBHelper.DATABASE_NAME);
                    DispositivoBDHelper dispositivoBDHelper = new DispositivoBDHelper(JobScheduleBDBackup.this);
                    DispositivoBDModel dispositivo = dispositivoBDHelper.getDispositivo();
                    if (usuario != null) {
                        SendBDBackup sendBDBackup = new SendBDBackup();
                        sendBDBackup.execute(usuario.getUsername(), usuario.getPassword(), respaldoBD, dispositivo.getFecha());
                    }

                }
            }
            super.onPostExecute(response);
        }
    }


    private class SendBDBackup extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... args) {

            String responseString, strResult = null, outputString = "";
            URL url;
            URLConnection connection = null;
            OutputStream out;
            InputStreamReader isr;
            SAXBuilder sxBuild = new SAXBuilder();
            Document doc;
            Element rootNode;
            rootNode = null;
            String salida = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:SiteControllerwsdl\">" +
                    "   <soapenv:Header/>" +
                    "   <soapenv:Body>" +
                    "      <urn:setBdBase64 soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                    "         <usuario xsi:type=\"xsd:string\">" + args[0] + "</usuario>" +
                    "         <password xsi:type=\"xsd:string\">" + args[1] + "</password>" +
                    "         <dataBase64 xsi:type=\"xsd:string\">" + args[2] + "</dataBase64>" +
                    "         <fecha_base xsi:type=\"xsd:string\">" + args[3] + "</fecha_base>" +
                    "      </urn:setBdBase64>" +
                    "   </soapenv:Body>" +
                    "</soapenv:Envelope>";

            try {
                url = new URL(EndPoints.SupervisionURL);
                connection = url.openConnection();
                connection.setConnectTimeout(15000);
                connection.setReadTimeout(15000);
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buffer = salida.getBytes();
            if (httpConn != null) {
                try {
                    bout.write(buffer);
                    byte[] b = bout.toByteArray();
                    httpConn.setRequestProperty("Contenedor-Length", String.valueOf(b.length));
                    httpConn.setRequestProperty("Contenedor-Type", "text/xml; charset=utf-8");
                    httpConn.setRequestProperty("SOAPAction", EndPoints.SoapAction);
                    httpConn.setConnectTimeout(15000);
                    httpConn.setReadTimeout(15000);
                    httpConn.setRequestMethod("POST");
                    httpConn.setDoOutput(true);
                    httpConn.setDoInput(true);
                    out = httpConn.getOutputStream();
                    out.write(b);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //Read the response.
            try {
                isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);
                while ((responseString = in.readLine()) != null) {
                    outputString = outputString + responseString;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (outputString != null) {
                    doc = sxBuild.build(new StringReader(outputString));
                    rootNode = doc.getRootElement();
                }
            } catch (JDOMException ex) {
                ex.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (rootNode != null) {
                List lista = rootNode.getChildren();
                for (int i = 0; i < lista.size(); i++) {
                    rootNode = (Element) lista.get(i);
                    lista = rootNode.getChildren();
                    for (int j = 0; j < lista.size(); j++) {
                        rootNode = (Element) lista.get(i);
                        lista = rootNode.getChildren();
                        rootNode = (Element) lista.get(0);
                        strResult = rootNode.getText();
                    }
                }
            }

            return strResult;
        }

        @Override
        protected void onPostExecute(String response) {
            if (response != null) {

            }
            super.onPostExecute(response);
        }
    }


}
