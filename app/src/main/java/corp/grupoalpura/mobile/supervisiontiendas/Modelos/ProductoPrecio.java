package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 22/03/2016.
 */
public class ProductoPrecio {
    private int id;
    private int id_producto;
    private int id_sucursal;
    private int id_compania;
    private double precio_unitario;
    private double precio_promocion;
    private int aplica_iva;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;
    private String compania;
    private String descripcionCompania;

    public  ProductoPrecio(){}

    public ProductoPrecio(int id, double precio_unitario, double precio_promocion) {
        this.id = id;
        this.precio_unitario = precio_unitario;
        this.precio_promocion = precio_promocion;
    }

    public ProductoPrecio(int id, int id_compania, double precio_unitario, double precio_promocion, int id_sucursal, int id_producto, String usuario, String descripcionCompania, int estatus) {
        this.id = id;
        this.id_compania = id_compania;
        this.precio_unitario = precio_unitario;
        this.precio_promocion = precio_promocion;
        this.id_sucursal = id_sucursal;
        this.id_producto = id_producto;
        this.usuario = usuario;
        this.descripcionCompania = descripcionCompania;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public int getId_compania() {
        return id_compania;
    }

    public void setId_compania(int id_compania) {
        this.id_compania = id_compania;
    }

    public double getPrecio_promocion() {
        return precio_promocion;
    }

    public void setPrecio_promocion(double precio_promocion) {
        this.precio_promocion = precio_promocion;
    }

    public double getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(double precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public int getAplica_iva() {
        return aplica_iva;
    }

    public void setAplica_iva(int aplica_iva) {
        this.aplica_iva = aplica_iva;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getDescripcionCompania() {
        return descripcionCompania;
    }

    public void setDescripcionCompania(String descripcionCompania) {
        this.descripcionCompania = descripcionCompania;
    }
}
