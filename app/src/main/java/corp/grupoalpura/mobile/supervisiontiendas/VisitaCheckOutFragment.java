package corp.grupoalpura.mobile.supervisiontiendas;

import android.Manifest;
import android.annotation.TargetApi;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBBackup;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.LocalizacionGps.Localizacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.RegistroAplicacionBDModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Visita;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.RegistroAplicacionBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.SucursalBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by consultor on 2/26/16.
 **/
public class VisitaCheckOutFragment extends Fragment implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback {

    GoogleMap googleMap;
    MapView mapView;
    Usuario usuario = null;
    Visita visita = null;
    RelativeLayout relativeLayoutMapa;
    DBHelper dbHelper;
    double latitud = 0, longitud = 0;
    int tipoVisita;
    Marker marker;
    ClienteSucursal sucursal = null;
    boolean isRutaAleatoriaActiva;
    public static String USUARIO = "Usuario";
    public static String TIPO_VISITA = "TipoVisitaKey";
    View viewMarker;
    Localizacion localizacion;
    RegistroAplicacionBDModel registroAplicacion;
    public static int CHECK_IN_INACTIVO = 0;

    @BindView(R.id.checkIn_btn_check)
    Button btnCheckOut;

    public VisitaCheckOutFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment VisitaCheckInFragment.
     */
    public VisitaCheckOutFragment newInstance(Usuario user, int tipoVisita) {
        VisitaCheckOutFragment fragment = new VisitaCheckOutFragment();
        Bundle args = new Bundle();
        args.putSerializable(USUARIO, user);
        args.putInt(TIPO_VISITA, tipoVisita);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usuario = (Usuario) getArguments().getSerializable(USUARIO);
            tipoVisita = getArguments().getInt(TIPO_VISITA);
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_visita_check_in, container, false);
        ButterKnife.bind(this, view);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        width = width - 120;


        viewMarker = inflater.inflate(R.layout.adapter_marker_vista, container, false);
        viewMarker.setLayoutParams(new RelativeLayout.LayoutParams(width, RelativeLayout.LayoutParams.WRAP_CONTENT));

        relativeLayoutMapa = (RelativeLayout) view.findViewById(R.id.contenedorMapa);

        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);

        RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
        registroAplicacion = registroAplicacionBDHelper.getRegistroAplicacion();

        Calendar calendar = Calendar.getInstance();
        int dia = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (dia == 0) dia = 7;

        isRutaAleatoriaActiva = dbHelper.getRutaAlternaActiva(dia);

        SucursalBDHelper sucursalBDHelper = new SucursalBDHelper(getActivity());
        if (isRutaAleatoriaActiva) {
            sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD", registroAplicacion.getNo_sucursal());
        } else {
            sucursal = sucursalBDHelper.getDataSucursal(dia, "PRIORIDAD_SUGERIDA", registroAplicacion.getNo_sucursal());
        }
        localizacion = new Localizacion(getActivity());

        mapView = (MapView) view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
                visita = dbHelper.getVisita(usuario.getId());
                if (localizacion.connect()) {
                    localizacion.getLocalizacion();
                    latitud = localizacion.getLatitud();
                    longitud = localizacion.getLongitud();
                }

                if (visita == null) {
                    Snackbar.make(relativeLayoutMapa, "DEBES HACER CHECKIN EN ESTA SUCURSAL ANTES", Snackbar.LENGTH_LONG).show();
                } else {
                    // AQUI IRA EL CODIGO PARA SABER SI LA VISITA ESTA EN CHEKIN O EN CHECKOUT
                    //SI HAY UN CHECKIN ACTIVO Y EL CHECKOUT NO TIENE HORA EN QUE SE FINALIZO ACTUALIZAMOS EL CAMPO DE LA HORA DE CHEKOUT
                    if (visita.getHora_check_fin().equals("")) {
                        //AQUI ACTUALIZAREMOS EL CHECKOUT

                        Date dateHoy = new Date();
                        String fechaHoy = Dates.dateToStringAlerta(dateHoy);

                        int alertasFaltantes = dbHelper.getAlertasNoLeidas(sucursal.getId(), fechaHoy);
                        int contadorMensajesFaltantes = 0;

                        Cursor cursor = getActivity().getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
                        int read;
                        if (cursor.moveToFirst()) {
                            do {
                                read = Integer.parseInt(cursor.getString(cursor.getColumnIndex("read")));
                                if (read == Constantes.MENSAJES.NO_LEIDO) {
                                    contadorMensajesFaltantes++;
                                }
                            } while (cursor.moveToNext());
                        }

                        if (alertasFaltantes > 0) {
                            Snackbar.make(relativeLayoutMapa, "Tienes alertas para hoy que no has leido", Snackbar.LENGTH_LONG).setAction("Alertas", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent fichaTecnicaAlertas = new Intent(getActivity(), Alertas.class);
                                    fichaTecnicaAlertas.putExtra("id_sucursal", sucursal);
                                    fichaTecnicaAlertas.putExtra("Usuario", usuario);
                                    startActivity(fichaTecnicaAlertas);
                                }
                            }).setActionTextColor(Color.YELLOW).show();
                        } else if (contadorMensajesFaltantes > 0) {
                            Snackbar.make(relativeLayoutMapa, "Tienes mensajes que no has leido", Snackbar.LENGTH_LONG).setAction("Mensajes", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent mensajes = new Intent(getActivity(), MensajesPush.class);
                                    mensajes.putExtra("id_sucursal", sucursal);
                                    mensajes.putExtra("Usuario", usuario);
                                    startActivity(mensajes);
                                }
                            }).setActionTextColor(Color.YELLOW).show();

                        } else {

                            dbHelper.updateVisita(visita.getId_visita(), latitud, longitud, usuario.getUsername());

                            /* ACUTALIZAMOS LA VISITA Y DESACTIVAMOS EL CHECK IN */
                            RegistroAplicacionBDHelper registroAplicacionBDHelper = new RegistroAplicacionBDHelper(getActivity());
                            registroAplicacionBDHelper.updateNoSucursal(registroAplicacion.getNo_sucursal() + 1);
                            registroAplicacionBDHelper.updateCheckIn(CHECK_IN_INACTIVO);


                            DBBackup backup = new DBBackup();
                            backup.createBackupDB(getActivity().getApplicationContext().getPackageName(), DBHelper.DATABASE_NAME, sucursal);
                            Intent dashboard = new Intent(getActivity(), Dashboard.class);
                            dashboard.putExtra("Usuario", usuario);
                            startActivity(dashboard);
                            getActivity().finish();

                        }
                    } else {
                        Snackbar.make(relativeLayoutMapa, "DEBES HACER CHECKIN EN ESTA SUCURSAL ANTES", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (permissions.length == 1 && permissions[0].equals("android.permission.ACCESS_FINE_LOCATION") && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(true);
                    LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                            CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);
                            googleMap.moveCamera(center);
                            googleMap.animateCamera(zoom);

                            if (marker != null) {
                                marker.remove();
                            }

                            marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Estoy Aqui").snippet("Population: 4,137,400").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                            marker.showInfoWindow();
                            latitud = location.getLatitude();
                            longitud = location.getLongitude();
                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                        }

                        @Override
                        public void onProviderEnabled(String provider) {
                        }

                        @Override
                        public void onStatusChanged(String provider, int status,
                                                    Bundle extras) {
                        }
                    });
                }
                googleMap.setMyLocationEnabled(true);
                LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);
                        googleMap.moveCamera(center);
                        googleMap.animateCamera(zoom);

                        if (marker != null) {
                            marker.remove();
                        }

                        marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Estoy Aqui").snippet("Population: 4,137,400").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                        marker.showInfoWindow();
                        latitud = location.getLatitude();
                        longitud = location.getLongitude();
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onStatusChanged(String provider, int status,
                                                Bundle extras) {
                    }
                });
            } else {
                //permission denied
            }
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setOnMarkerClickListener(this);

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {

                // Getting view from the layout file info_window_layout
                Calendar c = Calendar.getInstance();
                TextView nombre_sucursal = (TextView) viewMarker.findViewById(R.id.textView_title_nombre_princial_sucursal);
                TextView direccion = (TextView) viewMarker.findViewById(R.id.textView_info_direccion);
                TextView hora_visita = (TextView) viewMarker.findViewById(R.id.textView_info_hora);
                TextView hora_salida_titulo = (TextView) viewMarker.findViewById(R.id.textView_hora);

                hora_salida_titulo.setText("Hora de la salida");
                hora_visita.setText(String.format("%02d", c.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", c.get(Calendar.MINUTE)));
                nombre_sucursal.setText(sucursal.getAlias());
                direccion.setText(sucursal.getCalle() + " " + sucursal.getNo_ext() + " " + sucursal.getColonia());
                return viewMarker;

            }
        });


        // SI LA VERSION ES 6.0 O MAYOR PERDIREMOS EL PERMISO PARA ACTIVAR GPS LA PRIMERA VES QUE ENTRA A LA PANTALLA, SINO ACTIVAMOS EL GSP SIN PEDIR PERMISO
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            googleMap.setMyLocationEnabled(true);

            if (marker != null) {
                marker.remove();
            }

            marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(sucursal.getLatitud(), sucursal.getLongitud())).icon(BitmapDescriptorFactory.fromResource(R.drawable.tienda_verde)));
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    if (marker != null) {
                        marker.showInfoWindow();
                    }
                }
            });

            CameraUpdate center = CameraUpdateFactory.newLatLngZoom(new LatLng(sucursal.getLatitud(), sucursal.getLongitud()), 14);
            googleMap.animateCamera(center, 500, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    marker.showInfoWindow();
                }

                @Override
                public void onCancel() {
                    marker.showInfoWindow();
                }
            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

}

