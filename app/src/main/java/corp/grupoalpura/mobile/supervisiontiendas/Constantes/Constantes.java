package corp.grupoalpura.mobile.supervisiontiendas.Constantes;

/**
  Created by consultor on 2/24/16.
 **/
public class Constantes {

    /* VALORES STATICOS PARA LA BASE DE DATOS SQLITE */
    public static int VERSION_BASE_DE_DATOS = 4;

    //FORMATO DE PRECIO EN PRODUCTOS
    public static String FORMATO_PRECIO = "#,##0.00";
    public static String FORMATO_PRECIO_SIGNO = "$ #,##0.00";

    public static class ACTIVIDADES {
        public static final int ASISTENCIA = 0;
        public static final int GUIA_MERCADEO = 1;
        public static final int ESPACIOS = 2;
        public static final int PRODUCTOS = 3;
        public static final int FICHA_TECNICA = 4;
        public static final int FAMILIAS = 5;
        public static final int ACTIVIDAD_COMPETENCIA = 6;
        public static final int ENCUESTAS = 7;
    }

    public static class TIPO_VISITA {
        public static int CHECK_IN = 0;
        public static int CHECK_OUT = 1;
    }

    public static class TIPO_EVENTUALIDAD{
        public static String TIPO_SINCRONIZACION_INICIAL = "SINCRONIZACION_INICIAL";
        public static String TIPO_SINCRONIZACION_FINAL = "SINCRONIZACION_FINAL";
    }

    public static class EXHIBIDO {
        public static int EXHIBIDO = 0;
    }

    public static class SUCURSAL_LOCALIZACION_ACTIVA{
        public static int ACTIVA = 1;
        public static int INACTIVA = 0;
    }

    public static class MENSAJES{
        public static int NO_LEIDO = 0;
        public static int LEIDO = 1;
    }

    public static class FOTOS{
        public static int TAKE_PHOTO_CODE = 150;
        public static int TAKE_PHOTO_CODE_FRAGMENT = 151;
    }

    public static class TIPO_INCIDENCIA{
        public static int INCIDENCIA_POSITIVA = 1;
        public static int INCIDENCIA_NEGATIVA = 0;
    }

    public static class TIPO_ESPACIO_ADQUISICION {
        public static final int RENTADO = 0;
        public static final int GANADO = 1;
        public static final int BASE = 2;
    }

    public static class ESTATUS {
        public static int CREADO_WEB = 0;
        public static int CREADO_WEB_MODIFICADO_MOVIL = 1;
        public static int CREADO_MOVIL = 2;
        public static int CREADO_MOVIL_MODIFICADO_MOVIL = 3;
    }

    public static class ACTIVIDAD_HOME{
        public static int PRODUCTO_CATALOGADO = 0;
    }

    public static class PRODUCTO_PRECIO{
        public static int PRODUCTO_PRECIO_ACTIVO = 1;
        public static int PRODUCTO_PRECIO_INACTIVO = 0;
    }

    public static class CAMARA {
        public static int INTENT_REQUEST_GET_IMAGES = 20;
    }


}
