package corp.grupoalpura.mobile.supervisiontiendas.view_pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FotosEvidenciaRelacion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by consultor on 21/09/2015.
 */
public class FragmentAdapter extends FragmentStatePagerAdapter {
    List<FotosEvidenciaRelacion> fragments;

    public FragmentAdapter(FragmentManager fm, ArrayList<FotosEvidenciaRelacion> Photos) {
        super(fm);
        this.fragments = Photos;

    }

    @Override
    public Fragment getItem(int position) {
        return ImageSlidePager.newInstance(fragments.get(position).getArchivo());
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
