package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.R;

public class EspacioFrenteViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.img_producto)
    ImageView imgProducto;
    @BindView(R.id.card_view_espacios_frentes)
    CardView cvEspacio;
    @BindView(R.id.text_producto_descripcion)
    TextView tvProducto;
    @BindView(R.id.text_compania)
    TextView tvCompania;
    @BindView(R.id.text_frente)
    TextView tvFrente;

    public EspacioFrenteViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
