package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.LocalizacionUsuario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Encryption;

/**
 * Created by Develof on 06/04/17.
 */

public class UsuarioBDHelper extends DBHelper {

    public UsuarioBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }


    public Usuario getUsuario(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Usuario user = null;
        password = Encryption.md5(password);

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLA_USUARIO + " WHERE TRIM(" + COLUMNA_USUARIO_USERNAME + ") = '" + username.trim() + "' AND TRIM(" + COLUMNA_USUARIO_PASSWORD + ") = '" + password.trim() + "'", null);

        if (cursor.moveToFirst()) {
            do {
                int _ID = Integer.parseInt(cursor.getString(0));
                int UDN = Integer.parseInt(cursor.getString(1));
                int ID_JEFE = Integer.parseInt(cursor.getString(2));
                int CANAL = Integer.parseInt(cursor.getString(3));
                String NOMBRE = cursor.getString(4);
                String EMAIL = cursor.getString(5);
                String USERNAME = cursor.getString(6);
                String PASSWORD = cursor.getString(7);
                String ROL = cursor.getString(8);
                int CAMBIO_PASSWORD = Integer.parseInt(cursor.getString(9));
                int CAMBIO_LOCALIZACION = Integer.parseInt(cursor.getString(10));
                String HASH_RESET = cursor.getString(11);
                int INTENTOS = Integer.parseInt(cursor.getString(12));
                String FECHA_ERROR = cursor.getString(13);
                int NIVEL = Integer.parseInt(cursor.getString(14));
                String FECHA_PASSWORD = cursor.getString(15);
                String FECHA_CREACION = cursor.getString(16);
                String ULTIMA_MODIFICACION = cursor.getString(17);
                String USUARIO = cursor.getString(18);
                int USUARIO_ESTATUS = Integer.parseInt(cursor.getString(19));
                user = new Usuario(_ID, UDN, ID_JEFE, CANAL, NOMBRE, EMAIL, USERNAME, PASSWORD, ROL, CAMBIO_PASSWORD, HASH_RESET, CAMBIO_LOCALIZACION, INTENTOS, FECHA_ERROR, NIVEL, FECHA_PASSWORD, FECHA_CREACION, ULTIMA_MODIFICACION, USUARIO, USUARIO_ESTATUS);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return user;
    }


    public Usuario getFistUsuario() {
        SQLiteDatabase db = this.getReadableDatabase();
        Usuario user = null;
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLA_USUARIO, null);

        if (cursor.moveToFirst()) {
            int _ID = Integer.parseInt(cursor.getString(0));
            int UDN = Integer.parseInt(cursor.getString(1));
            int ID_JEFE = Integer.parseInt(cursor.getString(2));
            int TIPO_USUARIO = 0;
            String NOMBRE = cursor.getString(4);
            String EMAIL = cursor.getString(5);
            String USERNAME = cursor.getString(6);
            String PASSWORD = cursor.getString(7);
            String ROL = cursor.getString(8);
            int CAMBIO_PASSWORD = Integer.parseInt(cursor.getString(9));
            int CAMBIO_LOCALIZACION = Integer.parseInt(cursor.getString(10));
            String HASH_RESET = cursor.getString(11);
            int INTENTOS = Integer.parseInt(cursor.getString(12));
            String FECHA_ERROR = cursor.getString(13);
            int NIVEL = Integer.parseInt(cursor.getString(14));
            String FECHA_PASSWORD = cursor.getString(15);
            String FECHA_CREACION = cursor.getString(16);
            String ULTIMA_MODIFICACION = cursor.getString(17);
            String USUARIO = cursor.getString(18);
            int USUARIO_ESTATUS = Integer.parseInt(cursor.getString(19));
            user = new Usuario(_ID, UDN, ID_JEFE, TIPO_USUARIO, NOMBRE, EMAIL, USERNAME, PASSWORD, ROL, CAMBIO_PASSWORD, HASH_RESET, CAMBIO_LOCALIZACION, INTENTOS, FECHA_ERROR, NIVEL, FECHA_PASSWORD, FECHA_CREACION, ULTIMA_MODIFICACION, USUARIO, USUARIO_ESTATUS);

        }

        cursor.close();
        return user;
    }


    public void insetUserLocation(int id_usuario, double latitud, double longitud, String usuario) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_LOCALIZACION_USUARIO_ID_USUARIO, id_usuario);
        values.put(COLUMNA_LOCALIZACION_USUARIO_LATITUD, latitud);
        values.put(COLUMNA_LOCALIZACION_USUARIO_LONGITUD, longitud);
        values.put(COLUMNA_LOCALIZACION_USUARIO_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_LOCALIZACION_USUARIO_USUARIO, usuario);
        values.put(COLUMNA_LOCALIZACION_USUARIO_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, 2);
        database.insert(TABLA_LOCALIZACION_USUARIO, null, values);
        database.close();
    }


    public LocalizacionUsuario getLastUserLocation() {
        LocalizacionUsuario localizacion = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Localizacion_Usuario ORDER BY FECHA_CREACION DESC LIMIT 1", null);

        if (cursor.moveToFirst()) {
            localizacion = new LocalizacionUsuario();
            localizacion.setIdLocalizacion(Integer.parseInt(cursor.getString(0)));
            localizacion.setIdUsuario(Integer.parseInt(cursor.getString(1)));
            localizacion.setLatitud(Double.parseDouble(cursor.getString(2)));
            localizacion.setLongitud(Double.parseDouble(cursor.getString(3)));
            localizacion.setFecha_creacion(cursor.getString(4));
        }

        cursor.close();
        db.close();
        return localizacion;
    }


}
