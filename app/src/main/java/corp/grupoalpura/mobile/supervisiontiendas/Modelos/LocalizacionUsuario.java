package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by SergioMtz on 13/03/2017.
 */
public class LocalizacionUsuario {

    private int idLocalizacion;
    private int idUsuario;
    private double latitud;
    private double longitud;
    private String fecha_creacion;

    public LocalizacionUsuario(){};

    public LocalizacionUsuario(int idLocalizacion, int idUsuario, double latitud, double longitud) {
        this.idLocalizacion = idLocalizacion;
        this.idUsuario = idUsuario;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getIdLocalizacion() {
        return idLocalizacion;
    }

    public void setIdLocalizacion(int idLocalizacion) {
        this.idLocalizacion = idLocalizacion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
}
