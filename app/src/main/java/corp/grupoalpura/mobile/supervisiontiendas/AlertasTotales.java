package corp.grupoalpura.mobile.supervisiontiendas;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterAlertas;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;


import java.util.ArrayList;
import java.util.Date;


public class AlertasTotales extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static String USUARIO = "Usuario";

    Usuario usuario = null;
    ListView listViewAlertas;
    DBHelper dbHelper;
    CustomAdapter adapter;
    ArrayList<AdapterAlertas> alertas = new ArrayList<>();

    public AlertasTotales() {
        // Required empty public constructor
    }

    public AlertasTotales newInstance(Usuario usuario) {
        AlertasTotales fragment = new AlertasTotales();
        Bundle args = new Bundle();
        args.putSerializable(USUARIO, usuario);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usuario = (Usuario) getArguments().getSerializable(USUARIO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_alertas_totales, container, false);

        dbHelper = new DBHelper(getActivity(), null, Constantes.VERSION_BASE_DE_DATOS);
        alertas = dbHelper.getAlertasTotal();
        listViewAlertas = (ListView) view.findViewById(R.id.listView_alertas_totales);

        adapter = new CustomAdapter(getActivity());
        listViewAlertas.setAdapter(adapter);

        listViewAlertas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView txtFecha = (TextView) view.findViewById(R.id.textView_alerta_fecha_value);

                final Dialog dialogMensajeAlerta = new Dialog(getActivity());
                dialogMensajeAlerta.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogMensajeAlerta.setContentView(R.layout.dialog_total_alertas);

                TextView tituloAlerta = (TextView) dialogMensajeAlerta.findViewById(R.id.textview_title_mensaje_alerta);
                TextView fechaAlerta = (TextView) dialogMensajeAlerta.findViewById(R.id.textView_fecha_notificacion);
                TextView notificacion = (TextView) dialogMensajeAlerta.findViewById(R.id.textView_notificacion);
                TextView sucursalAlerta = (TextView) dialogMensajeAlerta.findViewById(R.id.textView_alerta_sucursal_values);
                TextView btnAceptar = (TextView) dialogMensajeAlerta.findViewById(R.id.btn_aceptar_espacio);

                tituloAlerta.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                btnAceptar.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

                fechaAlerta.setText(txtFecha.getText().toString());
                notificacion.setText(alertas.get(position).getDescripcion());
                sucursalAlerta.setText(alertas.get(position).getAlerta_sucursal());

                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogMensajeAlerta.dismiss();
                    }
                });

                dialogMensajeAlerta.show();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialogMensajeAlerta.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

            }
        });

        return view;

    }

    private class CustomAdapter extends BaseAdapter {
        Context context;

        public CustomAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return alertas.size();
        }

        @Override
        public Object getItem(int position) {
            return alertas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.adapter_alertas_total, parent, false);

            TextView txtSucursal = (TextView) row.findViewById(R.id.textView_alerta_sucursal_value);
            TextView txtFecha = (TextView) row.findViewById(R.id.textView_alerta_fecha_value);
            TextView txtMensaje = (TextView) row.findViewById(R.id.textView_alerta_alerta_value);

            txtFecha.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            Date fechaAlerta = Dates.stringToDate(alertas.get(position).getFecha());

            txtFecha.setText(Dates.dateToString(fechaAlerta));
            txtMensaje.setText(alertas.get(position).getDescripcion());
            txtSucursal.setText(alertas.get(position).getAlerta_sucursal());

            return row;
        }
    }

}
