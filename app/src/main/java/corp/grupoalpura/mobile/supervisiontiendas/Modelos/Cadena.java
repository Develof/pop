package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by uriel_000 on 03/03/2016.
 */
public class Cadena {
    private int id;
    private int id_cliente;
    private String rfc;
    private String razon_social;
    private String alias;
    private String fecha_creacion;
    private String ultima_modificacion;

    public void Cadena(){

    }

    public Cadena(int id, int id_cliente, String rfc, String razon_social, String alias, String fecha_creacion, String ultima_modificacion) {
        this.id = id;
        this.id_cliente = id_cliente;
        this.rfc = rfc;
        this.razon_social = razon_social;
        this.alias = alias;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
}
