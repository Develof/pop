package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 04/04/2016.
 */
public class AdapterForeCast {
    private int id_forecast;
    private String semana;
    private String deposito;
    private String clave;
    private String upc;
    private String item_nbr;
    private String producto;
    private String presentacion;
    private double precio_venta;
    private String fillrate;
    private float forecast;
    private String vta_prom_4sem;
    private int inventario_existente;
    private float dias_inventar;
    private float dif_prom_vta_vs;
    private String porcentaje_prom_vtavs_forecast;

    AdapterForeCast(){};

    public AdapterForeCast(String clave, String upc, String item, String producto, String presentacion, double precio_venta, String fillrate, float forecast, String vta_prom_cuatro_sem, int inventario_existente, float dias_inventar, float dif_prom_vta_vs, String porcentaje_prom_vta_vs_forecast) {
        this.clave = clave;
        this.upc = upc;
        this.item_nbr = item;
        this.producto = producto;
        this.presentacion = presentacion;
        this.precio_venta = precio_venta;
        this.fillrate = fillrate;
        this.forecast = forecast;
        this.vta_prom_4sem = vta_prom_cuatro_sem;
        this.inventario_existente = inventario_existente;
        this.dias_inventar = dias_inventar;
        this.dif_prom_vta_vs = dif_prom_vta_vs;
        this.porcentaje_prom_vtavs_forecast = porcentaje_prom_vta_vs_forecast;
    }


    public AdapterForeCast(int id_forecast, String semana, String deposito, String clave, String upc, String item_nbr, String producto, String presentacion, double precio_venta, String fillrate, float forecast, String vta_prom_4sem, int inventario_existente, float dias_inventar, float dif_prom_vta_vs, String porcentaje_prom_vtavs_forecast) {
        this.id_forecast = id_forecast;
        this.semana = semana;
        this.deposito = deposito;
        this.clave = clave;
        this.upc = upc;
        this.item_nbr = item_nbr;
        this.producto = producto;
        this.presentacion = presentacion;
        this.precio_venta = precio_venta;
        this.fillrate = fillrate;
        this.forecast = forecast;
        this.vta_prom_4sem = vta_prom_4sem;
        this.inventario_existente = inventario_existente;
        this.dias_inventar = dias_inventar;
        this.dif_prom_vta_vs = dif_prom_vta_vs;
        this.porcentaje_prom_vtavs_forecast = porcentaje_prom_vtavs_forecast;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getItem() {
        return item_nbr;
    }

    public void setItem(String item) {
        this.item_nbr = item;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public double getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(double precio_venta) {
        this.precio_venta = precio_venta;
    }

    public String getFillrate() {
        return fillrate;
    }

    public void setFillrate(String fillrate) {
        this.fillrate = fillrate;
    }

    public float getForecast() {
        return forecast;
    }

    public void setForecast(float forecast) {
        this.forecast = forecast;
    }

    public String getVta_prom_cuatro_sem() {
        return vta_prom_4sem;
    }

    public void setVta_prom_cuatro_sem(String vta_prom_cuatro_sem) {
        this.vta_prom_4sem = vta_prom_cuatro_sem;
    }

    public int getInventario_existente() {
        return inventario_existente;
    }

    public void setInventario_existente(int inventario_existente) {
        this.inventario_existente = inventario_existente;
    }

    public float getDias_inventar() {
        return dias_inventar;
    }

    public void setDias_inventar(float dias_inventar) {
        this.dias_inventar = dias_inventar;
    }

    public float getDif_prom_vta_vs() {
        return dif_prom_vta_vs;
    }

    public void setDif_prom_vta_vs(float dif_prom_vta_vs) {
        this.dif_prom_vta_vs = dif_prom_vta_vs;
    }

    public String getPorcentaje_prom_vta_vs_forecast() {
        return porcentaje_prom_vtavs_forecast;
    }

    public void setPorcentaje_prom_vta_vs_forecast(String porcentaje_prom_vta_vs_forecast) {
        this.porcentaje_prom_vtavs_forecast = porcentaje_prom_vta_vs_forecast;
    }
}
