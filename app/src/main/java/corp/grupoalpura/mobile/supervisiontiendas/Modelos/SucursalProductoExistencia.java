package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by consultor on 3/2/16.
 */
public class SucursalProductoExistencia {

    private int id;
    private int id_sucursal;
    private int id_producto;
    private String fecha;
    private int cantidad;
    private int id_foto;
    private int exhibido;
    private String ultima_modificacion;
    private String fecha_creacion;
    private String usuario;
    private int estatus;

    public SucursalProductoExistencia() {
    }

    public SucursalProductoExistencia(int id, int id_sucursal, int id_producto, String fecha, int cantidad, int id_foto, int exhibido, String ultima_modificacion, String fecha_creacion, String usuario, int estatus) {
        this.id = id;
        this.id_sucursal = id_sucursal;
        this.id_producto = id_producto;
        this.fecha = fecha;
        this.cantidad = cantidad;
        this.id_foto = id_foto;
        this.exhibido = exhibido;
        this.ultima_modificacion = ultima_modificacion;
        this.fecha_creacion = fecha_creacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getId_foto() {
        return id_foto;
    }

    public void setId_foto(int id_foto) {
        this.id_foto = id_foto;
    }

    public int getExhibido() {
        return exhibido;
    }

    public void setExhibido(int exhibido) {
        this.exhibido = exhibido;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
