package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;

public class Singleton {

    private static volatile Singleton sSoleInstance = new Singleton();
    private Usuario usuario;
    private ClienteSucursal cs;

    //private constructor.
    private Singleton() {
    }

    public static Singleton getInstance() {

        if (sSoleInstance == null) {
            sSoleInstance = new Singleton();
        }

        return sSoleInstance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ClienteSucursal getCs() {
        return cs;
    }

    public void setCs(ClienteSucursal cs) {
        this.cs = cs;
    }
}
