package corp.grupoalpura.mobile.supervisiontiendas.Application;

import android.app.Application;
import android.arch.persistence.room.Room;

import corp.grupoalpura.mobile.supervisiontiendas.Database.DBPop;

public class PopApplication extends Application {

    public DBPop database;
    private String db_name = "dbPop.db";


    @Override
    public void onCreate() {
        super.onCreate();
        database = Room.databaseBuilder(getApplicationContext(),
                DBPop.class, db_name).allowMainThreadQueries().build();
    }
}
