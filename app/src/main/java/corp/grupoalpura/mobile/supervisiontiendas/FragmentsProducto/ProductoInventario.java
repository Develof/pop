package corp.grupoalpura.mobile.supervisiontiendas.FragmentsProducto;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CatInventario;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.CatInventarioBDHelper;

/**
 * Clase para ingresar inventario en tienda por producto
 */
public class ProductoInventario extends Fragment {

    ClienteSucursal sucursal;
    Usuario usuario;
    FichaTecnicaProducto producto;
    List<corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProductoInventario> inventarios;
    static DecimalFormat form = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String PRODUCTO = "producto";

    @BindView(R.id.listView_inv_producto)
    ListView lvProducto;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    CatInventarioBDHelper catInventarioBDHelper;
    int idCatInv = 0;
    List<CatInventario> categorias;


    public static ProductoInventario newInstance(ClienteSucursal sucursal, Usuario usuario, FichaTecnicaProducto producto) {
        ProductoInventario fragment = new ProductoInventario();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(PRODUCTO, producto);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        form.applyPattern(Constantes.FORMATO_PRECIO);
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            producto = (FichaTecnicaProducto) getArguments().getSerializable(PRODUCTO);
        }

        catInventarioBDHelper = new CatInventarioBDHelper(getActivity());
        inventarios = catInventarioBDHelper.getProductoInventario(producto.getId(), sucursal.getId());
        categorias = catInventarioBDHelper.getCatInv();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_producto_inventario, container, false);
        ButterKnife.bind(this, view);

        CustomAdapter adapter = new CustomAdapter(getActivity());
        lvProducto.setAdapter(adapter);

        fab.setOnClickListener(view12 -> {

            final Dialog dialogInv = new Dialog(getActivity());
            dialogInv.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogInv.setContentView(R.layout.dialog_producto_inventario);

            Spinner spCatInv = dialogInv.findViewById(R.id.spinner_inventario_tipo);
            Button btnCancelar = dialogInv.findViewById(R.id.btn_atras_incidencia);
            Button btnAceptar = dialogInv.findViewById(R.id.btn_guardar_incidencia);
            final EditText edCantidad = dialogInv.findViewById(R.id.ed_inventario_cant);


            final ArrayAdapter<CatInventario> adapter1 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, categorias);
            spCatInv.setAdapter(adapter1);

            spCatInv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view1, int position, long id) {
                    idCatInv = categorias.get(position).getIdCategoriaInv();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            btnCancelar.setOnClickListener(view1212 -> dialogInv.dismiss());

            btnAceptar.setOnClickListener(view121 -> {
                if (!edCantidad.getText().toString().isEmpty()) {

                    catInventarioBDHelper.insertaInventario(idCatInv, sucursal.getId(), producto.getId(), Integer.parseInt(edCantidad.getText().toString()), usuario.getUsername());

                    inventarios.clear();
                    categorias.clear();

                    inventarios = catInventarioBDHelper.getProductoInventario(producto.getId(), sucursal.getId());
                    categorias = catInventarioBDHelper.getCatInv();
                    adapter.notifyDataSetChanged();

                    dialogInv.dismiss();

                }
            });

            dialogInv.setCancelable(false);
            dialogInv.show();

            WindowManager.LayoutParams lps = new WindowManager.LayoutParams();
            Window windows = dialogInv.getWindow();
            lps.copyFrom(windows != null ? windows.getAttributes() : null);
            lps.width = WindowManager.LayoutParams.MATCH_PARENT;
            lps.height = WindowManager.LayoutParams.WRAP_CONTENT;
            if (windows != null)

            {
                windows.setAttributes(lps);
            }

        });

        return view;
    }


    private class CustomAdapter extends BaseAdapter {

        Context ctx;

        public CustomAdapter(Context c) {
            this.ctx = c;
        }

        @Override
        public int getCount() {
            return inventarios.size();
        }

        @Override
        public Object getItem(int i) {
            return inventarios.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.adapter_reporte_invetario, viewGroup, false);

            TextView tvCategoria = row.findViewById(R.id.textView_inventario_categoria);
            TextView tvCant = row.findViewById(R.id.textView_inventario_cantidad);
            TextView tvFecha = row.findViewById(R.id.textView_inventario_fecha);

            tvCategoria.setText(categorias.get(inventarios.get(i).getIdCatInv() - 1).getCatInve());
            tvCant.setText(String.valueOf(inventarios.get(i).getCantidad()));
            tvFecha.setText("11-03-2018");

            return row;
        }
    }

    class IgnoreCaseComparator implements Comparator<String> {
        public int compare(String strA, String strB) {
            return strA.compareToIgnoreCase(strB);
        }
    }


}
