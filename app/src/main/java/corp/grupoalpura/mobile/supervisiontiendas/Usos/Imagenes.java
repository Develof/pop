package corp.grupoalpura.mobile.supervisiontiendas.Usos;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.Interactor.WSInteractor;
import corp.grupoalpura.mobile.supervisiontiendas.Login;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ImageRequest;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Imagenes {

    private int contador = 0;
    private ProgressDialog progressDialog;
    Usuario usuario;
    private String error_ws = "Error WS";

    /**
     * Metodo para comparar la imagenes que estan guardadas en la carpeta de PRODUCTOS contra el
     * catalogo de imagenes que descargo el ws. Las imagenes faltantes las mandamos a pedir
     *
     * @param imagenesWs Catalogo de imagenes que obtuvimos del ws.
     */
    public void compareFiles(Context ctx, List<String> imagenesWs, Usuario usuario) {

        this.usuario = usuario;

        File dirFotoProd = new File(Environment.getExternalStoragePublicDirectory(ctx.getResources().getString(R.string.app_img_productos)) + File.separator);
        String pathFolder = dirFotoProd.getAbsolutePath();

        // Creamos dos arreglos uno con que obtendra el nombre de las imagenes que ya obtuvimos del ws y están
        // guardadas en la carpeta imagenes y otro con las imagenes que faltan en comparación de las imagenes
        // del ws.
        ArrayList<String> imagenesFolder = new ArrayList<>();
        ArrayList<String> imagenesFaltantes = new ArrayList<>();

        File imgFolder = new File(pathFolder);
        File[] listOfFiles = imgFolder.listFiles();

        if (listOfFiles != null) {

            // Obtenemos el nombre de los archivos que estan en la carpeta IMAGENES (En caso de no estar vacío)
            for (File listOfFile : listOfFiles) {
                // Si el elemento encontrado es un archivo se guardara el nombre en un arreglo.
                if (listOfFile.isFile()) {
                    imagenesFolder.add(listOfFile.getName());
                }
            }

            // Si tenemos archivos dentro de la carpeta imagenes comparamos con el catalogo de ws para si hay nuevas imagenes
            // las guardamos en el arreglo de imagenesFaltantes
            if (listOfFiles.length > 0) {
                for (String imagen : imagenesWs) {
                    // Loop arrayList1 items
                    boolean found = false;
                    for (String imagenFolder : imagenesFolder) {
                        if (imagen.equals(imagenFolder)) {
                            found = true;
                        }
                    }
                    if (!found) {
                        imagenesFaltantes.add(imagen);   // Guardamos el nombre de la imagen que no tenemos en el arreglo
                    }
                }


                // Hacemos un loop de las imagenes que tenemos en el folder y las vamos comparando con la imagenes
                // que obtuvimos del ws si alguna imagen que tenemos en el folder no esta en el listado de imagenes del ws
                // se eliminan de la carpeta.
                for (String imagenFolder : imagenesFolder) {
                    // Loop arrayList1 items
                    boolean found = false;
                    for (String imagen : imagenesWs) {
                        if (imagen.equals(imagenFolder)) {
                            found = true;
                        }
                    }
                    if (!found) {
                        File fileDelete = new File(pathFolder + File.separator + imagenFolder);
                        fileDelete.delete();
                    }
                }


                // Mandamos a llamar la clase para pedir las imagenes que nos hacen falta.
                if (imagenesFaltantes.size() > 0) {
                    showMessage(ctx, imagenesWs.size());
                    saveImage(ctx, imagenesFaltantes.get(contador), imagenesFaltantes);
                } else {
                    Login login = new Login();
                    login.startActivity(usuario, ctx);
                }

            } else {    // Si no tenemos ningun archivo dentro de la carpeta IMAGENES pedimos el catalogo de imagenes
                // que obtuvo el ws completo.

                if (imagenesWs.size() > 0) {
                    // mandamos a llamar la clase para pedir las imagenes que nos hacen falta (el catalogo ws).
                    showMessage(ctx, imagenesWs.size());
                    saveImage(ctx, imagenesWs.get(contador), imagenesWs);
                }
            }
        } else {       // Si no tenemos ningun archivo dentro de la carpeta IMAGENES pedimos el catalogo de imagenes
            // que obtuvo el ws completo.

            if (imagenesWs.size() > 0) {
                // mandamos a llamar la clase para pedir las imagenes que nos hacen falta (el catalogo ws).
                showMessage(ctx, imagenesWs.size());
                saveImage(ctx, imagenesWs.get(contador), imagenesWs);
            }
        }

    }

    private void showMessage(Context ctx, int numeroFotos) {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("Enviando Imagenes...");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(numeroFotos);
        progressDialog.show();

    }


    private void saveImage(Context ctx, String imagen, List<String> imagenes) {
        ConfigWsRetrofit peticionesWs = new ConfigWsRetrofit();
        Retrofit retrofit = peticionesWs.getRetrofitParamsRest();
        WSInteractor wsInteractor = retrofit.create(WSInteractor.class);

        ImageRequest imageRequest = new ImageRequest();
        imageRequest.setFileName(imagen);

        Call<String> call = wsInteractor.getBase64Image(imageRequest);
        call.enqueue(new retrofit2.Callback<String>() {

            @Override
            public void onResponse(Call<String> call, final Response<String> response) {
                byte[] decodedString = Base64.decode(response.body(), Base64.DEFAULT);
                File dirPromotoria = new File(Environment.getExternalStoragePublicDirectory(ctx.getResources().getString(R.string.app_img_productos)) + File.separator);

                String getDirectoryPath = dirPromotoria.getParent();


                File f = new File(getDirectoryPath + "/" + ctx.getResources().getString(R.string.app_img_productos) + "/" + imagen);
                if (f.exists()) {
                    f.delete();
                }

                try {
                    FileOutputStream stream = new FileOutputStream(f);
                    stream.write(decodedString);
                    stream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                contador++;
                if (imagenes.size() > contador) {
                    progressDialog.setProgress(contador);
                    saveImage(ctx, imagenes.get(contador), imagenes);
                } else {
                    progressDialog.dismiss();
                    Login login = new Login();
                    login.startActivity(usuario, ctx);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.v(error_ws, t.getMessage());
                Login login = new Login();
                login.startActivity(usuario, ctx);
            }
        });
    }


}
