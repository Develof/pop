package corp.grupoalpura.mobile.supervisiontiendas.Database.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.Udn;

@Dao
public interface UdnDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertaUdn(Udn udn);

}
