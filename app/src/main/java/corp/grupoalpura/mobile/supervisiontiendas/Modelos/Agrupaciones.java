package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 20/07/16.
 */
public class Agrupaciones {
    private int id;
    private String nombre;
    private int estatus;

    Agrupaciones(){}

    public Agrupaciones(int id, String nombre, int estatus) {
        this.id = id;
        this.nombre = nombre;
        this.estatus = estatus;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
