package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 28/10/16.
 */

public class AdapterTipoAlerta {

    int idTipoAlerta;
    String nombre;
    String descripcion;

    public AdapterTipoAlerta(int idTipoAlerta, String nombre, String descripcion) {
        this.idTipoAlerta = idTipoAlerta;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public int getIdTipoAlerta() {
        return idTipoAlerta;
    }

    public void setIdTipoAlerta(int idTipoAlerta) {
        this.idTipoAlerta = idTipoAlerta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
