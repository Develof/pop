package corp.grupoalpura.mobile.supervisiontiendas.Database.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import corp.grupoalpura.mobile.supervisiontiendas.Database.DateConverter;

@Entity
public class InventarioBO {

    private static final String ID_INVENTORY = "idInventory";
    private static final String GRUPO_COMERCIAL = "grupocomercial";
    private static final String STORE_NUMBER = "storeNumber";
    private static final String FECHA = "fecha";
    private static final String ID_PRO = "idPro";
    private static final String HAND = "hand";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ID_INVENTORY)
    @SerializedName(ID_INVENTORY)
    private int idInventory;

    @ColumnInfo(name = GRUPO_COMERCIAL)
    @SerializedName(GRUPO_COMERCIAL)
    private String grupoComercial;

    @ColumnInfo(name = STORE_NUMBER)
    @SerializedName(STORE_NUMBER)
    private int storeNumber;

    @ColumnInfo(name = FECHA)
    @TypeConverters(DateConverter.class)
    @SerializedName(FECHA)
    private Date fecha;

    @ColumnInfo(name = ID_PRO)
    @SerializedName(ID_PRO)
    private String idPro;

    @ColumnInfo(name = HAND)
    @SerializedName(HAND)
    private int hand;

    public int getIdInventory() {
        return idInventory;
    }

    public void setIdInventory(int idInventory) {
        this.idInventory = idInventory;
    }

    public String getGrupoComercial() {
        return grupoComercial;
    }

    public void setGrupoComercial(String grupoComercial) {
        this.grupoComercial = grupoComercial;
    }

    public int getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(int storeNumber) {
        this.storeNumber = storeNumber;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIdPro() {
        return idPro;
    }

    public void setIdPro(String idPro) {
        this.idPro = idPro;
    }

    public int getHand() {
        return hand;
    }

    public void setHand(int hand) {
        this.hand = hand;
    }
}
