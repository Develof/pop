package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Contenedor;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.EspaciosFragment;
import corp.grupoalpura.mobile.supervisiontiendas.Interfaces.CallbackInterface;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CategoriaIncidencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Compania;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Departamentos;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.EspacioFrente;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FotosEvidenciaRelacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProdFamilia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Producto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.SucursalEspacio;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.MyDialogFragment;
import corp.grupoalpura.mobile.supervisiontiendas.R;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.FamiliasDBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.FotosBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.ProductoBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.SucursalEspacioBDHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.LoadImage;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.ScannActivity;

/**
 * Created by Develof on 26/03/17.
 **/

public class EspaciosAdapter extends RecyclerView.Adapter<EspaciosViewHolder> implements CallbackInterface {

    EspaciosFragment fragment;
    private Context context;
    private final LayoutInflater mInflater;
    private ClienteSucursal sucursal;
    private Usuario usuario;
    private List<SucursalEspacio> sucursalEspacios;
    private FragmentManager fg;
    private SucursalEspacioBDHelper sucursalEspacioBDHelper;
    private FotosBDHelper fotosBDHelper;
    private DBHelper dbHelper;
    FamiliasDBHelper fdbHelper;
    private ArrayList<Compania> companias = new ArrayList<>();
    private ArrayList<Departamentos> ubicaciones = new ArrayList<>();
    private List<ProdFamilia> familias;
    int idEspacio, tipoEspacio = 0, nUbicacion = 0, categoria = 0;
    private LoadImage loadImage;
    SucursalEspacio sucursalEsp;

    private String motivo;
    private int posicionCompania = 0;

    private static final int INCIDENCIAS_POSITIVAS = 1;
    private static final int INCIDENCIAS_ESPACIO = 3;

    public EspaciosAdapter(EspaciosFragment fragment, Context context, List<SucursalEspacio> sucursalEspacio, ClienteSucursal sucursal, Usuario usuario, FragmentManager fragmentManager, Integer idEspacio) {
        mInflater = LayoutInflater.from(context);
        loadImage = new LoadImage();
        this.fragment = fragment;
        this.sucursalEspacios = sucursalEspacio;
        this.sucursal = sucursal;
        this.usuario = usuario;
        this.fg = fragmentManager;
        this.context = context;
        this.idEspacio = idEspacio;
    }

    @Override
    public EspaciosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        dbHelper = new DBHelper(context, null, Constantes.VERSION_BASE_DE_DATOS);
        fdbHelper = new FamiliasDBHelper(context);
        sucursalEspacioBDHelper = new SucursalEspacioBDHelper(context);
        fotosBDHelper = new FotosBDHelper(context);
        tipoEspacio = dbHelper.getTipoEspacio(idEspacio);
        final View itemView = mInflater.inflate(R.layout.adapter_ficha_tecnica_espacios, parent, false);
        return new EspaciosViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(@NonNull final EspaciosViewHolder holder, final int position) {

        final SucursalEspacio sucursalEspacio = sucursalEspacios.get(position);


        if (tipoEspacio != 1) {
            holder.txtHeaderEspacio.setText(sucursalEspacio.getDepartamento());
        } else {
            holder.txtHeaderEspacio.setText(sucursalEspacio.getCategoria());
        }

        holder.txtSubHeaderEspacio.setText(setEspacioDescripcion(sucursalEspacio));
        holder.txtProducto.setText("Producto total: " + sucursalEspacioBDHelper.getFrentes(sucursalEspacio.getIdSucursalEspacio()));
        if (tipoEspacio == 2) {
            setTextColor(sucursalEspacioBDHelper.getFrentes(sucursalEspacio.getIdSucursalEspacio()), sucursalEspacio, holder.txtProducto);
        } else {
            holder.txtProducto.setTextColor(ContextCompat.getColor(context, R.color.colorVerde));
        }

        holder.imgEspacio.setOnClickListener(v -> {
            DialogFragment newFragment = MyDialogFragment.newInstance(sucursalEspacios.get(position).getIdSucursalEspacio(), DBHelper.TABLA_SUCURSAL_ESPACIOS, sucursalEspacio.getEspacio(), "IMG_ESPACIO_", sucursal, usuario);
            newFragment.show(fg, "");
            newFragment.setCancelable(false);
        });

        holder.txtTipoAdquisicion.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        switch (sucursalEspacio.getTipo_adquisicion()) {
            case Constantes.TIPO_ESPACIO_ADQUISICION.RENTADO:
                holder.txtTipoAdquisicion.setText("( RENTADO )");
                break;
            case Constantes.TIPO_ESPACIO_ADQUISICION.GANADO:
                holder.txtTipoAdquisicion.setText("( GANADO )");
                break;
            case Constantes.TIPO_ESPACIO_ADQUISICION.BASE:
                holder.txtTipoAdquisicion.setText("( BASE )");
                break;
        }

        ArrayList<FotosEvidenciaRelacion> fotos = fotosBDHelper.getFotos(DBHelper.TABLA_SUCURSAL_ESPACIOS, sucursalEspacio.getIdSucursalEspacio());
        if (fotos.size() > 0) {
            loadImage.load(context, fotos.get(0).getArchivo(), holder.imgEspacio);
        }

        holder.cvEspacio.setOnClickListener(view -> {
            fragment.replaceFragment(sucursalEspacio, idEspacio);
        });

        holder.toolbar.getMenu().clear();
        holder.toolbar.inflateMenu(R.menu.menu_espacios);
        holder.toolbar.setOnMenuItemClickListener(item -> {
            final int id = item.getItemId();
            switch (id) {
                case R.id.espacio_frentes:

                    sucursalEsp = sucursalEspacio;
                    ((Contenedor) context).backInterface(this);

                    Activity origin = (Activity) context;
                    Intent intent = new Intent(context, ScannActivity.class);
                    intent.putExtra("Usuario", usuario);
                    intent.putExtra("id_sucursal", sucursal);
                    intent.putExtra("ventana", 2);
                    origin.startActivityForResult(intent, 1);

                    break;

                case R.id.espacio_editar:

                    ubicaciones = dbHelper.getDepartamentos();
                    familias = fdbHelper.getFamiliasEspacio();
                    final int[] tipo_adquisicion = {0};

                    //ADAPTER PARA LOS SPINNER DE DEPARTAMENTOS Y Espacios
                    ArrayAdapter<Departamentos> dataAdapterUbicacion = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, ubicaciones);
                    ArrayAdapter<ProdFamilia> dataAdapterfamilias = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, familias);
                    dataAdapterUbicacion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapterfamilias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    final Dialog dialogEspacioupdate = new Dialog(context);
                    dialogEspacioupdate.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogEspacioupdate.setContentView(R.layout.dialog_update_espacio);
                    dialogEspacioupdate.setCancelable(false);

                    TextView txtTituloDepartamento = dialogEspacioupdate.findViewById(R.id.textView_cargaEspacio_ubicacion);
                    Spinner spUbicacion = dialogEspacioupdate.findViewById(R.id.spinnerUbicacion);
                    TextView btnEditar = dialogEspacioupdate.findViewById(R.id.btn_aceptar_espacio);
                    TextView btnCancelar = dialogEspacioupdate.findViewById(R.id.btn_cancelar);
                    RelativeLayout rlAdquisicion = dialogEspacioupdate.findViewById(R.id.contenedor_cargaEspacios_adquisicion);
                    RadioButton rbRentado = dialogEspacioupdate.findViewById(R.id.rb_cargaEspacios_rentado);
                    RadioButton rbGanado = dialogEspacioupdate.findViewById(R.id.rb_cargaEspacios_ganado);
                    RadioButton rbBase = dialogEspacioupdate.findViewById(R.id.rb_cargaEspacios_base);
                    TextView txtTituloEspacio = dialogEspacioupdate.findViewById(R.id.textView_espacio_titulo);
                    ImageView imgFoto = dialogEspacioupdate.findViewById(R.id.imageView_evidencia);

                    RelativeLayout rlNiveles = dialogEspacioupdate.findViewById(R.id.rl_niveles);
                    final EditText edNiveles = dialogEspacioupdate.findViewById(R.id.editText_cargaEpsacios_niveles);
                    RelativeLayout rlTarimas = dialogEspacioupdate.findViewById(R.id.rl_tarimas);
                    final EditText edTarimas = dialogEspacioupdate.findViewById(R.id.editText_cargaEpsacios_tarimas);
                    RelativeLayout rlTramo = dialogEspacioupdate.findViewById(R.id.rl_tramos);
                    final EditText edTramo = dialogEspacioupdate.findViewById(R.id.editText_cargaEpsacios_tramos);
                    RelativeLayout rlPuertas = dialogEspacioupdate.findViewById(R.id.rl_puertas);
                    final EditText edPuertas = dialogEspacioupdate.findViewById(R.id.editText_cargaEpsacios_puertas);

                    txtTituloEspacio.setText("Editar Espacio");
                    imgFoto.setVisibility(View.GONE);
                    spUbicacion.setAdapter(dataAdapterUbicacion);
                    setTipoEspacio(sucursalEspacios.get(position), rlNiveles, rlTarimas, rlTramo, rlPuertas, edNiveles, edTarimas, edTramo, edPuertas);


                    switch (sucursalEspacios.get(position).getTipo_adquisicion()) {
                        case Constantes.TIPO_ESPACIO_ADQUISICION.RENTADO:
                            rbRentado.setChecked(true);
                            break;
                        case Constantes.TIPO_ESPACIO_ADQUISICION.GANADO:
                            rbGanado.setChecked(true);
                            break;
                        case Constantes.TIPO_ESPACIO_ADQUISICION.BASE:
                            rbBase.setChecked(true);
                            break;
                    }

                    spUbicacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (tipoEspacio != 1) {
                                categoria = 0;
                                nUbicacion = ubicaciones.get(position).getId_departamento();
                            } else {
                                nUbicacion = 0;
                                categoria = familias.get(position).getId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    rbRentado.setOnClickListener(view -> tipo_adquisicion[0] = 0);


                    rbGanado.setOnClickListener(view -> tipo_adquisicion[0] = 1);


                    rbBase.setOnClickListener(view -> tipo_adquisicion[0] = 2);

                    if (tipoEspacio == 1) {
                        tipo_adquisicion[0] = 2;
                        rlAdquisicion.setVisibility(View.GONE);

                        txtTituloDepartamento.setText("Categoría");
                        spUbicacion.setAdapter(dataAdapterfamilias);
                    }


                    btnEditar.setOnClickListener(view -> {

                        if (edNiveles.getText().toString().equals("")) edNiveles.setText("0");
                        if (edTarimas.getText().toString().equals("")) edTarimas.setText("0");
                        if (edTramo.getText().toString().equals("")) edTramo.setText("0");
                        if (edPuertas.getText().toString().equals("")) edPuertas.setText("0");

                        if (tipoEspacio == 2) {
                            if (Double.parseDouble(edTarimas.getText().toString()) != 0) {
                                double frentes = sucursalEspacioBDHelper.getFrentes(sucursalEspacio.getIdSucursalEspacio());
                                if (!validaEspacioTotal(Double.parseDouble(edTarimas.getText().toString()), frentes)) {
                                    Toast.makeText(context, "Tienes producto en el espacio que supera la capacidad del espacio", Toast.LENGTH_LONG).show();
                                    return;
                                }
                            }
                        }

                        if (sucursalEspacios.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || sucursalEspacios.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                            sucursalEspacioBDHelper.updateEspacio(sucursalEspacios.get(position).getIdSucursalEspacio(), sucursalEspacio.getIdEspacio(), nUbicacion, categoria,
                                    Integer.parseInt(edNiveles.getText().toString()), Integer.parseInt(edTarimas.getText().toString()), Integer.parseInt(edTramo.getText().toString()), Integer.parseInt(edPuertas.getText().toString()),
                                    tipo_adquisicion, usuario.getUsername(), Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
                        } else {
                            sucursalEspacioBDHelper.updateEspacio(sucursalEspacios.get(position).getIdSucursalEspacio(), sucursalEspacio.getIdEspacio(), nUbicacion, categoria,
                                    Double.parseDouble(edNiveles.getText().toString()), Double.parseDouble(edTarimas.getText().toString()), Double.parseDouble(edTramo.getText().toString()), Double.parseDouble(edPuertas.getText().toString()),
                                    tipo_adquisicion, usuario.getUsername(), Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
                        }

                        sucursalEspacios = sucursalEspacioBDHelper.getSucursalEspacio(sucursal.getId(), sucursalEspacio.getIdEspacio());
                        notifyDataSetChanged();
                        dialogEspacioupdate.dismiss();
                        Toast toast = Toast.makeText(context, "Se ha modificado el espacio", Toast.LENGTH_LONG);
                        TextView tv = toast.getView().findViewById(android.R.id.message);
                        tv.setTextColor(Color.WHITE);
                        toast.show();

                    });

                    btnCancelar.setOnClickListener(view -> {
                        dialogEspacioupdate.dismiss();
                    });

                    dialogEspacioupdate.show();
                    WindowManager.LayoutParams lp1 = new WindowManager.LayoutParams();
                    Window window1 = dialogEspacioupdate.getWindow();
                    lp1.copyFrom(window1.getAttributes());

                    lp1.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp1.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window1.setAttributes(lp1);

                    break;

                case R.id.espacio_eliminar:

                    new AlertDialog.Builder(context)
                            .setTitle("Espacios")
                            .setMessage("¿Estás seguro que deseas eliminar el espacio?")
                            .setCancelable(false)
                            .setNegativeButton("NO", (dialog, id13) -> {

                            })
                            .setPositiveButton("SI", (dialog, which) -> {

                                final Dialog dialogEliminacion = new Dialog(context);
                                dialogEliminacion.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialogEliminacion.setContentView(R.layout.dialog_elimina_espacio);
                                dialogEliminacion.setCancelable(true);

                                final Spinner spMotivo = (Spinner) dialogEliminacion.findViewById(R.id.spinner_motivo_eliminacion);
                                TextView btnCancelar1 = (TextView) dialogEliminacion.findViewById(R.id.btn_cancelar);
                                TextView btnEliminar = (TextView) dialogEliminacion.findViewById(R.id.btn_actualizar);

                                ArrayList<CategoriaIncidencia> arregloCatalogoIncidencia;
                                arregloCatalogoIncidencia = dbHelper.getActividadCategoriaEliminaEspacio(INCIDENCIAS_ESPACIO, INCIDENCIAS_POSITIVAS);

                                final ArrayAdapter<CategoriaIncidencia> dataAdapterCategoria = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, arregloCatalogoIncidencia);
                                spMotivo.setAdapter(dataAdapterCategoria);

                                spMotivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view1, int position13, long id12) {
                                        motivo = arregloCatalogoIncidencia.get(position13).getCategoria();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });


                                btnEliminar.setOnClickListener(v -> {

                                    if (sucursalEspacios.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || sucursalEspacios.get(position).getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
                                        dbHelper.eliminarEspacio(sucursalEspacio.getIdSucursalEspacio(), motivo, Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
                                    } else {
                                        dbHelper.eliminarEspacio(sucursalEspacio.getIdSucursalEspacio(), motivo, Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
                                    }


                                    dialogEliminacion.dismiss();

                                    sucursalEspacios = sucursalEspacioBDHelper.getSucursalEspacio(sucursal.getId(), idEspacio);
                                    notifyDataSetChanged();

                                });

                                btnCancelar1.setOnClickListener(v -> {
                                    dialogEliminacion.dismiss();
                                });

                                dialogEliminacion.show();

                                WindowManager.LayoutParams lp2 = new WindowManager.LayoutParams();
                                Window window2 = dialogEliminacion.getWindow();
                                lp2.copyFrom(window2.getAttributes());

                                lp2.width = WindowManager.LayoutParams.MATCH_PARENT;
                                lp2.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window2.setAttributes(lp2);


                            })
                            .show();

                    break;

            }
            return true;
        });
    }


    private void setTipoEspacio(SucursalEspacio sucursalEspacio, RelativeLayout niveles, RelativeLayout tarimas, RelativeLayout tramos, RelativeLayout puertas,
                                EditText edNiveles, EditText edTarimas, EditText edTramos, EditText edPuertas) {

        String llenado = dbHelper.getTipoLlenado(idEspacio);
        for (int i = 0; i < llenado.length(); i++) {
            char llenadoPosicion = llenado.charAt(i);

            switch (llenadoPosicion) {

                case 'P':
                    edPuertas.setText(String.valueOf(sucursalEspacio.getPuertas()));
                    puertas.setVisibility(View.VISIBLE);
                    break;

                case 'N':
                    edNiveles.setText(String.valueOf(sucursalEspacio.getNivel()));
                    niveles.setVisibility(View.VISIBLE);
                    break;

                case 'T':
                    edTramos.setText(String.valueOf(sucursalEspacio.getTramos()));
                    tramos.setVisibility(View.VISIBLE);
                    break;

                case 'A':
                    edTarimas.setText(String.valueOf(sucursalEspacio.getTarimas()));
                    tarimas.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }


    @Override
    public int getItemCount() {
        return sucursalEspacios.size();
    }

    private String setEspacioDescripcion(SucursalEspacio espacio) {

        String descripcion = "";
        int contador = 0;

        if (espacio.getNivel() > 0) {
            descripcion = "Niveles: " + espacio.getNivel();
            contador = contador + 1;
        }

        if (espacio.getPuertas() > 0) {
            if (contador > 0) {
                descripcion = descripcion + " / Puertas: " + espacio.getPuertas();
            } else {
                descripcion = descripcion + "Puertas: " + espacio.getPuertas();
            }
        }

        if (espacio.getTramos() > 0) {
            if (contador > 0) {
                descripcion = descripcion + " / Tramos: " + espacio.getTramos();
            } else {
                descripcion = descripcion + "Tramos: " + espacio.getTramos();
            }
        }

        if (espacio.getTarimas() > 0) {
            if (contador > 0) {
                descripcion = descripcion + " / Tarimas: " + espacio.getTarimas();
            } else {
                descripcion = descripcion + "Tarimas: " + espacio.getTarimas();
            }
        }
        return descripcion;
    }

    private boolean validaEspacioTotal(double cantidad, double frente) {
        boolean validacion = false;
        if (cantidad >= frente) {
            validacion = true;
        }
        return validacion;
    }

    private boolean validaProducto(double cant, SucursalEspacio espacio) {

        boolean cantidadValida = false;
        String llenado = dbHelper.getTipoLlenado(idEspacio);
        double comparador = 1;

        for (int i = 0; i < llenado.length(); i++) {
            char llenadoPosicion = llenado.charAt(i);

            switch (llenadoPosicion) {
                case 'A':
                    comparador = espacio.getTarimas();
                    break;
            }
        }

        if (comparador >= cant) {
            cantidadValida = true;
        }

        return cantidadValida;
    }

    private void setTextColor(double cant, SucursalEspacio espacio, TextView tvCantidad) {
        String llenado = dbHelper.getTipoLlenado(idEspacio);
        double comparador = 1;

        if (cant != 0) {
            for (int i = 0; i < llenado.length(); i++) {
                char llenadoPosicion = llenado.charAt(i);

                switch (llenadoPosicion) {
                    case 'A':
                        comparador = espacio.getTarimas();
                        break;
                }
            }

            if (comparador > cant) {
                tvCantidad.setTextColor(ContextCompat.getColor(context, R.color.colorAmarillo));
            } else {
                tvCantidad.setTextColor(ContextCompat.getColor(context, R.color.colorVerde));
            }
        } else {
            tvCantidad.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
        }
    }


    @Override
    public void setProduct(FichaTecnicaProducto producto) {

        String tipoFrente = dbHelper.getTipoFrente(idEspacio);

        final Dialog dialogEspacioupdate = new Dialog(context);
        dialogEspacioupdate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEspacioupdate.setContentView(R.layout.dialog_carga_frente);
        dialogEspacioupdate.setCancelable(false);

        companias = dbHelper.getCompanias();
        String compania = dbHelper.getCompania(producto.getId_compania());
        ArrayAdapter<Compania> dataAdapterCompanias = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, companias);
        dataAdapterCompanias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ImageView imgProducto = dialogEspacioupdate.findViewById(R.id.img_descripcion);
        TextView txtDesc = dialogEspacioupdate.findViewById(R.id.textView_value_productos_catalogo_nombre);
        TextView txtSku = dialogEspacioupdate.findViewById(R.id.textView_value_productos_sku);
        EditText edFrentes = dialogEspacioupdate.findViewById(R.id.ed_frentes);
        EditText edDescipcion = dialogEspacioupdate.findViewById(R.id.ed_desc_prod_competencia);
        Spinner spCompania = dialogEspacioupdate.findViewById(R.id.spinner_compania_carga_frente);
        TextView btnCancelar = dialogEspacioupdate.findViewById(R.id.btn_cancelar);
        TextView btnAceptar = dialogEspacioupdate.findViewById(R.id.btn_aceptar);

        String imageResource = Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_img_productos)) +
                File.separator + producto.getClave().toLowerCase() + ".png";

        txtDesc.setText(producto.getDescripcion());
        txtSku.setText(producto.getUPC());
        loadImage.load(context, imageResource, imgProducto);

        edFrentes.setHint(tipoFrente);
        spCompania.setAdapter(dataAdapterCompanias);

        edDescipcion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtDesc.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        spCompania.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                posicionCompania = companias.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        if (producto.getId_compania() != -1) {
            int index = 0;
            int indexarreglo;
            for (int i = 0; i < companias.size(); i++) {
                indexarreglo = companias.get(i).getNombre().indexOf(compania);
                if (indexarreglo != -1) {
                    index = i;
                }
            }

            if (producto.getId_compania() != 8) {
                if (index != -1) {
                    spCompania.setEnabled(false);
                }
                spCompania.setSelection(index);
            }
        } else {
            edDescipcion.setVisibility(View.VISIBLE);
            spCompania.setEnabled(true);
        }

        btnAceptar.setOnClickListener(view -> {
            if (producto.getId_compania() != -1) {
                if (!edFrentes.getText().toString().equals("")) {
                    EspacioFrente espacioFrente = new EspacioFrente(posicionCompania, producto.getId(), producto.getIdPresentacion(), Double.parseDouble(edFrentes.getText().toString()));
                    if (tipoEspacio == 2) {
                        double cant = sucursalEspacioBDHelper.getFrentes(sucursalEsp.getIdSucursalEspacio());
                        cant = cant + espacioFrente.getFrente();
                        if (validaProducto(cant, sucursalEsp)) {
                            sucursalEspacioBDHelper.insertFrentes(espacioFrente, sucursalEsp.getIdSucursalEspacio(), usuario.getUsername());
                            notifyDataSetChanged();
                            dialogEspacioupdate.dismiss();
                        } else {
                            Toast.makeText(context, "No cuentas con espacio para almacenar el producto", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        sucursalEspacioBDHelper.insertFrentes(espacioFrente, sucursalEsp.getIdSucursalEspacio(), usuario.getUsername());
                        notifyDataSetChanged();
                        dialogEspacioupdate.dismiss();
                    }
                } else {
                    Toast.makeText(context, "Llena el níumero de frentes", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (!edFrentes.getText().toString().equals("")) {
                    if (!edDescipcion.getText().toString().isEmpty()) {

                        if (tipoEspacio == 2) {
                            double cant = sucursalEspacioBDHelper.getFrentes(sucursalEsp.getIdSucursalEspacio());
                            cant = cant + Double.parseDouble(edFrentes.getText().toString());
                            if (validaProducto(cant, sucursalEsp)) {

                                Producto nProducto = new Producto();
                                nProducto.setId(-1);
                                nProducto.setId_presentacion(producto.getIdPresentacion());
                                nProducto.setId_familia(2);
                                nProducto.setId_compania(posicionCompania);
                                nProducto.setClave(producto.getClave());
                                nProducto.setUnidad("N/A");
                                nProducto.setUpc(producto.getUPC());
                                nProducto.setNombre(edDescipcion.getText().toString());
                                nProducto.setDescripcion(edDescipcion.getText().toString());
                                nProducto.setPrecio_unitario(1.0);
                                nProducto.setEstatus(1);

                                ProductoBDHelper productoBDHelper = new ProductoBDHelper(context);
                                long idProducto = productoBDHelper.creaNuevoProducto(nProducto, sucursal, usuario);
                                int idProd = (int) idProducto;

                                EspacioFrente espacioFrente = new EspacioFrente(posicionCompania, idProd, producto.getIdPresentacion(), Double.parseDouble(edFrentes.getText().toString()));
                                sucursalEspacioBDHelper.insertFrentes(espacioFrente, sucursalEsp.getIdSucursalEspacio(), usuario.getUsername());
                                notifyDataSetChanged();
                                dialogEspacioupdate.dismiss();
                            } else {
                                Toast.makeText(context, "No cuentas con espacio para almacenar el producto", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            EspacioFrente espacioFrente = new EspacioFrente(posicionCompania, producto.getId(), producto.getIdPresentacion(), Double.parseDouble(edFrentes.getText().toString()));
                            sucursalEspacioBDHelper.insertFrentes(espacioFrente, sucursalEsp.getIdSucursalEspacio(), usuario.getUsername());
                            notifyDataSetChanged();
                            dialogEspacioupdate.dismiss();
                        }


                        dialogEspacioupdate.dismiss();
                    } else {
                        Toast.makeText(context, "Llena el níumero de frentes", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnCancelar.setOnClickListener(view -> {
            dialogEspacioupdate.dismiss();
        });

        dialogEspacioupdate.show();

        WindowManager.LayoutParams lp2 = new WindowManager.LayoutParams();
        Window window2 = dialogEspacioupdate.getWindow();
        lp2.copyFrom(window2.getAttributes());

        lp2.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp2.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window2.setAttributes(lp2);

    }

}
