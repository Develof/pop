package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;

public class ProductoPreciosBDHelper extends DBHelper {

    public ProductoPreciosBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }


    public Double getPrecioTienda(int idProducto, int idSucursal, int idCompania) {

        Double precioUnitario = 0.0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT PRECIO_UNITARIO FROM PRODUCTO_PRECIOS " +
                "WHERE ID_PRODUCTO = " + idProducto + " " +
                "AND ID_SUCURSAL = " + idSucursal + " " +
                "AND ID_COMPANIA = " + idCompania + " AND ESTATUS = 1 " +
                "ORDER BY FECHA_CREACION " +
                "LIMIT 1", null);

        if (cursor.moveToFirst()) precioUnitario = Double.parseDouble(cursor.getString(0));

        cursor.close();
        db.close();
        return precioUnitario;
    }


    public Double getPromocionTienda(int idProducto, int idSucursal, int idCompania) {

        Double precioUnitario = 0.0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT PRECIO_PROMOCION FROM PRODUCTO_PRECIOS " +
                "WHERE ID_PRODUCTO = " + idProducto + " " +
                "AND ID_SUCURSAL = " + idSucursal + " " +
                "AND ID_COMPANIA = " + idCompania + " ORDER BY FECHA_CREACION " +
                "LIMIT 1", null);

        if (cursor.moveToFirst()) precioUnitario = Double.parseDouble(cursor.getString(0));

        cursor.close();
        db.close();
        return precioUnitario;
    }





}
