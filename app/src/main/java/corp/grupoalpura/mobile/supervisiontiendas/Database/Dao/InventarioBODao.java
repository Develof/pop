package corp.grupoalpura.mobile.supervisiontiendas.Database.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Database.Entities.InventarioBO;

@Dao
public interface InventarioBODao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertaInventario(List<InventarioBO> inventarioBOS);

    @Query("SELECT * FROM InventarioBO")
    List<InventarioBO> getInventario();


    @Query("SELECT * FROM InventarioBO WHERE storeNumber = :claveSucursal AND idPro = :claveProducto")
    InventarioBO getInventoryProd(String claveSucursal, String claveProducto);

}
