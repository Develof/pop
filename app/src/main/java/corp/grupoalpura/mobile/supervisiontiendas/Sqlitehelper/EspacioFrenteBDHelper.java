package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterEspacioFrente;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Espacios;

public class EspacioFrenteBDHelper extends DBHelper {

    public EspacioFrenteBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public ArrayList<AdapterEspacioFrente> getFrentes(int idEspacioSucursal) {

        ArrayList<AdapterEspacioFrente> frentes = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT FR.ID_ESPACIO_FRENTE, PROD.CLAVE, PROD.DESCRIPCION, COMP.NOMBRE, FR.FRENTES, FR.ESTATUS2  " +
                "FROM ESPACIO_FRENTE FR " +
                "INNER JOIN PRODUCTO PROD ON PROD.ID_PRODUCTO = FR.ID_AGRUPACION " +
                "INNER JOIN COMPANIA COMP ON COMP.ID_COMPANIA = FR.ID_COMPANIA " +
                "WHERE ID_SUCURSAL_ESPACIO = " + idEspacioSucursal + " AND FR.ESTATUS = 1", null);

        if (cursor.moveToFirst()) {
            do {
                AdapterEspacioFrente espacioFrente = new AdapterEspacioFrente();
                espacioFrente.setId_espacio_frente(Integer.parseInt(cursor.getString(0)));
                espacioFrente.setClave(cursor.getString(1));
                espacioFrente.setDescipcion(cursor.getString(2));
                espacioFrente.setNombre_compania(cursor.getString(3));
                espacioFrente.setFrentes(Double.parseDouble(cursor.getString(4)));
                espacioFrente.setEstatus_dos(Integer.parseInt(cursor.getString(5)));
                frentes.add(espacioFrente);
            } while (cursor.moveToNext());
        }

        return frentes;
    }


    public Espacios getEspacio(int idEspacio) {

        Espacios espacio = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Espacios.ID_ESPACIO, Espacios.NOMBRE " +
                "FROM Espacios WHERE ESTATUS = 1 AND ID_ESPACIO = " + idEspacio + " ORDER BY NOMBRE", null);

        if (cursor.moveToFirst()) {
            String esp = cursor.getString(1);
            espacio = new Espacios(idEspacio, esp);
        }

        cursor.close();
        return espacio;
    }

    public double getNumeroFrentes(int idSucursalEspacio, int idEspacioFrente) {

        double totalFrente = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ESPACIO_FRENTE.FRENTES " +
                "FROM ESPACIO_FRENTE " +
                "INNER JOIN SUCURSAL_ESPACIO ON ESPACIO_FRENTE.ID_SUCURSAL_ESPACIO = " +
                "SUCURSAL_ESPACIO.ID_SUCURSAL_ESPACIO " +
                "WHERE ESPACIO_FRENTE.ID_SUCURSAL_ESPACIO = '" + idSucursalEspacio + "'  AND ESPACIO_FRENTE.ID_ESPACIO_FRENTE != "  + idEspacioFrente + " AND ESPACIO_FRENTE.ESTATUS = 1 " +
                "ORDER BY ESPACIO_FRENTE.ULTIMA_MODIFICACION", null);

        if (cursor.moveToFirst()) {
            do {
                totalFrente += Double.parseDouble(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        return totalFrente;

    }


    public String getTipoFrente(int idEspacio) {

        String tipoEspacio = "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT TIPO_FRENTE FROM ESPACIOS WHERE ID_ESPACIO = " + idEspacio + ";", null);

        if (cursor.moveToFirst()) {
            tipoEspacio = cursor.getString(0);

        }
        cursor.close();
        return tipoEspacio;
    }

    public void updateFrentes(AdapterEspacioFrente espacioFrente, double frente) {
        if (espacioFrente.getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || espacioFrente.getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
            updateFrente(frente, espacioFrente.getId_espacio_frente(), 1, Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
        } else {
            updateFrente(frente, espacioFrente.getId_espacio_frente(), 1, Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
        }
    }


    public void disableFrentes(AdapterEspacioFrente espacioFrente) {
        if (espacioFrente.getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB || espacioFrente.getEstatus_dos() == Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL) {
            updateDisFrente(espacioFrente.getId_espacio_frente(), 0, Constantes.ESTATUS.CREADO_WEB_MODIFICADO_MOVIL);
        } else {
            updateDisFrente(espacioFrente.getId_espacio_frente(), 0, Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL);
        }
    }

    private void updateFrente(double frentes, long idFrente, int estatus, int estatus_dos) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_ESPACIO_FRENTE + " SET " + COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "', " + COLUMNA_ESPACIO_FRENTE_FRENTES + " = '" + frentes + "', " + COLUMNA_ESPACIO_FRENTE_ESTATUS + " = '" + estatus + "' WHERE " + COLUMNA_ESPACIO_FRENTE_ID + " = " + idFrente;
        database.execSQL(strSQL);
    }

    private void updateDisFrente(long idFrente, int estatus, int estatus_dos) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_ESPACIO_FRENTE + " SET " + COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "', " + COLUMNA_ESPACIO_FRENTE_ESTATUS + " = '" + estatus + "' WHERE " + COLUMNA_ESPACIO_FRENTE_ID + " = " + idFrente;
        database.execSQL(strSQL);
    }


}
