package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 21/11/16.
 */

public class AdapterFotos {

    private int idFoto;
    private int idFotoRelacion;
    private String archivo;

    public AdapterFotos(int idFoto, int idFotoRelacion, String archivo) {
        this.idFoto = idFoto;
        this.idFotoRelacion = idFotoRelacion;
        this.archivo = archivo;
    }

    public int getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(int idFoto) {
        this.idFoto = idFoto;
    }

    public int getIdFotoRelacion() {
        return idFotoRelacion;
    }

    public void setIdFotoRelacion(int idFotoRelacion) {
        this.idFotoRelacion = idFotoRelacion;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }
}
