package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by Develof on 26/08/16.
 */

public class CatalogoActividades {
    private int id_actividad;
    private String actividad;

    public CatalogoActividades(int id_actividad, String actividad) {
        this.id_actividad = id_actividad;
        this.actividad = actividad;
    }

    public int getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(int id_actividad) {
        this.id_actividad = id_actividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    @Override
    public String toString() {
        return actividad;
    }

}
