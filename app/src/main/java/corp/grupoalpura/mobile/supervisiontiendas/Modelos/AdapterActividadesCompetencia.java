package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 11/07/16.
 */
public class AdapterActividadesCompetencia {
    private int id_actividad_competencia;
    private String actividad_categoria;
    private String fecha_creacion;
    private String compania;
    private String descripcion;
    private String producto;
    private int participantes;
    private String fecha_inicio;
    private String fecha_fin;
    private int estatus_dos;
    private String descripcion_compania;

    public AdapterActividadesCompetencia(int id_actividad_competencia, String actividad_categoria, String compania, String descripcion, String producto, String fecha_inicio, String fecha_fin, String fecha_creacion, int estatus_dos) {
        this.id_actividad_competencia = id_actividad_competencia;
        this.actividad_categoria = actividad_categoria;
        this.compania = compania;
        this.descripcion = descripcion;
        this.producto = producto;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.fecha_creacion = fecha_creacion;
        this.estatus_dos =  estatus_dos;
    }


    public AdapterActividadesCompetencia(int id_actividad_competencia, String actividad_categoria, String compania, String descripcion, String producto, String fecha_inicio, String fecha_fin, String fecha_creacion, int estatus_dos, String descripcion_compania) {
        this.id_actividad_competencia = id_actividad_competencia;
        this.actividad_categoria = actividad_categoria;
        this.compania = compania;
        this.descripcion = descripcion;
        this.producto = producto;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.fecha_creacion = fecha_creacion;
        this.estatus_dos =  estatus_dos;
        this.descripcion_compania = descripcion_compania;
    }


    public AdapterActividadesCompetencia(int id_actividad_competencia, String actividad_categoria, String compania, String descripcion, String producto, String fecha_inicio, String fecha_fin, String fecha_creacion, int estatus_dos, String descripcion_compania, int participantes) {
        this.id_actividad_competencia = id_actividad_competencia;
        this.actividad_categoria = actividad_categoria;
        this.compania = compania;
        this.descripcion = descripcion;
        this.producto = producto;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.fecha_creacion = fecha_creacion;
        this.estatus_dos =  estatus_dos;
        this.descripcion_compania = descripcion_compania;
        this.participantes = participantes;
    }



    public int getId_actividad_competencia() {
        return id_actividad_competencia;
    }

    public void setId_actividad_competencia(int id_actividad_competencia) {
        this.id_actividad_competencia = id_actividad_competencia;
    }

    public String getActividad_categoria() {
        return actividad_categoria;
    }

    public void setActividad_categoria(String actividad_categoria) {
        this.actividad_categoria = actividad_categoria;
    }

    public String getFecha() {
        return fecha_creacion;
    }

    public void setFecha(String fecha) {
        this.fecha_creacion = fecha;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }

    public String getDescripcion_compania() {
        return descripcion_compania;
    }

    public void setDescripcion_compania(String descripcion_compania) {
        this.descripcion_compania = descripcion_compania;
    }

    public int getParticipantes() {
        return participantes;
    }

    public void setParticipantes(int participantes) {
        this.participantes = participantes;
    }
}
