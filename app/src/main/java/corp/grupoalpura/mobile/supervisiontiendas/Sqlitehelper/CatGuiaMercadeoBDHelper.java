package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;

public class CatGuiaMercadeoBDHelper extends DBHelper {

    public CatGuiaMercadeoBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public void insertaFaltante(int idGuiaMercadeo, int idSucursal, String motivo, int estatus, String usuario) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_CAT_GUIA_MERCADEO_ID_GUIA_MERCADEO, idGuiaMercadeo);
        values.put(COLUMNA_CAT_GUIA_MERCADEO_ID_SUCURSAL, idSucursal);
        values.put(COLUMNA_CAT_GUIA_MERCADEO_MOTIVO, motivo);
        values.put(COLUMNA_CAT_GUIA_MERCADEO_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_CAT_GUIA_MERCADEO_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_CAT_GUIA_MERCADEO_USUARIO, usuario);
        values.put(COLUMNA_CAT_GUIA_MERCADEO_ESTATUS, estatus);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        database.insert(TABLA_GUIA_MERCADEO_FALTANTE, null, values);
        database.close();
    }


    public void eliminaEstatusGuia(int idGuiaMercadeo) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "DELETE FROM GUIA_MERCADEO_FALTANTE " +
                " WHERE ID_GUIA_MERCADEO = " + idGuiaMercadeo + ";";
        database.execSQL(strSQL);
        database.close();


    }

}
