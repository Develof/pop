package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 14/04/2016.
 */
public class Telefonos {
    String numeroTelefono;
    int tipoTelefono;
    String nombreTipoTelefono;

    public Telefonos(String numeroTelefono, int tipoTelefono, String nombreTipoTelefono) {
        this.numeroTelefono = numeroTelefono;
        this.tipoTelefono = tipoTelefono;
        this.nombreTipoTelefono = nombreTipoTelefono;
    }

    public Telefonos(String numeroTelefono, int tipoTelefono) {
        this.numeroTelefono = numeroTelefono;
        this.tipoTelefono = tipoTelefono;
    }

    public Telefonos(String numeroTelefono, String nombreTipoTelefono) {
        this.numeroTelefono = numeroTelefono;
        this.nombreTipoTelefono = nombreTipoTelefono;
    }

    public String getNombreTipoTelefono() {
        return nombreTipoTelefono;
    }

    public void setNombreTipoTelefono(String nombreTipoTelefono) {
        this.nombreTipoTelefono = nombreTipoTelefono;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public int getTipoTelefono() {
        return tipoTelefono;
    }

    public void setTipoTelefono(int tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }
}
