package corp.grupoalpura.mobile.supervisiontiendas;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters.ProductoCatalogadoAdapter;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.DetalleIncidenciaBDHelper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ProductosCatalogadosFragment extends Fragment implements LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SearchView.OnQueryTextListener {

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String VENTANA = "Ventana";
    private static final String ID_FAMILIA = "ID_FAMILIA";

    private RecyclerView recyclerView;
    private List<FichaTecnicaProducto> productos = new ArrayList<>();
    ClienteSucursal sucursal;
    DetalleIncidenciaBDHelper detalleIncidenciaBDHelper;
    Usuario usuario;
    GoogleApiClient googleApiClient;
    double latitud = 0, longitud = 0;
    protected LocationRequest mLocationRequest;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 5;
    int id_producto_existencia = 0;
    int ventana = 0, idfamilia = 0;

    ProductoCatalogadoAdapter rcAdapter;

    private SharedPreferences loginPrefs;
    private SharedPreferences.Editor loginEditor;
    int posicion;
    public static String PRODUCTO_POSICION = "position";

    public ProductosCatalogadosFragment() {
        // Required empty public constructor
    }

    public ProductosCatalogadosFragment newInstance(ClienteSucursal sucursal, Usuario usuario, Integer ventana, Integer idFamilia) {
        ProductosCatalogadosFragment fragment = new ProductosCatalogadosFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(VENTANA, ventana);
        args.putSerializable(ID_FAMILIA, idFamilia);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState != null)
            id_producto_existencia = savedInstanceState.getInt("id_producto_existencia");
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            ventana = (Integer) getArguments().getSerializable(VENTANA);
            idfamilia = (Integer) getArguments().getSerializable(ID_FAMILIA);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_productos_catalogados, container, false);
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null) {
            googleApiClient.connect();
        } else {
            LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 5, 0, this);
            } else {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 5, 0, this);
            }
        }

        detalleIncidenciaBDHelper = new DetalleIncidenciaBDHelper(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_productos_catalogados);

        if (ventana == 0){
            productos = detalleIncidenciaBDHelper.getAllProductoCatalogadoLista(sucursal.getId());
        } else {
            productos = detalleIncidenciaBDHelper.getAllProductoCatalogadoFamilia(sucursal.getId(), idfamilia);
        }

        rcAdapter = new ProductoCatalogadoAdapter(getActivity(), productos, sucursal, usuario, getFragmentManager());
        recyclerView.setAdapter(rcAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //CREAMOS UN OBJETO SHAREDPREFERENCES PARA GUARDAR LA SESION
        loginPrefs = getActivity().getSharedPreferences("producto_posicion", getActivity().MODE_PRIVATE);
        loginEditor = loginPrefs.edit();
        posicion = loginPrefs.getInt(PRODUCTO_POSICION, 0);

        DefaultItemAnimator animator = new DefaultItemAnimator();
        animator.setAddDuration(500);
        animator.setRemoveDuration(500);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.getLayoutManager().scrollToPosition(posicion);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.busqueda, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<FichaTecnicaProducto> filteredModelList = filter(productos, query);
        rcAdapter.animateTo(filteredModelList);
        recyclerView.scrollToPosition(0);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<FichaTecnicaProducto> filter(List<FichaTecnicaProducto> models, String query) {
        query = query.toLowerCase();

        final List<FichaTecnicaProducto> filteredModelList = new ArrayList<>();
        for (FichaTecnicaProducto model : models) {
            if (model.getClave().toLowerCase(Locale.getDefault()).contains(query) || model.getDescripcion().toLowerCase(Locale.getDefault()).contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (getActivity() != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {

                latitud = location.getLatitude();
                longitud = location.getLongitude();

            } else {
                mLocationRequest = new LocationRequest();

                mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

                mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                PendingResult<Status> locationUpdates = LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, new com.google.android.gms.location.LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        latitud = location.getLatitude();
                        longitud = location.getLongitude();

                    }
                });
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        latitud = location.getLatitude();
        longitud = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id_producto_existencia", id_producto_existencia);
    }

}
