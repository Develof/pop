package corp.grupoalpura.mobile.supervisiontiendas;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.MensajesSMS;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MensajesPush extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ArrayList<MensajesSMS> mensajesSMSes = new ArrayList<>();
    private Handler mHandler = new Handler();
    private Toolbar toolbar;
    ListView listMesagge;
    CustomAdapter arrayAdapter;
    SwipeRefreshLayout mSwipreRefreshLayout;
    int read;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensajes_push);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listMesagge = (ListView) findViewById(R.id.listView_mensajes);
        mSwipreRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiprRefresh);
        mSwipreRefreshLayout.setOnRefreshListener(this);
        mSwipreRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mensajesSMSes = mensages();


        arrayAdapter = new CustomAdapter(this);
        listMesagge.setAdapter(arrayAdapter);

        listMesagge.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                final Dialog dialogMensaje = new Dialog(MensajesPush.this);
                dialogMensaje.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogMensaje.setContentView(R.layout.dialog_mensaje_alerta);


                TextView textNumero = (TextView) dialogMensaje.findViewById(R.id.textview_title_mensaje_alerta);
                TextView textMensaje = (TextView) dialogMensaje.findViewById(R.id.textView_seleccionaFecha_notificacion);
                TextView textFehca = (TextView)  dialogMensaje.findViewById(R.id.textview_title_mensaje_alerta_fecha);
                TextView textAceptar = (TextView) dialogMensaje.findViewById(R.id.btn_aceptar_espacio);
                final CheckBox cbLeido = (CheckBox) dialogMensaje.findViewById(R.id.checkBox_leido);

                textNumero.setTextColor(ContextCompat.getColor(MensajesPush.this, R.color.colorPrimary));
                textAceptar.setTextColor(ContextCompat.getColor(MensajesPush.this, R.color.colorPrimary));

                if (mensajesSMSes.get(position).getEstado_leido() == Constantes.MENSAJES.LEIDO) {
                    cbLeido.setChecked(true);
                }

                textNumero.setText(mensajesSMSes.get(position).getAdress());
                textMensaje.setText(mensajesSMSes.get(position).getMensaje());
                textFehca.setText(mensajesSMSes.get(position).getFecha());

                textAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mensajesSMSes = mensages();
                        arrayAdapter.notifyDataSetChanged();
                        dialogMensaje.dismiss();
                    }
                });

                cbLeido.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cbLeido.isChecked()) {
                            ContentValues values = new ContentValues();
                            values.put("read", 1);
                            MensajesPush.this.getContentResolver().update(Uri.parse("content://sms/inbox"), values, "_id=" + mensajesSMSes.get(position).getId_mensaje(), null);

                        } else {
                            ContentValues values = new ContentValues();
                            values.put("read", 0);
                            MensajesPush.this.getContentResolver().update(Uri.parse("content://sms/inbox"), values, "_id=" + mensajesSMSes.get(position).getId_mensaje(), null);
                        }
                    }
                });

                dialogMensaje.show();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialogMensaje.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ArrayList<MensajesSMS> mensages() {
        ArrayList<MensajesSMS> mensajes = new ArrayList<>();
        mensajes.clear();
        Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);

        assert cursor != null;
        if (cursor.moveToFirst()) {
            do {
                read = Integer.parseInt(cursor.getString(cursor.getColumnIndex("read")));
                MensajesSMS sms = new MensajesSMS(cursor.getString(cursor.getColumnIndex("_id")), cursor.getString(cursor.getColumnIndex("address")), cursor.getString(cursor.getColumnIndex("body")), convertEpochToDate(Long.parseLong(cursor.getString(cursor.getColumnIndex("date_sent")))), Integer.parseInt(cursor.getString(cursor.getColumnIndex("read"))));
                if(sms.getMensaje() == null){
                    sms.setMensaje("Mensaje Dañado");
                }
                mensajes.add(sms);
            } while (cursor.moveToNext());
        } else {
            Toast toast = Toast.makeText(MensajesPush.this, "La bandeja de mensajes esta vacía", Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.WHITE);
            toast.show();
        }
        return mensajes;
    }

    private class CustomAdapter extends BaseAdapter {
        Context context;

        public CustomAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            return mensajesSMSes.size();
        }

        @Override
        public Object getItem(int position) {
            return mensajesSMSes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.adapter_mensajes_push, parent, false);

            mSwipreRefreshLayout.setRefreshing(false);
            TextView txtIdMensaje = (TextView) row.findViewById(R.id.textView_id_mensaje);
            TextView txtMensaje = (TextView) row.findViewById(R.id.textView_mensaje_push);
            TextView txtNumero = (TextView) row.findViewById(R.id.textView_numero_mensaje);
            TextView txtFecha = (TextView) row.findViewById(R.id.textView_fecha_mensaje);
            ImageView imgLetra = (ImageView) row.findViewById(R.id.img_mensaje_letra);

            if (mensajesSMSes.get(position).getEstado_leido() == Constantes.MENSAJES.NO_LEIDO) {
                txtNumero.setTypeface(null, Typeface.BOLD);
                txtMensaje.setTypeface(null, Typeface.BOLD);
                txtFecha.setTypeface(null, Typeface.BOLD);
                txtFecha.setTextColor(ContextCompat.getColor(MensajesPush.this, R.color.colorRed));
            } else {
                txtMensaje.setTypeface(null, Typeface.NORMAL);
                txtFecha.setTypeface(null, Typeface.NORMAL);
                txtNumero.setTypeface(null, Typeface.NORMAL);
                txtMensaje.setTextColor(ContextCompat.getColor(MensajesPush.this, android.R.color.darker_gray));
                txtNumero.setTextColor(ContextCompat.getColor(MensajesPush.this, android.R.color.darker_gray));
                txtFecha.setTextColor(ContextCompat.getColor(MensajesPush.this, android.R.color.darker_gray));
            }


            String character = mensajesSMSes.get(position).getMensaje().substring(0, 1);
            if (!Character.isLetter(character.charAt(0))) {
                character = "?";
            }

            txtMensaje.setText(mensajesSMSes.get(position).getMensaje());
            txtNumero.setText(mensajesSMSes.get(position).getAdress());
            txtFecha.setText(mensajesSMSes.get(position).getFecha());
            txtIdMensaje.setText(mensajesSMSes.get(position).getId_mensaje());

            return row;
        }
    }

    private Runnable actualizaMensajes = new Runnable() {
        public void run() {
            mensajesSMSes = mensages();
            arrayAdapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onRefresh() {
        mHandler.postDelayed(actualizaMensajes, 1500);

    }

    private String convertEpochToDate(long epoch) {
        Date date = new Date(epoch);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(date);
        return dateString;
    }


}
