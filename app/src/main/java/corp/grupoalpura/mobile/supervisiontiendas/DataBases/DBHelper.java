package corp.grupoalpura.mobile.supervisiontiendas.DataBases;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterActividadesCompetencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterAlertas;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterDatosGenerales;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterFotos;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterProductoIncidencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterTipoAlerta;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterTipoExistencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CatalogoActividades;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CatalogoCompanias;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.CategoriaIncidencia;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Compania;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Departamentos;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Espacios;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FotosEvidenciaRelacion;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ProductoPrecio;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Visita;
import corp.grupoalpura.mobile.supervisiontiendas.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by consultor on 2/24/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "ServicioTiendas.db";

    /* TABLA UDN */
    public static final String TABLA_UDN = "UDN";
    public static final String COLUMNA_UDN_ID = "ID_UDN";
    public static final String COLUMNA_UDN_CLAVE = "CLAVE";
    public static final String COLUMNA_UDN_ALIAS = "ALIAS";
    public static final String COLUMNA_UDN_LATITUD = "LATITUD";
    public static final String COLUMNA_UDN_LONGITUD = "LONGITUD";
    public static final String COLUMNA_UDN_DIRECCION = "DIRECCION";
    public static final String COLUMNA_UDN_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_UDN_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_UDN_USUARIO = "USUARIO";
    public static final String COLUMNA_UDN_ESTATUS = "ESTATUS";

    /* CANALES */
    private static final String TABLA_CANALES = "CANALES";
    private static final String COLUMNA_CANALES_ID_CANAL = "ID_USUARIO";
    private static final String COLUMNA_CANALES_CLAVE = "CLAVE";
    private static final String COLUMNA_CANALES_NOMBRE = "NOMBRE";

    /* TABLA USUARIO */
    public static final String TABLA_USUARIO = "USUARIO";
    public static final String COLUMNA_USUARIO_ID = "ID_USUARIO";
    public static final String COLUMNA_USUARIO_UDN = "ID_UDN";
    public static final String COLUMNA_USUARIO_ID_JEFE = "ID_USUARIO_JEFE";
    public static final String COLUMNA_USUARIO_ID_CANAL = "ID_CANAL";
    public static final String COLUMNA_USUARIO_NOMBRE = "NOMBRE";
    public static final String COLUMNA_USUARIO_TOKEN = "TOKEN";
    public static final String COLUMNA_USUARIO_EMAIL = "EMAIL";
    public static final String COLUMNA_USUARIO_USERNAME = "USERNAME";
    public static final String COLUMNA_USUARIO_PASSWORD = "PASSWORD";
    public static final String COLUMNA_USUARIO_ROL = "ROL";
    public static final String COLUMNA_USUARIO_CAMBIO_PASSWORD = "CAMBIO_PASSWORD";
    public static final String COLUMNA_USUARIO_CAMBIO_LOCALIZACION = "CAMBIO_LOCALIZACION";
    public static final String COLUMNA_USUARIO_HASH_RESET = "HASH_RESET";
    public static final String COLUMNA_USUARIO_INTENTOS = "INTENTOS";
    public static final String COLUMNA_USUARIO_FECHA_ERROR = "FECHA_ERROR";
    public static final String COLUMNA_USUARIO_FECHA_PASSWORD = "FECHA_PWD";
    public static final String COLUMNA_USUARIO_NIVEL = "NIVEL";
    public static final String COLUMNA_USUARIO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_USUARIO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_USUARIO = "USUARIO";
    public static final String COLUMNA_USUARIO_ESTATUS = "ESTATUS";


    /* LOCALIZACION USUARIO */
    public static final String TABLA_LOCALIZACION_USUARIO = "LOCALIZACION_USUARIO";
    public static final String COLUMNA_LOCALIZACION_USUARIO_ID = "ID_LOCALIZACION";
    public static final String COLUMNA_LOCALIZACION_USUARIO_ID_USUARIO = "ID_USUARIO";
    public static final String COLUMNA_LOCALIZACION_USUARIO_LATITUD = "LATITUD";
    public static final String COLUMNA_LOCALIZACION_USUARIO_LONGITUD = "LONGITUD";
    public static final String COLUMNA_LOCALIZACION_USUARIO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_LOCALIZACION_USUARIO_USUARIO = "USUARIO";
    public static final String COLUMNA_LOCALIZACION_USUARIO_ESTATUS = "ESTATUS";


    /* TABLA VISITA */
    public static final String TABLA_VISITA = "VISITA";
    public static final String COLUMNA_VISITA_ID = "ID_VISITA";
    public static final String COLUMNA_VISITAID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_VISITAID_USUARIO = "ID_USUARIO";
    public static final String COLUMNA_VISITA_HORA_CHECK_INICIO = "FECHA_INICIO";
    public static final String COLUMNA_VISITA_HORA_CHECK_FIN = "FECHA_FIN";
    public static final String COLUMNA_VISITA_LATITUD_CHECKIN = "LATITUD_CHECKIN";
    public static final String COLUMNA_VISITA_LONGITUD_CHECKIN = "LONGITUD_CHECKIN";
    public static final String COLUMNA_VISITA_LATITUD_CHECKOUT = "LATITUD_CHECKOUT";
    public static final String COLUMNA_VISITA_LONGITUD_CHECKOUT = "LONGITUD_CHECKOUT";
    public static final String COLUMNA_VISITA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_VISITA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_VISITA_USUARIO = "USUARIO";
    public static final String COLUMNA_VISITA_ESTATUS = "ESTATUS";

    /* TIPO ALERTA */
    public static final String TABLA_TIPO_ALERTA = "TIPO_ALERTA";
    public static final String COLUMNA_TIPO_ALERTA_ID = "ID_TIPO_ALERTA";
    public static final String COLUMNA_TIPO_ALERTA_NOMBRE = "NOMBRE";
    public static final String COLUMNA_TIPO_ALERTA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_TIPO_ALERTA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_TIPO_ALERTA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_TIPO_ALERTA_USUARIO = "USUARIO";
    public static final String COLUMNA_TIPO_ALERTA_ESTATUS = "ESTATUS";

    /* VISITA ALERTA */
    public static final String TABLA_VISITA_ALERTA = "VISITA_ALERTA";
    public static final String COLUMNA_VISITA_ALERTA_ID = "ID_ALERTA";
    public static final String COLUMNA_VISITA_ALERTAID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_VISITA_ALERTAID_TIPO_ALERTA = "ID_TIPO_ALERTA";
    public static final String COLUMNA_VISITA_ALERTA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_VISITA_ALERTA_LEIDA = "LEIDA";
    public static final String COLUMNA_VISITA_ALERTA_FECHA = "FECHA";
    public static final String COLUMNA_VISITA_ALERTA_PENDIENTE = "PENDIENTE";
    public static final String COLUMNA_VISITA_ALERTA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_VISITA_ALERTA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_VISITA_ALERTA_USUARIO = "USUARIO";
    public static final String COLUMNA_VISITA_ALERTA_ESTATUS = "ESTATUS";

    /* TABLA COMPAÑIA */
    public static final String TABLA_COMPANIA = "COMPANIA";
    public static final String COLUMNA_COMPANIA_ID = "ID_COMPANIA";
    public static final String COLUMNA_COMPANIA_NOMBRE = "NOMBRE";
    public static final String COLUMNA_COMPANIA_PRIORIDAD = "PRIORIDAD";
    public static final String COLUMNA_COMPANIA_TIPO_COMPANIA = "TIPO_COMPANIA";
    public static final String COLUMNA_COMPANIA_TIPO_USUARIO_COMPANIA = "TIPO_USUARIO_COMPANIA";
    public static final String COLUMNA_COMPANIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_COMPANIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_COMPANIA_USUARIO = "USUARIO";
    public static final String COLUMNA_COMPANIA_ESTATUS = "ESTATUS";

    /* TABLA PRODUCTO */
    public static final String TABLA_PRODUCTO = "PRODUCTO";
    public static final String COLUMNA_PRODUCTO_ID = "ID_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_ID_PRESENTACION = "ID_PRESENTACION";
    public static final String COLUMNA_PRODUCTO_ID_FAMILIA = "ID_FAMILIA";
    public static final String COLUMNA_PRODUCTO_AGRUPACION = "ID_AGRUPACION";
    public static final String COLUMNA_PRODUCTO_TIPO_PRODUCTO = "ID_TIPO_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_SUBCATEGORIA = "ID_SUBCATEGORIA";
    public static final String COLUMNA_PRODUCTO_TOP = "TOP_ITEM";
    public static final String COLUMNA_PRODUCTO_ID_COMPANIA = "ID_COMPANIA";
    public static final String COLUMNA_PRODUCTO_ORDENAMIENTO = "ORDENAMIENTO";
    public static final String COLUMNA_PRODUCTO_CLAVE = "CLAVE";
    public static final String COLUMNA_PRODUCTO_UPC = "UPC";
    public static final String COLUMNA_PRODUCTO_UNIDAD = "UNIDAD";
    public static final String COLUMNA_PRODUCTO_NOMBRE = "NOMBRE";
    public static final String COLUMNA_PRODUCTO_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_PRODUCTO_PRECIO_UNITARIO = "PRECIO_UNITARIO";
    public static final String COLUMNA_PRODUCTO_APLICA_IVA = "APLICA_IVA";
    public static final String COLUMNA_PRODUCTO_EMPAQUE = "EMPAQUE";
    public static final String COLUMNA_PRODUCTO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PRODUCTO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PRODUCTO_USUARIO = "USUARIO";
    public static final String COLUMNA_PRODUCTO_ESTATUS = "ESTATUS";

    /* PRODUCTO SUBCATEGORIA */
    public static final String TABLA_PRODUCTO_SUBCATEGORIA = "PRODUCTO_SUBCATEGORIA";
    public static final String COLUMNA_PRODUCTO_SUBCATEGORIA_ID = "ID_SUBCATEGORIA";
    public static final String COLUMNA_PRODUCTO_SUBCATEGORIA_SUBCATEGORIA = "SUBCATEGORIA";
    public static final String COLUMNA_PRODUCTO_SUBCATEGORIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PRODUCTO_SUBCATEGORIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PRODUCTO_SUBCATEGORIA_USUARIO = "USUARIO";
    public static final String COLUMNA_PRODUCTO_SUBCATEGORIA_ESTATUS = "ESTATUS";

    /* CATEGORIA INCIDENCIA */
    public static final String TABLA_CATEGORIA_INCIDENCIA = "CATEGORIA_INCIDENCIA";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_ID = "ID_CATEGORIA_INCIDENCIA";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_CATEGORIA = "CATEGORIA";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_TIPO_CATEGORIA = "TIPO_CATEGORIA";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_TIPO_INCIDENCIA = "TIPO_INCIDENCIA";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_CATEGORIA_INCIDENCIA_ESTATUS = "ESTATUS";

    /* PRODUCTO INCIDENCIA */
    public static final String TABLA_PRODUCTO_INCIDENCIA = "PRODUCTO_INCIDENCIA";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_ID = "ID_PRODUCTO_INCIDENCIA";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_ID_CATEGORIA = "ID_CATEGORIA_INCIDENCIA";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_ID_PRODUCTO = "ID_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_BAN_MEDICICON = "BAN_MEDICION";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_FECHA = "FECHA";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_PRODUCTO_INCIDENCIA_ESTATUS = "ESTATUS";

    /* TABLA CLIENTE */
    public static final String TABLA_CLIENTE = "CLIENTE";
    public static final String COLUMNA_CLIENTE_ID = "ID_CLIENTE";
    public static final String COLUMNA_CLIENTE_ALIAS = "ALIAS";
    public static final String COLUMNA_CLIENTE_ALIAS2 = "ALIAS2";
    public static final String COLUMNA_CLIENTE_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_CLIENTE_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_CLIENTE_USUARIO = "USUARIO";
    public static final String COLUMNA_CLIENTE_ESTATUS = "ESTATUS";

    /* TABLA CADENA */
    public static final String TABLA_CADENA = "CADENA";
    public static final String COLUMNA_CADENA_ID = "ID_CADENA";
    public static final String COLUMNA_CADENAID_CLIENTE = "ID_CLIENTE";
    public static final String COLUMNA_CADENA_RFC = "RFC";
    public static final String COLUMNA_CADENA_RAZON_SOCIAL = "RAZON_SOCIAL";
    public static final String COLUMNA_CADENA_ALIAS = "ALIAS";
    public static final String COLUMNA_CADENA_ALIAS2 = "ALIAS2";
    public static final String COLUMNA_CADENA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_CADENA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_CADENA_USUARIO = "USUARIO";
    public static final String COLUMNA_CADENA_ESTATUS = "ESTATUS";

    /* TABLA CLIENTE SUCURSAL */
    public static final String TABLA_SUCURSAL = "CLIENTE_SUCURSAL";
    public static final String COLUMNA_SUCRUSAL_ID = "ID_SUCURSAL";
    public static final String COLUMNA_SUCURSAL_CADENA = "ID_CADENA";
    public static final String COLUMNA_SUCURSAL_ID_UDN = "ID_UDN";
    public static final String COLUMNA_SUCURSAL_CLAVE = "CLAVE";
    public static final String COLUMNA_SUCURSAL_ALIAS = "ALIAS";
    public static final String COLUMNA_SUCURSAL_CALLE = "CALLE";
    public static final String COLUMNA_SUCURSAL_NO_EXT = "NO_EXT";
    public static final String COLUMNA_SUCURSAL_NO_INT = "NO_INT";
    public static final String COLUMNA_SUCURSAL_COLONIA = "COLONIA";
    public static final String COLUMNA_SUCURSAL_CP = "CP";
    public static final String COLUMNA_SUCURSAL_MUNICIPIO = "MUNICIPIO";
    public static final String COLUMNA_SUCURSAL_ESTADO = "ESTADO";
    public static final String COLUMNA_SUCURSAL_LATITUD = "LATITUD";
    public static final String COLUMNA_SUCURSAL_LOCALIZACION_ACTIVA = "LOCALIZACION_ACTIVA";
    public static final String COLUMNA_SUCURSAL_LONGITUD = "LONGITUD";
    public static final String COLUMNA_SUCRSAL_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_SUCURSAL_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_SUCURSAL_USUARIO = "USUARIO";
    public static final String COLUMNA_SUCURSAL_ESTATUS = "ESTATUS";

    /*PRODUCTO CATALOGADO */
    public static final String TABLA_PRODUCTO_CATALOGADO = "PRODUCTO_CATALOGADO";
    public static final String COLUMNA_PRODUCTO_CATALOGADO_ID = "ID_PRODUCTO_CATALOGADO";
    public static final String COLUMNA_PRODUCTO_CATALOGADO_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_CATALOGADO_ID_PRODUCTO = "ID_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_CATALOGADO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PRODUCTO_CATALOGADO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PRODUCTO_CATALOGADO_USUARIO = "USUARIO";
    public static final String COLUMNA_PRODUCTO_CATALOGADO_ESTATUS = "ESTATUS";

    /*SUCURSAL PRODUCTO EXISTENCIA */
    public static final String TABLA_SUCURSAL_PRODUCTO_EXISTENCIA = "SUCURSAL_PRODUCTO_EXISTENCIA";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID = "ID_SUCURSAL_PRODUCTO";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_PRODUCTO = "ID_PRODUCTO";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_TIPO_EXISTENCIA = "ID_TIPO_EXISTENCIA";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_CANTIDAD = "CANTIDAD";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_FECHA = "FECHA";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_EXHIBIDO = "EXHIBIDO";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_COMENTARIOS = "COMENTARIOS";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ESTATUS = "ESTATUS";

    /* TIPO EXISTENCIA */
    public static final String TABLA_TIPO_EXISTENCIA = "TIPO_EXISTENCIA";
    public static final String COLUMNA_TIPO_EXISTENCIA_ID = "ID_TIPO_EXISTENCIA";
    public static final String COLUMNA_TIPO_EXISTENCIA_NOMBRE = "NOMBRE";
    public static final String COLUMNA_TIPO_EXISTENCIA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_TIPO_EXISTENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_TIPO_EXISTENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_TIPO_EXISTENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_TIPO_EXISTENCIA_ESTATUS = "ESTATUS";

    /*PRODUCTO FAMILIA */
    public static final String TABLA_PROD_FAMILIA = "PRODUCTO_FAMILIA";
    public static final String COLUMNA_PROD_FAMILIA_ID = "ID_FAMILIA";
    public static final String COLUMNA_PROD_FAMILIA = "FAMILIA";
    public static final String COLUMNA_PROD_FAMILIA_ORDENAMIENTO = "ORDENAMIENTO";
    public static final String COLUMNA_PROD_FAMILIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PROD_FAMILIA_ULRIMO_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PROD_FAMILIA_USUARIO = "USUARIO";
    public static final String COLUMNA_PROD_FAMILIA_ESTATUS = "ESTATUS";

    /*PRODUCTO PRESENTACION */
    public static final String TABLA_PROD_PRESENTACION = "PRODUCTO_PRESENTACION";
    public static final String COLUMNA_PROD_PRESENTACION_ID = "ID_PRESENTACION";
    public static final String COLUMNA_PROD_PRESENTACION = "PRESENTACION";
    public static final String COLUMNA_PROD_PRESENTACION_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PROD_PRESENTACION_ULRIMO_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PROD_PRESENTACION_USUARIO = "USUARIO";
    public static final String COLUMNA_PROD_PRESENTACION_ESTATUS = "ESTATUS";

    /*PRODUCTO PRECIO */
    public static final String TABLA_PRODUCTO_PRECIO = "PRODUCTO_PRECIOS";
    public static final String COLUMNA_PRODUCTO_PRECIO_ID = "ID_PRECIO";
    public static final String COLUMNA_PRODUCTO_PRECIOID_PRODUCTO = "ID_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_PRECIOID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_PRECIOID_COMPANIA = "ID_COMPANIA";
    public static final String COLUMNA_PRODUCTO_PRECIO_PRECIO_UNITARIO = "PRECIO_UNITARIO";
    public static final String COLUMNA_PRODUCTO_PRECIO_PRECIO_PROMOCION = "PRECIO_PROMOCION";
    public static final String COLUMNA_PRODUCTO_PRECIO_APLICA_IVA = "APLICA_IVA";
    public static final String COLUMNA_PRODUCTO_PRECIO_DESCRIPCION_COMPANIA = "DESCRIPCION_COMPANIA";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_PRECIO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_PRECIO_USUARIO = "USUARIO";
    public static final String COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ESTATUS = "ESTATUS";

    /* Espacios */
    public static final String TABLA_ESPACIOS = "Espacios";
    public static final String COLUMNA_ESPACIOS_ID = "ID_ESPACIO";
    public static final String COLUMNA_ESPACIOS_NOMBRE = "NOMBRE";
    public static final String COLUMNA_ESPACIOS_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_ESPACIOS_TIPO_ESPACIO = "TIPO_ESPACIO";
    public static final String COLUMNA_ESPACIOS_TIPO_LLENADO = "TIPO_LLENADO";
    public static final String COLUMNA_ESPACIOS_TIPO_FRENTE = "TIPO_FRENTE";
    public static final String COLUMNA_ESPACIOS_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_ESPACIOS_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_ESPACIOS_USUARIO = "USUARIO";
    public static final String COLUMNA_ESPACIOS_ESTATUS = "ESTATUS";

    /* DEPARTAMENTOS */
    public static final String TABLA_DEPARTAMENTOS = "DEPARTAMENTOS";
    public static final String COLUMNA_DEPARTAMENTOS_ID = "ID_DEPARTAMENTO";
    public static final String COLUMNA_DEPARTAMENTOS_NOMBRE = "NOMBRE";
    public static final String COLUMNA_DEPARTAMENTOS_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_DEPARTAMENTOS_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_DEPARTAMENTOS_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_DEPARTAMENTOS_USUARIO = "USUARIO";
    public static final String COLUMNA_DEPARTAMENTOS_ESTATUS = "ESTATUS";

    /* SUCURSAL Espacios */
    public static final String TABLA_SUCURSAL_ESPACIOS = "SUCURSAL_ESPACIO";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_ID = "ID_SUCURSAL_ESPACIO";
    public static final String COLUMNA_SUCURSAL_ESPACIOSID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_ID_ESPACIO = "ID_ESPACIO";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_ID_DEPARTAMENTOS = "ID_DEPARTAMENTO";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_ID_CATEGORIA = "ID_CATEGORIA";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_TIPO_ADQUISICION = "TIPO_ADQUISICION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_NIVEL = "NIVEL";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_PUERTAS = "PUERTAS";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_TRAMOS = "TRAMOS";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_TARIMAS = "TARIMAS";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_PLANOGRAMA_RUTA = "PLANOGRAMA_RUTA";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_FECHA_ASIGNACION = "FECHA_ASIGNACION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_FECHA_ELIMINACION = "FECHA_ELIMINACION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_MOTIVO_ELIMINACION = "MOTIVO_ELIMINACION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_USUARIO = "USUARIO";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_ESTATUS = "ESTATUS";

    /* SUCURSAL_ESPACIO_EXISTENCIA */
    public static final String TABLA_SUCURSAL_ESPACIO_EXISTENCIA = "SUCURSAL_ESPACIO_EXISTENCIA";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID = "ID_SUCURSAL_ESPACIO_EXISTENCIA";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID_SUCURSAL_ESPACIO = "ID_SUCURSAL_ESPACIO";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID_TIPO_EXISTENCIA = "ID_TIPO_EXISTENCIA";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_FECHA = "FECHA";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_EXHIBIDO = "EXHIBIDO";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_COMENTARIOS = "COMENTARIOS";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ESTATUS = "ESTATUS";

    /* ESPACIO FRENTE */
    public static final String TABLA_ESPACIO_FRENTE = "ESPACIO_FRENTE";
    public static final String COLUMNA_ESPACIO_FRENTE_ID = "ID_ESPACIO_FRENTE";
    public static final String COLUMNA_ESPACIO_FRENTE_ID_SUCURSAL_ESPACIO = "ID_SUCURSAL_ESPACIO";
    public static final String COLUMNA_ESPACIO_FRENTE_ID_COMPANIA = "ID_COMPANIA";
    public static final String COLUMNA_ESPACIO_FRENTE_ID_AGRUPACION = "ID_AGRUPACION";
    public static final String COLUMNA_ESPACIO_FRENTE_ID_PRESENTACION = "ID_PRESENTACION";
    public static final String COLUMNA_ESPACIO_FRENTE_FRENTES = "FRENTES";
    public static final String COLUMNA_ESPACIO_FRENTE_DESCRIPCION_COMPANIA = "DESCRIPCION_COMPANIA";
    public static final String COLUMNA_ESPACIO_FRENTE_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_ESPACIO_FRENTE_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_ESPACIO_FRENTE_USUARIO = "USUARIO";
    public static final String COLUMNA_ESPACIO_FRENTE_ESTATUS = "ESTATUS";


    /* ESPACIO INCIDENCIA */
    public static final String TABLA_ESPACIO_INCIDENCIA = "ESPACIO_INCIDENCIA";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_ID = "ID_ESPACIO_INCIDENCIA";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_ID_CATEGORIA = "ID_CATEGORIA_INCIDENCIA";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_ID_SUCURSAL_ESPACIO = "ID_SUCURSAL_ESPACIO";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_BAN_MEDICION = "BAN_MEDICION";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_FECHA = "FECHA";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_ESPACIO_INCIDENCIA_ESTATUS = "ESTATUS";

    /* FOTOS EVIDENCIA */
    public static final String TABLA_FOTOS_EVIDENCIA = "FOTOS_EVIDENCIA";
    public static final String COLUMNA_FOTOS_EVIDENCIA_ID = "ID_FOTO";
    public static final String COLUMNA_FOTOS_EVIDENCIAID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_FOTOS_EVIDENCIA_ARCHIVO = "ARCHIVO";
    public static final String COLUMNA_FOTOS_EVIDENCIA_FILE_NAME = "FILE_NAME";
    public static final String COLUMNA_FOTOS_EVIDENCIA_LATITUD = "LATITUD";
    public static final String COLUMNA_FOTOS_EVIDENCIA_LONGITUD = "LONGITUD";
    public static final String COLUMNA_FOTOS_EVIDENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_FOTOS_EVIDENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_FOTOS_EVIDENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_FOTOS_EVIDENCIA_ESTATUS = "ESTATUS";

    /* FOTO EVIDENCIA RELACION */
    public static final String TABLA_FOTOS_EVIDENCIA_RELACION = "FOTOS_EVIDENCIA_RELACION";
    public static final String COLUMNA_FOTOS_EVIDENCIA_RELACION_ID_RELACION = "ID_RELACION";
    public static final String COLUMNA_FOTOS_EVIDENCIA_RELACION_ID_FOTO = "ID_FOTO";
    public static final String COLUMNA_FOTOS_EVIDENCIA_RELACION_TABLA = "TABLA";
    public static final String COLUMNA_FOTOS_EVIDENCIA_RELACION_ID = "ID";

    /* PUESTO */
    public static final String TABLA_PUESTO = "PUESTO";
    public static final String COLUMNA_PUESTO_ID = "ID_PUESTO";
    public static final String COLUMNA_PUESTO_NOMBRE = "NOMBRE";
    public static final String COLUMNA_PUESTO_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_PUESTO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PUESTO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PUESTO_USUARIO = "USUARIO";
    public static final String COLUMNA_PUESTO_ESTATUS = "ESTATUS";

    /* TIPO TELEFONO */
    public static final String TABLA_TIPO_TELEFONO = "TIPO_TELEFONO";
    public static final String COLUMNA_TIPO_TELEFONO_ID = "ID_TIPO_TELEFONO";
    public static final String COLUMNA_TIPO_TELEFONO_NOMBRE = "NOMBRE";
    public static final String COLUMNA_TIPO_TELEFONO_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_TIPO_TELEFONO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_TIPO_TELEFONO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_TIPO_TELEFONO_USUARIO = "USUARIO";
    public static final String COLUMNA_TIPO_TELEFONO_ESTATUS = "ESTATUS";

    /* PERSONAL */
    public static final String TABLA_PERSONAL = "PERSONAL";
    public static final String COLUMNA_PERONAL_ID = "ID_PERSONAL";
    public static final String COLUMNA_PERONALID_PUESTO = "ID_PUESTO";
    public static final String COLUMNA_PERONAL_CLAVE = "CLAVE";
    public static final String COLUMNA_PERONAL_NOMBRE = "NOMBRE";
    public static final String COLUMNA_PERONAL_AP_PATERNO = "AP_PATERNO";
    public static final String COLUMNA_PERONAL_AP_MATERNO = "AP_MATERNO";
    public static final String COLUMNA_PERONAL_EMAIL = "EMAIL";
    public static final String COLUMNA_PERONAL_FECHA_CONTRATACION = "FECHA_CONTRATACION";
    public static final String COLUMNA_PERONAL_FECHA_BAJA = "FECHA_BAJA";
    public static final String COLUMNA_PERONAL_FECHA_COMENTARIOS = "COMENTARIOS";
    public static final String COLUMNA_PERONAL_PRECIO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PERONAL_PRECIO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PERONAL_PRECIO_USUARIO = "USUARIO";
    public static final String COLUMNA_PERONAL_PRECIO_ESTATUS = "ESTATUS";

    /* TELEFONO */
    public static final String TABLA_TELEFONO = "TELEFONO";
    public static final String COLUMNA_TELEFONO_ID = "ID_TELEFONO";
    public static final String COLUMNA_TELEFONOID_PERSONAL = "ID_PERSONAL";
    public static final String COLUMNA_TELEFONO_ID_TIPO_TELEFONO = "ID_TIPO_TELEFONO";
    public static final String COLUMNA_TELEFONO_TELEFONO = "TELEFONO";
    public static final String COLUMNA_TELEFONO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_TELEFONO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_TELEFONO_USUARIO = "USUARIO";
    public static final String COLUMNA_TELEFONO_ESTATUS = "ESTATUS";

    /* SUCURSAL PERSONAL */
    public static final String TABLA_SUCURSAL_PERSONAL = "SUCURSAL_PERSONAL";
    public static final String COLUMNA_SUCURSAL_PERSONAL_ID = "ID_SUCURSAL_PERSONAL";
    public static final String COLUMNA_SUCURSAL_PERSONALID_PERSONAL = "ID_PERSONAL";
    public static final String COLUMNA_SUCURSAL_PERSONALID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_SUCURSAL_PERSONAL_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_SUCURSAL_PERSONAL_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_SUCURSAL_PERSONAL_USUARIO = "USUARIO";
    public static final String COLUMNA_SUCURSAL_PERSONAL_ESTATUS = "ESTATUS";

    /* DIAS */
    public static final String TABLA_DIAS = "DIAS";
    public static final String COLUMNA_DIAS_ID = "ID_DIA";
    public static final String COLUMNA_DIAS_NOMBRE = "NOMBRE";
    public static final String COLUMNA_DIAS_ESTATUS = "ESTATUS";

    /* SUCURSAL SUPERVISOR */
    public static final String TABLA_SUCURSAL_SUPERVISOR = "SUCURSAL_SUPERVISOR";
    public static final String COLUMNA_SUCURSAL_SUPERVISOR_ID = "ID_SUCURSAL_SUPERVISOR";
    public static final String COLUMNA_SUCURSAL_SUPERVISOR_ID_USUARIO = "ID_USUARIO";
    public static final String COLUMNA_SUCURSAL_SUPERVISOR_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_SUCURSAL_SUPERVISOR_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_SUCURSAL_SUPERVISOR_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_SUCURSAL_SUPERVISOR_USUARIO = "USUARIO";
    public static final String COLUMNA_SUCURSAL_SUPERVISOR_ESTATUS = "ESTATUS";

    /* DIAS_SUCURSAL_PERSONAL */
    public static final String TABLA_DIAS_SUCURSAL_PERSONAL = "DIAS_SUCURSAL_PERSONAL";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_ID = "ID_DIA_PERSONAL";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_ID_SUCRUSAL_PERSONAL = "ID_SUCURSAL_PERSONAL";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONALID_DIA = "ID_DIA";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_HORA_INICO = "HORA_INICIO";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_HORA_FIN = "HORA_FIN";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_USUARIO = "USUARIO";
    public static final String COLUMNA_DIAS_SUCURSAL_PERSONAL_ESTATUS = "ESTATUS";

    /* DIAS_SUCURSAL_SUPERVICION */
    public static final String TABLA_DIAS_SUCURSAL_SUPERVICION = "DIAS_SUCURSAL_SUPERVISOR";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_ID = "ID_DIA_SUPERVISOR";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_ID_SUCRUSAL_SUPERVICION = "ID_SUCURSAL_SUPERVISOR";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICIONID_DIA = "ID_DIA";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_ORDEN_VISITA = "ORDEN_VISITA";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_HORA_INICO = "HORA_INICIO";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_HORA_FIN = "HORA_FIN";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVISOR_BAN_PRIORIDAD = "BAN_PRIORIDAD";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVISOR_PRIORIDAD_SUGERIDA = "PRIORIDAD_SUGERIDA";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVISOR_PRIORIDAD = "PRIORIDAD";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_USUARIO = "USUARIO";
    public static final String COLUMNA_DIAS_SUCURSAL_SUPERVICION_ESTATUS = "ESTATUS";


    /* PRODUCTO ACTIVIDAD */
    public static final String TABLA_PRODUCTO_ACTIVIDAD = "PRODUCTO_ACTIVIDAD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_ID = "ID_PRODUCTO_ACTIVIDAD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_ID_ACTIVIDA = "ID_ACTIVIDAD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_ID_PRODUCTO = "ID_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_FECHA = "FECHA";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_LATITUD = "LATITUD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_LONGITUD = "LONGITUD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_TERMINADA = "TERMINADA";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_FECHA_TERMINO = "FECHA_TERMINO";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_COMENTARIO = "COMENTARIOS";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_USUARIO = "USUARIO";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_ESTATUS = "ESTATUS";


    /* PRODUCTO ACTIVIDAD SUCURSAL */
    public static final String TABLA_PRODUCTO_ACTIVIDAD_SUCURSAL = "PRODUCTO_ACTIVIDAD_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID = "ID_PRODUCTO_ACTIVIDAD_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID_ACTIVIDA = "ID_ACTIVIDAD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID_PRODUCTO = "ID_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_LATITUD = "LATITUD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_LONGITUD = "LONGITUD";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_OBLIGATORIA = "OBLIGATORIA";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_TERMINADA = "TERMINADA";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_USUARIO = "USUARIO";
    public static final String COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ESTATUS = "ESTATUS";

    /* ACTIVIDAD */
    public static final String TABLA_ACTIVIDAD = "ACTIVIDAD";
    public static final String COLUMNA_ACTIVIDAD_ID = "ID_ACTIVIDAD";
    public static final String COLUMNA_TIPO_ACTIVIDAD = "TIPO_ACTIVIDAD";
    public static final String COLUMNA_ACTIVIDAD_NOMBRE = "NOMBRE";
    public static final String COLUMNA_ACTIVIDAD_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_ACTIVIDAD_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_ACTIVIDAD_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_ACTIVIDAD_USUARIO = "USUARIO";
    public static final String COLUMNA_ACTIVIDAD_ESTATUS = "ESTATUS";

    /* CAT GUIA MERCADEO */
    public static final String TABLA_GUIA_MERCADEO_FALTANTE = "GUIA_MERCADEO_FALTANTE";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_ID = "ID_GUIA_MERCADEO_FALTANTE";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_ID_GUIA_MERCADEO = "ID_GUIA_MERCADEO";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_MOTIVO = "MOTIVO";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_USUARIO = "USUARIO";
    public static final String COLUMNA_CAT_GUIA_MERCADEO_ESTATUS = "ESTATUS";


    /* GUIA MERCADEO */
    public static final String TABLA_GUIA_MERCADEO = "GUIA_MERCADEO";
    public static final String COLUMNA_GUIA_MERCADEO_ID = "ID_GUIA_MERCADEO";
    public static final String COLUMNA_GUIA_MERCADEO_ID_CAT_GUIA_MERCADEO = "ID_CAT_GUIA_MERCADEO";
    public static final String COLUMNA_GUIA_MERCADEO_ID_CADENA = "ID_CADENA";
    public static final String COLUMNA_GUIA_MERCADEO_PROMOCION = "PROMOCION";
    public static final String COLUMNA_GUIA_MERCADEO_DESCRIPCION_PROMOCION = "DESCRIPCION_PRODUCTO";
    public static final String COLUMNA_GUIA_MERCADEO_VIGENCIA = "VIGENCIA";
    public static final String COLUMNA_GUIA_MERCADEO_PRECIO_OFERTA = "PRECIO_OFERTA";
    public static final String COLUMNA_GUIA_MERCADEO_REGION = "REGION";
    public static final String COLUMNA_GUIA_MERCADEO_CLAVE = "CLAVE";
    public static final String COLUMNA_GUIA_MERCADEO_MATERIAL_APOYO = "MATERIAL_APOYO";
    public static final String COLUMNA_GUIA_MERCADEO_OBSERVACIONES = "OBSERVACIONES";
    public static final String COLUMNA_GUIA_MERCADEO_RECOMENDACION_PRODUCTO = "RECOMENDACION_PRODUCTO";
    public static final String COLUMNA_GUIA_MERCADEO_RECOMENDACION_EXHIBICION = "RECOMENDACION_EXHIBICION";
    public static final String COLUMNA_GUIA_MERCADEO_FECHA_INICIO = "FECHA_INICIO";
    public static final String COLUMNA_GUIA_MERCADEO_FECHA_TERMINO = "FECHA_FIN";
    public static final String COLUMNA_GUIA_MERCADEO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_GUIA_MERCADEO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_GUIA_MERCADEO_USUARIO = "USUARIO";
    public static final String COLUMNA_GUIA_MERCADEO_ESTATUS = "ESTATUS";


    /* UDN GUIA MERCADEO */
    public static final String TABLA_UDN_GUIA_MERCADEO = "UDN_GUIA_MERCADEO";
    public static final String COLUMNA_UDN_GUIA_MERCADEO_ID_UDN = "UDN_ID_UDN";
    public static final String COLUMNA_UDN_GUIA_MERCADEO_ID_GUIA_MERCADEO = "ID_GUIA_MERCADEO";


    /* ACTIVIDAD CATEGORIA */
    public static final String TABLA_ACTIVIDAD_CATEGORIA = "ACTIVIDAD_CATEGORIA";
    public static final String COLUMNA_ACTIVIDAD_CATEGORIA_ID = "ID_ACTIVIDAD_CATEGORIA";
    public static final String COLUMNA_ACTIVIDAD_CATEGORIA_CATEGORIA = "CATEGORIA";
    public static final String COLUMNA_ACTIVIDAD_CATEGORIA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_ACTIVIDAD_CATEGORIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_ACTIVIDAD_CATEGORIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_ACTIVIDAD_CATEGORIA_USUARIO = "USUARIO";
    public static final String COLUMNA_ACTIVIDAD_CATEGORIA_ESTATUS = "ESTATUS";

    /* ACTIVIDAD COMPETENCIA */
    public static final String TABLA_ACTIVIDAD_COMPETENCIA = "ACTIVIDAD_COMPETENCIA";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_ID = "ID_ACTIVIDAD";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_ID_COMPANIA = "ID_COMPANIA";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_ID_ACTIVIDAD_CATEGORIA = "ID_ACTIVIDAD_CATEGORIA";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_PRODUCTO = "PRODUCTO";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_CLASIFICACION = "CLASIFICACION";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_INICIO = "FECHA_INICIO";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_FIN = "FECHA_FIN";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_PARTICIPANTES = "PARTICIPANTES";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_DESCRIPCION_COMPANIA = "DESCRIPCION_COMPANIA";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_USUARIO = "USUARIO";
    public static final String COLUMNA_ACTIVIDAD_COMPETENCIA_ESTATUS = "ESTATUS";


    /* DISPOSITIVO */
    public static final String TABLA_DISPOSITIVO = "DISPOSITIVO";
    public static final String COLUMNA_DISPOSIVITO_ID = "ID_DISPOSITIVO";
    public static final String COLUMNA_DISPOSIVITO_ID_USUARIO = "ID_USUARIO";
    public static final String COLUMNA_DISPOSIVITO_ID_UDN = "ID_UDN";
    public static final String COLUMNA_DISPOSIVITO_FECHA = "FECHA";
    public static final String COLUMNA_DISPOSIVITO_NO_SERIE = "NO_SERIE";
    public static final String COLUMNA_DISPOSIVITO_IMEI = "IMEI";
    public static final String COLUMNA_DISPOSIVITO_SIM = "SIM";
    public static final String COLUMNA_DISPOSIVITO_MODELO = "MODELO";
    public static final String COLUMNA_DISPOSIVITO_ANDROID = "ANDROID";
    public static final String COLUMNA_DISPOSIVITO_VERSION = "VERSION";
    public static final String COLUMNA_DISPOSIVITO_VERSION_BD = "VERSION_BD";
    public static final String COLUMNA_DISPOSIVITO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_DISPOSIVITO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_DISPOSIVITO_USUARIO = "USUARIO";
    public static final String COLUMNA_DISPOSIVITO_ESTATUS = "ESTATUS";


    /* DISPOSITIVO EVENTUALIDAD */
    public static final String TABLA_DISPOSITIVO_EVENTUALIDAD = "DISPOSITIVO_EVENTUALIDAD";
    public static final String COLUMNA_DISPOSIVITO_ID_EVENTUALIDAD = "ID_EVENTUALIDAD";
    public static final String COLUMNA_DISPOSIVITO_ID_DISPOSITIVO = "ID_DISPOSITIVO";
    public static final String COLUMNA_DISPOSIVITO_EVENTUALIDAD_ID_USUARIO = "ID_USUARIO";
    public static final String COLUMNA_DISPOSIVITO_MODULO_EVENTUALIDAD = "MODULO_EVENTUALIDAD";
    public static final String COLUMNA_DISPOSIVITO_BATERIA = "BATERIA";
    public static final String COLUMNA_DISPOSIVITO_EVENTUALIDAD_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_DISPOSIVITO_EVENTUALIDAD_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_DISPOSIVITO_EVENTUALIDAD_USUARIO = "USUARIO";
    public static final String COLUMNA_DISPOSIVITO_EVENTUALIDAD_ESTATUS = "ESTATUS";


    /* REGISTRO APLICACION */
    public static final String TABLA_REGISTRO_APLICACION = "REGISTRO_APLICACION";
    public static final String COLUMNA_REGISTRO_APLICACION_ID = "ID_REGISTRO_APLICACION";
    public static final String COLUMNA_REGISTRO_APLICACION_NO_SUCURSAL = "NO_SUCURSAL ";
    public static final String COLUMNA_REGISTRO_APLICACION_CHECK_IN_ACTIVO = "CHECK_IN_ACTIVO"; // 1 SI 0 NO
    public static final String COLUMNA_REGISTRO_APLICACION_SINC_INICIAL_ACTIVO = "SINC_INICIAL_ACTIVO";  // 1 SI 0 NO


    /* CAT INVENTARIO */
    public static final String TABLA_CAT_INVENTARIO = "CAT_INVENTARIO";
    public static final String COLUMNA_CATEGORIA_INVENTARIO_ID = "ID_CATEGORIA_INVENTARIO";
    public static final String COLUMNA_INVENTARIO_CATEGORIA = "CAT_INVENTARIO";
    public static final String COLUMNA_CATEGORIA_INVENTARIO_DESCRIPCION = "DESCRIPCION";
    public static final String COLUMNA_CATEGORIA_INVENTARIO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_CATEGORIA_INVENTARIO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_CATEGORIA_INVENTARIO_USUARIO = "USUARIO";
    public static final String COLUMNA_CATEGORIA_INVENTARIO_ESTATUS = "ESTATUS";


    /* PRODUCTO INVENTARIO */
    public static final String TABLA_PRODUCTO_INVENTARIO = "PRODUCTO_INVENTARIO";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_ID = "ID_PRODUCTO_INVENTARIO";
    public static final String COLUMNA_PRODUCTO_CATEGORIA_ID = "ID_CATEGORIA_INVENTARIO";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_ID_SUCURSAL = "ID_SUCURSAL";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_ID_PRODUCTO = "ID_PRODUCTO";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_CANTIDAD = "CANTIDAD";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_FECHA = "FECHA";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_FECHA_CREACION = "FECHA_CREACION";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_USUARIO = "USUARIO";
    public static final String COLUMNA_PRODUCTO_INVENTARIO_ESTATUS = "ESTATUS";


    public static final String COLUMNA_ESTATUS_DOS = "ESTATUS2";


    private static final String UDN = "create table " + TABLA_UDN +
            "(" + COLUMNA_UDN_ID + " integer primary key, " + COLUMNA_UDN_CLAVE + " text, " + COLUMNA_UDN_ALIAS + " text, " + COLUMNA_UDN_LATITUD + " double, " + COLUMNA_UDN_LONGITUD + " double, " + COLUMNA_UDN_DIRECCION + " text, " +
            COLUMNA_UDN_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_UDN_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_UDN_USUARIO + " text, " + COLUMNA_UDN_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String CANALES = "create table " + TABLA_CANALES +
            "(" + COLUMNA_CANALES_ID_CANAL + " integer primary key, " + COLUMNA_CANALES_CLAVE + " text, " + COLUMNA_CANALES_NOMBRE + " text, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String USUARIO = "create table " + TABLA_USUARIO +
            "(" + COLUMNA_USUARIO_ID + " integer primary key, " + COLUMNA_USUARIO_UDN + " integer, " + COLUMNA_USUARIO_ID_JEFE + " integer, " + COLUMNA_USUARIO_ID_CANAL + " integer, " + COLUMNA_USUARIO_NOMBRE + " text, " +
            COLUMNA_USUARIO_EMAIL + " text, " + COLUMNA_USUARIO_USERNAME + " text, " + COLUMNA_USUARIO_PASSWORD + " text, " + COLUMNA_USUARIO_ROL + " text, " + COLUMNA_USUARIO_CAMBIO_PASSWORD + " integer, " + COLUMNA_USUARIO_CAMBIO_LOCALIZACION + " integer, " +
            COLUMNA_USUARIO_HASH_RESET + " text, " + COLUMNA_USUARIO_INTENTOS + " integer, " + COLUMNA_USUARIO_FECHA_ERROR + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_USUARIO_NIVEL + " integer, " + COLUMNA_USUARIO_FECHA_PASSWORD + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_USUARIO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_USUARIO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_USUARIO + " text, " + COLUMNA_USUARIO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " + COLUMNA_USUARIO_TOKEN + " text, " +
            " FOREIGN KEY (" + COLUMNA_USUARIO_ID + ") REFERENCES " + TABLA_UDN + "(" + COLUMNA_UDN_ID + "));";

    private static final String LOCALIZACION_USUARIO = "create table " + TABLA_LOCALIZACION_USUARIO +
            "(" + COLUMNA_LOCALIZACION_USUARIO_ID + " integer primary key, " + COLUMNA_LOCALIZACION_USUARIO_ID_USUARIO + " integer, " + COLUMNA_LOCALIZACION_USUARIO_LATITUD + " double, " + COLUMNA_LOCALIZACION_USUARIO_LONGITUD + " double, " +
            COLUMNA_LOCALIZACION_USUARIO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_LOCALIZACION_USUARIO_USUARIO + " text, " + COLUMNA_LOCALIZACION_USUARIO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_LOCALIZACION_USUARIO_ID_USUARIO + ") REFERENCES " + TABLA_USUARIO + "(" + COLUMNA_USUARIO_ID + "));";


    private static final String PROD_FAMILIA = "create table " + TABLA_PROD_FAMILIA +
            "(" + COLUMNA_PROD_FAMILIA_ID + " integer primary key, " + COLUMNA_PROD_FAMILIA + " text, " + COLUMNA_PROD_FAMILIA_ORDENAMIENTO + " integer, " + COLUMNA_PROD_FAMILIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PROD_FAMILIA_ULRIMO_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_PROD_FAMILIA_USUARIO + " text, " + COLUMNA_PROD_FAMILIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String PROD_PRESENTACION = "create table " + TABLA_PROD_PRESENTACION +
            "(" + COLUMNA_PROD_PRESENTACION_ID + " integer primary key, " + COLUMNA_PROD_PRESENTACION + " text, " + COLUMNA_PROD_PRESENTACION_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PROD_PRESENTACION_ULRIMO_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_PROD_PRESENTACION_USUARIO + " text, " + COLUMNA_PROD_PRESENTACION_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String PRODUCTO_SUBCATEGORIA = "create table " + TABLA_PRODUCTO_SUBCATEGORIA +
            "(" + COLUMNA_PRODUCTO_SUBCATEGORIA_ID + " integer primary key, " + COLUMNA_PRODUCTO_SUBCATEGORIA_SUBCATEGORIA + " text, " +
            COLUMNA_PRODUCTO_SUBCATEGORIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_SUBCATEGORIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_PRODUCTO_SUBCATEGORIA_USUARIO + " text, " + COLUMNA_PRODUCTO_SUBCATEGORIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String PRODUCTO = "create table " + TABLA_PRODUCTO +
            "(" + COLUMNA_PRODUCTO_ID + " integer primary key, " + COLUMNA_PRODUCTO_ID_PRESENTACION + " integer, " + COLUMNA_PRODUCTO_ID_FAMILIA + " integer, " + COLUMNA_PRODUCTO_AGRUPACION + " integer, " + COLUMNA_PRODUCTO_ORDENAMIENTO + " integer, " + COLUMNA_PRODUCTO_CLAVE + " text, " + COLUMNA_PRODUCTO_UPC + " text, " +
            COLUMNA_PRODUCTO_SUBCATEGORIA + " integer, " + COLUMNA_PRODUCTO_TOP + " integer, " + COLUMNA_PRODUCTO_ID_COMPANIA + " integer, " +
            COLUMNA_PRODUCTO_UNIDAD + " text, " + COLUMNA_PRODUCTO_NOMBRE + " text, " + COLUMNA_PRODUCTO_DESCRIPCION + " text, " + COLUMNA_PRODUCTO_PRECIO_UNITARIO + " double, " + COLUMNA_PRODUCTO_APLICA_IVA + " integer, " +
            COLUMNA_PRODUCTO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_USUARIO + " text, " + COLUMNA_PRODUCTO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " + COLUMNA_PRODUCTO_TIPO_PRODUCTO + " integer, " + COLUMNA_PRODUCTO_EMPAQUE + " integer, " +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ID_PRESENTACION + ") REFERENCES " + TABLA_PROD_PRESENTACION + "(" + COLUMNA_PROD_PRESENTACION_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ID_FAMILIA + ") REFERENCES " + TABLA_PROD_FAMILIA + "(" + COLUMNA_PROD_FAMILIA_ID + "));";


    private static final String CATEGORIA_INCIDENCIA = "create table " + TABLA_CATEGORIA_INCIDENCIA +
            "(" + COLUMNA_CATEGORIA_INCIDENCIA_ID + " integer primary key, " + COLUMNA_CATEGORIA_INCIDENCIA_CATEGORIA + " text, " + COLUMNA_CATEGORIA_INCIDENCIA_DESCRIPCION + " text, " + COLUMNA_CATEGORIA_INCIDENCIA_TIPO_CATEGORIA + " integer, " + COLUMNA_CATEGORIA_INCIDENCIA_TIPO_INCIDENCIA + " integer, " + COLUMNA_CATEGORIA_INCIDENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CATEGORIA_INCIDENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_CATEGORIA_INCIDENCIA_USUARIO + " text, " + COLUMNA_CATEGORIA_INCIDENCIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String PRODUCTO_INCIDENCIA = "create table " + TABLA_PRODUCTO_INCIDENCIA +
            "(" + COLUMNA_PRODUCTO_INCIDENCIA_ID + " integer primary key, " + COLUMNA_PRODUCTO_INCIDENCIA_ID_CATEGORIA + " integer, " + COLUMNA_PRODUCTO_INCIDENCIA_ID_SUCURSAL + " integer, " + COLUMNA_PRODUCTO_INCIDENCIA_ID_PRODUCTO + " integer, " +
            COLUMNA_PRODUCTO_INCIDENCIA_DESCRIPCION + " text, " + COLUMNA_PRODUCTO_INCIDENCIA_BAN_MEDICICON + " integer, " + COLUMNA_PRODUCTO_INCIDENCIA_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_PRODUCTO_INCIDENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_INCIDENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_INCIDENCIA_USUARIO + " text, " + COLUMNA_PRODUCTO_INCIDENCIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_INCIDENCIA_ID_CATEGORIA + ") REFERENCES " + TABLA_CATEGORIA_INCIDENCIA + "(" + COLUMNA_CATEGORIA_INCIDENCIA_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_INCIDENCIA_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_INCIDENCIA_ID_PRODUCTO + ") REFERENCES " + TABLA_PRODUCTO + "(" + COLUMNA_PRODUCTO_ID + "));";


    private static final String COMPANIA = "create table " + TABLA_COMPANIA +
            "(" + COLUMNA_COMPANIA_ID + " integer primary key, " + COLUMNA_COMPANIA_NOMBRE + " text, " + COLUMNA_COMPANIA_PRIORIDAD + " integer, " + COLUMNA_COMPANIA_TIPO_COMPANIA + " integer, " + COLUMNA_COMPANIA_TIPO_USUARIO_COMPANIA + " integer, " +
            COLUMNA_COMPANIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_COMPANIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_COMPANIA_USUARIO + " text, " + COLUMNA_COMPANIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String CLIENTE = "create table " + TABLA_CLIENTE +
            "(" + COLUMNA_CLIENTE_ID + " integer primary key, " + COLUMNA_CLIENTE_ALIAS + " text, " + COLUMNA_CLIENTE_ALIAS2 + " text, " +
            COLUMNA_CLIENTE_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CLIENTE_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CLIENTE_USUARIO + " text, " + COLUMNA_CLIENTE_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String CADENA = "create table " + TABLA_CADENA +
            "(" + COLUMNA_CADENA_ID + " integer primary key, " + COLUMNA_CADENAID_CLIENTE + " integer, " + COLUMNA_CADENA_RFC + " text, " + COLUMNA_CADENA_RAZON_SOCIAL + " text, " + COLUMNA_CADENA_ALIAS + " text ," + COLUMNA_CADENA_ALIAS2 + " text, " +
            COLUMNA_CADENA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CADENA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CADENA_USUARIO + " text, " + COLUMNA_CADENA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_CADENAID_CLIENTE + ") REFERENCES " + TABLA_CLIENTE + "(" + COLUMNA_CLIENTE_ID + "));";


    private static final String CLIENTE_SUCURSAL = "create table " + TABLA_SUCURSAL +
            "(" + COLUMNA_SUCRUSAL_ID + " integer primary key, " + COLUMNA_SUCURSAL_CADENA + " integer, " + COLUMNA_SUCURSAL_ID_UDN + " integer, " + COLUMNA_SUCURSAL_CLAVE + " text, " + COLUMNA_SUCURSAL_ALIAS + " text, " +
            COLUMNA_SUCURSAL_CALLE + " text, " + COLUMNA_SUCURSAL_NO_EXT + " text, " + COLUMNA_SUCURSAL_NO_INT + " text, " +
            COLUMNA_SUCURSAL_COLONIA + " text, " + COLUMNA_SUCURSAL_CP + " text, " + COLUMNA_SUCURSAL_MUNICIPIO + " text, " + COLUMNA_SUCURSAL_ESTADO + " text, " + COLUMNA_SUCURSAL_LATITUD + " double, " + COLUMNA_SUCURSAL_LONGITUD + " double, " + COLUMNA_SUCURSAL_LOCALIZACION_ACTIVA + " integer, " + COLUMNA_SUCRSAL_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_USUARIO + " text, " + COLUMNA_SUCURSAL_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_CADENA + ") REFERENCES " + TABLA_CADENA + "(" + COLUMNA_CADENA_ID + "));";

    private static final String PRODUCTO_CATALOGADO = "create table " + TABLA_PRODUCTO_CATALOGADO +
            "(" + COLUMNA_PRODUCTO_CATALOGADO_ID + " integer primary key, " + COLUMNA_PRODUCTO_CATALOGADO_ID_SUCURSAL + " integer, " + COLUMNA_PRODUCTO_CATALOGADO_ID_PRODUCTO + " integer, " +
            COLUMNA_PRODUCTO_CATALOGADO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_CATALOGADO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_CATALOGADO_USUARIO + " text, " + COLUMNA_PRODUCTO_CATALOGADO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_CATALOGADO_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_CATALOGADO_ID_PRODUCTO + ") REFERENCES " + TABLA_PRODUCTO + "(" + COLUMNA_PRODUCTO_ID + "));";


    private static final String TIPO_EXISTENCIA = "create table " + TABLA_TIPO_EXISTENCIA +
            "(" + COLUMNA_TIPO_EXISTENCIA_ID + " integer primary key, " + COLUMNA_TIPO_EXISTENCIA_NOMBRE + " text, " + COLUMNA_TIPO_EXISTENCIA_DESCRIPCION + " text, " +
            COLUMNA_TIPO_EXISTENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TIPO_EXISTENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TIPO_EXISTENCIA_USUARIO + " text, " + COLUMNA_TIPO_EXISTENCIA_ESTATUS + " integer," + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String SUCURSAL_PRODUCTO_EXISTENCIA = "create table " + TABLA_SUCURSAL_PRODUCTO_EXISTENCIA +
            "(" + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID + " integer primary key, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_SUCURSAL + " integer, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_PRODUCTO + " integer, " +
            COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_EXHIBIDO + " integer," + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_CANTIDAD + " integer, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_TIPO_EXISTENCIA + " integer, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_COMENTARIOS + " text, " +
            COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_USUARIO + " text, " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_PRODUCTO + ") REFERENCES " + TABLA_PRODUCTO + "(" + COLUMNA_PRODUCTO_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_TIPO_EXISTENCIA + ") REFERENCES " + TABLA_TIPO_EXISTENCIA + "(" + COLUMNA_TIPO_EXISTENCIA_ID + "));";

    private static final String VISITAS = "create table " + TABLA_VISITA +
            "(" + COLUMNA_VISITA_ID + " integer primary key, " + COLUMNA_VISITAID_SUCURSAL + " integer, " + COLUMNA_VISITAID_USUARIO + " integer, " +
            COLUMNA_VISITA_HORA_CHECK_INICIO + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_VISITA_HORA_CHECK_FIN + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_VISITA_LATITUD_CHECKIN + " float, " +
            COLUMNA_VISITA_LONGITUD_CHECKIN + " float, " + COLUMNA_VISITA_LATITUD_CHECKOUT + " float, " + COLUMNA_VISITA_LONGITUD_CHECKOUT + " float, " + COLUMNA_VISITA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_VISITA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_VISITA_USUARIO + " text, " + COLUMNA_VISITA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_VISITAID_USUARIO + ") REFERENCES " + TABLA_USUARIO + "(" + COLUMNA_USUARIO_ID + "));";

    private static final String TIPO_ALERTA = "create table " + TABLA_TIPO_ALERTA +
            "(" + COLUMNA_TIPO_ALERTA_ID + " integer primary key, " + COLUMNA_TIPO_ALERTA_NOMBRE + " text, " + COLUMNA_TIPO_ALERTA_DESCRIPCION + " text, " +
            COLUMNA_TIPO_ALERTA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TIPO_ALERTA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TIPO_ALERTA_USUARIO + " text, " + COLUMNA_TIPO_ALERTA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String ALERTA = "create table " + TABLA_VISITA_ALERTA +
            "(" + COLUMNA_VISITA_ALERTA_ID + " integer primary key, " + COLUMNA_VISITA_ALERTAID_SUCURSAL + " integer, " + COLUMNA_VISITA_ALERTAID_TIPO_ALERTA + " integer, " + COLUMNA_VISITA_ALERTA_DESCRIPCION + " text, " + COLUMNA_VISITA_ALERTA_LEIDA + " text, " +
            COLUMNA_VISITA_ALERTA_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_VISITA_ALERTA_PENDIENTE + " integer, " +
            COLUMNA_VISITA_ALERTA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_VISITA_ALERTA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_VISITA_ALERTA_USUARIO + " text, " + COLUMNA_VISITA_ALERTA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_VISITA_ALERTAID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_VISITA_ALERTAID_TIPO_ALERTA + ") REFERENCES " + TABLA_TIPO_ALERTA + "(" + COLUMNA_TIPO_ALERTA_ID + "));";

    private static final String PRODUCTO_PRECIO = "create table " + TABLA_PRODUCTO_PRECIO +
            "(" + COLUMNA_PRODUCTO_PRECIO_ID + " integer primary key, " + COLUMNA_PRODUCTO_PRECIOID_PRODUCTO + " integer, " + COLUMNA_PRODUCTO_PRECIOID_SUCURSAL + " integer, " + COLUMNA_PRODUCTO_PRECIOID_COMPANIA + " integer, " +
            COLUMNA_PRODUCTO_PRECIO_PRECIO_UNITARIO + " double, " + COLUMNA_PRODUCTO_PRECIO_PRECIO_PROMOCION + " integer, " + COLUMNA_PRODUCTO_PRECIO_APLICA_IVA + " integer, " + COLUMNA_PRODUCTO_PRECIO_DESCRIPCION_COMPANIA + " text, " + COLUMNA_SUCURSAL_PRODUCTO_PRECIO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_PRODUCTO_PRECIO_USUARIO + " text, " + COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_PRECIOID_PRODUCTO + ") REFERENCES " + TABLA_PRODUCTO + "(" + COLUMNA_PRODUCTO_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_PRECIOID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_PRECIOID_COMPANIA + ") REFERENCES " + TABLA_COMPANIA + "(" + COLUMNA_COMPANIA_ID + "));";

    private static final String ESPACIOS = "create table " + TABLA_ESPACIOS +
            "(" + COLUMNA_ESPACIOS_ID + " integer primary key, " + COLUMNA_ESPACIOS_NOMBRE + " text, " + COLUMNA_ESPACIOS_DESCRIPCION + " text, " +
            COLUMNA_ESPACIOS_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ESPACIOS_TIPO_ESPACIO + " integer, " + COLUMNA_ESPACIOS_TIPO_LLENADO + " text, " + COLUMNA_ESPACIOS_TIPO_FRENTE + " text, " + COLUMNA_ESPACIOS_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ESPACIOS_USUARIO + " text, " + COLUMNA_ESPACIOS_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String DEPARTAMENTOS = "create table " + TABLA_DEPARTAMENTOS +
            "(" + COLUMNA_DEPARTAMENTOS_ID + " integer primary key, " + COLUMNA_DEPARTAMENTOS_NOMBRE + " text, " + COLUMNA_DEPARTAMENTOS_DESCRIPCION + " text, " +
            COLUMNA_DEPARTAMENTOS_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DEPARTAMENTOS_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DEPARTAMENTOS_USUARIO + " text, " + COLUMNA_DEPARTAMENTOS_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String FOTOS_EVIDENCIA = "create table " + TABLA_FOTOS_EVIDENCIA +
            "(" + COLUMNA_FOTOS_EVIDENCIA_ID + " integer primary key, " + COLUMNA_FOTOS_EVIDENCIAID_SUCURSAL + " integer, " + COLUMNA_FOTOS_EVIDENCIA_ARCHIVO + " text, " + COLUMNA_FOTOS_EVIDENCIA_FILE_NAME + " text, " +
            COLUMNA_FOTOS_EVIDENCIA_LATITUD + " double, " + COLUMNA_FOTOS_EVIDENCIA_LONGITUD + " double, " +
            COLUMNA_FOTOS_EVIDENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_FOTOS_EVIDENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_FOTOS_EVIDENCIA_USUARIO + " text, " + COLUMNA_FOTOS_EVIDENCIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_FOTOS_EVIDENCIAID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + "));";


    private static final String FOTOS_EVIDENCIA_RELACION = "create table " + TABLA_FOTOS_EVIDENCIA_RELACION +
            "(" + COLUMNA_FOTOS_EVIDENCIA_RELACION_ID_RELACION + " integer primary key, " + COLUMNA_FOTOS_EVIDENCIA_RELACION_ID_FOTO + " integer, " + COLUMNA_FOTOS_EVIDENCIA_RELACION_TABLA + " text, " + COLUMNA_FOTOS_EVIDENCIA_RELACION_ID + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_FOTOS_EVIDENCIA_RELACION_ID_FOTO + ") REFERENCES " + TABLA_FOTOS_EVIDENCIA + "(" + COLUMNA_FOTOS_EVIDENCIA_ID + "));";


    private static final String SUCURSAL_ESPACIOS = "create table " + TABLA_SUCURSAL_ESPACIOS +
            "(" + COLUMNA_SUCURSAL_ESPACIOS_ID + " integer primary key, " + COLUMNA_SUCURSAL_ESPACIOSID_SUCURSAL + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_ID_ESPACIO + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_ID_DEPARTAMENTOS + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_ID_CATEGORIA + " integer, " +
            COLUMNA_SUCURSAL_ESPACIOS_TIPO_ADQUISICION + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_NIVEL + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_PUERTAS + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_TRAMOS + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_TARIMAS + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_PLANOGRAMA_RUTA + " text, " + COLUMNA_SUCURSAL_ESPACIOS_FECHA_ASIGNACION + " DATETIME DEFAULT CURRENT_TIMESTAMP," + COLUMNA_SUCURSAL_ESPACIOS_FECHA_ELIMINACION + " DATETIME DEFAULT CURRENT_TIMESTAMP," + COLUMNA_SUCURSAL_ESPACIOS_MOTIVO_ELIMINACION + " text," +
            COLUMNA_SUCURSAL_ESPACIOS_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_ESPACIOS_USUARIO + " text, " + COLUMNA_SUCURSAL_ESPACIOS_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_ESPACIOSID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_ESPACIOS_ID_ESPACIO + ") REFERENCES " + TABLA_ESPACIOS + "(" + COLUMNA_ESPACIOS_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_ESPACIOS_ID_DEPARTAMENTOS + ") REFERENCES " + TABLA_DEPARTAMENTOS + "(" + COLUMNA_DEPARTAMENTOS_ID + "));";


    private static final String SUCURSAL_ESPACIOS_EXISTENCIA = "create table " + TABLA_SUCURSAL_ESPACIO_EXISTENCIA +
            "(" + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID + " integer primary key, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_SUCURSAL + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID_SUCURSAL_ESPACIO + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID_TIPO_EXISTENCIA + " integer, " +
            COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_EXHIBIDO + " integer, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_COMENTARIOS + " text, " +
            COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_USUARIO + " text, " + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID_SUCURSAL_ESPACIO + ") REFERENCES " + TABLA_SUCURSAL_ESPACIOS + "(" + COLUMNA_SUCURSAL_ESPACIOS_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_ESPACIOS_EXISTENCIA_ID_TIPO_EXISTENCIA + ") REFERENCES " + TABLA_TIPO_EXISTENCIA + "(" + COLUMNA_TIPO_EXISTENCIA_ID + "));";


    private static final String ESPACIO_FRENTE = "create table " + TABLA_ESPACIO_FRENTE +
            "(" + COLUMNA_ESPACIO_FRENTE_ID + " integer primary key, " + COLUMNA_ESPACIO_FRENTE_ID_SUCURSAL_ESPACIO + " integer, " + COLUMNA_ESPACIO_FRENTE_ID_COMPANIA + " integer, " + COLUMNA_ESPACIO_FRENTE_ID_AGRUPACION + " integer, " + COLUMNA_ESPACIO_FRENTE_ID_PRESENTACION + " integer, " + COLUMNA_ESPACIO_FRENTE_FRENTES + " integer, " + COLUMNA_ESPACIO_FRENTE_DESCRIPCION_COMPANIA + " text, " +
            COLUMNA_ESPACIO_FRENTE_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ESPACIO_FRENTE_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ESPACIO_FRENTE_USUARIO + " text, " + COLUMNA_ESPACIO_FRENTE_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_ESPACIO_FRENTE_ID_SUCURSAL_ESPACIO + ") REFERENCES " + TABLA_SUCURSAL_ESPACIOS + "(" + COLUMNA_SUCURSAL_ESPACIOS_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_ESPACIO_FRENTE_ID_COMPANIA + ") REFERENCES " + TABLA_COMPANIA + "(" + COLUMNA_COMPANIA_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_ESPACIO_FRENTE_ID_PRESENTACION + ") REFERENCES " + TABLA_PROD_PRESENTACION + "(" + COLUMNA_PROD_PRESENTACION_ID + "));";


    private static final String ESPACIO_INCIDENCIA = "create table " + TABLA_ESPACIO_INCIDENCIA +
            "(" + COLUMNA_ESPACIO_INCIDENCIA_ID + " integer primary key, " + COLUMNA_ESPACIO_INCIDENCIA_ID_CATEGORIA + " integer, " + COLUMNA_ESPACIO_INCIDENCIA_ID_SUCURSAL_ESPACIO + " integer, " + COLUMNA_ESPACIO_INCIDENCIA_ID_SUCURSAL + " integer, " + COLUMNA_ESPACIO_INCIDENCIA_DESCRIPCION + " text, " +
            COLUMNA_ESPACIO_INCIDENCIA_BAN_MEDICION + " integer, " + COLUMNA_ESPACIO_INCIDENCIA_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_ESPACIO_INCIDENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ESPACIO_INCIDENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ESPACIO_INCIDENCIA_USUARIO + " text, " + COLUMNA_ESPACIO_INCIDENCIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_ESPACIO_INCIDENCIA_ID_CATEGORIA + ") REFERENCES " + TABLA_CATEGORIA_INCIDENCIA + "(" + COLUMNA_CATEGORIA_INCIDENCIA_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_ESPACIO_INCIDENCIA_ID_SUCURSAL_ESPACIO + ") REFERENCES " + TABLA_SUCURSAL_ESPACIOS + "(" + COLUMNA_SUCURSAL_ESPACIOS_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_ESPACIO_INCIDENCIA_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + "));";

    private static final String PUESTO = "create table " + TABLA_PUESTO +
            "(" + COLUMNA_PUESTO_ID + " integer primary key, " + COLUMNA_PUESTO_NOMBRE + " text, " + COLUMNA_PUESTO_DESCRIPCION + " text, " +
            COLUMNA_PUESTO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PUESTO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PUESTO_USUARIO + " text, " + COLUMNA_PUESTO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String TIPO_TELEFONO = "create table " + TABLA_TIPO_TELEFONO +
            "(" + COLUMNA_TIPO_TELEFONO_ID + " integer primary key, " + COLUMNA_TIPO_TELEFONO_NOMBRE + " text, " + COLUMNA_TIPO_TELEFONO_DESCRIPCION + " text, " +
            COLUMNA_TIPO_TELEFONO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TIPO_TELEFONO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TIPO_TELEFONO_USUARIO + " text, " + COLUMNA_TIPO_TELEFONO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String PERSONAL = "create table " + TABLA_PERSONAL +
            "(" + COLUMNA_PERONAL_ID + " integer primary key, " + COLUMNA_PERONALID_PUESTO + " integer, " + COLUMNA_PERONAL_CLAVE + " integer, " + COLUMNA_PERONAL_NOMBRE + " text, " + COLUMNA_PERONAL_AP_PATERNO + " text, " +
            COLUMNA_PERONAL_AP_MATERNO + " text, " + COLUMNA_PERONAL_EMAIL + " text, " + COLUMNA_PERONAL_FECHA_CONTRATACION + " text, " +
            COLUMNA_PERONAL_FECHA_BAJA + " text, " + COLUMNA_PERONAL_FECHA_COMENTARIOS + " text, " + COLUMNA_PERONAL_PRECIO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PERONAL_PRECIO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PERONAL_PRECIO_USUARIO + " text, " + COLUMNA_PERONAL_PRECIO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_PERONALID_PUESTO + ") REFERENCES " + TABLA_PUESTO + "(" + COLUMNA_PUESTO_ID + "));";


    private static final String TELEFONO = "create table " + TABLA_TELEFONO +
            "(" + COLUMNA_TELEFONO_ID + " integer primary key, " + COLUMNA_TELEFONOID_PERSONAL + " integer, " + COLUMNA_TELEFONO_ID_TIPO_TELEFONO + " integer, " + COLUMNA_TELEFONO_TELEFONO + " text, " +
            COLUMNA_TELEFONO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TELEFONO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_TELEFONO_USUARIO + " text, " + COLUMNA_TELEFONO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_TELEFONOID_PERSONAL + ") REFERENCES " + TABLA_PERSONAL + "(" + COLUMNA_PERONAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_TELEFONO_ID_TIPO_TELEFONO + ") REFERENCES " + TABLA_TIPO_TELEFONO + "(" + COLUMNA_TIPO_TELEFONO_ID + "));";

    private static final String DIAS = "create table " + TABLA_DIAS +
            "(" + COLUMNA_DIAS_ID + " integer primary key, " + COLUMNA_DIAS_NOMBRE + " text, " + COLUMNA_DIAS_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String SUCURSAL_PERSONAL = "create table " + TABLA_SUCURSAL_PERSONAL +
            "(" + COLUMNA_SUCURSAL_PERSONAL_ID + " integer primary key, " + COLUMNA_SUCURSAL_PERSONALID_PERSONAL + " integer, " + COLUMNA_SUCURSAL_PERSONALID_SUCURSAL + " integer, " +
            COLUMNA_SUCURSAL_PERSONAL_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_PERSONAL_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_PERSONAL_USUARIO + " text, " + COLUMNA_SUCURSAL_PERSONAL_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_PERSONALID_PERSONAL + ") REFERENCES " + TABLA_PERSONAL + "(" + COLUMNA_PERONAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_PERSONALID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + "));";


    private static final String SUCURSAL_SUPERVISOR = "create table " + TABLA_SUCURSAL_SUPERVISOR +
            "(" + COLUMNA_SUCURSAL_SUPERVISOR_ID + " integer primary key, " + COLUMNA_SUCURSAL_SUPERVISOR_ID_USUARIO + " integer, " + COLUMNA_SUCURSAL_SUPERVISOR_ID_SUCURSAL + " integer, " +
            COLUMNA_SUCURSAL_SUPERVISOR_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_SUPERVISOR_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_SUCURSAL_SUPERVISOR_USUARIO + " text, " + COLUMNA_SUCURSAL_SUPERVISOR_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_SUPERVISOR_ID_USUARIO + ") REFERENCES " + TABLA_USUARIO + "(" + COLUMNA_USUARIO_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_SUCURSAL_SUPERVISOR_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + "));";


    private static final String DIAS_SUCURSAL_PERSONAL = "create table " + TABLA_DIAS_SUCURSAL_PERSONAL +
            "(" + COLUMNA_DIAS_SUCURSAL_PERSONAL_ID + " integer primary key, " + COLUMNA_DIAS_SUCURSAL_PERSONAL_ID_SUCRUSAL_PERSONAL + " integer, " + COLUMNA_DIAS_SUCURSAL_PERSONALID_DIA + " integer, " +
            COLUMNA_DIAS_SUCURSAL_PERSONAL_HORA_INICO + " text, " + COLUMNA_DIAS_SUCURSAL_PERSONAL_HORA_FIN + " text, " +
            COLUMNA_DIAS_SUCURSAL_PERSONAL_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DIAS_SUCURSAL_PERSONAL_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DIAS_SUCURSAL_PERSONAL_USUARIO + " text, " + COLUMNA_DIAS_SUCURSAL_PERSONAL_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_DIAS_SUCURSAL_PERSONAL_ID_SUCRUSAL_PERSONAL + ") REFERENCES " + TABLA_PERSONAL + "(" + COLUMNA_PERONAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_DIAS_SUCURSAL_PERSONALID_DIA + ") REFERENCES " + TABLA_DIAS + "(" + COLUMNA_DIAS_ID + "));";


    private static final String DIAS_SUCURSAL_SUPERVICION = "create table " + TABLA_DIAS_SUCURSAL_SUPERVICION +
            "(" + COLUMNA_DIAS_SUCURSAL_SUPERVICION_ID + " integer primary key, " + COLUMNA_DIAS_SUCURSAL_SUPERVICION_ID_SUCRUSAL_SUPERVICION + " integer, " + COLUMNA_DIAS_SUCURSAL_SUPERVICIONID_DIA + " integer, " +
            COLUMNA_DIAS_SUCURSAL_SUPERVICION_ORDEN_VISITA + " text, " + COLUMNA_DIAS_SUCURSAL_SUPERVICION_HORA_INICO + " text, " + COLUMNA_DIAS_SUCURSAL_SUPERVICION_HORA_FIN + " text, " +
            COLUMNA_DIAS_SUCURSAL_SUPERVISOR_BAN_PRIORIDAD + " boolean, " + COLUMNA_DIAS_SUCURSAL_SUPERVISOR_PRIORIDAD_SUGERIDA + " integer, " + COLUMNA_DIAS_SUCURSAL_SUPERVISOR_PRIORIDAD + " integer, " +
            COLUMNA_DIAS_SUCURSAL_SUPERVICION_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DIAS_SUCURSAL_SUPERVICION_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DIAS_SUCURSAL_SUPERVICION_USUARIO + " text, " + COLUMNA_DIAS_SUCURSAL_SUPERVICION_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_DIAS_SUCURSAL_SUPERVICION_ID_SUCRUSAL_SUPERVICION + ") REFERENCES " + TABLA_SUCURSAL_SUPERVISOR + "(" + COLUMNA_DIAS_SUCURSAL_SUPERVICION_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_DIAS_SUCURSAL_PERSONALID_DIA + ") REFERENCES " + TABLA_DIAS + "(" + COLUMNA_DIAS_ID + "));";


    private static final String ACTIVIDAD = "create table " + TABLA_ACTIVIDAD +
            "(" + COLUMNA_ACTIVIDAD_ID + " integer primary key, " + COLUMNA_TIPO_ACTIVIDAD + " integer, " + COLUMNA_ACTIVIDAD_NOMBRE + " text, " + COLUMNA_ACTIVIDAD_DESCRIPCION + " text, " +
            COLUMNA_ACTIVIDAD_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_USUARIO + " text, " + COLUMNA_ACTIVIDAD_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";


    private static final String PRODUCTO_ACTIVIDAD = "create table " + TABLA_PRODUCTO_ACTIVIDAD +
            "(" + COLUMNA_PRODUCTO_ACTIVIDAD_ID + " integer primary key, " + COLUMNA_PRODUCTO_ACTIVIDAD_ID_ACTIVIDA + " integer, " + COLUMNA_PRODUCTO_ACTIVIDAD_ID_PRODUCTO + " integer, " + COLUMNA_PRODUCTO_ACTIVIDAD_ID_SUCURSAL + " integer, " + COLUMNA_PRODUCTO_ACTIVIDAD_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_PRODUCTO_ACTIVIDAD_LATITUD + " double, " + COLUMNA_PRODUCTO_ACTIVIDAD_LONGITUD + " double, " + COLUMNA_PRODUCTO_ACTIVIDAD_TERMINADA + " integer, " + COLUMNA_PRODUCTO_ACTIVIDAD_FECHA_TERMINO + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_ACTIVIDAD_COMENTARIO + " text, " +
            COLUMNA_PRODUCTO_ACTIVIDAD_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_ACTIVIDAD_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_ACTIVIDAD_USUARIO + " text, " + COLUMNA_PRODUCTO_ACTIVIDAD_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ACTIVIDAD_ID_ACTIVIDA + ") REFERENCES " + TABLA_ACTIVIDAD + "(" + COLUMNA_ACTIVIDAD_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ACTIVIDAD_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ACTIVIDAD_ID_PRODUCTO + ") REFERENCES " + TABLA_PRODUCTO + "(" + COLUMNA_PRODUCTO_ID + "));";


    private static final String PRODUCTO_ACTIVIDAD_SUCURSAL = "create table " + TABLA_PRODUCTO_ACTIVIDAD_SUCURSAL +
            "(" + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID + " integer primary key, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID_ACTIVIDA + " integer, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID_PRODUCTO + " integer, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ID_SUCURSAL + " integer, " +
            COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_LATITUD + " double, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_LONGITUD + " double, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_OBLIGATORIA + " integer, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_TERMINADA + " integer, " +
            COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_USUARIO + " text, " + COLUMNA_PRODUCTO_ACTIVIDAD_SUCURSAL_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ACTIVIDAD_ID_ACTIVIDA + ") REFERENCES " + TABLA_ACTIVIDAD + "(" + COLUMNA_ACTIVIDAD_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ACTIVIDAD_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_ACTIVIDAD_ID_PRODUCTO + ") REFERENCES " + TABLA_PRODUCTO + "(" + COLUMNA_PRODUCTO_ID + "));";

    private static final String GUIA_MERCADEO_FALTANTE = "create table " + TABLA_GUIA_MERCADEO_FALTANTE +
            "(" + COLUMNA_CAT_GUIA_MERCADEO_ID + " integer primary key, " + COLUMNA_CAT_GUIA_MERCADEO_ID_GUIA_MERCADEO + " integer, " + COLUMNA_CAT_GUIA_MERCADEO_ID_SUCURSAL + " integer, " + COLUMNA_CAT_GUIA_MERCADEO_MOTIVO + " text, " +
            COLUMNA_CAT_GUIA_MERCADEO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CAT_GUIA_MERCADEO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CAT_GUIA_MERCADEO_USUARIO + " text, " + COLUMNA_CAT_GUIA_MERCADEO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer);";

    private static final String GUIA_MERCADEO = "create table " + TABLA_GUIA_MERCADEO +
            "(" + COLUMNA_GUIA_MERCADEO_ID + " integer primary key, " + COLUMNA_GUIA_MERCADEO_ID_CAT_GUIA_MERCADEO + " integer, " + COLUMNA_GUIA_MERCADEO_ID_CADENA + " integer, " + COLUMNA_GUIA_MERCADEO_PROMOCION + " text, " + COLUMNA_GUIA_MERCADEO_DESCRIPCION_PROMOCION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_GUIA_MERCADEO_MATERIAL_APOYO + " text, " + COLUMNA_GUIA_MERCADEO_OBSERVACIONES + " text, " + COLUMNA_GUIA_MERCADEO_RECOMENDACION_PRODUCTO + " text, " +
            COLUMNA_GUIA_MERCADEO_VIGENCIA + " text, " + COLUMNA_GUIA_MERCADEO_PRECIO_OFERTA + " text, " + COLUMNA_GUIA_MERCADEO_REGION + " text, " + COLUMNA_GUIA_MERCADEO_CLAVE + " text, " +
            COLUMNA_GUIA_MERCADEO_RECOMENDACION_EXHIBICION + " text, " + COLUMNA_GUIA_MERCADEO_FECHA_INICIO + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_GUIA_MERCADEO_FECHA_TERMINO + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_GUIA_MERCADEO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_GUIA_MERCADEO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_GUIA_MERCADEO_USUARIO + " text, " + COLUMNA_GUIA_MERCADEO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_GUIA_MERCADEO_ID_CAT_GUIA_MERCADEO + ") REFERENCES " + TABLA_GUIA_MERCADEO_FALTANTE + "(" + COLUMNA_CAT_GUIA_MERCADEO_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_GUIA_MERCADEO_ID_CADENA + ") REFERENCES " + TABLA_CADENA + "(" + COLUMNA_CADENA_ID + "));";

    private static final String UDN_GUIA_MERCADEO = "create table " + TABLA_UDN_GUIA_MERCADEO +
            "(" + COLUMNA_UDN_GUIA_MERCADEO_ID_UDN + " integer, " + COLUMNA_UDN_GUIA_MERCADEO_ID_GUIA_MERCADEO + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_UDN_GUIA_MERCADEO_ID_UDN + ") REFERENCES " + TABLA_UDN + "(" + COLUMNA_UDN_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_UDN_GUIA_MERCADEO_ID_GUIA_MERCADEO + ") REFERENCES " + TABLA_GUIA_MERCADEO + "(" + COLUMNA_GUIA_MERCADEO_ID + "));";

    private static final String ACTIVIDAD_CATEGORIA = "create table " + TABLA_ACTIVIDAD_CATEGORIA +
            "(" + COLUMNA_ACTIVIDAD_CATEGORIA_ID + " integer primary key, " + COLUMNA_ACTIVIDAD_CATEGORIA_CATEGORIA + " text, " + COLUMNA_ACTIVIDAD_CATEGORIA_DESCRIPCION + " text, " +
            COLUMNA_ACTIVIDAD_CATEGORIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_CATEGORIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_CATEGORIA_USUARIO + " text, " + COLUMNA_ACTIVIDAD_CATEGORIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String ACTIVIDAD_COMPETENCIA = "create table " + TABLA_ACTIVIDAD_COMPETENCIA +
            "(" + COLUMNA_ACTIVIDAD_COMPETENCIA_ID + " integer primary key, " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_SUCURSAL + " integer, " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_COMPANIA + " integer, " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_ACTIVIDAD_CATEGORIA + " integer, " +
            COLUMNA_ACTIVIDAD_COMPETENCIA_PRODUCTO + " text, " + COLUMNA_ACTIVIDAD_COMPETENCIA_DESCRIPCION + " text, " + COLUMNA_ACTIVIDAD_COMPETENCIA_CLASIFICACION + " text, " + COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_INICIO + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_FIN + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_COMPETENCIA_DESCRIPCION_COMPANIA + " text, " + COLUMNA_ACTIVIDAD_COMPETENCIA_PARTICIPANTES + " integer, " +
            COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_COMPETENCIA_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_ACTIVIDAD_COMPETENCIA_USUARIO + " text, " + COLUMNA_ACTIVIDAD_COMPETENCIA_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_COMPANIA + ") REFERENCES " + TABLA_COMPANIA + "(" + COLUMNA_COMPANIA_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_ACTIVIDAD_CATEGORIA + ") REFERENCES " + TABLA_ACTIVIDAD_CATEGORIA + "(" + COLUMNA_ACTIVIDAD_CATEGORIA_ID + "));";


    private static final String DISPOSITIVO = "create table " + TABLA_DISPOSITIVO +
            "(" + COLUMNA_DISPOSIVITO_ID + " integer primary key, " + COLUMNA_DISPOSIVITO_ID_USUARIO + " integer, " + COLUMNA_DISPOSIVITO_ID_UDN + " integer, " + COLUMNA_DISPOSIVITO_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
            COLUMNA_DISPOSIVITO_NO_SERIE + " text, " + COLUMNA_DISPOSIVITO_IMEI + " text, " + COLUMNA_DISPOSIVITO_SIM + " text, " + COLUMNA_DISPOSIVITO_MODELO + " text, " + COLUMNA_DISPOSIVITO_ANDROID + " text, " + COLUMNA_DISPOSIVITO_VERSION + " text, " + COLUMNA_DISPOSIVITO_VERSION_BD + " text, " +
            COLUMNA_DISPOSIVITO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DISPOSIVITO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DISPOSIVITO_USUARIO + " text, " + COLUMNA_DISPOSIVITO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_DISPOSIVITO_ID_USUARIO + ") REFERENCES " + TABLA_USUARIO + "(" + COLUMNA_USUARIO_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_DISPOSIVITO_ID_UDN + ") REFERENCES " + TABLA_UDN + "(" + COLUMNA_UDN_ID + "));";


    private static final String DISPOSITIVO_EVENTUALIDAD = "create table " + TABLA_DISPOSITIVO_EVENTUALIDAD +
            "(" + COLUMNA_DISPOSIVITO_ID_EVENTUALIDAD + " integer primary key, " + COLUMNA_DISPOSIVITO_ID_DISPOSITIVO + " integer, " + COLUMNA_DISPOSIVITO_EVENTUALIDAD_ID_USUARIO + " integer, " + COLUMNA_DISPOSIVITO_MODULO_EVENTUALIDAD + " text, " +
            COLUMNA_DISPOSIVITO_BATERIA + " text, " + COLUMNA_DISPOSIVITO_EVENTUALIDAD_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DISPOSIVITO_EVENTUALIDAD_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_DISPOSIVITO_EVENTUALIDAD_USUARIO + " text, " + COLUMNA_DISPOSIVITO_EVENTUALIDAD_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer, " +
            " FOREIGN KEY (" + COLUMNA_DISPOSIVITO_ID_USUARIO + ") REFERENCES " + TABLA_USUARIO + "(" + COLUMNA_USUARIO_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_DISPOSIVITO_ID_DISPOSITIVO + ") REFERENCES " + TABLA_DISPOSITIVO + "(" + COLUMNA_DISPOSIVITO_ID + "));";


    private static final String REGISTRO_APLICACION = "create table " + TABLA_REGISTRO_APLICACION +
            "(" + COLUMNA_REGISTRO_APLICACION_ID + " integer primary key, " + COLUMNA_REGISTRO_APLICACION_NO_SUCURSAL + " integer, " + COLUMNA_REGISTRO_APLICACION_CHECK_IN_ACTIVO + " integer, " + COLUMNA_REGISTRO_APLICACION_SINC_INICIAL_ACTIVO + " integer);";


    private static final String CAT_INVENTARIO = "create table " + TABLA_CAT_INVENTARIO +
            "(" + COLUMNA_CATEGORIA_INVENTARIO_ID + " integer primary key, " + COLUMNA_INVENTARIO_CATEGORIA + " text, " + COLUMNA_CATEGORIA_INVENTARIO_DESCRIPCION + " text, " +
            COLUMNA_CATEGORIA_INVENTARIO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CATEGORIA_INVENTARIO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_CATEGORIA_INVENTARIO_USUARIO + " text, " + COLUMNA_CATEGORIA_INVENTARIO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer)";

    private static final String PRODUCTO_INVENTARIO = "create table " + TABLA_PRODUCTO_INVENTARIO +
            "(" + COLUMNA_PRODUCTO_INVENTARIO_ID + " integer primary key, " + COLUMNA_PRODUCTO_CATEGORIA_ID + " integer, " + COLUMNA_PRODUCTO_INVENTARIO_ID_SUCURSAL + " integer, " + COLUMNA_PRODUCTO_INVENTARIO_ID_PRODUCTO + " integer, " +
            COLUMNA_PRODUCTO_INVENTARIO_CANTIDAD + " integer, " + COLUMNA_PRODUCTO_INVENTARIO_FECHA + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_INVENTARIO_FECHA_CREACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_INVENTARIO_ULTIMA_MODIFICACION + " DATETIME DEFAULT CURRENT_TIMESTAMP, " + COLUMNA_PRODUCTO_INVENTARIO_USUARIO + " text, " + COLUMNA_PRODUCTO_INVENTARIO_ESTATUS + " integer, " + COLUMNA_ESTATUS_DOS + " integer," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_CATEGORIA_ID + ") REFERENCES " + TABLA_CAT_INVENTARIO + "(" + COLUMNA_CATEGORIA_INVENTARIO_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_INVENTARIO_ID_SUCURSAL + ") REFERENCES " + TABLA_SUCURSAL + "(" + COLUMNA_SUCRUSAL_ID + ")," +
            " FOREIGN KEY (" + COLUMNA_PRODUCTO_INVENTARIO_ID_PRODUCTO + ") REFERENCES " + TABLA_PRODUCTO + "(" + COLUMNA_PRODUCTO_ID + "));";


    public DBHelper(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(UDN);
        db.execSQL(CANALES);
        db.execSQL(USUARIO);
        db.execSQL(LOCALIZACION_USUARIO);
        db.execSQL(PROD_FAMILIA);
        db.execSQL(PROD_PRESENTACION);
        db.execSQL(PRODUCTO_SUBCATEGORIA);
        db.execSQL(PRODUCTO);
        db.execSQL(CATEGORIA_INCIDENCIA);
        db.execSQL(PRODUCTO_INCIDENCIA);
        db.execSQL(COMPANIA);
        db.execSQL(CLIENTE);
        db.execSQL(CADENA);
        db.execSQL(CLIENTE_SUCURSAL);
        db.execSQL(PRODUCTO_CATALOGADO);
        db.execSQL(SUCURSAL_PRODUCTO_EXISTENCIA);
        db.execSQL(VISITAS);
        db.execSQL(TIPO_ALERTA);
        db.execSQL(ALERTA);
        db.execSQL(PRODUCTO_PRECIO);
        db.execSQL(PUESTO);
        db.execSQL(TIPO_TELEFONO);
        db.execSQL(PERSONAL);
        db.execSQL(TELEFONO);
        db.execSQL(ESPACIOS);
        db.execSQL(DEPARTAMENTOS);
        db.execSQL(FOTOS_EVIDENCIA);
        db.execSQL(SUCURSAL_ESPACIOS);
        db.execSQL(SUCURSAL_ESPACIOS_EXISTENCIA);
        db.execSQL(ESPACIO_FRENTE);
        db.execSQL(ESPACIO_INCIDENCIA);
        db.execSQL(DIAS);
        db.execSQL(SUCURSAL_PERSONAL);
        db.execSQL(SUCURSAL_SUPERVISOR);
        db.execSQL(DIAS_SUCURSAL_PERSONAL);
        db.execSQL(DIAS_SUCURSAL_SUPERVICION);
        db.execSQL(ACTIVIDAD);
        db.execSQL(PRODUCTO_ACTIVIDAD);
        db.execSQL(PRODUCTO_ACTIVIDAD_SUCURSAL);
        db.execSQL(TIPO_EXISTENCIA);
        db.execSQL(FOTOS_EVIDENCIA_RELACION);
        db.execSQL(GUIA_MERCADEO_FALTANTE);
        db.execSQL(GUIA_MERCADEO);
        db.execSQL(UDN_GUIA_MERCADEO);
        db.execSQL(ACTIVIDAD_CATEGORIA);
        db.execSQL(ACTIVIDAD_COMPETENCIA);
        db.execSQL(DISPOSITIVO);
        db.execSQL(REGISTRO_APLICACION);
        db.execSQL(DISPOSITIVO_EVENTUALIDAD);
        db.execSQL(CAT_INVENTARIO);
        db.execSQL(PRODUCTO_INVENTARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_UDN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_CANALES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_USUARIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_LOCALIZACION_USUARIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PROD_FAMILIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PROD_PRESENTACION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO_SUBCATEGORIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_CATEGORIA_INCIDENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO_INCIDENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_COMPANIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_CLIENTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_CADENA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_SUCURSAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO_CATALOGADO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_SUCURSAL_PRODUCTO_EXISTENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_VISITA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_VISITA_ALERTA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_TIPO_ALERTA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO_PRECIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PUESTO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_TIPO_TELEFONO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PERSONAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_TELEFONO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_ESPACIOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_DEPARTAMENTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_FOTOS_EVIDENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_SUCURSAL_ESPACIOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_SUCURSAL_ESPACIO_EXISTENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_ESPACIO_FRENTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_ESPACIO_INCIDENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_DIAS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_SUCURSAL_PERSONAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_SUCURSAL_SUPERVISOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_DIAS_SUCURSAL_PERSONAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_DIAS_SUCURSAL_SUPERVICION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_ACTIVIDAD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO_ACTIVIDAD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO_ACTIVIDAD_SUCURSAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_ACTIVIDAD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_TIPO_EXISTENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_FOTOS_EVIDENCIA_RELACION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_GUIA_MERCADEO_FALTANTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_GUIA_MERCADEO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_UDN_GUIA_MERCADEO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_ACTIVIDAD_CATEGORIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_ACTIVIDAD_COMPETENCIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_DISPOSITIVO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_DISPOSITIVO_EVENTUALIDAD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_REGISTRO_APLICACION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_CAT_INVENTARIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO_INVENTARIO);
        onCreate(db);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void insertDinamicTableInicial(JSONObject datos) throws JSONException {
        SQLiteDatabase database = this.getWritableDatabase();
        for (int i = 2; i < 49; i++) {
            if (datos.has(String.valueOf(i))) {
                String tabla;
                JSONObject json = datos.getJSONObject(String.valueOf(i));
                Iterator iter = json.keys();
                tabla = (String) iter.next();

                Object intervention = json.get(tabla);
                if (intervention instanceof JSONArray) {
                    JSONArray jsonMainArr = json.getJSONArray(tabla);
                    for (int s = 0; s < jsonMainArr.length(); s++) {
                        Map<String, String> map = new HashMap<>();
                        JSONObject jsons = jsonMainArr.getJSONObject(s);
                        Iterator itera = jsons.keys();
                        while (itera.hasNext()) {
                            String key = (String) itera.next();
                            String value = jsons.getString(key);

                            if (tabla.equals("PEDIDO_DETALLE") && key.equals("PEDIDO_FIJO")) {

                            } else {
                                map.put(key, value);
                            }


                            if (tabla.equals("PEDIDO_DETALLE") && key.equals("PEDIDO")) {
                                map.put("PEDIDO_FIJO", value);
                            }

                        }
                        ContentValues values = new ContentValues();
                        for (Map.Entry<String, String> entry : map.entrySet()) {
                            values.put(entry.getKey(), entry.getValue());
                        }
                        database.insert(tabla, null, values);
                    }
                } else if (intervention instanceof JSONObject) {
                    Map<String, String> map = new HashMap<>();
                    JSONObject jsonempty = json.getJSONObject(tabla);
                    Iterator iterempty = jsonempty.keys();
                    while (iterempty.hasNext()) {
                        String key = (String) iterempty.next();
                        String value = jsonempty.getString(key);
                        if (tabla.equals("PEDIDO_DETALLE") && key.equals("PEDIDO_FIJO")) {

                        } else {
                            map.put(key, value);
                        }

                        if (tabla.equals("PEDIDO_DETALLE") && key.equals("PEDIDO")) {
                            map.put("PEDIDO_FIJO", value);
                        }

                    }

                    ContentValues values = new ContentValues();
                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        values.put(entry.getKey(), entry.getValue());
                    }
                    database.insert(tabla, null, values);
                }
            }
        }
        database.close();
    }


    public void checkIn(int idSucursal, int idUsuario, double LATITUD, double LONGITUD, String usuario) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_VISITAID_SUCURSAL, idSucursal);
        values.put(COLUMNA_VISITAID_USUARIO, idUsuario);
        values.put(COLUMNA_VISITA_HORA_CHECK_INICIO, getDateTime());
        values.put(COLUMNA_VISITA_HORA_CHECK_FIN, "");
        values.put(COLUMNA_VISITA_LATITUD_CHECKIN, LATITUD);
        values.put(COLUMNA_VISITA_LONGITUD_CHECKIN, LONGITUD);
        values.put(COLUMNA_VISITA_LATITUD_CHECKOUT, 0.0);
        values.put(COLUMNA_VISITA_LONGITUD_CHECKOUT, 0.0);
        values.put(COLUMNA_VISITA_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_VISITA_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_VISITA_USUARIO, usuario);
        values.put(COLUMNA_VISITA_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        database.insert(TABLA_VISITA, null, values);
        database.close();
    }

    public void updateVisita(int _idVisita, double LATITUD, double LONGITUD, String username) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_VISITA + " SET " + COLUMNA_VISITA_HORA_CHECK_FIN + " = '" + getDateTime() + "', " + COLUMNA_VISITA_LATITUD_CHECKOUT + " = '" + LATITUD + "', " + COLUMNA_VISITA_LONGITUD_CHECKOUT + " = '" + LONGITUD + "', " + COLUMNA_VISITA_USUARIO + " = '" + username + "', " + COLUMNA_ESTATUS_DOS + " = '" + Constantes.ESTATUS.CREADO_MOVIL_MODIFICADO_MOVIL + "' WHERE " + COLUMNA_VISITA_ID + " = '" + _idVisita + "'";
        database.execSQL(strSQL);
        database.close();
    }

    public Visita getVisita(int _idUsuario) {
        Visita visita = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLA_VISITA + " WHERE " + COLUMNA_VISITAID_USUARIO + " = '" + _idUsuario + "' ORDER BY " + COLUMNA_VISITA_ID + " DESC LIMIT 1", null);

        if (cursor.moveToFirst()) {
            do {
                int _ID = Integer.parseInt(cursor.getString(0));
                int ID_SUCURSAL = Integer.parseInt(cursor.getString(1));
                int ID_USUARIO = Integer.parseInt(cursor.getString(2));
                String HORA_CHECK_INICIO = cursor.getString(3);
                String HORA_CHECK_FIN = cursor.getString(4);
                double LATITUD_CHECKIN = Double.parseDouble(cursor.getString(5));
                double LONGITUD_CHECKIN = Double.parseDouble(cursor.getString(6));
                double LATITUD_CHECKOUT = Double.parseDouble(cursor.getString(7));
                double LONGITUD_CHECKOUT = Double.parseDouble(cursor.getString(8));
                String FECHA_CREACION = cursor.getString(9);
                String ULTIMA_MODIFICACION = cursor.getString(10);
                String USUARIO = cursor.getString(11);
                int USUARIO_ESTATUS = Integer.parseInt(cursor.getString(12));
                visita = new Visita(_ID, ID_SUCURSAL, ID_USUARIO, HORA_CHECK_INICIO, HORA_CHECK_FIN, LATITUD_CHECKIN, LONGITUD_CHECKIN, LATITUD_CHECKOUT, LONGITUD_CHECKOUT, FECHA_CREACION, ULTIMA_MODIFICACION, USUARIO, USUARIO_ESTATUS);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return visita;
    }

    public AdapterDatosGenerales getSucursalDatosGenerales(int id_sucursal) {
        AdapterDatosGenerales datosGenerales = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT DISTINCT Cliente.ALIAS, Cliente_Sucursal.ID_SUCURSAL, Cliente_Sucursal.ALIAS, Cliente_Sucursal.CALLE, Cliente_Sucursal.NO_EXT, Cliente_Sucursal.NO_INT, Cliente_Sucursal.COLONIA, Cliente_Sucursal.MUNICIPIO, Cliente_Sucursal.ESTADO FROM Cliente_Sucursal INNER JOIN Cadena ON Cliente_Sucursal.ID_CADENA = Cadena.ID_CADENA INNER JOIN Cliente ON Cadena.ID_CLIENTE = Cliente.ID_CLIENTE INNER JOIN Sucursal_Supervisor ON  Cliente_Sucursal.ID_SUCURSAL = Sucursal_Supervisor.ID_SUCURSAL WHERE Cliente_Sucursal.ID_SUCURSAL = '" + id_sucursal + "';", null);
        if (cursor.moveToFirst()) {
            String CLIENTE = cursor.getString(0);
            int ID_SUCURSAL = Integer.parseInt(cursor.getString(1));
            String SUCURSAL = cursor.getString(2);
            String DOMICILIO = cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(6);
            String MUNICIPIO = cursor.getString(7) + ", " + cursor.getString(8);

            datosGenerales = new AdapterDatosGenerales(CLIENTE, ID_SUCURSAL, SUCURSAL, DOMICILIO, MUNICIPIO);
        }

        cursor.close();
        return datosGenerales;
    }

    public int getIdSucursalSupervisor(int id_sucursal) {

        int ID_SUCURSAL_SUPERVISOR = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUCURSAL_SUPERVISOR.ID_SUCURSAL_SUPERVISOR FROM SUCURSAL_SUPERVISOR WHERE SUCURSAL_SUPERVISOR.ID_SUCURSAL = " + id_sucursal + "", null);
        if (cursor.moveToFirst()) {
            ID_SUCURSAL_SUPERVISOR = Integer.parseInt(cursor.getString(0));
        }

        cursor.close();
        return ID_SUCURSAL_SUPERVISOR;
    }

    public ArrayList<AdapterDatosGenerales> getSucursalDatosGeneralesHorario(int id_sucursal) {
        ArrayList<AdapterDatosGenerales> arregloHorario = new ArrayList<>();
        AdapterDatosGenerales datosGenerales;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUCURSAL_SUPERVISOR.ID_SUCURSAL_SUPERVISOR, Dias_Sucursal_Supervisor.HORA_INICIO, Dias_Sucursal_Supervisor.HORA_FIN, Dias.NOMBRE FROM SUCURSAL_SUPERVISOR INNER JOIN Dias_Sucursal_Supervisor  ON Sucursal_Supervisor.ID_SUCURSAL_SUPERVISOR = Dias_Sucursal_Supervisor.ID_SUCURSAL_SUPERVISOR INNER JOIN Dias ON Dias_Sucursal_Supervisor.ID_DIA = Dias.ID_DIA WHERE SUCURSAL_SUPERVISOR.ID_SUCURSAL = " + id_sucursal + " and DIAS_SUCURSAL_SUPERVISOR.ESTATUS = 1", null);
        if (cursor.moveToFirst()) {
            do {
                int ID_SUCURSAL_SUPERVISOR = Integer.parseInt(cursor.getString(0));
                String HORA_INICIO = cursor.getString(1);
                String HORA_FIN = cursor.getString(2);
                String DIA = cursor.getString(3);

                datosGenerales = new AdapterDatosGenerales(ID_SUCURSAL_SUPERVISOR, HORA_INICIO, HORA_FIN, DIA);
                arregloHorario.add(datosGenerales);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloHorario;
    }

    public ArrayList<AdapterDatosGenerales> getSucursalDatosGeneralesPersonal(int id_sucursal) {
        ArrayList<AdapterDatosGenerales> arregloPersonal = new ArrayList<>();
        AdapterDatosGenerales datosGenerales;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Personal.ID_PERSONAL, Personal.ID_PUESTO, Puesto.NOMBRE as puesto, Personal.CLAVE, Personal.NOMBRE, Personal.AP_PATERNO, Personal.AP_MATERNO, Personal.EMAIL, Personal.FECHA_CONTRATACION, Personal.ESTATUS2 FROM Personal INNER JOIN Puesto ON Personal.ID_PUESTO = Puesto.ID_PUESTO INNER JOIN Sucursal_Personal ON Sucursal_Personal.ID_PERSONAL = Personal.ID_PERSONAL INNER JOIN Cliente_Sucursal ON Sucursal_Personal.ID_SUCURSAL = Cliente_Sucursal.ID_SUCURSAL WHERE Cliente_Sucursal.ID_SUCURSAL = '" + id_sucursal + "' and Personal.ESTATUS = 1", null);
        if (cursor.moveToFirst()) {
            do {
                int ID_PERSONAL = Integer.parseInt(cursor.getString(0));
                int ID_PUESTO = Integer.parseInt(cursor.getString(1));
                String PUESTO = cursor.getString(2);
                String CLAVE = cursor.getString(3);
                String NOMBRE_PERSONAL = cursor.getString(4);
                String AP_PATERNO = cursor.getString(5);
                String AP_MATERNO = cursor.getString(6);
                String EMAIL = cursor.getString(7);
                String FECHA_CONTRATACION = cursor.getString(8);
                int ESTATUS_DOS = Integer.parseInt(cursor.getString(9));

                datosGenerales = new AdapterDatosGenerales(ID_PERSONAL, ID_PUESTO, PUESTO, CLAVE, NOMBRE_PERSONAL, AP_PATERNO, AP_MATERNO, EMAIL, FECHA_CONTRATACION, ESTATUS_DOS);
                arregloPersonal.add(datosGenerales);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return arregloPersonal;
    }


    public int getFamiliaPorProducto(int idProducto) {

        int idFamilia = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_FAMILIA FROM PRODUCTO WHERE ID_PRODUCTO = '" + idProducto + "'", null);

        if (cursor.moveToFirst()) {
            idFamilia = Integer.parseInt(cursor.getString(0));
        }

        return idFamilia;
    }


    public int getPresentacionPorProducto(int idProducto) {

        int idFamilia = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_PRESENTACION FROM PRODUCTO WHERE ID_PRODUCTO = '" + idProducto + "'", null);

        if (cursor.moveToFirst()) {
            idFamilia = Integer.parseInt(cursor.getString(0));
        }

        return idFamilia;
    }


    public ArrayList<FichaTecnicaProducto> getAllProductoPorPrecio(int id_sucursal, int idFamilia, int idPresentacion) {
        ArrayList<FichaTecnicaProducto> fichaPRODUCTOs = new ArrayList<>();
        FichaTecnicaProducto p;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT DISTINCT PRODUCTO.ID_PRODUCTO, PRODUCTO.UPC, PRODUCTO.CLAVE, PRODUCTO.NOMBRE, PRODUCTO_PRESENTACION.PRESENTACION, " +
                "PRODUCTO.ORDENAMIENTO, PRODUCTO_FAMILIA.ORDENAMIENTO " +
                "FROM PRODUCTO_CATALOGADO " +
                "INNER JOIN PRODUCTO ON PRODUCTO_CATALOGADO.ID_PRODUCTO = PRODUCTO.ID_PRODUCTO " +
                "INNER JOIN PRODUCTO_PRESENTACION ON PRODUCTO.ID_PRESENTACION = PRODUCTO_PRESENTACION.ID_PRESENTACION " +
                "INNER JOIN PRODUCTO_FAMILIA ON PRODUCTO.ID_FAMILIA = PRODUCTO_FAMILIA.ID_FAMILIA " +
                "WHERE PRODUCTO_CATALOGADO.ID_SUCURSAL = '" + id_sucursal + "' " +
                "AND PRODUCTO.ID_FAMILIA = '" + idFamilia + "' " +
                "AND PRODUCTO.ID_PRESENTACION = '" + idPresentacion + " ' " +
                "AND PRODUCTO_CATALOGADO.ESTATUS = 1 AND PRODUCTO.ID_COMPANIA = 4  ORDER BY PRODUCTO_FAMILIA.ORDENAMIENTO, PRODUCTO.ORDENAMIENTO;", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_PRODUCTO = Integer.parseInt(cursor.getString(0));
                String UPC = cursor.getString(1);
                String CLAVE = cursor.getString(2);
                String NOMBRE = cursor.getString(3);
                String PRESENTACION = cursor.getString(4);

                p = new FichaTecnicaProducto(ID_PRODUCTO, UPC, CLAVE, NOMBRE, PRESENTACION, Constantes.PRODUCTO_PRECIO.PRODUCTO_PRECIO_ACTIVO);
                fichaPRODUCTOs.add(p);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return fichaPRODUCTOs;
    }


    public ArrayList<AdapterTipoExistencia> getTipoExistencia() {
        AdapterTipoExistencia tipoExistencia;
        ArrayList<AdapterTipoExistencia> arregloTipoExistenca = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_TIPO_EXISTENCIA, NOMBRE from Tipo_Existencia", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_TIPO_EXISTENCIA = Integer.parseInt(cursor.getString(0));
                String TIPO = cursor.getString(1);

                tipoExistencia = new AdapterTipoExistencia(ID_TIPO_EXISTENCIA, TIPO);
                arregloTipoExistenca.add(tipoExistencia);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloTipoExistenca;
    }

    public int getIdProductoExhibido(int id_producto, int id_sucursal) {

        int idProductoExhibido = Constantes.EXHIBIDO.EXHIBIDO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT IFNULL(SUCURSAL_PRODUCTO_EXISTENCIA.ID_SUCURSAL_PRODUCTO ,0) ID_SUCURSAL_PRODUCTO FROM Producto INNER JOIN SUCURSAL_PRODUCTO_EXISTENCIA ON Producto.ID_PRODUCTO = SUCURSAL_PRODUCTO_EXISTENCIA.ID_PRODUCTO WHERE Producto.ID_PRODUCTO = '" + id_producto + "' AND SUCURSAL_PRODUCTO_EXISTENCIA.ID_SUCURSAL = '" + id_sucursal + "';", null);

        if (cursor.moveToFirst()) {
            idProductoExhibido = Integer.parseInt(cursor.getString(0));
        }

        cursor.close();
        return idProductoExhibido;
    }


    public ArrayList<Compania> getCompaniaProducto(int id_producto, int id_sucursal) {
        ArrayList<Compania> arregloCompania = new ArrayList<>();
        Compania compania;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select Distinct COMPANIA.ID_COMPANIA, COMPANIA.NOMBRE from PRODUCTO_PRECIOS INNER JOIN COMPANIA ON PRODUCTO_PRECIOS.ID_COMPANIA = COMPANIA.ID_COMPANIA Where PRODUCTO_PRECIOS.ID_PRODUCTO = " + id_producto + " AND PRODUCTO_PRECIOS.ID_SUCURSAL = " + id_sucursal + " AND COMPANIA.NOMBRE != 'OTROS'  AND COMPANIA.ESTATUS = 1 ORDER BY COMPANIA.PRIORIDAD LIMIT 4;", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_COMPANIA = Integer.parseInt(cursor.getString(0));
                String NOMBRE_COMPANIA = cursor.getString(1);
                compania = new Compania(ID_COMPANIA, NOMBRE_COMPANIA);
                arregloCompania.add(compania);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloCompania;
    }

    public AdapterTipoExistencia getNombreProductoExistencia(int _idExistencia) {

        AdapterTipoExistencia adapterTipoExistenciaEspacio = null;
        String NOMBRE;
        int ID_TIPO_EXISTENCIA;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select TIPO_EXISTENCIA.ID_TIPO_EXISTENCIA, TIPO_EXISTENCIA.NOMBRE FROM SUCURSAL_PRODUCTO_EXISTENCIA INNER JOIN TIPO_EXISTENCIA ON SUCURSAL_PRODUCTO_EXISTENCIA.ID_TIPO_EXISTENCIA = TIPO_EXISTENCIA.ID_TIPO_EXISTENCIA WHERE SUCURSAL_PRODUCTO_EXISTENCIA.ID_SUCURSAL_PRODUCTO = " + _idExistencia + ";", null);

        if (cursor.moveToFirst()) {
            ID_TIPO_EXISTENCIA = Integer.parseInt(cursor.getString(0));
            NOMBRE = cursor.getString(1);
            adapterTipoExistenciaEspacio = new AdapterTipoExistencia(ID_TIPO_EXISTENCIA, NOMBRE);
        }

        cursor.close();
        return adapterTipoExistenciaEspacio;
    }


    public String getComentarioProductoExistencia(int _idExistencia) {

        String Comentario = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select COMENTARIOS FROM SUCURSAL_PRODUCTO_EXISTENCIA WHERE ID_SUCURSAL_PRODUCTO = " + _idExistencia + ";", null);

        if (cursor.moveToFirst()) {
            Comentario = cursor.getString(0);
        }

        cursor.close();
        return Comentario;
    }

    public void updateProductoExistenciaNo(int _idExistencia, int id_sucursal, String username, String descripcion, int id_tipo_exhibicion) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_SUCURSAL_PRODUCTO_EXISTENCIA + " SET " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_EXHIBIDO + " = '" + 0 + "', " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_USUARIO + " = '" + username + "', " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_COMENTARIOS + " = '" + descripcion + "', " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_TIPO_EXISTENCIA + " = '" + id_tipo_exhibicion + "', " + COLUMNA_ESTATUS_DOS + " = '" + 2 + "' WHERE SUCURSAL_PRODUCTO_EXISTENCIA." + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID + " = " + _idExistencia + " AND " + COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_SUCURSAL + " = " + id_sucursal + "";
        database.execSQL(strSQL);
        database.close();
    }


    public long insertProductoExistencia(int id_sucursal, int id_producto, int id_tipo_existencia, String comentario, String usuario) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_SUCURSAL, id_sucursal);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ID_PRODUCTO, id_producto);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_TIPO_EXISTENCIA, id_tipo_existencia);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_CANTIDAD, 0);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_FECHA, getDateTime());
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_EXHIBIDO, 0);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_EXHIBIDO, 0);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_COMENTARIOS, comentario);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_USUARIO, usuario);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_EXISTENCIA_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, 2);
        long id_sucursal_producto_existencia = database.insert(TABLA_SUCURSAL_PRODUCTO_EXISTENCIA, null, values);
        database.close();
        return id_sucursal_producto_existencia;
    }


    public void deleteProductoExistencia(Context context, int id_producto_existencia, int id_producto, int id_sucursal) {
        List<AdapterFotos> listFotos = new ArrayList<>();
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery("SELECT ID_RELACION, FOTOS_EVIDENCIA_RELACION.ID_FOTO, FILE_NAME FROM FOTOS_EVIDENCIA_RELACION INNER JOIN FOTOS_EVIDENCIA ON FOTOS_EVIDENCIA_RELACION.ID_FOTO = FOTOS_EVIDENCIA.ID_FOTO WHERE TABLA = '" + TABLA_SUCURSAL_PRODUCTO_EXISTENCIA + "' AND ID = '" + id_producto_existencia + "'", null);

        if (cursor.moveToFirst()) {
            do {
                int idRelacion = Integer.parseInt(cursor.getString(0));
                int idFoto = Integer.parseInt(cursor.getString(1));
                String archivo = cursor.getString(2);
                AdapterFotos fotos = new AdapterFotos(idFoto, idRelacion, archivo);
                listFotos.add(fotos);
            } while (cursor.moveToNext());


            for (int i = 0; i < listFotos.size(); i++) {
                String strSQL = "DELETE FROM " + TABLA_FOTOS_EVIDENCIA_RELACION + " WHERE ID_RELACION = '" + listFotos.get(i).getIdFotoRelacion() + "'";
                database.execSQL(strSQL);

                String strSQLFoto = "DELETE FROM " + TABLA_FOTOS_EVIDENCIA + " WHERE ID_FOTO = '" + listFotos.get(i).getIdFoto() + "'";
                database.execSQL(strSQLFoto);

                String myPath = Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_foto)) + "/" + listFotos.get(i).getArchivo();
                File f = new File(myPath);
                f.delete();

            }
        }

        String strSQL = "DELETE FROM " + TABLA_SUCURSAL_PRODUCTO_EXISTENCIA + " WHERE ID_SUCURSAL_PRODUCTO = '" + id_producto_existencia + "' AND ID_PRODUCTO = '" + id_producto + "' AND ID_SUCURSAL = '" + id_sucursal + "'";
        database.execSQL(strSQL);
        database.close();
    }


    public ArrayList<Espacios> getEspaciosTipo(int tipo_espacio) {

        Espacios espacios;
        ArrayList<Espacios> arregloEspaciosTipo = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Espacios.ID_ESPACIO, Espacios.NOMBRE FROM Espacios WHERE ESTATUS = 1 AND TIPO_ESPACIO = " + tipo_espacio + " ORDER BY NOMBRE", null);

        if (cursor.moveToFirst()) {
            do {
                int idEspacio = Integer.parseInt(cursor.getString(0));
                String espacio = cursor.getString(1);

                espacios = new Espacios(idEspacio, espacio);
                arregloEspaciosTipo.add(espacios);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloEspaciosTipo;
    }


    public ArrayList<Departamentos> getDepartamentos() {

        Departamentos departamentos;
        ArrayList<Departamentos> arregloDepartamentos = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Departamentos.ID_DEPARTAMENTO, Departamentos.NOMBRE FROM Departamentos ORDER BY NOMBRE", null);

        if (cursor.moveToFirst()) {
            do {

                int idDepartamento = Integer.parseInt(cursor.getString(0));
                String departamento = cursor.getString(1);

                departamentos = new Departamentos(idDepartamento, departamento);
                arregloDepartamentos.add(departamentos);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloDepartamentos;
    }

    public String getTipoLlenado(int idEspacio) {

        String llenado = "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT TIPO_LLENADO FROM ESPACIOS WHERE ID_ESPACIO = " + idEspacio + ";", null);

        if (cursor.moveToFirst()) {
            llenado = cursor.getString(0);

        }
        cursor.close();
        return llenado;
    }


    public Integer getTipoEspacio(int idEspacio) {

        int tipoEspacio = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT TIPO_ESPACIO FROM ESPACIOS WHERE ID_ESPACIO = " + idEspacio + ";", null);

        if (cursor.moveToFirst()) {
            tipoEspacio = Integer.parseInt(cursor.getString(0));

        }
        cursor.close();
        return tipoEspacio;
    }


    public String getTipoFrente(int idEspacio) {

        String tipoEspacio = "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT TIPO_FRENTE FROM ESPACIOS WHERE ID_ESPACIO = " + idEspacio + ";", null);

        if (cursor.moveToFirst()) {
            tipoEspacio = cursor.getString(0);

        }
        cursor.close();
        return tipoEspacio;
    }


    public long insertFoto(int id_sucursal, String archivo, String nombre_archivo, double latitud, double longitud, String usuario) {

        archivo = archivo.replace("[", "");
        archivo = archivo.replace("]", "");

        long ID;
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_FOTOS_EVIDENCIAID_SUCURSAL, id_sucursal);
        values.put(COLUMNA_FOTOS_EVIDENCIA_ARCHIVO, archivo);
        values.put(COLUMNA_FOTOS_EVIDENCIA_FILE_NAME, nombre_archivo);
        values.put(COLUMNA_FOTOS_EVIDENCIA_LATITUD, latitud);
        values.put(COLUMNA_FOTOS_EVIDENCIA_LONGITUD, longitud);
        values.put(COLUMNA_FOTOS_EVIDENCIA_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_FOTOS_EVIDENCIA_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_FOTOS_EVIDENCIA_USUARIO, usuario);
        values.put(COLUMNA_FOTOS_EVIDENCIA_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        ID = database.insert(TABLA_FOTOS_EVIDENCIA, null, values);
        database.close();
        return ID;
    }


    public void insertFotoRelacion(int id_foto, String tabla, int id_tabla) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_FOTOS_EVIDENCIA_RELACION_ID_FOTO, id_foto);
        values.put(COLUMNA_FOTOS_EVIDENCIA_RELACION_TABLA, tabla);
        values.put(COLUMNA_FOTOS_EVIDENCIA_RELACION_ID, id_tabla);
        values.put(COLUMNA_ESTATUS_DOS, 2);
        database.insert(TABLA_FOTOS_EVIDENCIA_RELACION, null, values);
        database.close();
    }

    public int getNumeroFotos() {
        int numero_fotos = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select count(*) FROM FOTOS_EVIDENCIA WHERE ESTATUS2 != 0", null);

        if (cursor.moveToFirst()) {
            numero_fotos = Integer.parseInt(cursor.getString(0));
        }

        cursor.close();
        db.close();
        return numero_fotos;
    }

    public void updateimage(int id_foto, String archivo, String nombre_archivo, double latitud, double longitud) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_FOTOS_EVIDENCIA + " SET " + COLUMNA_FOTOS_EVIDENCIA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_FOTOS_EVIDENCIA_ARCHIVO + " = '" + archivo + "', " + COLUMNA_FOTOS_EVIDENCIA_FILE_NAME + " = '" + nombre_archivo + "', " + COLUMNA_FOTOS_EVIDENCIA_LATITUD + " = '" + latitud + "', " + COLUMNA_FOTOS_EVIDENCIA_LONGITUD + " = '" + longitud + "' WHERE " + COLUMNA_FOTOS_EVIDENCIA_ID + " = " + id_foto + "";
        database.execSQL(strSQL);
        database.close();
    }

    public void deleteFoto(int id_foto) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_FOTOS_EVIDENCIA + " SET " + COLUMNA_FOTOS_EVIDENCIA_ESTATUS + " = '0' WHERE " + COLUMNA_FOTOS_EVIDENCIA_ID + " = " + id_foto + "";
        database.execSQL(strSQL);
        database.close();
    }

    public String getArchivo(int id_foto) {
        String nombre_archivo = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT FOTOS_EVIDENCIA.FILE_NAME FROM FOTOS_EVIDENCIA  WHERE FOTOS_EVIDENCIA.ID_FOTO = '" + id_foto + "'", null);

        if (cursor.moveToFirst()) {
            nombre_archivo = cursor.getString(0);
        }

        cursor.close();
        return nombre_archivo;
    }

    public ArrayList<FotosEvidenciaRelacion> getFotos(String tabla, int id) {
        ArrayList<FotosEvidenciaRelacion> fotos = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT FOTOS_EVIDENCIA.ID_FOTO, FOTOS_EVIDENCIA.ARCHIVO, FOTOS_EVIDENCIA.FILE_NAME " +
                "FROM FOTOS_EVIDENCIA INNER JOIN Fotos_Evidencia_Relacion ON FOTOS_EVIDENCIA.ID_FOTO = Fotos_Evidencia_Relacion.ID_FOTO " +
                "WHERE Fotos_Evidencia_Relacion.TABLA = '" + tabla + "' AND Fotos_Evidencia_Relacion.ID = " + id + " AND FOTOS_EVIDENCIA.ESTATUS = 1", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_FOTO = Integer.parseInt(cursor.getString(0));
                String ARCHIVO = cursor.getString(1);
                String FILE_NAME = cursor.getString(2);
                FotosEvidenciaRelacion fotosevidencai = new FotosEvidenciaRelacion(ID_FOTO, ARCHIVO, FILE_NAME);
                fotos.add(fotosevidencai);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return fotos;
    }

    public long insertSucursalEspacios(int sucursal, int espacio, int departamento, int categoria, int tipo_adquisicion, double nivel, int puertas, double tramos, double tarimas, String ruta, String usuario) {

        long id_sucursal_espacio;
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_SUCURSAL_ESPACIOSID_SUCURSAL, sucursal);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_ID_ESPACIO, espacio);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_ID_DEPARTAMENTOS, departamento);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_ID_CATEGORIA, categoria);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_TIPO_ADQUISICION, tipo_adquisicion);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_NIVEL, nivel);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_PUERTAS, puertas);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_TRAMOS, tramos);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_TARIMAS, tarimas);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_PLANOGRAMA_RUTA, ruta);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_FECHA_ASIGNACION, getDateTime());
        values.put(COLUMNA_SUCURSAL_ESPACIOS_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_SUCURSAL_ESPACIOS_USUARIO, usuario);
        values.put(COLUMNA_SUCURSAL_ESPACIOS_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        id_sucursal_espacio = database.insert(TABLA_SUCURSAL_ESPACIOS, null, values);
        database.close();
        return id_sucursal_espacio;
    }

    public void eliminarEspacio(int _idEspacio, String motivoBaja, int estatus_dos) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_SUCURSAL_ESPACIOS + " SET " + COLUMNA_SUCURSAL_ESPACIOS_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_SUCURSAL_ESPACIOS_MOTIVO_ELIMINACION + " = '" + motivoBaja + "', " + COLUMNA_SUCURSAL_ESPACIOS_FECHA_ELIMINACION + " = '" + getDateTime() + "', " + COLUMNA_SUCURSAL_ESPACIOS_ESTATUS + " = '" + 0 + "', " + COLUMNA_ESTATUS_DOS + " = " + estatus_dos + " WHERE Sucursal_Espacio." + COLUMNA_SUCURSAL_ESPACIOS_ID + " = " + _idEspacio + "";
        database.execSQL(strSQL);
        database.close();
    }

    public ArrayList<AdapterAlertas> getAlertasTotal() {
        ArrayList<AdapterAlertas> arregloAlertas = new ArrayList<>();
        AdapterAlertas adapterAlertas;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  Visita_Alerta.ID_ALERTA,  Visita_Alerta.DESCRIPCION,  Visita_Alerta.FECHA, Visita_Alerta.LEIDA, Cliente_Sucursal.ALIAS FROM Visita_Alerta INNER JOIN Cliente_Sucursal ON Cliente_Sucursal.ID_SUCURSAL = Visita_Alerta.ID_SUCURSAL WHERE Visita_Alerta.ESTATUS = 1 order by datetime(Visita_Alerta.FECHA) DESC", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_ALERTA = Integer.parseInt(cursor.getString(0));
                String DESCRIPCION = cursor.getString(1);
                String FECHA = cursor.getString(2);
                String LEIDA = cursor.getString(3);
                String SUCURSAL_NOMBRE = cursor.getString(4);

                adapterAlertas = new AdapterAlertas(ID_ALERTA, DESCRIPCION, FECHA, LEIDA, SUCURSAL_NOMBRE);
                arregloAlertas.add(adapterAlertas);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloAlertas;
    }

    public ArrayList<AdapterAlertas> getAlertas(int id_sucursal) {
        ArrayList<AdapterAlertas> arregloAlertas = new ArrayList<>();
        AdapterAlertas adapterAlertas;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Tipo_Alerta.NOMBRE, Visita_Alerta.ID_ALERTA, Visita_Alerta.DESCRIPCION, Visita_Alerta.FECHA, Visita_Alerta.LEIDA, Visita_Alerta.ESTATUS2 FROM Visita_Alerta INNER JOIN Cliente_Sucursal ON Visita_Alerta.ID_SUCURSAL = Cliente_Sucursal.ID_SUCURSAL INNER JOIN Tipo_Alerta ON Tipo_Alerta.ID_TIPO_ALERTA = Visita_Alerta.ID_TIPO_ALERTA Where Cliente_Sucursal.ID_SUCURSAL = '" + id_sucursal + "' AND Visita_Alerta.ESTATUS = 1 order by datetime(Visita_Alerta.FECHA) DESC", null);

        if (cursor.moveToFirst()) {
            do {
                String TIPO_ALERTA_NOMBRE = cursor.getString(0);
                int ID_ALERTA = Integer.parseInt(cursor.getString(1));
                String DESCRIPCION = cursor.getString(2);
                String FECHA = cursor.getString(3);
                String LEIDA = cursor.getString(4);
                int ESTATUS2 = Integer.parseInt(cursor.getString(5));

                adapterAlertas = new AdapterAlertas(TIPO_ALERTA_NOMBRE, ID_ALERTA, DESCRIPCION, FECHA, LEIDA, ESTATUS2);
                arregloAlertas.add(adapterAlertas);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloAlertas;
    }


    public int getAlertasSucursal(int id_sucursal, String FECHA) {

        int numAlertas = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT count(*) FROM Visita_Alerta INNER JOIN Cliente_Sucursal ON Visita_Alerta.ID_SUCURSAL = Cliente_Sucursal.ID_SUCURSAL Where Cliente_Sucursal.ID_SUCURSAL = '" + id_sucursal + "' AND Visita_Alerta.ESTATUS = 1 AND substr(Visita_Alerta.FECHA, 1,10) = '" + FECHA + "'", null);

        if (cursor.moveToFirst()) {
            numAlertas = Integer.valueOf(cursor.getString(0));
        }

        cursor.close();
        return numAlertas;
    }

    public void updateAlertaLeido(int _idAlerta, String leida, int estatus_dos, String username) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_VISITA_ALERTA + " SET " + COLUMNA_VISITA_ALERTA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_VISITA_ALERTA_LEIDA + " = '" + leida + "', " + COLUMNA_VISITA_ALERTA_USUARIO + " = '" + username + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "' WHERE " + COLUMNA_VISITA_ALERTA_ID + " = " + _idAlerta + "";
        database.execSQL(strSQL);
        database.close();
    }

    public void updateAlerta(int _idAlerta, int estatus_dos, String username) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_VISITA_ALERTA + " SET " + COLUMNA_VISITA_ALERTA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_VISITA_ALERTA_ESTATUS + " = '" + 0 + "', " + COLUMNA_VISITA_ALERTA_USUARIO + " = '" + username + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "' WHERE " + COLUMNA_VISITA_ALERTA_ID + " = " + _idAlerta + "";
        database.execSQL(strSQL);
    }

    public void updateDescripcionAlerta(int _idAlerta, String DESCRIPCION, String FECHA, int estatus_dos, String username) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_VISITA_ALERTA + " SET " + COLUMNA_VISITA_ALERTA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_VISITA_ALERTA_DESCRIPCION + " = '" + DESCRIPCION + "', " + COLUMNA_VISITA_ALERTA_USUARIO + " = '" + username + "', " + COLUMNA_VISITA_ALERTA_FECHA + " = '" + FECHA + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "' WHERE " + COLUMNA_VISITA_ALERTA_ID + " = " + _idAlerta + "";
        database.execSQL(strSQL);
        database.close();
    }

    public ArrayList<AdapterTipoAlerta> getTiposAlerta() {
        ArrayList<AdapterTipoAlerta> arregloTIPOsAlerta = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT TIPO_Alerta.ID_TIPO_ALERTA, TIPO_Alerta.NOMBRE, TIPO_Alerta.DESCRIPCION from TIPO_Alerta", null);

        if (cursor.moveToFirst()) {
            do {

                int idTipoAlerta = Integer.parseInt(cursor.getString(0));
                String nombre = cursor.getString(1);
                String descripcion = cursor.getString(2);

                AdapterTipoAlerta alertas = new AdapterTipoAlerta(idTipoAlerta, nombre, descripcion);
                arregloTIPOsAlerta.add(alertas);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloTIPOsAlerta;
    }

    public void insertAlerta(int sucursal, int TIPOAlerta, String DESCRIPCION, String FECHA, String USUARIO, int PENDIENTE) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_VISITA_ALERTAID_SUCURSAL, sucursal);
        values.put(COLUMNA_VISITA_ALERTAID_TIPO_ALERTA, TIPOAlerta);
        values.put(COLUMNA_VISITA_ALERTA_DESCRIPCION, DESCRIPCION);
        values.put(COLUMNA_VISITA_ALERTA_LEIDA, 0);
        values.put(COLUMNA_VISITA_ALERTA_FECHA, FECHA);
        values.put(COLUMNA_VISITA_ALERTA_PENDIENTE, PENDIENTE);
        values.put(COLUMNA_VISITA_ALERTA_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_VISITA_ALERTA_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_VISITA_ALERTA_USUARIO, USUARIO);
        values.put(COLUMNA_VISITA_ALERTA_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        database.insert(TABLA_VISITA_ALERTA, null, values);
        database.close();
    }


    public int getAlertasNoLeidas(int _id_sucursal, String fecha) {
        int noAlertas = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM VISITA_ALERTA WHERE VISITA_ALERTA.ID_SUCURSAL = " + _id_sucursal + " AND VISITA_ALERTA.ESTATUS = 1 AND VISITA_ALERTA.LEIDA = 0 AND substr(VISITA_ALERTA.FECHA, 0, 11) <=  DATE('" + fecha + "')", null);
        if (cursor.moveToFirst()) {
            noAlertas = Integer.parseInt(cursor.getString(0));
        }

        cursor.close();
        return noAlertas;
    }

    public ArrayList<CatalogoCompanias> getCompania() {

        CatalogoCompanias catalogoCompanias;
        ArrayList<CatalogoCompanias> arregloCompania = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Compania.ID_COMPANIA, Compania.NOMBRE FROM Compania WHERE Compania.TIPO_COMPANIA = 1 AND Compania.TIPO_USUARIO_COMPANIA = 1 AND Compania.ESTATUS = 1 ORDER BY NOMBRE;", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_COMPANIA = Integer.parseInt(cursor.getString(0));
                String COMPANIA = cursor.getString(1);
                catalogoCompanias = new CatalogoCompanias(ID_COMPANIA, COMPANIA);
                arregloCompania.add(catalogoCompanias);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloCompania;
    }


    public ArrayList<CategoriaIncidencia> getActividadCategoriaIncidencia(int tipoIncidencia, int tipoCategoria) {


        ArrayList<CategoriaIncidencia> arregloActividad = new ArrayList<>();
        CategoriaIncidencia catIncidencia;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_CATEGORIA_INCIDENCIA, CATEGORIA FROM CATEGORIA_INCIDENCIA WHERE CATEGORIA_INCIDENCIA.ESTATUS = 1 AND (TIPO_INCIDENCIA =  " + tipoIncidencia + " OR TIPO_INCIDENCIA = 0) AND (TIPO_CATEGORIA = " + tipoCategoria + " OR TIPO_CATEGORIA = 0) ORDER BY CATEGORIA", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_CATEGORIA = Integer.parseInt(cursor.getString(0));
                String CATEGORIA = cursor.getString(1);
                catIncidencia = new CategoriaIncidencia(ID_CATEGORIA, CATEGORIA);
                arregloActividad.add(catIncidencia);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloActividad;
    }


    public ArrayList<CategoriaIncidencia> getActividadCategoriaEliminaEspacio(int tipoIncidencia, int tipoCategoria) {


        ArrayList<CategoriaIncidencia> arregloActividad = new ArrayList<>();
        CategoriaIncidencia catIncidencia;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_CATEGORIA_INCIDENCIA, CATEGORIA FROM CATEGORIA_INCIDENCIA WHERE CATEGORIA_INCIDENCIA.ESTATUS = 1 AND TIPO_INCIDENCIA =  " + tipoIncidencia + " AND TIPO_CATEGORIA = " + tipoCategoria + " ORDER BY CATEGORIA", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_CATEGORIA = Integer.parseInt(cursor.getString(0));
                String CATEGORIA = cursor.getString(1);
                catIncidencia = new CategoriaIncidencia(ID_CATEGORIA, CATEGORIA);
                arregloActividad.add(catIncidencia);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloActividad;
    }


    public long insertActividadCompetencia(int id_sucursal, int id_compania, int id_actividad_cat, String producto, String descripcion, String fecha_inicio, String fecha_fin, String usuario, String descripcion_compania, int participantes) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_ID_SUCURSAL, id_sucursal);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_ID_COMPANIA, id_compania);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_ID_ACTIVIDAD_CATEGORIA, id_actividad_cat);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_PRODUCTO, producto);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_DESCRIPCION, descripcion);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_INICIO, fecha_inicio);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_FIN, fecha_fin);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_PARTICIPANTES, participantes);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_DESCRIPCION_COMPANIA, descripcion_compania);
        values.put(COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_VISITA_ALERTA_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_VISITA_ALERTA_USUARIO, usuario);
        values.put(COLUMNA_VISITA_ALERTA_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        long id_actividad_competencia = database.insert(TABLA_ACTIVIDAD_COMPETENCIA, null, values);

        database.close();
        return id_actividad_competencia;
    }


    public ArrayList<CatalogoActividades> getActividadCategoria() {

        CatalogoActividades catalogoActividades;
        ArrayList<CatalogoActividades> arregloActividad = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ID_ACTIVIDAD_CATEGORIA, CATEGORIA FROM ACTIVIDAD_CATEGORIA WHERE ACTIVIDAD_CATEGORIA.ESTATUS = 1 ORDER BY CATEGORIA", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_ACTIVIDAD = Integer.parseInt(cursor.getString(0));
                String CATEGORIA = cursor.getString(1);
                catalogoActividades = new CatalogoActividades(ID_ACTIVIDAD, CATEGORIA);
                arregloActividad.add(catalogoActividades);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloActividad;
    }


    public long insertIncidenciProducto(int id_categoria_incidencia, int id_sucursal, int id_producto, String descripcion, int ban_medicion, String usuario) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_ID_CATEGORIA, id_categoria_incidencia);
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_ID_SUCURSAL, id_sucursal);
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_ID_PRODUCTO, id_producto);
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_DESCRIPCION, descripcion);
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_BAN_MEDICICON, ban_medicion);
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_FECHA, getDateTime());
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_USUARIO, usuario);
        values.put(COLUMNA_PRODUCTO_INCIDENCIA_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        long id_producto_incidencia = database.insert(TABLA_PRODUCTO_INCIDENCIA, null, values);

        database.close();
        return id_producto_incidencia;
    }


    public ArrayList<Compania> getCompanias() {

        ArrayList<Compania> arregloCompania = new ArrayList<>();
        Compania compania;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT DISTINCT COMPANIA.ID_COMPANIA, COMPANIA.NOMBRE FROM COMPANIA WHERE COMPANIA.TIPO_COMPANIA = 1 AND COMPANIA.ESTATUS = 1 AND COMPANIA.NOMBRE != 'OTROS' ORDER BY NOMBRE", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_COMPANIA = Integer.parseInt(cursor.getString(0));
                String NOMBRE_COMPANIA = cursor.getString(1);

                compania = new Compania(ID_COMPANIA, NOMBRE_COMPANIA);
                arregloCompania.add(compania);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloCompania;
    }


    public String getCompania(int idCompania) {

        String nCompania = "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COMPANIA.NOMBRE FROM COMPANIA WHERE COMPANIA.ID_COMPANIA = " + idCompania + ";", null);

        if (cursor.moveToFirst()) {
            nCompania = cursor.getString(0);
        }

        cursor.close();
        return nCompania;
    }


    public ArrayList<AdapterActividadesCompetencia> getActividadescompetencia(int id_sucursal) {

        ArrayList<AdapterActividadesCompetencia> arregloActividadCompetencia = new ArrayList<>();
        AdapterActividadesCompetencia actividadCompetencia;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select ACTIVIDAD_COMPETENCIA.ID_ACTIVIDAD, ACTIVIDAD_CATEGORIA.CATEGORIA, COMPANIA.NOMBRE, " +
                "ACTIVIDAD_COMPETENCIA.DESCRIPCION, ACTIVIDAD_COMPETENCIA.PRODUCTO, ACTIVIDAD_COMPETENCIA.FECHA_INICIO, " +
                "ACTIVIDAD_COMPETENCIA.FECHA_FIN, ACTIVIDAD_COMPETENCIA.FECHA_CREACION, ACTIVIDAD_COMPETENCIA.ESTATUS2, " +
                "ACTIVIDAD_COMPETENCIA.DESCRIPCION_COMPANIA, " +
                "IFNULL(ACTIVIDAD_COMPETENCIA.PARTICIPANTES ,0) " +
                "From ACTIVIDAD_COMPETENCIA " +
                "INNER JOIN ACTIVIDAD_CATEGORIA ON ACTIVIDAD_COMPETENCIA.ID_ACTIVIDAD_CATEGORIA = ACTIVIDAD_CATEGORIA.ID_ACTIVIDAD_CATEGORIA " +
                "INNER JOIN COMPANIA ON ACTIVIDAD_COMPETENCIA.ID_COMPANIA = COMPANIA.ID_COMPANIA " +
                "WHERE ACTIVIDAD_COMPETENCIA.ID_SUCURSAL = " + id_sucursal + " AND ACTIVIDAD_COMPETENCIA.ESTATUS = 1 " +
                "ORDER BY ACTIVIDAD_COMPETENCIA.FECHA_CREACION DESC;", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_ACTIVIDAD_COMPETENCIA = Integer.parseInt(cursor.getString(0));
                String CATEOGRIA = cursor.getString(1);
                String NOMBRE_COMPANIA = cursor.getString(2);
                String DESCRIPCION = cursor.getString(3);
                String PRODUCTO = cursor.getString(4);
                String FECHA_INICIO = cursor.getString(5);
                String FECHA_FIN = cursor.getString(6);
                String FECHA_CREACION = cursor.getString(7);
                int ESTATUS_DOS = Integer.parseInt(cursor.getString(8));
                String DESCRIPCION_COMPANIA = cursor.getString(9);
                int PARTICIPANTES = Integer.parseInt(cursor.getString(10));

                actividadCompetencia = new AdapterActividadesCompetencia(ID_ACTIVIDAD_COMPETENCIA, CATEOGRIA, NOMBRE_COMPANIA, DESCRIPCION, PRODUCTO, FECHA_INICIO, FECHA_FIN, FECHA_CREACION, ESTATUS_DOS, DESCRIPCION_COMPANIA, PARTICIPANTES);
                arregloActividadCompetencia.add(actividadCompetencia);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arregloActividadCompetencia;
    }


    public void updateActividadCompetencia(int id_actividad, int id_categoria_actividad, int id_compania, int id_sucursal, String producto, String descripcion, String participantes, String fechaIncio, String fechaFin, int estatus_dos, String username) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_ACTIVIDAD_COMPETENCIA + " SET " + COLUMNA_ACTIVIDAD_COMPETENCIA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_ACTIVIDAD_CATEGORIA + " = '" + id_categoria_actividad + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_COMPANIA + " = '" + id_compania + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_PRODUCTO + " = '" + producto + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_DESCRIPCION + " = '" + descripcion + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_PARTICIPANTES + " = '" + participantes + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_INICIO + " = '" + fechaIncio + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_FECHA_FIN + " = '" + fechaFin + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_USUARIO + " = '" + username + "', " + COLUMNA_ESTATUS_DOS + " = " + estatus_dos + " WHERE " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID + " = " + id_actividad + " AND " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_SUCURSAL + " = " + id_sucursal + "";
        database.execSQL(strSQL);
        database.close();
    }

    public void eliminarActividadCompetencia(int id_actividad, int id_sucursal, String usuario, int estatus_dos) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_ACTIVIDAD_COMPETENCIA + " SET " + COLUMNA_ACTIVIDAD_COMPETENCIA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_ACTIVIDAD_COMPETENCIA_ESTATUS + " = '" + 0 + "', " + COLUMNA_ESTATUS_DOS + " = " + estatus_dos + ", " + COLUMNA_ACTIVIDAD_COMPETENCIA_USUARIO + " = '" + usuario + "' WHERE " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID + " = " + id_actividad + " AND " + COLUMNA_ACTIVIDAD_COMPETENCIA_ID_SUCURSAL + " = " + id_sucursal + ";";
        database.execSQL(strSQL);
        database.close();
    }


    public ArrayList<AdapterProductoIncidencia> getIncidencias(int id_sucursal, int id_producto) {

        ArrayList<AdapterProductoIncidencia> arrgeloIncidencias = new ArrayList<>();
        AdapterProductoIncidencia Incidencia;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select PRODUCTO_INCIDENCIA.ID_PRODUCTO_INCIDENCIA, PRODUCTO_INCIDENCIA.ID_CATEGORIA_INCIDENCIA, CATEGORIA_INCIDENCIA.CATEGORIA, PRODUCTO_INCIDENCIA.BAN_MEDICION, PRODUCTO_INCIDENCIA.DESCRIPCION, PRODUCTO_INCIDENCIA.FECHA, PRODUCTO_INCIDENCIA.ESTATUS2 From PRODUCTO_INCIDENCIA INNER JOIN CATEGORIA_INCIDENCIA ON PRODUCTO_INCIDENCIA.ID_CATEGORIA_INCIDENCIA = CATEGORIA_INCIDENCIA.ID_CATEGORIA_INCIDENCIA WHERE PRODUCTO_INCIDENCIA.ID_SUCURSAL = " + id_sucursal + " AND PRODUCTO_INCIDENCIA.ID_PRODUCTO = " + id_producto + " AND PRODUCTO_INCIDENCIA.ESTATUS = 1 order by datetime(PRODUCTO_INCIDENCIA.FECHA) DESC;", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_PRODUCTO_INCIDENCIA = Integer.parseInt(cursor.getString(0));
                int ID_CATEGORIA_INCIDENCIA = Integer.parseInt(cursor.getString(1));
                String CATEOGRIA = cursor.getString(2);
                int TIPO_INCIDENCIA = Integer.parseInt(cursor.getString(3));
                String DESCRIPCION = cursor.getString(4);
                String FECHA = cursor.getString(5);
                int ESTATUS_DOS = Integer.parseInt(cursor.getString(6));

                Incidencia = new AdapterProductoIncidencia(ID_PRODUCTO_INCIDENCIA, ID_CATEGORIA_INCIDENCIA, CATEOGRIA, TIPO_INCIDENCIA, DESCRIPCION, FECHA, ESTATUS_DOS);
                arrgeloIncidencias.add(Incidencia);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arrgeloIncidencias;
    }


    public void updateProductoIncidencia(int id_producto_incidencia, int id_categoria_incidencia, int id_sucursal, int id_producto, String descripcion, int ban_medicion, int estatus_dos) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_PRODUCTO_INCIDENCIA + " SET " + COLUMNA_PRODUCTO_INCIDENCIA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_PRODUCTO_INCIDENCIA_ID_CATEGORIA + " = '" + id_categoria_incidencia + "', " + COLUMNA_PRODUCTO_INCIDENCIA_DESCRIPCION + " = '" + descripcion + "', " + COLUMNA_PRODUCTO_INCIDENCIA_BAN_MEDICICON + " = '" + ban_medicion + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "' WHERE " + COLUMNA_PRODUCTO_INCIDENCIA_ID + " = " + id_producto_incidencia + " AND " + COLUMNA_PRODUCTO_INCIDENCIA_ID_SUCURSAL + " = " + id_sucursal + " AND " + COLUMNA_PRODUCTO_INCIDENCIA_ID_PRODUCTO + " = '" + id_producto + "';";
        database.execSQL(strSQL);
        database.close();
    }

    public void eliminarProductoIncidencia(int id_producto_incidencia, int id_sucursal, int id_producto, int estatus_dos) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_PRODUCTO_INCIDENCIA + " SET " + COLUMNA_PRODUCTO_INCIDENCIA_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_PRODUCTO_INCIDENCIA_ESTATUS + " = '" + 0 + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "' WHERE " + COLUMNA_PRODUCTO_INCIDENCIA_ID + " = " + id_producto_incidencia + " AND " + COLUMNA_PRODUCTO_INCIDENCIA_ID_SUCURSAL + " = " + id_sucursal + " AND " + COLUMNA_PRODUCTO_INCIDENCIA_ID_PRODUCTO + " = '" + id_producto + "';";
        database.execSQL(strSQL);
        database.close();
    }


    public double getPrecioUnitarioReal(String clave_producto) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select PRODUCTO.PRECIO_UNITARIO FROM PRODUCTO WHERE PRODUCTO.CLAVE = '" + clave_producto + "'", null);
        double precio_real = 0;

        if (cursor.moveToFirst()) {
            precio_real = Double.parseDouble(cursor.getString(0));
        }

        cursor.close();
        db.close();
        return precio_real;
    }

    public ProductoPrecio getPrecioUnitario(int idCompania, int idProducto, int idSucursal) {
        ProductoPrecio productoPrecio;
        double precio, precio_promocion;
        int _idPrecio;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " +
                "(SELECT * FROM " +
                "(SELECT PP.ID_PRECIO " +
                "FROM PRODUCTO_PRECIOS PP " +
                "WHERE PP.ID_SUCURSAL = PRODUCTO_PRECIOS.ID_SUCURSAL AND " +
                "PP.ID_PRODUCTO = PRODUCTO_PRECIOS.ID_PRODUCTO AND " +
                "PP.ID_COMPANIA = PRODUCTO_PRECIOS.ID_COMPANIA " +
                "ORDER BY PP.FECHA_CREACION DESC) LIMIT 1) ID_PRECIO, " +
                "(SELECT * FROM " +
                "(SELECT PP.PRECIO_UNITARIO " +
                "FROM PRODUCTO_PRECIOS PP " +
                "WHERE PP.ID_SUCURSAL = PRODUCTO_PRECIOS.ID_SUCURSAL AND " +
                "PP.ID_PRODUCTO = PRODUCTO_PRECIOS.ID_PRODUCTO AND " +
                "PP.ID_COMPANIA = PRODUCTO_PRECIOS.ID_COMPANIA " +
                "ORDER BY PP.FECHA_CREACION DESC) LIMIT 1) PRECIO_UNITARIO, " +
                "(SELECT * FROM " +
                "(SELECT PP.PRECIO_PROMOCION " +
                "FROM PRODUCTO_PRECIOS PP " +
                "WHERE PP.ID_SUCURSAL = PRODUCTO_PRECIOS.ID_SUCURSAL AND " +
                "PP.ID_PRODUCTO = PRODUCTO_PRECIOS.ID_PRODUCTO AND " +
                "PP.ID_COMPANIA = PRODUCTO_PRECIOS.ID_COMPANIA " +
                "ORDER BY PP.FECHA_CREACION DESC) LIMIT 1) PRECIO_PROMOCION, " +
                "COMPANIA.NOMBRE, PRODUCTO_PRECIOS.DESCRIPCION_COMPANIA  " +
                "FROM PRODUCTO_PRECIOS " +
                "INNER JOIN COMPANIA ON PRODUCTO_PRECIOS.ID_COMPANIA = COMPANIA.ID_COMPANIA  " +
                "WHERE ID_SUCURSAL = '" + idSucursal + "' AND ID_PRODUCTO = '" + idProducto + "' AND Compania.ID_COMPANIA = '" + idCompania + "' and PRODUCTO_PRECIOS.ESTATUS = 1 " +
                "GROUP BY COMPANIA.NOMBRE, PRODUCTO_PRECIOS.DESCRIPCION_COMPANIA, " +
                "PRODUCTO_PRECIOS.ID_SUCURSAL, PRODUCTO_PRECIOS.ID_PRODUCTO, PRODUCTO_PRECIOS.ID_COMPANIA;", null);

        if (cursor.moveToFirst()) {
            do {
                if (cursor != null && cursor.getString(0) != null) {
                    _idPrecio = Integer.parseInt(cursor.getString(0));
                    precio = Double.parseDouble(cursor.getString(1));
                    precio_promocion = Double.parseDouble(cursor.getString(2));
                    productoPrecio = new ProductoPrecio(_idPrecio, precio, precio_promocion);
                } else {
                    productoPrecio = new ProductoPrecio(0, 0, 0);
                }
            } while (cursor.moveToNext());
        } else {
            productoPrecio = new ProductoPrecio(0, 0, 0);
        }

        cursor.close();
        db.close();
        return productoPrecio;
    }


    public ProductoPrecio getPrecioUnitario(int idCompania, String claveProdcuto, int idSucursal, String descripcionCompania) {
        ProductoPrecio productoPrecio;
        double precio, precio_promocion;
        int _idPrecio;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select PRODUCTO_PRECIOS.ID_PRECIO, PRODUCTO_PRECIOS.PRECIO_UNITARIO, PRODUCTO_PRECIOS.PRECIO_PROMOCION from PRODUCTO_PRECIOS " +
                "INNER JOIN Compania ON PRODUCTO_PRECIOS.ID_COMPANIA = Compania.ID_COMPANIA " +
                "INNER JOIN  PRODUCTO ON PRODUCTO_PRECIOS.ID_PRODUCTO = PRODUCTO.ID_PRODUCTO " +
                "WHERE Compania.ID_COMPANIA = '" + idCompania + "' " +
                "AND PRODUCTO.CLAVE = '" + claveProdcuto + "' " +
                "AND PRODUCTO_PRECIOS.ID_SUCURSAL = '" + idSucursal + "' " +
                "AND PRODUCTO_PRECIOS.DESCRIPCION_COMPANIA LIKE '%" + descripcionCompania + "%'", null);

        if (cursor.moveToFirst()) {
            do {
                if (cursor != null && cursor.getString(0) != null) {
                    _idPrecio = Integer.parseInt(cursor.getString(0));
                    precio = Double.parseDouble(cursor.getString(1));
                    precio_promocion = Double.parseDouble(cursor.getString(2));
                    productoPrecio = new ProductoPrecio(_idPrecio, precio, precio_promocion);
                } else {
                    productoPrecio = new ProductoPrecio(0, 0, 0);
                }
            } while (cursor.moveToNext());
        } else {
            productoPrecio = new ProductoPrecio(0, 0, 0);
        }

        cursor.close();
        db.close();
        return productoPrecio;
    }


    public List<ProductoPrecio> getAllPrecios(int idSucursal, int idProducto) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " +
                "(SELECT * FROM " +
                "(SELECT PP.ID_PRECIO " +
                "FROM PRODUCTO_PRECIOS PP " +
                "WHERE PP.ID_SUCURSAL = PRODUCTO_PRECIOS.ID_SUCURSAL AND " +
                "PP.ID_PRODUCTO = PRODUCTO_PRECIOS.ID_PRODUCTO AND " +
                "PP.ID_COMPANIA = PRODUCTO_PRECIOS.ID_COMPANIA AND  PP.ESTATUS = PRODUCTO_PRECIOS.ESTATUS " +
                "ORDER BY PP.FECHA_CREACION DESC) LIMIT 1) ID_PRECIO, " +
                "(SELECT * FROM " +
                "(SELECT PP.PRECIO_UNITARIO " +
                "FROM PRODUCTO_PRECIOS PP " +
                "WHERE PP.ID_SUCURSAL = PRODUCTO_PRECIOS.ID_SUCURSAL AND " +
                "PP.ID_PRODUCTO = PRODUCTO_PRECIOS.ID_PRODUCTO AND " +
                "PP.ID_COMPANIA = PRODUCTO_PRECIOS.ID_COMPANIA AND  PP.ESTATUS = PRODUCTO_PRECIOS.ESTATUS " +
                "ORDER BY PP.FECHA_CREACION DESC) LIMIT 1) PRECIO_UNITARIO, " +
                "(SELECT * FROM " +
                "(SELECT PP.PRECIO_PROMOCION " +
                "FROM PRODUCTO_PRECIOS PP " +
                "WHERE PP.ID_SUCURSAL = PRODUCTO_PRECIOS.ID_SUCURSAL AND " +
                "PP.ID_PRODUCTO = PRODUCTO_PRECIOS.ID_PRODUCTO AND " +
                "PP.ID_COMPANIA = PRODUCTO_PRECIOS.ID_COMPANIA AND  PP.ESTATUS = PRODUCTO_PRECIOS.ESTATUS " +
                "ORDER BY PP.FECHA_CREACION DESC) LIMIT 1) PRECIO_PROMOCION, " +
                "COMPANIA.NOMBRE, PRODUCTO_PRECIOS.DESCRIPCION_COMPANIA, PRODUCTO_PRECIOS.ULTIMA_MODIFICACION, PRODUCTO_PRECIOS.ID_COMPANIA, PRODUCTO_PRECIOS.ESTATUS2  " +
                "FROM PRODUCTO_PRECIOS " +
                "INNER JOIN COMPANIA ON PRODUCTO_PRECIOS.ID_COMPANIA = COMPANIA.ID_COMPANIA  " +
                "WHERE ID_SUCURSAL = '" + idSucursal + "' AND ID_PRODUCTO = '" + idProducto + "' and PRODUCTO_PRECIOS.ESTATUS = 1 " +
                "GROUP BY COMPANIA.NOMBRE, PRODUCTO_PRECIOS.DESCRIPCION_COMPANIA, " +
                "PRODUCTO_PRECIOS.ID_SUCURSAL, PRODUCTO_PRECIOS.ID_PRODUCTO, PRODUCTO_PRECIOS.ID_COMPANIA;", null);


        List<ProductoPrecio> listPrecios = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                ProductoPrecio productoPrecio = new ProductoPrecio();
                productoPrecio.setId(Integer.parseInt(cursor.getString(0)));
                productoPrecio.setPrecio_unitario(Double.parseDouble(cursor.getString(1)));
                productoPrecio.setPrecio_promocion(Double.parseDouble(cursor.getString(2)));
                productoPrecio.setCompania(cursor.getString(3));
                productoPrecio.setDescripcionCompania(cursor.getString(4));
                productoPrecio.setUltima_modificacion(cursor.getString(5));
                productoPrecio.setId_compania(Integer.parseInt(cursor.getString(6)));
                productoPrecio.setEstatus(Integer.parseInt(cursor.getString(7)));
                listPrecios.add(productoPrecio);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return listPrecios;
    }

    public void deleteProductoPrecio(int idPrecio, int idSucursal, int estatus2) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_PRODUCTO_PRECIO + " SET " + COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ESTATUS + " = 0, " + COLUMNA_ESTATUS_DOS + " = " + estatus2 + " WHERE " + COLUMNA_PRODUCTO_PRECIO_ID + " = " + idPrecio + " AND " + COLUMNA_PRODUCTO_PRECIOID_SUCURSAL + " = " + idSucursal;
        database.execSQL(strSQL);
        database.close();
    }


    public void insertPrecioUnitario(int idCompania, double precio, double precio_promocion, int id_sucursal, int id_producto, String usuario, String descripcionCompania) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_PRODUCTO_PRECIOID_PRODUCTO, id_producto);
        values.put(COLUMNA_PRODUCTO_PRECIOID_SUCURSAL, id_sucursal);
        values.put(COLUMNA_PRODUCTO_PRECIOID_COMPANIA, idCompania);
        values.put(COLUMNA_PRODUCTO_PRECIO_PRECIO_UNITARIO, precio);
        values.put(COLUMNA_PRODUCTO_PRECIO_PRECIO_PROMOCION, precio_promocion);
        values.put(COLUMNA_PRODUCTO_PRECIO_APLICA_IVA, 0);
        values.put(COLUMNA_PRODUCTO_PRECIO_DESCRIPCION_COMPANIA, descripcionCompania);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_PRECIO_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ULTIMA_MODIFICACION, getDateTime());
        values.put(COLUMNA_SUCURSAL_PRODUCTO_PRECIO_USUARIO, usuario);
        values.put(COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        database.insert(TABLA_PRODUCTO_PRECIO, null, values);
        database.close();

    }

    public void updatePrecioUnitario(int id_producto_precio, double precio, double precio_promocion, int idCompania, int id_sucursal, String descripcionCompania, String usuario, int estatus_dos) {

        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "UPDATE " + TABLA_PRODUCTO_PRECIO + " SET " + COLUMNA_SUCURSAL_PRODUCTO_PRECIO_ULTIMA_MODIFICACION + " = '" + getDateTime() + "', " + COLUMNA_PRODUCTO_PRECIO_PRECIO_UNITARIO + " = '" + precio + "', " + COLUMNA_PRODUCTO_PRECIO_PRECIO_PROMOCION + " = '" + precio_promocion + "', " + COLUMNA_PRODUCTO_PRECIO_DESCRIPCION_COMPANIA + " = '" + descripcionCompania + "', " + COLUMNA_SUCURSAL_PRODUCTO_PRECIO_USUARIO + " = '" + usuario + "', " + COLUMNA_ESTATUS_DOS + " = '" + estatus_dos + "' WHERE " + COLUMNA_PRODUCTO_PRECIO_ID + " = " + id_producto_precio + " AND " + COLUMNA_PRODUCTO_PRECIOID_COMPANIA + " = " + idCompania + " AND " + COLUMNA_PRODUCTO_PRECIOID_SUCURSAL + " = " + id_sucursal + "";
        database.execSQL(strSQL);
        database.close();
    }


    public int getSucursalesTotal(int dia) {

        int SUCURSALES_TOTAL = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select count(*) from dias_sucursal_supervisor where id_dia = " + dia + " and estatus= 1;", null);

        if (cursor.moveToFirst()) {
            SUCURSALES_TOTAL = Integer.parseInt(cursor.getString(0));
        }

        cursor.close();
        return SUCURSALES_TOTAL;

    }


    public boolean getRutaAlternaActiva(int dia) {
        boolean RUTA_ACTIVA = false;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT DISTINCT Dias_Sucursal_Supervisor.BAN_PRIORIDAD FROM Dias_Sucursal_Supervisor  WHERE id_dia = " + dia + " and estatus= 1;", null);

        if (cursor.moveToFirst()) {
            int index = Integer.parseInt(cursor.getString(0));
            if (index == 1) {
                RUTA_ACTIVA = true;
            }
        }

        cursor.close();
        db.close();
        return RUTA_ACTIVA;
    }


    public ArrayList<String> getTables() {
        ArrayList<String> tablas = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' and name not like '%android%';", null);

        if (cursor.moveToFirst()) {
            do {
                tablas.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return tablas;
    }


    public void clearDB(String tabla) {
        SQLiteDatabase database = this.getWritableDatabase();
        String strSQL = "DELETE FROM " + tabla + "";
        database.execSQL(strSQL);
    }

    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
