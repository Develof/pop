package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;

/**
 * Created by Develof on 28/10/16.
 */

public class DispositivoBDModel implements Serializable {

    public static final String ID_Usuario = "ID_USUARIO";
    public static final String ID_UDN = "ID_UDN";
    public static final String FECHA = "FECHA";
    public static final String NO_SERIE = "NO_SERIE";
    public static final String IMEI = "IMEI";
    public static final String SIM = "SIM";
    public static final String MODELO = "MODELO";
    public static final String ANDROID = "ANDROID";
    public static final String VERSION = "VERSION";
    public static final String FECHA_CREACION = "FECHA_CREACION";
    public static final String ULTIMA_MODIFICACION = "ULTIMA_MODIFICACION";
    public static final String USUARIO = "USUARIO";
    public static final String ESTATUS_DOS = "ESTATUS2";
    public static final String VERSION_BD = "VERSION_BD";

    Integer idDispositivo;

    Integer idUsario;

    Integer idUdn;

    String fecha;

    String noSerie;

    String imei;

    String sim;

    String modelo;

    String android;

    String version;

    Integer estatusDos;

    String versionBd;


    public Integer getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(Integer idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public Integer getIdUsario() {
        return idUsario;
    }

    public void setIdUsario(Integer idUsario) {
        this.idUsario = idUsario;
    }

    public Integer getIdUdn() {
        return idUdn;
    }

    public void setIdUdn(Integer idUdn) {
        this.idUdn = idUdn;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNoSerie() {
        return noSerie;
    }

    public void setNoSerie(String noSerie) {
        this.noSerie = noSerie;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAndroid() {
        return android;
    }

    public void setAndroid(String android) {
        this.android = android;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getEstatusDos() {
        return estatusDos;
    }

    public void setEstatusDos(Integer estatusDos) {
        this.estatusDos = estatusDos;
    }

    public String getVersionBd() {
        return versionBd;
    }

    public void setVersionBd(String versionBd) {
        this.versionBd = versionBd;
    }
}
