package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by oflores on 19/07/16.
 */
public class AdapterEspacioIncidencia {

    private int id_espacio_incidencia;
    private int id_categoria;
    private String categoria;
    private int tipo_incidencia;
    private String descripcion;
    private String fecha;

    private String espacio_nombre;
    private int id_sucursal_espacio;

    AdapterEspacioIncidencia(){

    }

    public AdapterEspacioIncidencia(int id_espacio_incidencia, int id_categoria, String categoria, int tipo_incidencia, String descripcion, String fecha) {
        this.id_espacio_incidencia = id_espacio_incidencia;
        this.id_categoria = id_categoria;
        this.categoria = categoria;
        this.tipo_incidencia = tipo_incidencia;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public AdapterEspacioIncidencia(int id_espacio_incidencia, int id_categoria, int id_sucursal_espacio, String espacio_nombre, String categoria, int tipo_incidencia, String descripcion, String fecha) {
        this.id_espacio_incidencia = id_espacio_incidencia;
        this.id_categoria = id_categoria;
        this.id_sucursal_espacio = id_sucursal_espacio;
        this.espacio_nombre = espacio_nombre;
        this.categoria = categoria;
        this.tipo_incidencia = tipo_incidencia;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public int getId_espacio_incidencia() {
        return id_espacio_incidencia;
    }

    public void setId_espacio_incidencia(int id_espacio_incidencia) {
        this.id_espacio_incidencia = id_espacio_incidencia;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getTipo_incidencia() {
        return tipo_incidencia;
    }

    public void setTipo_incidencia(int tipo_incidencia) {
        this.tipo_incidencia = tipo_incidencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEspacio_nombre() {
        return espacio_nombre;
    }

    public void setEspacio_nombre(String espacio_nombre) {
        this.espacio_nombre = espacio_nombre;
    }

    public int getId_sucursal_espacio() {
        return id_sucursal_espacio;
    }

    public void setId_sucursal_espacio(int id_sucursal_espacio) {
        this.id_sucursal_espacio = id_sucursal_espacio;
    }
}
