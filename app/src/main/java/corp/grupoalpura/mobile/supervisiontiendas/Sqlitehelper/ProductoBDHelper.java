package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Producto;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;

public class ProductoBDHelper extends DBHelper {


    public ProductoBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }


    public FichaTecnicaProducto getProductoUPC(String upc, Integer idSucursal) {

        FichaTecnicaProducto producto = new FichaTecnicaProducto();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT PRODUCTO.ID_PRODUCTO, " +
                "PRODUCTO.UPC, " +
                "PRODUCTO.CLAVE, " +
                "PRODUCTO.NOMBRE, " +
                "PRODUCTO_PRESENTACION.PRESENTACION, " +
                "PRODUCTO_PRESENTACION.ID_PRESENTACION, " +
                "PRODUCTO.ID_COMPANIA, " +
                "PRODUCTO.UPC, " +
                "PRODUCTO.DESCRIPCION " +
                "FROM PRODUCTO_CATALOGADO " +
                "INNER JOIN PRODUCTO ON PRODUCTO_CATALOGADO.ID_PRODUCTO = PRODUCTO.ID_PRODUCTO " +
                "INNER JOIN PRODUCTO_PRESENTACION ON PRODUCTO.ID_PRESENTACION = PRODUCTO_PRESENTACION.ID_PRESENTACION " +
                "INNER JOIN PRODUCTO_FAMILIA ON PRODUCTO.ID_FAMILIA = PRODUCTO_FAMILIA.ID_FAMILIA " +
                "WHERE PRODUCTO_CATALOGADO.ID_SUCURSAL = " + idSucursal +
                " AND PRODUCTO_CATALOGADO.ESTATUS = 1 AND PRODUCTO.UPC = " + upc + ";", null);

        if (cursor.moveToFirst()) {
            producto.setId(Integer.parseInt(cursor.getString(0)));
            producto.setUPC(cursor.getString(1));
            producto.setClave(cursor.getString(2));
            producto.setDescripcion(cursor.getString(3));
            producto.setPresentacion(cursor.getString(4));
            producto.setIdPresentacion(Integer.parseInt(cursor.getString(5)));
            producto.setId_compania(Integer.parseInt(cursor.getString(6)));
            producto.setUPC(cursor.getString(7));
            producto.setNombre(cursor.getString(8));
        }

        cursor.close();
        db.close();
        return producto;
    }


    public long creaNuevoProducto(Producto producto, ClienteSucursal sucursal, Usuario usuario) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMNA_PRODUCTO_ID_PRESENTACION, producto.getId_presentacion());
        values.put(COLUMNA_PRODUCTO_ID_FAMILIA, producto.getId_familia());
        values.put(COLUMNA_PRODUCTO_ID_COMPANIA, producto.getId_compania());
        values.put(COLUMNA_PRODUCTO_CLAVE, producto.getClave());
        values.put(COLUMNA_PRODUCTO_UNIDAD, producto.getUnidad());
        values.put(COLUMNA_PRODUCTO_UPC, producto.getUpc());
        values.put(COLUMNA_PRODUCTO_NOMBRE, producto.getDescripcion());
        values.put(COLUMNA_PRODUCTO_DESCRIPCION, producto.getDescripcion());
        values.put(COLUMNA_PRODUCTO_PRECIO_UNITARIO, producto.getPrecio_unitario());
        values.put(COLUMNA_PRODUCTO_APLICA_IVA, 1);
        values.put(COLUMNA_PRODUCTO_FECHA_CREACION, getDateTime());
        values.put(COLUMNA_PRODUCTO_ACTIVIDAD_MODIFICACION, getDateTime());
        values.put(COLUMNA_PRODUCTO_USUARIO, "SYSADMIN");
        values.put(COLUMNA_PRODUCTO_ESTATUS, 1);
        values.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        long idProd = database.insert(TABLA_PRODUCTO, null, values);


        ContentValues valuesCat = new ContentValues();
        valuesCat.put(COLUMNA_PRODUCTO_CATALOGADO_ID_SUCURSAL, sucursal.getId());
        valuesCat.put(COLUMNA_PRODUCTO_CATALOGADO_ID_PRODUCTO, idProd);
        valuesCat.put(COLUMNA_PRODUCTO_CATALOGADO_FECHA_CREACION, getDateTime());
        valuesCat.put(COLUMNA_PRODUCTO_CATALOGADO_ULTIMA_MODIFICACION, getDateTime());
        valuesCat.put(COLUMNA_PRODUCTO_CATALOGADO_USUARIO, usuario.getUsername());
        valuesCat.put(COLUMNA_PRODUCTO_CATALOGADO_ESTATUS, 1);
        valuesCat.put(COLUMNA_ESTATUS_DOS, Constantes.ESTATUS.CREADO_MOVIL);
        database.insert(TABLA_PRODUCTO_CATALOGADO, null, valuesCat);

        database.close();
        return  idProd;
    }

}
