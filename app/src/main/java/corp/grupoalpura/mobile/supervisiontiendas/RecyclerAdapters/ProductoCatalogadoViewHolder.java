package corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.R;


/**
 * Created by Develof on 20/11/16.
 */

public class ProductoCatalogadoViewHolder extends RecyclerView.ViewHolder  {

    @BindView(R.id.img_star_rate)
    ImageView imgRate;
    ImageView imgDescripcion;
    TextView txtClveProducto, txtNombreProducto, txtPresentcionProducto, txtPrecioBaseProducto, txtPrecioBaseTitulo;
    RelativeLayout relativeLayoutProductoExhibido;
    LinearLayout contenidoPrecios;
    CardView cardViewProducto;
    CheckBox checkBoxExhibido;

    public ProductoCatalogadoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        cardViewProducto =  itemView.findViewById(R.id.card_view_producto_catalogado);

        imgDescripcion =  itemView.findViewById(R.id.img_descripcion);
        txtClveProducto =  itemView.findViewById(R.id.textView_value_productos_catalogo_clave);
        txtPresentcionProducto =  itemView.findViewById(R.id.textView_value_productos_catalogo_presentacion);
        txtNombreProducto =  itemView.findViewById(R.id.textView_value_productos_catalogo_nombre);
        txtPrecioBaseProducto =  itemView.findViewById(R.id.textView_value_productos_catalogo_prec);
        txtPrecioBaseTitulo =  itemView.findViewById(R.id.textView_title_productos_catalogo_prec);
        contenidoPrecios =  itemView.findViewById(R.id.contenedor_producto_companias);
        relativeLayoutProductoExhibido =  itemView.findViewById(R.id.relativeLayout_producto_catalogo_exhibido);
        checkBoxExhibido =  itemView.findViewById(R.id.cb_guia_mercadeo);
    }
}
