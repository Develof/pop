package corp.grupoalpura.mobile.supervisiontiendas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterEspacioFrente;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Espacios;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.SucursalEspacio;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters.EspacioFrenteAdapter;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.EspacioFrenteBDHelper;

/**
 *
 */
public class EspacioFrenteFragment extends Fragment {

    private static final String ID_SUCURSAL = "id_sucursal";
    private static final String ID_USUARIO = "Usuario";
    private static final String ID_ESPACIO = "ID_ESPACIO";
    private static final String SUCURSAL_ESPACIO = "SUCURSAL_ESPACIO";

    private ClienteSucursal sucursal;
    private Usuario usuario;
    private SucursalEspacio espacio;
    private int idEspacio = 0;
    private EspacioFrenteBDHelper dbHelper;
    private List<AdapterEspacioFrente> frentes;

    @BindView(R.id.rv_espacio_frentes_prod)
    RecyclerView rvFrentes;

    /**
     * @param sucursal  Sucursal
     * @param usuario   Usuario
     * @param espacio   SucursalEspacio
     * @param idEspacio id del espacio
     * @return A new instance of fragment EspacioFrenteFragment.
     */
    public static EspacioFrenteFragment newInstance(ClienteSucursal sucursal, Usuario usuario, SucursalEspacio espacio, Integer idEspacio) {
        EspacioFrenteFragment fragment = new EspacioFrenteFragment();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(ID_USUARIO, usuario);
        args.putSerializable(SUCURSAL_ESPACIO, espacio);
        args.putSerializable(ID_ESPACIO, idEspacio);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            usuario = (Usuario) getArguments().getSerializable(ID_USUARIO);
            espacio = (SucursalEspacio) getArguments().getSerializable(SUCURSAL_ESPACIO);
            idEspacio = (Integer) getArguments().getSerializable(ID_ESPACIO);
        }
        dbHelper = new EspacioFrenteBDHelper(getActivity());
        Espacios espacio_ = dbHelper.getEspacio(idEspacio);
        ((Contenedor) getActivity()).getIdEspacio(idEspacio, espacio_);
        frentes = dbHelper.getFrentes(espacio.getIdSucursalEspacio());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_espacio_frente, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFrentes.setHasFixedSize(true);
        rvFrentes.setLayoutManager(linearLayoutManager);

        EspacioFrenteAdapter adapter = new EspacioFrenteAdapter(getActivity(), espacio, frentes, idEspacio, sucursal, usuario);
        rvFrentes.setAdapter(adapter);

        return view;
    }

}
