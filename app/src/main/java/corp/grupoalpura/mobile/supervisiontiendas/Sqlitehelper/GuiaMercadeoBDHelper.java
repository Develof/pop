package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.GuiaMercadeoModel;
import corp.grupoalpura.mobile.supervisiontiendas.Usos.Dates;

public class GuiaMercadeoBDHelper extends DBHelper {


    public GuiaMercadeoBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public List<GuiaMercadeoModel> getGuiaMercadeo(int id_cadena) {

        List<GuiaMercadeoModel> arreglo = new ArrayList<>();
        GuiaMercadeoModel guiaMercadeoModel;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT DISTINCT GUIA_MERCADEO.ID_GUIA_MERCADEO, ID_CADENA, DESCRIPCION_PRODUCTO, " +
                "PROMOCION, VIGENCIA, PRECIO_OFERTA, REGION, IFNULL(GUIA_MERCADEO_FALTANTE.ESTATUS,1) " +
                "FROM GUIA_MERCADEO " +
                "LEFT JOIN GUIA_MERCADEO_FALTANTE ON GUIA_MERCADEO.ID_GUIA_MERCADEO = GUIA_MERCADEO_FALTANTE.ID_GUIA_MERCADEO " +
                "WHERE GUIA_MERCADEO.ID_CADENA = " + id_cadena + " " +
                "ORDER BY DESCRIPCION_PRODUCTO ASC", null);

        if (cursor.moveToFirst()) {
            do {

                int idGuiaMercadeo = cursor.getInt(0);
                int idCadena = cursor.getInt(1);
                String producto = cursor.getString(2);
                String promocion = cursor.getString(3);
                String vigencia = cursor.getString(4);
                String precio_oferta = cursor.getString(5);
                String region = cursor.getString(6);
                int estatus = cursor.getInt(7);

                guiaMercadeoModel = new GuiaMercadeoModel(idGuiaMercadeo, idCadena, promocion, producto, vigencia, precio_oferta, region, estatus);
                arreglo.add(guiaMercadeoModel);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return arreglo;
    }


    public GuiaMercadeoModel getPromocionGuia(String descProd, int idSucursal) {

        GuiaMercadeoModel guia = new GuiaMercadeoModel();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT PROMOCION, VIGENCIA, PRECIO_OFERTA FROM GUIA_MERCADEO " +
                "WHERE DESCRIPCION_PRODUCTO = '" + descProd + "' " +
                " AND id_cadena = " + idSucursal + "" +
                " LIMIT 1", null);

        if (cursor.moveToFirst()) {
            guia.setPromocion(cursor.getString(0));
            guia.setVigencia(cursor.getString(1));
            guia.setPrecioOferta(cursor.getString(2));
        }

        cursor.close();
        db.close();
        return guia;
    }


}
