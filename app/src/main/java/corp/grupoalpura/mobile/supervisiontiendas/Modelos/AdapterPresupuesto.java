package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 31/03/2016.
 */
public class AdapterPresupuesto {
    private int id_presupuesto;
    private int id_udn;
    private int id_cliente;
    private int id_cadena;
    private int id_agrupacion;
    private int id_supervisor;
    private int id_sucursal;
    private String cadena;
    private String sucursal;
    private String udn;
    private String grupo;
    private String sub_grupo;
    private double promotoria;
    private String meta_mes_anterior;
    private String obj_mes_actual;
    private String proyeccion_mes_actual;
    private String diferencia_litros;
    private String porcentaje_avance;
    private String supervisor;
    private String agrupacion;
    private String fecha;

    AdapterPresupuesto(){}

    public AdapterPresupuesto(String sucursal, String agrupacion, double promotoria, String meta_mes_anterior, String obj_mes_actual, String proyeccion_mes_actual, String diferencia_litros, String porcentaje_avance) {
        this.sucursal = sucursal;
        this.agrupacion = agrupacion;
        this.promotoria = promotoria;
        this.meta_mes_anterior = meta_mes_anterior;
        this.obj_mes_actual = obj_mes_actual;
        this.proyeccion_mes_actual = proyeccion_mes_actual;
        this.diferencia_litros = diferencia_litros;
        this.porcentaje_avance = porcentaje_avance;
    }

    public AdapterPresupuesto(int id_sucursal, String grupo, String sub_grupo, String sucursal, double promotoria, String meta_mes_anterior, String obj_mes_actual, String proyeccion_mes_actual, String diferencia_litros, String porcentaje_avance) {
        this.grupo = grupo;
        this.sub_grupo = sub_grupo;
        this.sucursal = sucursal;
        this.promotoria = promotoria;
        this.meta_mes_anterior = meta_mes_anterior;
        this.obj_mes_actual = obj_mes_actual;
        this.proyeccion_mes_actual = proyeccion_mes_actual;
        this.diferencia_litros = diferencia_litros;
        this.porcentaje_avance = porcentaje_avance;
        this.id_sucursal = id_sucursal;
    }

    public AdapterPresupuesto(int id_agrupacion, String agrupacion, double promotoria, String meta_mes_anterior, String obj_mes_actual, String proyeccion_mes_actual, String diferencia_litros, String porcentaje_avance) {
        this.id_agrupacion = id_agrupacion;
        this.agrupacion = agrupacion;
        this.promotoria = promotoria;
        this.meta_mes_anterior = meta_mes_anterior;
        this.obj_mes_actual = obj_mes_actual;
        this.proyeccion_mes_actual = proyeccion_mes_actual;
        this.diferencia_litros = diferencia_litros;
        this.porcentaje_avance = porcentaje_avance;
    }


    public AdapterPresupuesto(String cadena, String meta_mes_anterior, String obj_mes_actual, String proyeccion_mes_actual, String diferencia_litros, String porcentaje_avance) {
        this.cadena = cadena;
        this.meta_mes_anterior = meta_mes_anterior;
        this.obj_mes_actual = obj_mes_actual;
        this.proyeccion_mes_actual = proyeccion_mes_actual;
        this.diferencia_litros = diferencia_litros;
        this.porcentaje_avance = porcentaje_avance;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public int getId_agrupacion() {
        return id_agrupacion;
    }

    public void setId_agrupacion(int id_agrupacion) {
        this.id_agrupacion = id_agrupacion;
    }

    public int getId_presupuesto() {
        return id_presupuesto;
    }

    public void setId_presupuesto(int id_presupuesto) {
        this.id_presupuesto = id_presupuesto;
    }

    public String getUdn() {
        return udn;
    }

    public void setUdn(String udn) {
        this.udn = udn;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getSub_grupo() {
        return sub_grupo;
    }

    public void setSub_grupo(String sub_grupo) {
        this.sub_grupo = sub_grupo;
    }

    public double getPromotoria() {
        return promotoria;
    }

    public void setPromotoria(double promotoria) {
        this.promotoria = promotoria;
    }

    public String getMeta_mes_anterior() {
        return meta_mes_anterior;
    }

    public void setMeta_mes_anterior(String meta_mes_anterior) {
        this.meta_mes_anterior = meta_mes_anterior;
    }

    public String getObj_mes_actual() {
        return obj_mes_actual;
    }

    public void setObj_mes_actual(String obj_mes_actual) {
        this.obj_mes_actual = obj_mes_actual;
    }

    public String getProyeccion_mes_actual() {
        return proyeccion_mes_actual;
    }

    public void setProyeccion_mes_actual(String proyeccion_mes_actual) {
        this.proyeccion_mes_actual = proyeccion_mes_actual;
    }

    public String getDiferencia_litros() {
        return diferencia_litros;
    }

    public void setDiferencia_litros(String diferencia_litros) {
        this.diferencia_litros = diferencia_litros;
    }

    public String getPorcentaje_avance() {
        return porcentaje_avance;
    }

    public void setPorcentaje_avance(String porcentaje_avance) {
        this.porcentaje_avance = porcentaje_avance;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getAgrupacion() {
        return agrupacion;
    }

    public void setAgrupacion(String agrupacion) {
        this.agrupacion = agrupacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
}
