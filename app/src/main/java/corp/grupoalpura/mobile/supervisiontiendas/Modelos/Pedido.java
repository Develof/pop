package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

import java.io.Serializable;

/**
 * Created by Develof on 21/06/16.
 */
public class Pedido implements Serializable {
    private int id_pedido;
    private String fecha;
    private String ruta;
    private String fechaWalmart;


    public Pedido() {}

    public Pedido(int id_pedido, String fecha, String ruta) {
        this.id_pedido = id_pedido;
        this.fecha = fecha;
        this.ruta = ruta;
    }

    public Pedido(String fechaWalmart, String fecha) {
        this.fechaWalmart = fechaWalmart;
        this.fecha = fecha;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getFechaWalmart() {
        return fechaWalmart;
    }

    public void setFechaWalmart(String fechaWalmart) {
        this.fechaWalmart = fechaWalmart;
    }
}
