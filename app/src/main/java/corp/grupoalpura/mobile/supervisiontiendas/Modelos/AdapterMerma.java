package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by omarflores on 06/06/17.
 */

public class AdapterMerma {

    int idMerma;
    int idProductoMerma;
    int idUsuario;
    int idSucursal;
    int productoFaltante;
    String fechaCaducidad;
    String fechaCreacion;
    String ultimaModificacion;
    String usuario;
    int estatus;
    int estatusDos;

    public AdapterMerma(){}

    public AdapterMerma(int idMerma, int idProductoMerma, int idUsuario, int idSucursal, int productoFaltante, String fechaCaducidad, String fechaCreacion, String ultimaModificacion, String usuario, int estatus, int estatusDos) {
        this.idMerma = idMerma;
        this.idProductoMerma = idProductoMerma;
        this.idUsuario = idUsuario;
        this.idSucursal = idSucursal;
        this.productoFaltante = productoFaltante;
        this.fechaCaducidad = fechaCaducidad;
        this.fechaCreacion = fechaCreacion;
        this.ultimaModificacion = ultimaModificacion;
        this.usuario = usuario;
        this.estatus = estatus;
        this.estatusDos = estatusDos;
    }


    public int getIdMerma() {
        return idMerma;
    }

    public void setIdMerma(int idMerma) {
        this.idMerma = idMerma;
    }

    public int getIdProductoMerma() {
        return idProductoMerma;
    }

    public void setIdProductoMerma(int idProductoMerma) {
        this.idProductoMerma = idProductoMerma;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public int getProductoFaltante() {
        return productoFaltante;
    }

    public void setProductoFaltante(int productoFaltante) {
        this.productoFaltante = productoFaltante;
    }

    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUltimaModificacion() {
        return ultimaModificacion;
    }

    public void setUltimaModificacion(String ultimaModificacion) {
        this.ultimaModificacion = ultimaModificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public int getEstatusDos() {
        return estatusDos;
    }

    public void setEstatusDos(int estatusDos) {
        this.estatusDos = estatusDos;
    }
}
