package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 29/03/2016.
 */
public class AdapterPedidos {
    private int id_detalle_pedido;
    private int id_pedido;
    private String clave;
    private int pedido;
    private String agregar;
    private String vence;
    private int inventario;
    private double dias_inventario;
    private String cambios;
    private double promedio_venta;
    private double precio_unitario;
    private String orden;
    private int pz_recibidas;
    private String upc;
    private String fillrate;
    private String forecast;
    private String dif_promventa_vs_forecat;
    private String p_promventa_vs_forecast;
    private String item_nbr;
    private int pedido_fijo;
    private int estatus_dos;
    private String degustacion;

    private String producto;
    private double precio;

    public AdapterPedidos(int id_detalle_pedido, String clave, String upc, String producto, String orden, int pedido, int pedido_fijo, String agregar, String vence, int inventario, int dias_inventario, double promedio_venta, double precio, String cambios, int estatus_dos, String degustacion) {
        this.id_detalle_pedido = id_detalle_pedido;
        this.clave = clave;
        this.upc = upc;
        this.producto = producto;
        this.orden = orden;
        this.pedido = pedido;
        this.pedido_fijo = pedido_fijo;
        this.agregar = agregar;
        this.vence = vence;
        this.inventario = inventario;
        this.dias_inventario = dias_inventario;
        this.promedio_venta = promedio_venta;
        this.precio = precio;
        this.cambios = cambios;
        this.estatus_dos = estatus_dos;
        this.degustacion = degustacion;
    }

    public AdapterPedidos(int id_detalle_pedido, int id_pedido, String clave, int pedido, String agregar, String vence, int inventario, int dias_inventario, String cambios, double promedio_venta, double precio_unitario, String orden, int pz_recibidas, String upc, String fillrate, String forecast, String dif_promventa_vs_forecat, String p_promventa_vs_forecast, String item_nbr, int pedido_fijo) {
        this.id_detalle_pedido = id_detalle_pedido;
        this.id_pedido = id_pedido;
        this.clave = clave;
        this.pedido = pedido;
        this.agregar = agregar;
        this.vence = vence;
        this.inventario = inventario;
        this.dias_inventario = dias_inventario;
        this.cambios = cambios;
        this.promedio_venta = promedio_venta;
        this.precio_unitario = precio_unitario;
        this.orden = orden;
        this.pz_recibidas = pz_recibidas;
        this.upc = upc;
        this.fillrate = fillrate;
        this.forecast = forecast;
        this.dif_promventa_vs_forecat = dif_promventa_vs_forecat;
        this.p_promventa_vs_forecast = p_promventa_vs_forecast;
        this.item_nbr = item_nbr;
        this.pedido_fijo = pedido_fijo;
    }

    public int getId_detalle_pedido() {
        return id_detalle_pedido;
    }

    public void setId_detalle_pedido(int id_detalle_pedido) {
        this.id_detalle_pedido = id_detalle_pedido;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }


    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public void setPromedio_venta(double promedio_venta) {
        this.promedio_venta = promedio_venta;
    }

    public double getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(double precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public int getPedido() {
        return pedido;
    }

    public void setPedido(int pedido) {
        this.pedido = pedido;
    }

    public int getPedido_fijo() {
        return pedido_fijo;
    }

    public void setPedido_fijo(int pedido_fijo) {
        this.pedido_fijo = pedido_fijo;
    }

    public String getVence() {
        return vence;
    }

    public void setVence(String vence) {
        this.vence = vence;
    }

    public int getInventario() {
        return inventario;
    }

    public void setInventario(int inventario) {
        this.inventario = inventario;
    }

    public double getDias_inventario() {
        return dias_inventario;
    }

    public void setDias_inventario(int dias_inventario) {
        this.dias_inventario = dias_inventario;
    }

    public double getPromedio_venta() {
        return promedio_venta;
    }

    public void setPromedio_venta(int promedio_venta) {
        this.promedio_venta = promedio_venta;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getAgregar() {
        return agregar;
    }

    public void setAgregar(String agregar) {
        this.agregar = agregar;
    }

    public String getCambios() {
        return cambios;
    }

    public void setCambios(String cambios) {
        this.cambios = cambios;
    }

    public int getPz_recibidas() {
        return pz_recibidas;
    }

    public void setPz_recibidas(int pz_recibidas) {
        this.pz_recibidas = pz_recibidas;
    }

    public String getFillrate() {
        return fillrate;
    }

    public void setFillrate(String fillrate) {
        this.fillrate = fillrate;
    }

    public String getForecast() {
        return forecast;
    }

    public void setForecast(String forecast) {
        this.forecast = forecast;
    }

    public String getDif_promventa_vs_forecat() {
        return dif_promventa_vs_forecat;
    }

    public void setDif_promventa_vs_forecat(String dif_promventa_vs_forecat) {
        this.dif_promventa_vs_forecat = dif_promventa_vs_forecat;
    }

    public String getP_promventa_vs_forecast() {
        return p_promventa_vs_forecast;
    }

    public void setP_promventa_vs_forecast(String p_promventa_vs_forecast) {
        this.p_promventa_vs_forecast = p_promventa_vs_forecast;
    }

    public String getItem_nbr() {
        return item_nbr;
    }

    public void setItem_nbr(String item_nbr) {
        this.item_nbr = item_nbr;
    }

    public int getEstatus_dos() {
        return estatus_dos;
    }

    public void setEstatus_dos(int estatus_dos) {
        this.estatus_dos = estatus_dos;
    }

    public String getDegustacion() {
        return degustacion;
    }

    public void setDegustacion(String degustacion) {
        this.degustacion = degustacion;
    }
}
