package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by omarflores on 05/06/17.
 */

public class ProductoMerma {

    Integer idProductoMerma;
    Integer idProductoCatalogado;
    double pd;
    double inv;
    String inicioVigencia;
    String finVigencia;
    String fechaCreacion;
    String ultimaModificacion;
    String usuario;
    int estatus;
    int estatudDos;
    Producto producto;

    public ProductoMerma(){}

    public ProductoMerma(Integer idProductoMerma, Integer idProductoCatalogado, double pd, double inv, String inicioVigencia, String finVigencia, String fechaCreacion, String ultimaModificacion, String usuario, int estatus, int estatudDos) {
        this.idProductoMerma = idProductoMerma;
        this.idProductoCatalogado = idProductoCatalogado;
        this.pd = pd;
        this.inv = inv;
        this.inicioVigencia = inicioVigencia;
        this.finVigencia = finVigencia;
        this.fechaCreacion = fechaCreacion;
        this.ultimaModificacion = ultimaModificacion;
        this.usuario = usuario;
        this.estatus = estatus;
        this.estatudDos = estatudDos;
    }

    public ProductoMerma(Integer idProductoMerma, Integer idProductoCatalogado, double pd, double inv, String inicioVigencia, String finVigencia, String fechaCreacion, String ultimaModificacion, String usuario, int estatus, int estatudDos, Producto producto) {
        this.idProductoMerma = idProductoMerma;
        this.idProductoCatalogado = idProductoCatalogado;
        this.pd = pd;
        this.inv = inv;
        this.inicioVigencia = inicioVigencia;
        this.finVigencia = finVigencia;
        this.fechaCreacion = fechaCreacion;
        this.ultimaModificacion = ultimaModificacion;
        this.usuario = usuario;
        this.estatus = estatus;
        this.estatudDos = estatudDos;
        this.producto = producto;
    }

    public Integer getIdProductoMerma() {
        return idProductoMerma;
    }

    public void setIdProductoMerma(Integer idProductoMerma) {
        this.idProductoMerma = idProductoMerma;
    }

    public Integer getIdProductoCatalogado() {
        return idProductoCatalogado;
    }

    public void setIdProductoCatalogado(Integer idProductoCatalogado) {
        this.idProductoCatalogado = idProductoCatalogado;
    }

    public double getPd() {
        return pd;
    }

    public void setPd(double pd) {
        this.pd = pd;
    }

    public double getInv() {
        return inv;
    }

    public void setInv(double inv) {
        this.inv = inv;
    }

    public String getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(String inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public String getFinVigencia() {
        return finVigencia;
    }

    public void setFinVigencia(String finVigencia) {
        this.finVigencia = finVigencia;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUltimaModificacion() {
        return ultimaModificacion;
    }

    public void setUltimaModificacion(String ultimaModificacion) {
        this.ultimaModificacion = ultimaModificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public int getEstatudDos() {
        return estatudDos;
    }

    public void setEstatudDos(int estatudDos) {
        this.estatudDos = estatudDos;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
}
