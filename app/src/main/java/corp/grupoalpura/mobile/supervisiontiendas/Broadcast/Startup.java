package corp.grupoalpura.mobile.supervisiontiendas.Broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by SergioMtz on 15/03/2017.
 */
public class Startup extends BroadcastReceiver {

    public Startup(){}

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, GPSService.class));
    }
}
