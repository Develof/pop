package corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import corp.grupoalpura.mobile.supervisiontiendas.Constantes.Constantes;
import corp.grupoalpura.mobile.supervisiontiendas.DataBases.DBHelper;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.AdapterEspacios;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.FichaTecnicaProducto;

/**
 * Created by camovilesb on 29/3/2017.
 */

public class DetalleIncidenciaBDHelper extends DBHelper {

    public DetalleIncidenciaBDHelper(Context context) {
        super(context, null, Constantes.VERSION_BASE_DE_DATOS);
    }

    public ArrayList<FichaTecnicaProducto> getAllProductoCatalogadoLista(int id_sucursal) {
        ArrayList<FichaTecnicaProducto> productos = new ArrayList<>();
        FichaTecnicaProducto p;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT DISTINCT PRODUCTO.ID_PRODUCTO, PRODUCTO.UPC, PRODUCTO.CLAVE, PRODUCTO.NOMBRE, " +
                "PRODUCTO_PRESENTACION.PRESENTACION, PRODUCTO.ORDENAMIENTO, PRODUCTO_FAMILIA.ORDENAMIENTO FROM " +
                "PRODUCTO_CATALOGADO INNER JOIN PRODUCTO ON PRODUCTO_CATALOGADO.ID_PRODUCTO = PRODUCTO.ID_PRODUCTO " +
                "INNER JOIN PRODUCTO_PRESENTACION ON PRODUCTO.ID_PRESENTACION = PRODUCTO_PRESENTACION.ID_PRESENTACION " +
                "INNER JOIN PRODUCTO_FAMILIA ON PRODUCTO.ID_FAMILIA = PRODUCTO_FAMILIA.ID_FAMILIA " +
                "WHERE PRODUCTO_CATALOGADO.ID_SUCURSAL = " + id_sucursal + " AND PRODUCTO_CATALOGADO.ESTATUS = 1 AND PRODUCTO.ID_COMPANIA = 4" +
                "ORDER BY PRODUCTO_FAMILIA.ORDENAMIENTO, PRODUCTO.ORDENAMIENTO;", null);

        if (cursor.moveToFirst()) {
            do {
                int ID_PRODUCTO = Integer.parseInt(cursor.getString(0));
                String UPC = cursor.getString(1);
                String CLAVE = cursor.getString(2);
                String NOMBRE = cursor.getString(3);
                String PRESENTACION = cursor.getString(4);

                p = new FichaTecnicaProducto(ID_PRODUCTO, UPC, CLAVE, NOMBRE, PRESENTACION);
                productos.add(p);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return productos;
    }

    public ArrayList<FichaTecnicaProducto> getAllProductoCatalogadoFamilia(int id_sucursal, int id_familia) {
        ArrayList<FichaTecnicaProducto> productos = new ArrayList<>();
        FichaTecnicaProducto p;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT DISTINCT PRODUCTO.ID_PRODUCTO, PRODUCTO.UPC, PRODUCTO.CLAVE, PRODUCTO.NOMBRE, " +
                "PRODUCTO_PRESENTACION.PRESENTACION, PRODUCTO.ORDENAMIENTO, PRODUCTO_FAMILIA.ORDENAMIENTO, PRODUCTO_PRESENTACION.ID_PRESENTACION, PRODUCTO.DESCRIPCION, " +
                "IFNULL(PRODUCTO.ID_SUBCATEGORIA, '0'), IFNULL(PRODUCTO.TOP_ITEM, 0), IFNULL(PRODUCTO.ID_COMPANIA, 0) " +
                "FROM PRODUCTO_CATALOGADO INNER JOIN PRODUCTO ON PRODUCTO_CATALOGADO.ID_PRODUCTO = PRODUCTO.ID_PRODUCTO " +
                "INNER JOIN PRODUCTO_PRESENTACION ON PRODUCTO.ID_PRESENTACION = PRODUCTO_PRESENTACION.ID_PRESENTACION " +
                "INNER JOIN PRODUCTO_FAMILIA ON PRODUCTO.ID_FAMILIA = PRODUCTO_FAMILIA.ID_FAMILIA " +
                "WHERE PRODUCTO_CATALOGADO.ID_SUCURSAL = " + id_sucursal + " AND PRODUCTO_CATALOGADO.ESTATUS = 1 AND PRODUCTO_FAMILIA.ID_FAMILIA  =  " + id_familia + " AND PRODUCTO.ID_COMPANIA = 4" +
                " ORDER BY PRODUCTO.TOP_ITEM, PRODUCTO.CLAVE;", null);

        if (cursor.moveToFirst()) {
            do {


                int ID_PRODUCTO = Integer.parseInt(cursor.getString(0));
                String UPC = cursor.getString(1);
                String CLAVE = cursor.getString(2);
                String NOMBRE = cursor.getString(3);
                String PRESENTACION = cursor.getString(4);
                int ID_PRESENTACION = Integer.parseInt(cursor.getString(7));
                String DESC = cursor.getString(8);
                int ID_SUBCATEGORIA = cursor.getString(9).equals("") ? 0 : Integer.parseInt(cursor.getString(9));
                int TOP_ITEM = cursor.getString(10).equals("") ? 0 : Integer.parseInt(cursor.getString(10));
                int ID_COMPANIA = cursor.getString(11).equals("") ? 0 : Integer.parseInt(cursor.getString(11));

                p = new FichaTecnicaProducto(ID_PRODUCTO, UPC, CLAVE, NOMBRE, ID_PRESENTACION, PRESENTACION, DESC, ID_SUBCATEGORIA, TOP_ITEM, ID_COMPANIA);
                productos.add(p);

            } while (cursor.moveToNext());
        }

        cursor.close();
        return productos;
    }


}
