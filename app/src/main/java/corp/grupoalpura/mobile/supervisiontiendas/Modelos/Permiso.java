package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by uriel_000 on 03/03/2016.
 */
public class Permiso {
    private int id;
    private String nombre_permiso;
    private String descripcion_permiso;
    private String rol;
    private String categoria;
    private String fecha_creacion;
    private String ultima_modificacion;
    private String usuario;
    private int estatus;

    Permiso(){

    }

    public Permiso(int id, String nombre_permiso, String descripcion_permiso, String rol, String categoria, String fecha_creacion, String ultima_modificacion, String usuario, int estatus) {
        this.id = id;
        this.nombre_permiso = nombre_permiso;
        this.descripcion_permiso = descripcion_permiso;
        this.rol = rol;
        this.categoria = categoria;
        this.fecha_creacion = fecha_creacion;
        this.ultima_modificacion = ultima_modificacion;
        this.usuario = usuario;
        this.estatus = estatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_permiso() {
        return nombre_permiso;
    }

    public void setNombre_permiso(String nombre_permiso) {
        this.nombre_permiso = nombre_permiso;
    }

    public String getDescripcion_permiso() {
        return descripcion_permiso;
    }

    public void setDescripcion_permiso(String descripcion_permiso) {
        this.descripcion_permiso = descripcion_permiso;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
