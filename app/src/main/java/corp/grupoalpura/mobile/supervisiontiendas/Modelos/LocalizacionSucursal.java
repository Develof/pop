package corp.grupoalpura.mobile.supervisiontiendas.Modelos;

/**
 * Created by prestamo on 06/04/2016.
 */
public class LocalizacionSucursal {
    private int id;
    private int numero_ruta;
    private double latitud;
    private double longitud;
    private String alias;

    LocalizacionSucursal(){}

    public LocalizacionSucursal(int numero_ruta, double latitud, double longitud, String alias) {
        this.numero_ruta = numero_ruta;
        this.latitud = latitud;
        this.longitud = longitud;
        this.alias = alias;
    }

    public LocalizacionSucursal(double latitud, double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public LocalizacionSucursal(int id, String alias) {
        this.id = id;
        this.alias = alias;
    }

    public LocalizacionSucursal(int numero_ruta, double latitud, double longitud, String alias, int id_sucursal) {
        this.numero_ruta = numero_ruta;
        this.latitud = latitud;
        this.longitud = longitud;
        this.alias = alias;
        this.id = id_sucursal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero_ruta() {
        return numero_ruta;
    }

    public void setNumero_ruta(int numero_ruta) {
        this.numero_ruta = numero_ruta;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
