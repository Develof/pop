package corp.grupoalpura.mobile.supervisiontiendas;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import corp.grupoalpura.mobile.supervisiontiendas.Modelos.ClienteSucursal;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.GuiaMercadeoModel;
import corp.grupoalpura.mobile.supervisiontiendas.Modelos.Usuario;
import corp.grupoalpura.mobile.supervisiontiendas.RecyclerAdapters.GuiaMercadeoAdapter;
import corp.grupoalpura.mobile.supervisiontiendas.Sqlitehelper.GuiaMercadeoBDHelper;


public class GuiaMercadeo extends Fragment {


    public static String USUARIO = "Usuario";
    private static final String ID_SUCURSAL = "id_sucursal";

    ClienteSucursal sucursal;
    Usuario usuario;
    RecyclerView rvGuia;
    GuiaMercadeoAdapter adapter;

    //Instancia a la bd
    GuiaMercadeoBDHelper dbHelper;

    public GuiaMercadeo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GuiaMercadeoModel.
     */
    public GuiaMercadeo newInstance(ClienteSucursal sucursal, Usuario usuario) {
        GuiaMercadeo fragment = new GuiaMercadeo();
        Bundle args = new Bundle();
        args.putSerializable(ID_SUCURSAL, sucursal);
        args.putSerializable(USUARIO, usuario);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usuario = (Usuario) getArguments().getSerializable(USUARIO);
            sucursal = (ClienteSucursal) getArguments().getSerializable(ID_SUCURSAL);
            initBD();
            initData();
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guia_mercadeo, container, false);

        rvGuia = (RecyclerView) view.findViewById(R.id.recycler_view_guia_mercadeo);
        rvGuia.setAdapter(adapter);

        return view;
    }


    private void initBD() {
        dbHelper = new GuiaMercadeoBDHelper(getActivity());
    }

    private void initData() {

        List<GuiaMercadeoModel> listGuia = dbHelper.getGuiaMercadeo(sucursal.getId_cadena());
        adapter = new GuiaMercadeoAdapter(getActivity(), listGuia, usuario);
    }


    @Override
    public void onResume() {
        super.onResume();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvGuia.setHasFixedSize(true);
        rvGuia.setLayoutManager(linearLayoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}


