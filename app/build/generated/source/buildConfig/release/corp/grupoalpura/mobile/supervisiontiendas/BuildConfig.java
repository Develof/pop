/**
 * Automatically generated file. DO NOT MODIFY
 */
package corp.grupoalpura.mobile.supervisiontiendas;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "corp.grupoalpura.mobile.supervisiontiendas";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 20;
  public static final String VERSION_NAME = "1.3.4";
}
